export const images = {
    Login: require("./images/Login.png"),
    splash_new_image: require("./images/splash_new_image.png"),
    Splash_375: require("./images/01_Splash_375.png"),
    Splash: require("./images/Splash.png"),
    swipeup: require("./images/swipeup.png"),
    splash_logo: require("./images/splash_logo.png"),
    new_splash_img: require("./images/new_splash_img.png"),
    splash_bottom: require("./images/splash_bottom.png"),
    rupee_indian: require("./images/rupee-indian.png"),   
    
    Sign_up: require("./images/Sign_up.png"),
    Sign_up_178: require("./images/Sign_up_178.png"),

    user_box: require("./images/user_box.png"),
    user_camera: require("./images/user_camera.png"),
    right_ic: require("./images/right_ic.png"),
    drop_down: require("./images/drop_down.png"),
    check: require("./images/check.png"),
    unchecked: require("./images/unchecked.png"),

    After_splash: require("./images/After_splash.png"),
    back_btn: require("./images/back_ic.png"),
    login_logo: require("./images/login_logo.png"),
    after_splash_logo: require("./images/after_splash_logo.png"),
    have_omy_black: require("./images/haveomy_black.png"),
    switzerland_ic: require("./images/switzerland_ic.png"),
    enterprise_ic__blue: require("./images/enterprise_ic__blue.png"),
    be_omy_blue: require("./images/beomy_blue.png"),
    wrong_ic: require("./images/wrong_ic.png"),
    gallery_image: require("./images/gallery_image.png"),
    radio_btn_selected: require("./images/radio_btn_selected.png"),
    radio_btn_un_selected: require("./images/radio_btn_un_selected.png"),

    eye_off: require("./images/eye_off.png"),
    eye_on: require("./images/eye_on.png"),
    eye_hidden: require("./images/eye_hidden.png"),

    facebook_ic: require("./images/facebook_ic.png"),
    apple_ic: require("./images/apple_ic.png"),
    mail_ic: require("./images/mail_ic.png"),
    lock_ic: require("./images/lock_ic.png"),
    setting_next: require("./images/setting_next.png"),


    dog_ic: require("./images/dog_ic.png"),
    calendar_date_ic: require("./images/calendar_date_ic.png"),

    location_pin_ic: require("./images/location_pin_ic.png"),
    profile_care_home_img: require("./images/profile_care_home_img.png"),



    previous_img: require("./images/previous_img.png"),
    next_img: require("./images/next_img.png"),
    clock_ic: require("./images/clock_ic.png"),
    gps_ic: require("./images/gps_ic.png"),
    settings_ic: require("./images/settings_ic.png"),
    profile_senior_img: require("./images/profile_senior_img.png"),

    timer_ic: require("./images/timer_ic.png"),
    student_Profile: require("./images/student_profile_img.png"),
    star_ic: require("./images/star_ic.png"),
    star_outline_ic: require("./images/star_outline_ic.png"),
    downs: require("./images/downs.png"),
    ups: require("./images/ups.png"),
    edit_ic: require("./images/edit_ic.png"),
    cloud_upload_ic: require("./images/cloud_upload_ic.png"),
    play: require("./images/play.png"),
    artist: require("./images/artist.png"),
    search_ic: require("./images/search_ic.png"),




    home_active_ic: require("./images/home_active_ic.png"),
    home_ic: require("./images/home_ic.png"),
    experiences_active_ic: require("./images/experiences_active_ic.png"),
    experiences_ic: require("./images/experiences_ic.png"),
    earnings_active_ic: require("./images/earnings_active_ic.png"),
    earnings_ic: require("./images/earnings_ic.png"),
    messages_active_ic: require("./images/messages_active_ic.png"),
    messages_ic: require("./images/messages_ic.png"),
    profile_active_ic: require("./images/profile_active_ic.png"),
    default: require("./images/default.jpg"),
    profile_ic: require("./images/profile_ic.png"),
    bell_ic: require("./images/bell_ic.png"),
    user_edit_ic: require("./images/user_edit_ic.png"),
    student_Profile: require("./images/student_profile_img.png"),

    do_walking: require("./images/do_walking.png"),
    grocery_img: require("./images/grocery_img.png"),
    music_img: require("./images/music_img.png"),

    //Jamil
    music_ic: require("./images/music_ic.png"),
    shopping_ic: require("./images/shopping_ic.png"),
    rating_white_ic: require("./images/rating_white_ic.png"),
    dog_white_ic: require("./images/dog_white_ic.png"),
    music_round_ic: require("./images/music_round_ic.png"),
    reader_round_ic: require("./images/reader_round_ic.png"),
    shopping_round_ic: require("./images/shopping_round_ic.png"),
    star_half_ic: require("./images/star_half_ic.png"),
    distance_ic: require("./images/distance_ic.png"),
    filter_ic: require("./images/filter_ic.png"),
    sort_ic: require("./images/sort_ic.png"),
    calendar_ic: require("./images/calendar_ic.png"),


    //manoj add new icon
    service_add_ic: require("./images/service_add_ic.png"),
    thankyou_check_ic: require("./images/thankyou_check_ic.png"),
    rating_she_img: require("./images/rating_she_img.png"),
    rating_he_img: require("./images/rating_he_img.png"),
    home_profile_img_rating: require("./images/home_profile_img_rating.png"),
    cross: require("./images/cross.png"),

    
    German: require("./images/German.png"),
    French: require("./images/French.png"),
    English: require("./images/English.png"),
    Filter: require("./images/Filter.png"),
    dollar_currency_symbol: require("./images/dollar_currency_symbol.png"),
    franc_swiss_business_CHF: require("./images/franc_swiss_business_CHF.png"),
   

    
    chat_pic: require("./images/chat_pic.png"),
    more_options: require("./images/more_options.png"),
    send_icon1: require("./images/send_icon1.png"),
    attach: require("./images/attach.png"),
    typeFile: require("./images/typeFile.png"),
    camera_chat: require("./images/camera_chat.png"),

    location_user_pin: require("./images/location_user_pin.png"),
    location_student_pin: require("./images/location_student_pin.png")
}