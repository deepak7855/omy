import React from 'react'
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import en from "./english.json"
import fr from "./francias.json"
import de from "./german.json"

i18n.fallbacks = true;

export default class LanguagesIndex extends React.Component {

  static MyLanguage = 'de';

  static translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
  );
 
  static setI18nConfig = (lang) => {
    LanguagesIndex.translate.cache.clear();
    i18n.translations = {
        en,
        de,
        fr,
        
    };
    i18n.locale = lang;
    //return true;
  };
}