import { StyleSheet } from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/fonts';

export default AddMoneyStyles = StyleSheet.create({
    container: { flex: 1, },
    TxtCSS: { fontSize: fonts.fontSize16, margin: 15 , fontWeight: 'bold', },
    buttonView: { marginHorizontal: 20, marginVertical: 50 }

});