import React, { Component } from 'react'
import { Text, View, DeviceEventEmitter } from 'react-native'
import WebView from 'react-native-webview';
import Colors from '../../Assets/Colors';
import Helper from '../../Lib/Helper';
import { AsyncStorageHelper, Constants, ApiCall } from '../../Api';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import ListLoader from '../../Comman/ListLoader';

export default class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            urls: this.props.route.params.url,
            isLoading: true,
            findUrl:false
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('Payment'),
            borderBottomRadius: 25,
        })
    }

    onNavigationStateChange = (state) => {

        if (state.url == this.state.urls.success_url && !this.state.findUrl) {
            this.setState({ findUrl: true })
            setTimeout(() => {
                ApiCall.postMethodWithHeader(Constants.get_profile, JSON.stringify({}), Constants.APIPost).then((response) => {
                    if (response.data) {
                        AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                        Helper.user_data = response.data
                    }
                    Helper.showToast(LanguagesIndex.translate('Moneyaddedtoyouraccount'))
                    handleNavigation({ type: 'pop', navigation: this.props.navigation });
                    setTimeout(() => {
                        handleNavigation({ type: 'pop', navigation: this.props.navigation });
                    }, 100);
                })

            }, 2000);
        } else if (state.url == this.state.urls.failure_url && !this.state.findUrl) {
            this.setState({ findUrl: true })
            setTimeout(() => {
                Helper.showToast(LanguagesIndex.translate('paymentCanceled'))
                handleNavigation({ type: 'pop', navigation: this.props.navigation });
                setTimeout(() => {
                    handleNavigation({ type: 'pop', navigation: this.props.navigation });
                }, 100);
            }, 2000);
        }
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.white }}>
                <WebView
                    source={{ uri: this.state.urls.payment_url }}
                    javaScriptEnabled={true}
                    originWhitelist={['*']}
                    domStorageEnabled={true}
                    scrollEnabled
                    scalesPageToFit
                    contentMode={'mobile'}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    contentMode={'mobile'}
                    onLoadProgress={this._onLoadProgress}
                    onLoadEnd={() => this.setState({ isLoading: false })}
                    onNavigationStateChange={this.onNavigationStateChange}
                    containerStyle={{ paddingHorizontal: 5, backgroundColor: Colors.white, paddingTop: 5 }}
                    style={{}}
                />
                {this.state.isLoading ?
                    <View style={{ position: 'absolute', width: '100%', top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.white }}>
                        <ListLoader />
                    </View>
                    : null}
            </View>
        )
    }
}
