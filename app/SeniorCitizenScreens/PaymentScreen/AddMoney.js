import React, { Component } from 'react'
import { Text, View,DeviceEventEmitter, Keyboard } from 'react-native'
import AppHeader from '../../Comman/AppHeader';
import styles from './AddMoneyStyles';
import IconInput from '../../Comman/Input';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import { GButton } from '../../Comman/GButton';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import { Constants, ApiCall } from '../../Api';
import Helper from '../../Lib/Helper';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import * as RNLocalize from "react-native-localize";

export default class AddMoney extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: '',
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('Payment'),
            borderBottomRadius: 25,
            settingsIcon: true,
            settingIconClick: () => this.settingIconClick()
        })
    }

    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    getPaymentStatus = () => {
        //console.log("-----ok-----",)
        Keyboard.dismiss()
        if (Number(this.state.amount)) {
            Helper.globalLoader.showLoader();
            let data = new FormData();
            data.append('amount', this.state.amount)
            console.log('data----data=====++',data)
            ApiCall.postMethodWithHeader(Constants.get_payment, data, Constants.APIImageUploadAndroid).then((response) => {
               // console.log('payment add --- response',response)
                Helper.globalLoader.hideLoader();
                if (response.data && response.data.payment_url) {
                    this.goToPayment(response.data)
                }
            }
            )
        } else {
            Helper.showToast(LanguagesIndex.translate('PleaseEnterAmount'))
        }
    }

    goToPayment(data) {
        let url = {
            payment_url: data.payment_url,
            success_url: data.success_url,
            failure_url: data.failure_url
        }
        this.props.navigation.navigate('Payment', { url })
    }

    render() {
        return (
            <View style={styles.container}>
                <KeyboardScroll>
                    <Text style={styles.TxtCSS}>{LanguagesIndex.translate('AddMoneytoyourAccount')}</Text>
                    <View style={{ marginHorizontal: 16 }}>
                        <IconInput
                            showCurrency
                            placeholder={LanguagesIndex.translate('Amount')}
                            placeholderTextColor={Colors.warmGrey}
                            keyboardType={"numeric"}
                            maxLength={8}
                            onChangeText={(text) => this.setState({ amount: text })}
                            value={this.state.amount}
                        />
                    </View>

                    <View style={styles.buttonView}>
                        <GButton
                            onPress={() => { this.getPaymentStatus() }}
                            Text={LanguagesIndex.translate('Submit')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                        />
                    </View>
                </KeyboardScroll>
            </View>
        )
    }
}
