import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';

export default ScheduleBookingScreenStyles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
    backgroundColor: Colors.whiteFive
  },
  date_view: {
    marginHorizontal: 16,
    marginVertical: 10
  },
  s_date_txt: {
    color: Colors.blackTwo,
    fontSize: fonts.fontSize14,
    fontFamily: fonts.RoBoToMedium_1,
    fontWeight: '800'
  },
  calender_view: {
    marginHorizontal: 16,
    marginVertical: 5
  },
  white_view: {
    height: 8,
    backgroundColor: Colors.white,
    width: '100%',
    marginVertical: 5
  },
  time_parent_view: {
    backgroundColor: Colors.white
  },
  time_view: {
    marginHorizontal: 16,
    marginVertical: 10
  },
  time_txt: {
    color: Colors.blackTwo,
    fontSize: fonts.fontSize14,
    fontFamily: fonts.RoBoToMedium_1,
    lineHeight: 17
  },
  time_show_icon_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5
  },
  time_flex: {
    flex: 4
  },
  txt_time: {
    color: Colors.warmGreyThree,
    fontSize: fonts.fontSize12,
    fontFamily: fonts.RoBoToRegular_1,
    lineHeight: 14
  },
  clock_touch_flex: {
    flex: 4,
    alignItems: 'flex-end'
  },
  clock_img: {
    width: 19.3,
    height: 19.3
  },
  location_activity_view: {
    backgroundColor: Colors.white,
    marginTop: 5
  },
  location_activity_child_view: {
    marginHorizontal: 16,
    marginVertical: 10
  },
  location_activity_txt: {
    color: Colors.blackTwo,
    fontSize: fonts.fontSize14,
    fontFamily: fonts.RoBoToMedium_1,
    lineHeight: 17
  },
  view_loc: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5
  },
  add_flex: {
    flex: 8
  },
  add_txt: {
    color: Colors.warmGreyThree,
    fontSize: fonts.fontSize12,
    fontFamily: fonts.RoBoToRegular_1,
    lineHeight: 14
  },
  loc_touch_img: {
    flex: 2,
    alignItems: 'flex-end'
  },
  loc_img: {
    width: 18,
    height: 18
  },
  drop_img: {
    height: 6,
    position: "absolute",
    right: 0,
    top: 24,
    width: 14
  },
  input_view: {
    width: '100%',
    marginTop: 10
  },
  input_text: {
    fontSize: fonts.fontSize12,
    paddingLeft: 10,
    height: 70,
    borderColor: Colors.whiteSix,
    borderWidth: 1,
    borderRadius: 10
  },
  service_view: {
    alignSelf: 'flex-end',
    bottom: 25,
    right: 16,
    position: 'absolute',
    backgroundColor: Colors.cerulean,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50 / 2
  },
  plus_img: {
    width: 14.1,
    height: 14.1
  },
  service_txt: {
    fontSize: fonts.fontSize10,
    fontFamily: fonts.RoBoToMedium_1,
    color: Colors.white
  },

  short_txt: {
    color: Colors.blackTwo,
    fontSize: fonts.fontSize14,
    fontFamily: fonts.RoBoToMedium_1,
  },
  text_input: {
    fontSize: fonts.fontSize12,
    paddingLeft: 10,
    height: 70,
    borderColor: Colors.whiteSix,
    borderWidth: 1,
    borderRadius: 10
  },
  available_view: {
    marginHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },
  available_balance_txt: {
    color: Colors.blackTwo,
    fontSize: fonts.fontSize14,
    fontFamily: fonts.RoBoToMedium_1,
  },
  wallet_amount_txt: {
    fontSize: fonts.fontSize14, color: Colors.cerulean,
    fontFamily: fonts.RoBoToMedium_1
  },
  pay_now_btn_touch: {
    backgroundColor: Colors.darkSkyBlue,
    borderRadius: 10,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  pay_now_text: {
    fontSize: fonts.fontSize14, color: Colors.white,
    fontFamily: fonts.RoBoToMedium_1
  },
  calculation_text: {
    fontSize: fonts.fontSize14,
    color: Colors.white,
    lineHeight: 17,
    fontFamily: fonts.RoBoToMedium_1
  },
  btn_view: {
    marginVertical: 10,
    marginTop: 35,
    marginHorizontal: 16
  },
});


// import { StyleSheet } from 'react-native';
// import fonts from '../../Assets/fonts';
// import Colors from '../../Assets/Colors';


// export default ScheduleBookingScreenStyles = StyleSheet.create({
//     safe_area_view: {
//         flex: 1,
//         backgroundColor: Colors.whiteFive
//     },
//    date_time_parent_view:{
//     marginTop: 5, 
//     backgroundColor: Colors.white, 
//     paddingVertical: 5
//    },
//    date_time_text_view:{
//     marginHorizontal: 16,
//     paddingVertical:5
//    },
//    date_time_text:{
//     fontSize: fonts.fontSize14, 
//     color: Colors.blackTwo, 
//     fontFamily: fonts.RoBoToMedium_1, 
//     lineHeight: 17
//    },
//    date_time_view:{
//     marginHorizontal: 16, 
//     paddingVertical:5, 
//     flexDirection: 'row', 
//     justifyContent: 'space-between'
//    },
//    date_text:{
//     fontSize: fonts.fontSize12, 
//     color: Colors.warmGreyThree, 
//     fontFamily: fonts.RoBoToRegular_1, 
//     lineHeight: 14
//    },
//    flex_view:{
//     flex: 5, 
//     alignItems: 'flex-end'
//    },
//    time_text:{
//     fontSize: fonts.fontSize12, 
//     color: Colors.warmGreyThree, 
//     fontFamily: fonts.RoBoToRegular_1, 
//     lineHeight: 14
//    },
//    location_parent_view:{
//     marginTop: 5, 
//     backgroundColor: Colors.white, 
//     paddingVertical: 5
//    },
//    location_text_view:{
//     marginHorizontal: 16,
//     paddingVertical:5
//    },
//    location_text:{
//     fontSize: fonts.fontSize14, 
//     color: Colors.blackTwo, 
//     fontFamily: fonts.RoBoToMedium_1, 
//     lineHeight: 17
//    },
//    address_view:{
//     marginHorizontal: 16, 
//     paddingVertical:10, 
//     justifyContent: 'center'
//    },
//    address_text:{
//     fontSize: fonts.fontSize12, 
//     color: Colors.warmGreyThree, 
//     fontFamily: fonts.RoBoToRegular_1, 
//     lineHeight: 14
//    },
//    booking_one_view:{
//     backgroundColor: Colors.whiteFive,
//    },
//    booking_blue_view:{
//     marginHorizontal: 16, 
//     paddingVertical: 8,
//     justifyContent:'center'
//    },
//    blue_booking_text:{
//     fontSize: fonts.fontSize14, 
//     color: Colors.cerulean, 
//     fontFamily: fonts.RoBoToMedium_1, 
//     lineHeight: 17
//    },
//    activity_parent_view:{
//     backgroundColor: Colors.white, 
//     paddingVertical: 5
//    },
//    activity_view:{
//     marginHorizontal: 16, 
//     paddingVertical:5
//    },
//    activity_duration_txt:{
//     fontSize: fonts.fontSize14, 
//     color: Colors.blackTwo, 
//     fontFamily: fonts.RoBoToMedium_1, 
//     lineHeight: 17
//    },
//    walking_parent_view:{
//     marginHorizontal: 16, 
//     paddingVertical:10, 
//     flexDirection: 'row', 
//     justifyContent: 'space-between'
//    },
//    walking_text:{
//     fontSize: fonts.fontSize12, 
//     color: Colors.warmGreyThree, 
//     fontFamily: fonts.RoBoToRegular_1, 
//     lineHeight: 14
//    },
//    number_of_stud_parent_view:{
//     marginTop: 5, 
//     backgroundColor: Colors.white, 
//     paddingVertical: 5
//    },
//   number_stud_view:{
//     marginHorizontal: 16,
//     paddingVertical: 5
//   },
//   student_language_text:{
//     fontSize: fonts.fontSize14, 
//     color: Colors.blackTwo, 
//     fontFamily: fonts.RoBoToMedium_1, 
//     lineHeight: 17
//   },
//   student_count_view:{
//     marginHorizontal: 16,
//     paddingVertical: 5,  
//     justifyContent: 'center'
//   },
//   student_count_text:{
//     fontSize: fonts.fontSize12, 
//     color: Colors.warmGreyThree, 
//     fontFamily: fonts.RoBoToRegular_1, 
//     lineHeight: 14
//   },
//   language_view:{
//     marginTop: 5, 
//     backgroundColor: Colors.white, 
//     paddingVertical: 5
//   },
//   language_view_text:{
//     marginHorizontal: 16,
//     paddingVertical: 5 
//   },
//   selected_language_view:{
//     marginHorizontal: 16, 
//     paddingVertical: 5 , 
//     justifyContent: 'center'
//   },
//   language_text:{
//     fontSize: fonts.fontSize12,
//      color: Colors.warmGreyThree, 
//      fontFamily: fonts.RoBoToRegular_1, 
//      lineHeight: 14
//   },
//   short_note_view:{
//     marginTop: 5, 
//     backgroundColor: Colors.white, 
//     paddingVertical: 5
//   },
//   short_view_note:{
//     marginHorizontal: 16,
//     paddingVertical: 5
//   },
//   short_text:{
//     fontSize: fonts.fontSize14, 
//     color: Colors.blackTwo, 
//     fontFamily: fonts.RoBoToMedium_1, 
//     lineHeight: 17
//   },
//   short_desc_view:{
//     marginHorizontal: 16, 
//     paddingVertical: 5 , 
//     justifyContent: 'center'
//   },
//   short_desc_text:{
//     fontSize: fonts.fontSize12, 
//     color: Colors.warmGreyThree, 
//     fontFamily: fonts.RoBoToRegular_1, 
//     lineHeight: 14
//   },
//   btn_view:{
//     marginVertical: 10, 
//     marginTop: 35, 
//     marginHorizontal: 16
//   },


// });