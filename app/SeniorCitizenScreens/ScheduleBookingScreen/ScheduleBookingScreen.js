import React from 'react';
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  StyleSheet,
  TextInput,
  Keyboard,
  DeviceEventEmitter,
  Alert,
} from 'react-native';
import styles from './ScheduleBookingScreenStyles';
import Colors from '../../Assets/Colors';
import {images} from '../../Assets/imagesUrl';
import fonts from '../../Assets/fonts';
import AppHeader from '../../Comman/AppHeader';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import RNPickerSelect from 'react-native-picker-select';
import {handleNavigation} from '../../navigation/Navigation';
import Helper from '../../Lib/Helper';
import {ApiCall, Constants} from '../../Api';
import LanguagesIndex from '../../Languages';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import {getAdminSetting} from '../../Api/BookingApis';
import * as RNLocalize from 'react-native-localize';

const CustomCalendarArrow = (direction) => {
  return (
    <Image
      source={direction == 'left' ? images.previous_img : images.next_img}
      resizeMode={'contain'}
      style={{height: 16.3, width: 8.2}}
    />
  );
};

export default class ScheduleBookingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      commission_value: 0,
      commission_percentage: 50,
      commission_valueN: 0,
      isUpdate: false,
      mon_to_fri: [],
      miniDateTime: new Date(),
      sat: [],
      sun: [],
      activeMon: moment().format('YYYY-MM'),
      studentIdGet: this.props.route.params.studentId,
      arrStudentActivity: this.props.route.params.arrStudentActivity,
      unavailability: this.props.route.params.unavailability,
      user_slots: this.props.route.params.user_slots,
      isDatePickerVisible: false,
      isGermanChosen: false,
      isEnglishChosen: false,
      arrActivity: [
        {label: LanguagesIndex.translate('SelectActivity'), value: null},
      ],
      activity_duration: '',
      selectedBookingDate: '',
      //selectedDateTime:"",
      date: '',
      availableMarkingDates: {},
      time: '',
      app_languagepreference: [],
      language: [],
      bookRequest: {
        student_id: this.props.route.params.studentId,
        activity_id: this.props.route.params.selected_service_id,
        activity_time: '',
        note: '',
      },
      location: Helper.user_data.street_no ? Helper.user_data.street_no : '',
      location_lat: Helper.user_data.lat ? Helper.user_data.lat : '',
      location_lng: Helper.user_data.lng ? Helper.user_data.lng : '',
      is_duplicate: 0,

      arrDuration: [
        {label: LanguagesIndex.translate('SelectDuration'), value: null},
        {label: '1 hrs', value: '1'},
        {label: '2 hrs', value: '2'},
        {label: '3 hrs', value: '3'},
        {label: '4 hrs', value: '4'},
      ],
      activityPrice: 0,
    };
    AppHeader({
      ...this.props.navigation,
      leftTitle: LanguagesIndex.translate('ScheduleBooking'),
      borderBottomRadius: 25,
      bellIcon: true,
      settingsIcon: true,
      bellIconClick: () => this.bellIconClick(),
      settingIconClick: () => this.settingIconClick(),
    });
  }

  componentDidMount() {
    this.props.navigation.addListener('focus', () => {
      Helper.updateUserData((res) => {
        this.setState({isUpdate: true});
      });
      getAdminSetting((data) => {
        //console.log('datadatadatadata',data)
        this.setState({
          commission_percentage: Number(data.data.percentage),
          commission_value: Number(data.data.amount),
        });
      });
    });
    Helper.updateUserData((res) => {
      this.setState({isUpdate: true});
    });

    this.getActivitiesApiCall();
    this.markAvailableDates();
  }

  markAvailableDates() {
    let arrDates = {};
    var startOfMonth = moment(this.state.activeMon).startOf('month');

    if (this.state.user_slots) {
      if (this.state.user_slots.mon_to_fri) {
        if (Array.isArray(this.state.user_slots.mon_to_fri)) {
          this.state.mon_to_fri = this.state.user_slots.mon_to_fri;
        } else {
          this.state.mon_to_fri = this.state.user_slots.mon_to_fri
            .split(',')
            .map((n) => parseInt(n, 10));
        }
      }
      if (this.state.user_slots.sat) {
        if (Array.isArray(this.state.user_slots.sat)) {
          this.state.sat = this.state.user_slots.sat;
        } else {
          this.state.sat = this.state.user_slots.sat
            .split(',')
            .map((n) => parseInt(n, 10));
        }
      }
      if (this.state.user_slots.sun) {
        if (Array.isArray(this.state.user_slots.sun)) {
          this.state.sun = this.state.user_slots.sun;
        } else {
          this.state.sun = this.state.user_slots.sun
            .split(',')
            .map((n) => parseInt(n, 10));
        }
      }
    }

    this.setState({availableMarkingDates: {}}, () => {
      for (
        let index = 0;
        index < moment(this.state.activeMon, 'YYYY-MM').daysInMonth() + 1;
        index++
      ) {
        if (this.state.mon_to_fri.length > 0) {
          if (
            moment(startOfMonth).weekday() != 6 &&
            moment(startOfMonth).weekday() != 0 &&
            !this.checkUnavailableDate(
              moment(startOfMonth).format('YYYY-MM-DD'),
            )
          ) {
            arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
              marked: true,
              dotColor: Colors.darkSkyBlue,
            };
          } else if (
            this.checkUnavailableDate(moment(startOfMonth).format('YYYY-MM-DD'))
          ) {
            arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
              disabled: true,
            };
          }
        } else if (
          moment(startOfMonth).weekday() != 6 &&
          moment(startOfMonth).weekday() != 0
        ) {
          arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
            disabled: true,
          };
        }

        if (this.state.sat.length > 0) {
          if (
            moment(startOfMonth).weekday() == 6 &&
            !this.checkUnavailableDate(
              moment(startOfMonth).format('YYYY-MM-DD'),
            )
          ) {
            arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
              marked: true,
              dotColor: Colors.darkSkyBlue,
            };
          } else if (
            this.checkUnavailableDate(moment(startOfMonth).format('YYYY-MM-DD'))
          ) {
            arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
              disabled: true,
            };
          }
        } else if (moment(startOfMonth).weekday() == 6) {
          arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
            disabled: true,
          };
        }

        if (this.state.sun.length > 0) {
          if (
            moment(startOfMonth).weekday() == 0 &&
            !this.checkUnavailableDate(
              moment(startOfMonth).format('YYYY-MM-DD'),
            )
          ) {
            arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
              marked: true,
              dotColor: Colors.darkSkyBlue,
            };
          } else if (
            this.checkUnavailableDate(moment(startOfMonth).format('YYYY-MM-DD'))
          ) {
            arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
              disabled: true,
            };
          }
        } else if (moment(startOfMonth).weekday() == 0) {
          arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = {
            disabled: true,
          };
        }
        startOfMonth.add(1, 'days');
        // startOfMonth.add(1, 'days')
      }
      this.setState({availableMarkingDates: arrDates});
    });
  }

  checkUnavailableDate(date) {
    let isExist = false;
    if (
      this.state &&
      this.state.unavailability &&
      this.state.unavailability.length > 0
    ) {
      this.state.unavailability.forEach((element) => {
        if (element.date == date) {
          isExist = true;
        }
      });
    }
    return isExist;
  }

  setValues(key, value) {
    let bookRequest = {...this.state.bookRequest};
    bookRequest[key] = value;
    this.setState({bookRequest}, () => {
      this.ChangeActivityPrice();
    });
  }

  ChangeActivityPrice = () => {
    this.state.arrStudentActivity.forEach((element) => {
      if (element.activity.id == this.state.bookRequest.activity_id) {
        this.setState({
          activityPrice:
            Number(this.state.bookRequest.activity_time) *
            Number(element.price),
        });
      }
    });
  };

  settingIconClick() {
    handleNavigation({
      type: 'push',
      page: 'SettingsScreen',
      navigation: this.props.navigation,
    });
  }

  bellIconClick() {
    handleNavigation({
      type: 'push',
      page: 'NotificationsScreen',
      navigation: this.props.navigation,
    });
  }

  showDatePicker = () => {
    Keyboard.dismiss();
    var date1 = new Date();
    var date2 = new Date(this.state.selectedBookingDate);
    var bookingDateChange = date1.getTime() > date2.getTime();
    // console.log("dsjdhjds",bookingDateChange)
    if (bookingDateChange) {
      let a = new Date();
      let changeTime = moment(new Date()).add(5, 'hours').format();
      // console.log("ddss",changeTime)
      this.setState({miniDateTime: changeTime}, () => {
        //console.log("miniDateTime",this.state.miniDateTime)
        this.setState({isDatePickerVisible: true});
      });
    } else {
      this.setState({miniDateTime: this.state.selectedBookingDate}, () => {
        //console.log("dsds",this.state.miniDateTime)
        this.setState({isDatePickerVisible: true});
      });
    }
    this.setState({isDatePickerVisible: true});
  };

  hideDatePicker = () => {
    this.setState({isDatePickerVisible: false});
  };

  handleConfirm = (date) => {
    //console.log("sdsdssd",date)
    this.hideDatePicker();
    //this.setState({selectedDateTime : date})
    let currentDate = moment(new Date()).format('YYYY-MM-DD');
    let checkDate = moment(this.state.selectedBookingDate).format('YYYY-MM-DD');
    if (currentDate == checkDate) {
      let a = new Date();
      let changeTime = moment(new Date()).add(5, 'hours').format();
      this.setState({miniDateTime: changeTime});
      // console.log("aaaaaaaa-------", moment(a).format("HH:mm"));
      // console.log("aaaaaaabb-------", moment(changeTime).format("HH:mm"));

      // let checkCurrentDate = moment(new Date(), 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')
      let checkCurrentDate = moment(new Date(), 'YYYY-MM-DD HH:mm')
        .add(5, 'hours')
        .format('YYYY-MM-DD HH:mm');
      let tempSelectTime = moment(date).format('HH:mm');
      let compareD = this.state.selectedBookingDate + ' ' + tempSelectTime;
      if (checkCurrentDate < compareD) {
        if (this.checkTimeSlot(date)) {
          this.setState({time: date});
        } else {
          Helper.showToast(
            LanguagesIndex.translate('Studentnotavailableinselectedtimeslot'),
          );
        }
      } else {
        Helper.showToast(LanguagesIndex.translate('Pleaseselectvalidtime'));

        this.setState({time: ''});
      }
    } else {
      this.setState({miniDateTime: new Date()});
      if (this.checkTimeSlot(date)) {
        this.setState({time: date});
      } else {
        Helper.showToast(
          LanguagesIndex.translate('Studentnotavailableinselectedtimeslot'),
        );
      }
    }
  };

  checkTimeSlot(pickTime) {
    let getSlotId = '';
    let returnStatus = false;
    let formatedTime = moment(pickTime).format('HHmm');

    let availableTimes = [
      {startTime: '0759', endTime: '1200', id: 1},
      {startTime: '1159', endTime: '1600', id: 2},
      {startTime: '1559', endTime: '2000', id: 3},
    ];

    availableTimes.forEach((element) => {
      if (formatedTime > element.startTime && formatedTime < element.endTime) {
        getSlotId = element.id;
      }
    });

    if (!getSlotId) {
      returnStatus = false;
    } else {
      let weekDay = moment(this.state.selectedBookingDate).weekday();
      if (weekDay != 6 && weekDay != 0) {
        if (
          this.state.mon_to_fri &&
          this.state.mon_to_fri.includes(getSlotId)
        ) {
          returnStatus = true;
        } else {
          returnStatus = false;
        }
      } else if (weekDay == 6) {
        if (this.state.sat && this.state.sat.includes(getSlotId)) {
          returnStatus = true;
        } else {
          returnStatus = false;
        }
      } else if (weekDay == 0) {
        if (this.state.sun && this.state.sun.includes(getSlotId)) {
          returnStatus = true;
        } else {
          returnStatus = false;
        }
      } else {
        returnStatus = false;
      }
    }
    return returnStatus;
  }

  getActivitiesApiCall = () => {
    // Helper.globalLoader.showLoader();
    // let data = {}
    // ApiCall.postMethodWithHeader(Constants.get_activities, data, Constants.APIPost).then((response) => {
    //     Helper.globalLoader.hideLoader();

    //     if (response.status == Constants.TRUE) {
    //         const newColumns = response.data.map(item => {
    //             const { id: value, name: label, ...rest } = item;
    //             return { value, label, ...rest }
    //         }
    //         );
    //         this.state.arrActivity = [...this.state.arrActivity, ...newColumns]
    //     }
    //     else {
    //         Helper.showToast(response.message)
    //     }
    //     this.setState({})
    // }
    // )

    let actArr = [];
    this.state.arrStudentActivity.map((item) => {
      actArr.push({
        value: item.activity.id,
        label: item.activity.name,
      });
    });
    this.setState({arrActivity: [...this.state.arrActivity, ...actArr]});
  };

  methodSendRequest(item) {
    Alert.alert(
      LanguagesIndex.translate('kAppName'),
      item,
      [
        {
          text: LanguagesIndex.translate('OK'),
          onPress: () => {
            this.setState({is_duplicate: '1'}, () =>
              this.getBookingRequestApiCall(),
            );
          },
        },
        {
          text: LanguagesIndex.translate('Cancel'),
          onPress: () => {
            console.log('Cancel Pressed');
          },
        },
      ],
      {cancelable: false},
    );
  }

  dup() {
    // if(this.state.is_duplicate == 0){
    //     //alert(this.state.is_duplicate)
    //     this.methodSendRequest(this.state.is_duplicate)
    // }else{
    this.getBookingRequestApiCall();
    // }
  }

  getBookingRequestApiCall = () => {
    Keyboard.dismiss();
    let count = Helper.user_data.wallet
      ? Number(Helper.user_data.wallet.amount)
      : 0;
    if (!this.state.selectedBookingDate) {
      Helper.showToast(LanguagesIndex.translate('Pleaseselectbookingdate'));
      return;
    } else if (!this.state.time) {
      Helper.showToast(LanguagesIndex.translate('Pleaseselectbookingtime'));
      return;
    } else if (!this.state.bookRequest.activity_id) {
      Helper.showToast(LanguagesIndex.translate('Pleaseselectactivity'));
      return;
    } else if (!this.state.bookRequest.activity_time) {
      Helper.showToast(LanguagesIndex.translate('Pleaseselectduration'));
      return;
    } else if (!this.state.bookRequest.note) {
      Helper.showToast(LanguagesIndex.translate('Entershortnote'));
      return;
    } else if (
      Number(this.state.activityPrice) + this.state.commission_valueN >
      count
    ) {
      Helper.confirm(
        LanguagesIndex.translate('Insufficientbalancepleaseaddrequiredamount'),
        (status) => {
          if (status) {
            handleNavigation({
              type: 'push',
              page: 'AddMoney',
              navigation: this.props.navigation,
            });
          }
        },
      );
      return;
    }
    console.log(
      this.state.selectedBookingDate,
      'this.state.time',
      moment(this.state.time).format('HH:mm'),
    );

    // var sendTime = "";
    // if (this.state.selectedBookingDate == moment(new Date()).format("YYYY-MM-DD")) {
    //     sendTime = moment(this.state.selectedDateTime).add(5, 'hours').format('HH:mm');
    // }else{
    //     sendTime = moment(this.state.selectedDateTime).format("HH:mm");
    // }

    // console.log('sendTime--sendTime',sendTime)
    let fullUTCDate = moment(
      this.state.selectedBookingDate + moment(this.state.time).format('HH:mm'),
      'YYYY-MM-DD HH:mm',
    ).utc()
      .format('YYYY-MM-DD h:mm a');
    //let fullUTCDate = moment(this.state.selectedBookingDate+sendTime,'YYYY-MM-DD HH:mm').utc().format('YYYY-MM-DD h:mm a');
    let finalUtc = fullUTCDate.split(' ');

    let timezone = RNLocalize.getTimeZone();
    let data = new FormData();
    data.append('student_id', this.state.bookRequest.student_id);
    data.append('date', finalUtc[0]);
    data.append('time', `${finalUtc[1]} ${finalUtc[2]}`);
    //data.append('time',sendTime);// `${finalUtc[1]} ${finalUtc[2]}`);
    data.append('time_zone', timezone);

    data.append('activity_id', this.state.bookRequest.activity_id);
    data.append('activity_time', this.state.bookRequest.activity_time);
    data.append('note', this.state.bookRequest.note);
    data.append('amount', this.state.activityPrice);
    data.append('commission', this.state.commission_valueN);
    data.append('location', this.state.location);
    data.append('location_lat', this.state.location_lat);
    data.append('location_lng', this.state.location_lng);
    data.append('is_duplicate', this.state.is_duplicate);

     //console.log("data------book_request--",data)
    // return
    Helper.globalLoader.showLoader();
    ApiCall.postMethodWithHeader(
      Constants.book_request,
      data,
      Constants.APIImageUploadAndroid,
    ).then((response) => {
      Helper.globalLoader.hideLoader();
      //   console.log("response.message------book_request--",response.data)
     //console.log("response.message------book_request--",response)
      !response.data.duplicate ? Helper.showToast(response.message) : '';
      if (response.status == Constants.TRUE) {
        Helper.showToast(response.message);

        setTimeout(() => {
          //   handleNavigation({
          //     type: 'popToTop',
          //     navigation: this.props.navigation,
          //   });
          handleNavigation({
            type: 'setRoot',
            page: 'SeniorCitizenBottomTab',
            navigation: this.props.navigation,
          });

          DeviceEventEmitter.emit('Book_Request_Schedule', 'done');
        }, 100);
      } else {
        if (response.data.duplicate == true) {
          this.methodSendRequest(response.message);
        }
      }
    });
  };

  selectDates(day) {
    let pressDate = moment(day.dateString).format('YYYY-MM-DD');
    let arrDates = {...this.state.availableMarkingDates};
    this.state.time = '';
    if (arrDates[pressDate] && !arrDates[pressDate].disabled) {
      for (let key in arrDates) {
        if (
          this.checkUnavailableDate(key) ||
          (moment(key).weekday() == 0 && this.state.sun.length == 0) ||
          (moment(key).weekday() == 6 && this.state.sat.length == 0) ||
          (moment(key).weekday() > 0 &&
            moment(key).weekday() < 6 &&
            this.state.mon_to_fri.length == 0)
        ) {
          arrDates[key] = {disabled: true};
        } else {
          arrDates[key] = {marked: true, dotColor: Colors.darkSkyBlue};
        }
      }

      arrDates[pressDate] = {
        marked: true,
        dotColor: Colors.darkSkyBlue,
        selected: true,
        selectedColor: Colors.darkSkyBlue,
      };

      this.setState({
        availableMarkingDates: arrDates,
        selectedBookingDate: pressDate,
      });
    }
  }

  chooseLanguage(language) {
    let isExist = this.state.app_languagepreference.indexOf(language);
    if (isExist > -1) {
      this.state.app_languagepreference.splice(isExist, 1);
    } else {
      this.state.app_languagepreference.push(language);
    }
    this.state.language = this.state.app_languagepreference.toString();
    this.setState({});
  }

  getDuration() {
    let format = 'HH:mm';
    let matchEndSlot = '';
    let createDuration = [
      {label: LanguagesIndex.translate('SelectDuration'), value: null},
    ];
    let availableTimes = [
      {startTime: moment('07:59', format), endTime: moment('12:00', format)},
      {startTime: moment('11:59', format), endTime: moment('16:00', format)},
      {startTime: moment('15:59', format), endTime: moment('20:00', format)},
    ];

    if (this.state.time) {
      let bookingTime = moment(this.state.time).format(format);

      availableTimes.forEach((element) => {
        if (
          moment(bookingTime, format).isBetween(
            element.startTime,
            element.endTime,
          )
        ) {
          matchEndSlot = element.endTime;
        }
      });

      if (matchEndSlot) {
        let nowHours = matchEndSlot.diff(moment(bookingTime, format), 'hours');
        for (let index = 1; index <= nowHours; index++) {
          createDuration.push({label: `${index} hrs`, value: index});
        }
      }
    }
    return createDuration;
  }

  getCommissionValue() {
    //console.log('commission_percentage',this.state.commission_percentage,this.state.commission_value)
    let cval = 0;
    cval =
      (Number(this.state.activityPrice) *
        Number(this.state.commission_percentage)) /
        100 +
      Number(this.state.commission_value);
    this.state.commission_valueN = cval;
    return cval;
  }

  render() {
    // console.log("Helper.token---Helper.token",Helper.token)
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <KeyboardScroll contentContainerStyle={{paddingBottom: 22}}>
          <View style={{marginHorizontal: 16, marginVertical: 10}}>
            <Text style={styles.short_txt}>
              {LanguagesIndex.translate('SelectDate')}
            </Text>
          </View>

          <View style={{marginHorizontal: 16, marginVertical: 5}}>
            <Calendar
              style={{
                borderWidth: 0.5,
                borderColor: Colors.white,
                borderRadius: 10,
                elevation: 1.5,
                backgroundColor: Colors.cerulean,
              }}
              minDate={new Date()}
              hideArrows={false}
              onMonthChange={(month) => {
                this.setState(
                  {activeMon: moment(month.dateString).format('YYYY-MM')},
                  () => {
                    this.markAvailableDates();
                  },
                );
              }}
              onDayPress={(day) => this.selectDates(day)}
              renderArrow={CustomCalendarArrow}
              disableArrowLeft={false}
              firstDay={1}
              theme={{
                'stylesheet.calendar.main': {
                  container: {
                    paddingLeft: 0,
                    paddingRight: 0,
                  },
                },
                'stylesheet.calendar.header': {
                  week: {
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    backgroundColor: Colors.veryLightBlue,
                    color: Colors.black,
                  },
                  headerContainer: {
                    flexDirection: 'row',
                  },
                  arrow: {
                    padding: 10,
                    color: Colors.white,
                  },
                  arrowImage: {
                    tintColor: Colors.white,
                  },
                },
                monthTextColor: Colors.white,
                textDayFontWeight: 'bold',
                textMonthFontWeight: 'bold',
                textDayHeaderFontWeight: '300',
                textDayFontSize: 16,
                textMonthFontSize: 14,
                textDayHeaderFontSize: 12,
              }}
              markingType={'simple'}
              // markedDates={this.state.date}
              markedDates={this.state.availableMarkingDates}
            />
          </View>

          <View
            style={{
              height: 8,
              backgroundColor: Colors.white,
              width: '100%',
              marginVertical: 5,
            }}></View>
          <View style={{backgroundColor: Colors.white}}>
            <View style={{marginHorizontal: 16, marginVertical: 10}}>
              <Text style={styles.short_txt}>
                {LanguagesIndex.translate('SelectTime')}
              </Text>

              <TouchableOpacity
                onPress={() => this.showDatePicker()}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: 5,
                }}>
                <View style={{flex: 4}}>
                  <Text
                    style={{
                      color: Colors.warmGreyThree,
                      fontSize: fonts.fontSize12,
                      fontFamily: fonts.RoBoToRegular_1,
                      lineHeight: 14,
                    }}>
                    {this.state.time
                      ? moment(this.state.time).format('h:mm a')
                      : ''}
                  </Text>
                </View>

                <View style={{flex: 4, alignItems: 'flex-end'}}>
                  <Image
                    source={images.clock_ic}
                    resizeMode={'contain'}
                    style={{width: 19.3, height: 19.3}}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{backgroundColor: Colors.white, marginTop: 5}}>
            <View style={{marginHorizontal: 16, marginVertical: 10}}>
              <Text style={styles.short_txt}>
                {LanguagesIndex.translate('SelectActivity')}
              </Text>
              <View style={{flex: 1}}>
                <RNPickerSelect
                  placeholder={{}}
                  items={this.state.arrActivity}
                  onValueChange={(value) => {
                    this.setValues('activity_id', value);
                  }}
                  value={this.state.bookRequest.activity_id}
                  useNativeAndroidPickerStyle={false}
                  style={pickerSelectStyles}
                  Icon={() => (
                    <Image
                      resizeMode="contain"
                      source={images.drop_down}
                      style={{
                        marginHorizontal: 5,
                        height: 6,
                        width: 14,
                        marginTop: 15,
                        // tintColor:Colors.black,
                        marginTop: 24,
                        resizeMode: 'contain',
                      }}
                    />
                  )}
                />
                {/* <Image source={images.drop_down} style={{ height: 6, resizeMode: "contain", position: "absolute", right: 0, top: 24, width: 14 }} /> */}
              </View>
            </View>
          </View>

          <View style={{backgroundColor: Colors.white, marginTop: 5}}>
            <View style={{marginHorizontal: 16, marginVertical: 10}}>
              <Text style={styles.short_txt}>
                {LanguagesIndex.translate('ActivityDuration')}
              </Text>
              <View style={{flex: 1}}>
                <RNPickerSelect
                  placeholder={{}}
                  items={this.getDuration()}
                  // items={this.state.arrDuration}
                  onValueChange={(value) => {
                    this.setValues('activity_time', value);
                  }}
                  selectValue={this.state.bookRequest.activity_time}
                  useNativeAndroidPickerStyle={false}
                  style={pickerSelectStyles}
                  Icon={() => (
                    <Image
                      resizeMode="contain"
                      source={images.drop_down}
                      style={{
                        marginHorizontal: 5,
                        height: 6,
                        width: 14,
                        marginTop: 15,
                        // tintColor:Colors.black,
                        marginTop: 24,
                        resizeMode: 'contain',
                      }}
                    />
                  )}
                />
                {/* <Image source={images.drop_down} style={{ height: 6, resizeMode: "contain", position: "absolute", right: 0, top: 24, width: 14 }} /> */}
              </View>
            </View>
          </View>

          <View style={{backgroundColor: Colors.white, marginTop: 5}}>
            <View style={{marginHorizontal: 16, marginVertical: 10}}>
              <Text style={styles.short_txt}>
                {LanguagesIndex.translate('ShortNote')}
              </Text>
              <View style={{width: '100%', marginTop: 10}}>
                <TextInput
                  style={styles.text_input}
                  placeholder={LanguagesIndex.translate('WriteHere')}
                  underlineColorAndroid="transparent"
                  multiline={true}
                  keyboardType={'default'}
                  returnKeyType="next"
                  textAlignVertical="top"
                  onChangeText={(note) => this.setValues('note', note)}
                  value={this.state.bookRequest.note}
                />
              </View>
            </View>
          </View>

          <View style={styles.available_view}>
            <Text style={styles.available_balance_txt}>
              {LanguagesIndex.translate('AvailableBalance')}
            </Text>
            <View style={{alignItems: 'center', flexDirection: 'row'}}>
              <Text style={styles.wallet_amount_txt}>
                CHF{' '}
                {Helper.user_data?.wallet?.amount
                  ? Helper.user_data?.wallet?.amount
                  : 0}
              </Text>
            </View>
          </View>

          {/* <View style={styles.available_view}>
                        <Text style={styles.available_balance_txt}>{LanguagesIndex.translate('ServiceAmount')}</Text>
                        <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                            <Text style={styles.wallet_amount_txt}>CHF {this.state.activityPrice}</Text>
                        </View>
                    </View>

                    <View style={styles.available_view}>
                        <Text style={styles.available_balance_txt}>{LanguagesIndex.translate('Commission')}</Text>
                        <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                            <Text style={styles.wallet_amount_txt}>CHF {this.getCommissionValue()}</Text>
                        </View>
                    </View> */}
          <View style={styles.available_view}>
            <Text style={styles.available_balance_txt}>
              {LanguagesIndex.translate('Activity')}
            </Text>
            <View style={{alignItems: 'center', flexDirection: 'row'}}>
              <Text style={styles.wallet_amount_txt}>
                CHF {this.state.activityPrice}
              </Text>
            </View>
          </View>

          <View style={styles.available_view}>
            <Text style={styles.available_balance_txt}>
              {LanguagesIndex.translate('Service Fee')}
            </Text>
            <View style={{alignItems: 'center', flexDirection: 'row'}}>
              <Text style={styles.wallet_amount_txt}>
                CHF {this.getCommissionValue()}
              </Text>
            </View>
          </View>

          <View style={styles.btn_view}>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.dup();
              }}
              style={styles.pay_now_btn_touch}>
              <Text style={styles.pay_now_text}>
                {LanguagesIndex.translate('PAYNOW')}
              </Text>
              {this.state.activityPrice == 0 ? null : (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginLeft: 15,
                  }}>
                  <Text style={styles.calculation_text}>
                    (CHF{' '}
                    {this.state.activityPrice + this.state.commission_valueN})
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          </View>

          {/* 
                    <View style={styles.btn_view}>
                        <GButton
                            Text='PAY'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.getBookingRequestApiCall() }}
                        />
                    </View> */}
        </KeyboardScroll>
        <DateTimePickerModal
          isVisible={this.state.isDatePickerVisible}
          mode="time"
          headerTextIOS={'Pick a time'}
          onConfirm={this.handleConfirm}
          onCancel={this.hideDatePicker}
          is24Hour={false}
          minimumDate={new Date(this.state.miniDateTime)}
          date={new Date(this.state.miniDateTime)}
        />
      </SafeAreaView>
    );
  }
}
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: fonts.fontSize12,
    height: 40,
    color: Colors.warmGrey,
    fontFamily: fonts.RoBoToMedium_1,
    width: '100%',
  },
  inputAndroid: {
    fontSize: fonts.fontSize12,
    height: 40,
    width: '100%',
    color: Colors.warmGrey,
    fontFamily: fonts.RoBoToMedium_1,
  },
});
