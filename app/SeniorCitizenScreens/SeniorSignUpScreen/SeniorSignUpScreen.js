import React from 'react';
import { Text, View, Image, StyleSheet, Keyboard, Dimensions, TouchableOpacity,Modal,FlatList } from 'react-native';
import styles from './SignUpScreenStyles';
import { GButton } from '../../Comman/GButton';
import { SocialButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import IconInput from '../../Comman/GInput';
import fonts from '../../Assets/fonts';
import RNPickerSelect from '../../Comman/CommonPicker';
import { handleNavigation } from '../../navigation/Navigation';
import Helper from '../../Lib/Helper';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import { FacebookLogin, } from '../../Comman/SocialLogin';
import LanguagesIndex from '../../Languages';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import CameraController from '../../Lib/CameraController';

const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;
export default class SeniorSignUpScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            CheckedBox: false,
            isOnBehalfVisible: false,
            arrRelationship: [
                { label: LanguagesIndex.translate('Relationship'), value: null }
            ],

            userSignUpForm: {
                profile_picture: '',
                name: '',
                mobile_number: '',
                email: '',
                // location: '',
                street_no: '',
                postcode: '',
                city: '',
                password: '',
                confirm_password: '',
                user_type: "BEOMY",
                device_type: '',
                device_id: '',
                lat: '',
                lng: '',

                validators: {
                    name: { required: true, minLength: 2, maxLength: 45, },
                    mobile_number: { required: true, minLengthDigit: 9, maxLengthDigit: 9 },
                    email: { required: true, email: true },
                    street_no: { required: true, minLength: 1, maxLength: 45 },
                    postcode: { required: true, minLengthDigit: 4, maxLengthDigit: 10 },
                    city: { required: true, minLength: 1, maxLength: 45 },
                    password: { required: true, minLength: 8, maxLength: 15 },
                    confirm_password: { required: true, matchWith: "password" },
                }
            },
            modalVisible:false,
            postalData:[],
            modalVisibleCity:false,
            modalVisibleB:false,
            modalVisibleCityB:false,

            userOnBehalfSignUpForm: {
                behalf_name: "",
                behalf_mobile_number: "",
                behalf_email: "",
                // behalf_location: "",
                behalf_street_no: "",
                behalf_postcode: "",
                behalf_city: "",
                behalf_lat: '',
                behalf_lng: '',
                // behalf_lat: "454895488",
                // behalf_lng: "548745554",
                behalf_relation: "",

                validators: {
                    behalf_name: { required: true, minLength: 2, maxLength: 45, },
                    behalf_mobile_number: { required: true, minLengthDigit: 9, maxLengthDigit: 9 },
                    behalf_email: { required: true, email: true },
                    // behalf_location: { required: true },
                    behalf_street_no: { required: true, minLength: 1, maxLength: 45 },
                    behalf_postcode: { required: true, minLengthDigit: 4, maxLengthDigit: 10 },
                    behalf_city: { required: true, minLength: 1, maxLength: 45 },
                    behalf_relation: { required: true },
                }
            },
            
        }
    }

    componentDidMount() {
        this.getRelationshipsApiCall()
    }


    setValues(key, value) {
        let userSignUpForm = { ...this.state.userSignUpForm }
        // if (key == 'city') {
        //     value = value.replace(/[^a-zA-Z ]/g, '');
        // } else 
        if (key == 'mobile_number') {
            value = value.replace(/\D/g, '');
        }
        userSignUpForm[key] = value
        this.setState({ userSignUpForm })
    }


    setValuesOnBehalf(key, value) {
        let userOnBehalfSignUpForm = { ...this.state.userOnBehalfSignUpForm }
        // if (key == 'behalf_city') {
        //     value = value.replace(/[^a-zA-Z ]/g, '');
        // }else 
        if (key == 'behalf_mobile_number') {
            value = value.replace(/\D/g, '');
        }
        userOnBehalfSignUpForm[key] = value
        this.setState({ userOnBehalfSignUpForm })
    }


    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    getPostalsCode = () => {
        let data = {
            postcode:this.state.search_p
        };
        console.log('datadata',data)
        this.setState({postalData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    postalData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderList = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.setValues('postcode',item.code)
                            //this.setValues('city','')
                            
                            this.setModalVisible(false)
                            setTimeout(() => {
                                 //this code use for clear city
                                this.setValues('city','')
                                this.setState({postalData:[]})
                                this.getCity()
                            },1000)
                            
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.code}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    postalModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisible(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By Postal')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            keyboardType={'numeric'}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_p:search}, () => {
                                this.getPostalsCode()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.postalData}
                        renderItem={this._renderList}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisible(false) }}
                        />
                    </View>
                </Modal>


        )
    }


    setModalVisibleCity = (visible) => {
        this.setState({ modalVisibleCity: visible });
    }

    getCity = () => {
        let data = {
            postcode:this.state.userSignUpForm.postcode,
            city:this.state.search_c
        };
        console.log('datadata',data)
        this.setState({cityData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    cityData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderListC = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            
                            //this.setValues('postcode',item.code)
                            this.setValues('city',item.city)
                            this.setModalVisibleCity(false)
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.city}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    cityModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisibleCity}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisibleCity(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By City')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_c:search}, () => {
                                this.getCity()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.cityData}
                        renderItem={this._renderListC}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisibleCity(false) }}
                        />
                    </View>
                </Modal>


        )
    }



    setModalVisibleB = (visible) => {
        this.setState({ modalVisibleB: visible });
    }

    getPostalsCodeB = () => {
        let data = {
            postcode:this.state.search_pb
        };
        console.log('datadata',data)
        this.setState({postalDataB:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    postalDataB:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderListB = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.setValuesOnBehalf('behalf_postcode',item.code)
                            //this.setValues('city','')
                            
                            this.setModalVisibleB(false)
                            setTimeout(() => {
                                 //this code use for clear city
                                this.setValuesOnBehalf('behalf_city','')
                                this.setState({postalDataB:[]})
                                this.getCityB()
                            },1000)
                            
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.code}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    postalModalB = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisibleB}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisibleB(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By Postal')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            keyboardType={'numeric'}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_pb:search}, () => {
                                this.getPostalsCodeB()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.postalDataB}
                        renderItem={this._renderListB}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisibleB(false) }}
                        />
                    </View>
                </Modal>


        )
    }


    setModalVisibleCityB = (visible) => {
        this.setState({ modalVisibleCityB: visible });
    }

    getCityB = () => {
        let data = {
            postcode:this.state.userOnBehalfSignUpForm.behalf_postcode,
            city:this.state.search_cb
        };
        console.log('datadata',data)
        this.setState({cityDataB:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    cityDataB:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderListCB = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            
                            //this.setValues('postcode',item.code)
                            this.setValuesOnBehalf('behalf_city',item.city)
                            this.setModalVisibleCityB(false)
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.city}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    cityModalB = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisibleCityB}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisibleCityB(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By City')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            returnKeyType="next"
                            onChangeText={(search) => this.setValuesOnBehalf({search_cb:search}, () => {
                                this.getCityB()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.cityDataB}
                        renderItem={this._renderListCB}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisibleCityB(false) }}
                        />
                    </View>
                </Modal>


        )
    }

    onChangeCheckedBox = () => {
        this.setState({ CheckedBox: !this.state.CheckedBox })
    }

    goToLogin() {
        handleNavigation({ type: 'pop', navigation: this.props.navigation })
    }

    goToSeniorScreen() {
        Keyboard.dismiss()
        let isValid = Helper.validate(this.state.userSignUpForm);
        if (isValid) {
            if (this.state.isOnBehalfVisible) {
                let isValidNew = Helper.validate(this.state.userOnBehalfSignUpForm);
                if (isValidNew) {
                    this.processSignup()
                }
            } else {
                this.processSignup()
            }
        }
    }

    async processSignup() {
        if (!this.state.CheckedBox) {
            Helper.showToast(LanguagesIndex.translate('Pleaseacceptourtermsandcondition'))
            return
        }

        Keyboard.dismiss()
        Helper.globalLoader.showLoader();
        await Helper.GetAddressFromLatLong(`${this.state.userSignUpForm.street_no} ${this.state.userSignUpForm.city} ${this.state.userSignUpForm.postcode}`, (resp) => {
            if (!resp) {
                Helper.globalLoader.hideLoader();
                return;
            }
            this.state.userSignUpForm.lat = resp.lat;
            this.state.userSignUpForm.lng = resp.lng;

            if (this.state.isOnBehalfVisible) {
                Helper.GetAddressFromLatLong(`${this.state.userOnBehalfSignUpForm.behalf_street_no} ${this.state.userOnBehalfSignUpForm.behalf_city} ${this.state.userOnBehalfSignUpForm.behalf_postcode}`, (respNew) => {
                    if (!respNew) {
                        Helper.globalLoader.hideLoader();
                        return;
                    }
                    this.state.userOnBehalfSignUpForm.behalf_lat = respNew.lat;
                    this.state.userOnBehalfSignUpForm.behalf_lng = respNew.lng;
                    this.processSignupCall()
                })
            } else {
                this.processSignupCall()
            }
        })
    }

    processSignupCall() {
        let data = new FormData();
        data.append('device_type', Helper.device_type)
        data.append('user_type', this.state.userSignUpForm.user_type)
        data.append('device_id', Helper.device_id)
        data.append('name', this.state.userSignUpForm.name)
        data.append('mobile_number', this.state.userSignUpForm.mobile_number)
        data.append('email', this.state.userSignUpForm.email)
        data.append('street_no', this.state.userSignUpForm.street_no)
        data.append('postcode', this.state.userSignUpForm.postcode)
        data.append('city', this.state.userSignUpForm.city)
        data.append('password', this.state.userSignUpForm.password)
        data.append('lat', this.state.userSignUpForm.lat)
        data.append('lng', this.state.userSignUpForm.lng)
        data.append('onbehalf', this.state.isOnBehalfVisible ? "1" : "0")

        if (this.state.isOnBehalfVisible) {
            data.append('behalf_name', this.state.userOnBehalfSignUpForm.behalf_name)
            data.append('behalf_mobile_number', this.state.userOnBehalfSignUpForm.behalf_mobile_number)
            data.append('behalf_email', this.state.userOnBehalfSignUpForm.behalf_email)
            data.append('behalf_street_no', this.state.userOnBehalfSignUpForm.behalf_street_no)
            data.append('behalf_postcode', this.state.userOnBehalfSignUpForm.behalf_postcode)
            data.append('behalf_city', this.state.userOnBehalfSignUpForm.behalf_city)
            data.append('behalf_relation', this.state.userOnBehalfSignUpForm.behalf_relation)
            data.append('behalf_lat', this.state.userOnBehalfSignUpForm.behalf_lat)
            data.append('behalf_lng', this.state.userOnBehalfSignUpForm.behalf_lng)
        }

        if (this.state.userSignUpForm.profile_picture) {
            data.append('profile_picture', {
                uri: this.state.userSignUpForm.profile_picture,
                name: 'test.jpeg',
                type: 'image/jpeg'
            });
        }
        ApiCall.postMethodWithHeader(Constants.SIGNUP, data, Constants.APIImageUploadAndroid).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                Helper.showToast(response.message);
                setTimeout(() => {
                    handleNavigation({ type: 'pop', navigation: this.props.navigation });
                }, 200);
            }
            else {
                Helper.showToast(response.message)
            }
        }
        ).catch(err => {
            Helper.globalLoader.hideLoader();
        })
    }

    getRelationshipsApiCall = () => {
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_relationships, data, Constants.APIPost).then((response) => {
            if (response.status == Constants.TRUE) {
                let newColumns = [];
                response.data.map(item => {
                    newColumns.push({ value: item.id, label: item.name.charAt(0).toUpperCase() + item.name.slice(1) });
                });

                this.setState({ arrRelationship: [...this.state.arrRelationship, ...newColumns] })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        ).catch(err => {
            Helper.globalLoader.hideLoader();
        })
    }


    chooseImage = () => {
        CameraController.open((response) => {
            if (response) {
                //this.UploadMediaMethod(response.uri);
                this.UploadMediaMethod(response.path);
            }
        });
    }

    UploadMediaMethod(uri) {
        this.setValues('profile_picture', uri)
        this.setState({ avatarSource: uri })
    }

    onOpenOnBehalf() {
        this.setState({ isOnBehalfVisible: !this.state.isOnBehalfVisible })
    }

    hideModal = () => {
        this.setState({ isOnBehalfVisible: false })
    }

    gotoFacebookSocialSide = () => {
        FacebookLogin((result) => {
            if (result) {
                Helper.globalLoader.showLoader();
                result.user_type = this.state.userSignUpForm.user_type;
                this.callSocialLogin(result);
            } else {
            }
        })
    }

    appleSignIn = (result) => {
        if (!result.user) return
        try {
            let formdata = {
                device_id: Helper.device_id,
                device_type: Helper.device_type,
                social_id: result.user,
                social_type: 'APPLE',
                name: '',
                email: '',
                profile_picture: '',
                user_type: this.state.userSignUpForm.user_type
            }

            if (result.email) {
                formdata.email = result.email;
            }
            if (result.fullName && result.fullName.givenName) {
                formdata.name = result.fullName.givenName;
            }

            Helper.globalLoader.showLoader();
            this.callSocialLogin(formdata);

        } catch (error) {
        }
    }

    callSocialLogin = (result) => {
        result.lan = LanguagesIndex.MyLanguage;
        ApiCall.postMethod(Constants.user_social, JSON.stringify(result), Constants.APIPost,).then((response) => {
            Keyboard.dismiss()
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                Helper.token = response.token
                AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                AsyncStorageHelper.setData("token", response.token)
                Helper.user_data = response.data

                LanguagesIndex.MyLanguage = response.data.default_lang
                AsyncStorageHelper.setData("lan", LanguagesIndex.MyLanguage)

                if (response.data.user_type == 'HAVEOMY') {
                    Helper.navRef.switchNavigation('2');
                } else if (response.data.user_type == 'BEOMY') {
                    Helper.navRef.switchNavigation('3');
                } else if (response.data.user_type == 'ENTERPRISE') {
                    Helper.navRef.switchNavigation('1');
                }

            } else {
                Helper.globalLoader.hideLoader();
                Helper.showToast(response.message)
            }
        }).catch(err => {
            Helper.globalLoader.hideLoader();
        })
    }


    termsCondition(value) {
        if (value == 1) {
            handleNavigation({ type: 'push', page: 'TermsAndCondition', navigation: this.props.navigation })
        } else {
            handleNavigation({ type: 'push', page: 'PrivacyPolicy', navigation: this.props.navigation })
        }
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                {this.state.modalVisible ? this.postalModal() : null}
                {this.state.modalVisibleCity ? this.cityModal() : null}
                {this.state.modalVisibleB ? this.postalModalB() : null}
                {this.state.modalVisibleCityB ? this.cityModalB() : null}
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View>
                        <Image source={images.Sign_up_178} resizeMode={'cover'}
                            style={{ height: DeviceH / 3.5, width: DeviceW }} />
                        <View style={styles.welcome_text_view}>
                            <Text style={styles.welcome_text}>{LanguagesIndex.translate('Welcome!')}</Text>
                        </View>

                        <View style={styles.user_pro_img_view}>
                            <Image
                                resizeMode={'cover'}
                                source={this.state.userSignUpForm.profile_picture ? { uri: this.state.userSignUpForm.profile_picture } : images.user_box}
                                // source={images.user_box}
                                style={styles.user_img} />

                            <TouchableOpacity
                                onPress={() => { this.chooseImage() }}
                                style={styles.camera_img_touch}>
                                <Image resizeMode={'cover'} source={images.user_camera} style={styles.camera_img} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.input_parent_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('Name')}
                            setFocus={() => { this.mobile_number.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(name) => this.setValues('name', name)}
                            value={this.state.userSignUpForm.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            getFocus={(input) => { this.mobile_number = input }}
                            setFocus={(input) => { this.email.focus(); }}
                            returnKeyType="next"
                            keyboardType={'number-pad'}
                            onChangeText={(mobile_number) => this.setValues('mobile_number', mobile_number)}
                            value={this.state.userSignUpForm.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('email')}
                            getFocus={(input) => { this.email = input }}
                            setFocus={(input) => { this.street_no.focus(); }}
                            returnKeyType="next"
                            maxLength={50}
                            keyboardType={'email-address'}
                            onChangeText={(email) => this.setValues('email', email)}
                            value={this.state.userSignUpForm.email}
                        />


                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            getFocus={(input) => { this.street_no = input }}
                            setFocus={(input) => { this.postcode.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(street_no) => this.setValues('street_no', street_no)}
                            value={this.state.userSignUpForm.street_no}
                        />

                        <View style={styles.post_city_parent_view}>
                            <View style={styles.post_view}>
                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    getFocus={(input) => { this.postcode = input }}
                                    setFocus={(input) => { this.city.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'number-pad'}
                                    onChangeText={(postcode) => this.setValues('postcode', postcode)}
                                    value={this.state.userSignUpForm.postcode}
                                /> */}
                                <TouchableOpacity 
                                onPress={() => this.setModalVisible(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userSignUpForm.postcode ? this.state.userSignUpForm.postcode : LanguagesIndex.translate('Post')}</Text>

                                </TouchableOpacity>
                            </View>

                            <View style={styles.city_view}>
                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('City')}
                                    getFocus={(input) => { this.city = input }}
                                    setFocus={(input) => { this.password.focus(); }}
                                    returnKeyType="next"
                                    // inputedit={false}
                                    // maxLength={15}
                                    keyboardType={'default'}
                                    onChangeText={(city) => this.setValues('city', city)}
                                    value={this.state.userSignUpForm.city}
                                /> */}
                                <TouchableOpacity 
                                onPress={() => this.setModalVisibleCity(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userSignUpForm.city ? this.state.userSignUpForm.city : LanguagesIndex.translate('City')}</Text>

                                </TouchableOpacity>
                            </View>
                        </View>

                        <IconInput
                            placeholder={LanguagesIndex.translate('Password')}
                            secureTextEntry={true}
                            getFocus={(input) => { this.password = input }}
                            setFocus={(input) => { this.confirm_password.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(password) => this.setValues('password', password)}
                            value={this.state.userSignUpForm.password}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('ConfirmPassword')}
                            secureTextEntry={true}
                            getFocus={(input) => { this.confirm_password = input }}
                            setFocus={() => { }}
                            returnKeyType="done"
                            keyboardType={'default'}
                            onChangeText={(confirm_password) => this.setValues('confirm_password', confirm_password)}
                            value={this.state.userSignUpForm.confirm_password}
                        />
                    </View>

                    <TouchableOpacity
                        onPress={() => this.onOpenOnBehalf()}
                        style={styles.behalf_par_view}>
                        <Text style={styles.behalf_txt}>{LanguagesIndex.translate('OnBehalf')}</Text>
                        <View
                            style={styles.check_box_touch}>
                            <Image resizeMode={'cover'}
                                source={this.state.isOnBehalfVisible ? images.check : images.unchecked}
                                style={styles.box_check_img}
                            />
                        </View>
                    </TouchableOpacity>

                    {this.state.isOnBehalfVisible ?
                        <View style={styles.behalf_modal_view}>
                            <IconInput
                                placeholder={LanguagesIndex.translate('Name')}
                                setFocus={() => { this.behalf_mobile_number.focus(); }}
                                returnKeyType="next"
                                keyboardType={'default'}
                                onChangeText={(behalf_name) => this.setValuesOnBehalf('behalf_name', behalf_name)}
                                value={this.state.userOnBehalfSignUpForm.behalf_name}
                            />

                            <IconInput
                                phoneCode
                                placeholder={LanguagesIndex.translate('ContactNumber')}
                                getFocus={(input) => { this.behalf_mobile_number = input }}
                                setFocus={(input) => { this.behalf_email.focus(); }}
                                returnKeyType="next"
                                keyboardType={'number-pad'}
                                onChangeText={(behalf_mobile_number) => this.setValuesOnBehalf('behalf_mobile_number', behalf_mobile_number)}
                                value={this.state.userOnBehalfSignUpForm.behalf_mobile_number}
                            />

                            <IconInput
                                placeholder={LanguagesIndex.translate('email')}
                                getFocus={(input) => { this.behalf_email = input }}
                                setFocus={(input) => { this.behalf_street_no.focus(); }}
                                returnKeyType="next"
                                maxLength={50}
                                keyboardType={'email-address'}
                                onChangeText={(behalf_email) => this.setValuesOnBehalf('behalf_email', behalf_email)}
                                value={this.state.userOnBehalfSignUpForm.behalf_email}
                            />


                            <IconInput
                                placeholder={LanguagesIndex.translate('Street/No')}
                                getFocus={(input) => { this.behalf_street_no = input }}
                                setFocus={(input) => { this.behalf_postcode.focus(); }}
                                returnKeyType="next"
                                keyboardType={'default'}
                                onChangeText={(behalf_street_no) => this.setValuesOnBehalf('behalf_street_no', behalf_street_no)}
                                value={this.state.userOnBehalfSignUpForm.behalf_street_no}
                            />

                            <View style={styles.post_city_parent_view}>
                                <View style={styles.post_view}>
                                    {/* <IconInput
                                        placeholder={LanguagesIndex.translate('Post')}
                                        getFocus={(input) => { this.behalf_postcode = input }}
                                        setFocus={(input) => { this.behalf_city.focus(); }}
                                        returnKeyType="next"
                                        keyboardType={'number-pad'}
                                        onChangeText={(behalf_postcode) => this.setValuesOnBehalf('behalf_postcode', behalf_postcode)}
                                        value={this.state.userOnBehalfSignUpForm.behalf_postcode}
                                    /> */}
                                    <TouchableOpacity 
                                onPress={() => this.setModalVisibleB(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userOnBehalfSignUpForm.behalf_postcode ? this.state.userOnBehalfSignUpForm.behalf_postcode : LanguagesIndex.translate('Post')}</Text>

                                </TouchableOpacity>
                                </View>

                                <View style={styles.city_view}>
                                    {/* <IconInput
                                        placeholder={LanguagesIndex.translate('City')}
                                        getFocus={(input) => { this.behalf_city = input }}
                                        // setFocus={(input) => { this.behalf_relation.focus(); }}
                                        returnKeyType="next"
                                        // maxLength={15}
                                        // inputedit={false}
                                        keyboardType={'default'}
                                        onChangeText={(behalf_city) => this.setValuesOnBehalf('behalf_city', behalf_city)}
                                        value={this.state.userOnBehalfSignUpForm.behalf_city}
                                    /> */}
                                     <TouchableOpacity 
                                onPress={() => this.setModalVisibleCityB(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userOnBehalfSignUpForm.behalf_city ? this.state.userOnBehalfSignUpForm.behalf_city : LanguagesIndex.translate('City')}</Text>

                                </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginVertical: 10 }}>
                                <RNPickerSelect
                                    items={this.state.arrRelationship}
                                    placeHolder={{}}
                                    onValueChange={(value) => {
                                        this.setValuesOnBehalf('behalf_relation', value)
                                    }}
                                    selectValue={this.state.userOnBehalfSignUpForm.behalf_relation}
                                    useNativeAndroidPickerStyle={false}
                                    style={pickerSelectStyles}
                                />
                            </View>

                        </View> : null
                    }

                    <View style={styles.check_box_text_view}>
                        <TouchableOpacity style={styles.check_box_touch} onPress={() => { this.onChangeCheckedBox(); }}>
                            <Image resizeMode={'cover'} source={this.state.CheckedBox ? images.check : images.unchecked} style={styles.box_check_img} />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', marginLeft: 10, }}>
                            <Text style={styles.terms_text}>{LanguagesIndex.translate('IAgreeTermsPrivacyPolicy')} </Text>
                            <Text onPress={() => this.termsCondition(1)} style={[styles.terms_text, { color: Colors.azul }]}>{LanguagesIndex.translate('Terms&Conditions')} </Text>
                            <Text style={styles.terms_text}>{LanguagesIndex.translate('and')} </Text>
                            <Text onPress={() => this.termsCondition(2)} style={[styles.terms_text, { color: Colors.azul }]}>{LanguagesIndex.translate('PrivacyPolicy')}</Text>
                        </View>
                    </View>

                    <View style={styles.sign_up_btn}>
                        <GButton
                            Text={LanguagesIndex.translate('SIGN_UP_PAGE')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.goToSeniorScreen() }}
                        />
                    </View>

                    <View style={styles.or_view}>
                        <View style={styles.or_line_view}></View>
                        <Text style={styles.or_text}>{LanguagesIndex.translate('or')}</Text>
                        <View style={styles.or_line_view}></View>
                    </View>

                    <View style={styles.social_btn_view}>
                        <SocialButton
                            facebookSignIn={this.gotoFacebookSocialSide}
                            appleSignIn={this.appleSignIn}
                        />
                    </View>

                    <TouchableOpacity
                        onPress={() => { this.goToLogin() }} style={styles.already_account_touch}>
                        <Text style={styles.already_text}>{LanguagesIndex.translate('alreadyAccount')}{' '}<Text style={styles.login_text}>{LanguagesIndex.translate('log_in')}</Text></Text>
                    </TouchableOpacity>
                </KeyboardScroll>

            </View>
        )
    }

};
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: fonts.fontSize14,
        height: 50,
        width: '100%',
        color: Colors.warmGrey,
        marginRight: 20,
        marginLeft: 8,
        marginBottom: 10,
        fontFamily: fonts.RoBoToMedium_1,
    },
    inputAndroid: {
        fontSize: fonts.fontSize14,
        width: '100%',
        color: Colors.warmGrey,
        marginRight: 20,
        marginLeft: 8,
        fontFamily: fonts.RoBoToMedium_1,
        textTransform: 'capitalize',
    },
});





