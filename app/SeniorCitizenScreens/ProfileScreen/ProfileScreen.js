import React from 'react';
import { View, Text, DeviceEventEmitter } from 'react-native';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import styles from './ProfileScreenStyles';
import IconInput from '../../Comman/GInput';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import AppHeader from '../../Comman/AppHeader';
import fonts from '../../Assets/fonts';
import Helper from '../../Lib/Helper';
import ImageLoadView from '../../Lib/ImageLoadView';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import * as RNLocalize from "react-native-localize";

export default class ProfileScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            seniorCitizenDetails: {},
            onBehalfUserDetails: {},
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('Profile'),
            hideLeftBackIcon: true,
            bellIcon: true, settingsIcon: true, profileIcon: true,
            profileIconClick: () => this.profileIconClick(),
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    profileIconClick() {
        handleNavigation({ type: 'push', page: 'EditProfileScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }


    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            AppHeader({
                ...this.props.navigation, leftTitle: LanguagesIndex.translate('Profile'),
                hideLeftBackIcon: true,
                bellIcon: true, settingsIcon: true, profileIcon: true,
                profileIconClick: () => this.profileIconClick(),
                bellIconClick: () => this.bellIconClick(),
                settingIconClick: () => this.settingIconClick()
            })

        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            Helper.updateUserData();
            this.methodFillForm();
        });
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
        this._unsubscribe();
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    methodFillForm() {
        if (Helper.user_data) {
            this.setState({ seniorCitizenDetails: Helper.user_data });
        }
    }

    render() {
        console.log("Helper.user_data",Helper.user_data)
        return (
            <View style={styles.safe_area_view}>
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.profile_view}>
                        <View style={styles.profile_img_view}>
                            <ImageLoadView
                                resizeMode={'cover'}
                                source={this.state.seniorCitizenDetails.profile_picture ? { uri: this.state.seniorCitizenDetails.profile_picture } : images.default}
                                style={styles.profile_img} />
                        </View>
                    </View>

                    <View style={styles.input_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('Name')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.seniorCitizenDetails.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.seniorCitizenDetails.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('email')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.seniorCitizenDetails.email}
                        />


                        {/* <View
                            style={{
                                marginVertical: 10,
                                borderRadius: 15,
                                padding: 15,
                                backgroundColor: Colors.whiteThree,
                            }}>
                            <Text numberOfLines={1} style={{
                                fontSize: fonts.fontSize14,
                                color: Colors.warmGrey,
                                fontFamily: fonts.RoBoToMedium_1,
                            }}>{this.state.seniorCitizenDetails.location ? this.state.seniorCitizenDetails.location : LanguagesIndex.translate('Location')}</Text>
                        </View> */}


                        {/* 
                        <IconInput
                            placeholder={LanguagesIndex.translate('Location')}
                            maxLength={45}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.seniorCitizenDetails.location}
                        /> */}

                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.seniorCitizenDetails.street_no}
                        />

                        <View style={styles.input_zip_city_view}>
                            <View style={styles.input_zip_view}>
                                <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.postcode}
                                />
                            </View>

                            <View style={styles.input_city_view}>

                                <View
                                    style={{
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.seniorCitizenDetails.city ? this.state.seniorCitizenDetails.city : LanguagesIndex.translate('City')}</Text>
                                </View>
                                {/* <IconInput
                                    maxLength={15}
                                    placeholder={LanguagesIndex.translate('City')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.city}
                                /> */}
                            </View>
                        </View>

                        {this.state.seniorCitizenDetails.onbehalf ?
                            <>
                                <View style={{ marginVertical: 5 }}>
                                    <Text style={{ color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1, }}>{LanguagesIndex.translate('OnBehalf')}</Text>
                                </View>


                                <IconInput
                                    placeholder={LanguagesIndex.translate('Name')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.on_behalf_user.behalf_name}
                                />

                                <IconInput
                                    phoneCode
                                    placeholder={LanguagesIndex.translate('ContactNumber')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.on_behalf_user.behalf_mobile_number}
                                />

                                <IconInput
                                    placeholder={LanguagesIndex.translate('email')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.on_behalf_user.behalf_email}
                                />


                                {/* <View
                                    style={{
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.seniorCitizenDetails.on_behalf_user.location ? this.state.seniorCitizenDetails.on_behalf_user.location : LanguagesIndex.translate('Location')}</Text>
                                </View> */}


                                {/* <IconInput
                                    maxLength={45}
                                    placeholder={LanguagesIndex.translate('Location')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.on_behalf_user.location}
                                /> */}

                                <IconInput
                                    placeholder={LanguagesIndex.translate('Street/No')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.on_behalf_user.street_no}
                                />

                                <View style={styles.input_zip_city_view}>
                                    <View style={styles.input_zip_view}>
                                        <IconInput
                                            placeholder={LanguagesIndex.translate('Post')}
                                            placeholderTextColor={Colors.greyishBrownTwo}
                                            inputedit={false}
                                            value={this.state.seniorCitizenDetails.on_behalf_user.postcode}
                                        />
                                    </View>

                                    <View style={styles.input_city_view}>


                                        <View
                                            style={{
                                                marginVertical: 10,
                                                borderRadius: 15,
                                                padding: 15,
                                                backgroundColor: Colors.whiteThree,
                                            }}>
                                            <Text numberOfLines={1} style={{
                                                fontSize: fonts.fontSize14,
                                                color: Colors.warmGrey,
                                                fontFamily: fonts.RoBoToMedium_1,
                                            }}>{this.state.seniorCitizenDetails.on_behalf_user.city ? this.state.seniorCitizenDetails.on_behalf_user.city : LanguagesIndex.translate('City')}</Text>
                                        </View>


                                        {/* <IconInput
                                            maxLength={15}
                                            placeholder={LanguagesIndex.translate('City')}
                                            placeholderTextColor={Colors.greyishBrownTwo}
                                            inputedit={false}
                                            value={this.state.seniorCitizenDetails.on_behalf_user.city}
                                        /> */}
                                    </View>
                                </View>


                                <IconInput
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.seniorCitizenDetails.on_behalf_user.myrelation}
                                />
                            </>
                            : null}
                    </View>
                </KeyboardScroll>
            </View>
        )
    }

};





