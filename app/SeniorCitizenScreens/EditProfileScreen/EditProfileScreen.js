import React from 'react';
import { Text, View, ScrollView, Image, StyleSheet, TouchableOpacity, SafeAreaView, DeviceEventEmitter, Keyboard,Modal,FlatList } from 'react-native';
import styles from './EditProfileScreenStyles';
import { GButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import IconInput from '../../Comman/GInput';
import fonts from '../../Assets/fonts'; 
import RNPickerSelect from '../../Comman/CommonPicker';
import AppHeader from '../../Comman/AppHeader';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import CameraController from '../../Lib/CameraController';


export default class EditProfileScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatarSource: '',
            arrRelationship: [
                { label: LanguagesIndex.translate('Relationship'), value: null }
            ],

            userSeniorEditForm: {
                profile_picture: Helper.user_data.profile_picture ? Helper.user_data.profile_picture : '',
                name: Helper.user_data.name ? Helper.user_data.name : '',
                mobile_number: Helper.user_data.mobile_number ? Helper.user_data.mobile_number : '',
                email: Helper.user_data.email ? Helper.user_data.email : '',
                // location: Helper.user_data.location ? Helper.user_data.location : '',
                lat: Helper.user_data.lat ? Helper.user_data.lat : '',
                lng: Helper.user_data.lng ? Helper.user_data.lng : '',
                street_no: Helper.user_data.street_no ? Helper.user_data.street_no : '',
                postcode: Helper.user_data.postcode ? Helper.user_data.postcode : '',
                city: Helper.user_data.city ? Helper.user_data.city : '',
                onbehalf: '',

                validators: {
                    name: { required: true, minLength: 2, maxLength: 45 },
                    mobile_number: { required: true, minLengthDigit: 9, maxLengthDigit: 9 },
                    email: { required: true, email: true },
                    // location: { required: true },
                    street_no: { required: true, minLength: 1, maxLength: 45 },
                    postcode: { required: true, minLengthDigit: 4, maxLengthDigit: 10 },
                    city: { required: true, minLength: 1, maxLength: 45 }
                }
            },

            userSeniorBehalfEditForm: {
                behalf_name: Helper.user_data.on_behalf_user && Helper.user_data.on_behalf_user.behalf_name ? Helper.user_data.on_behalf_user.behalf_name : '',
                behalf_mobile_number: Helper.user_data.on_behalf_user && Helper.user_data.on_behalf_user.behalf_mobile_number ? Helper.user_data.on_behalf_user.behalf_mobile_number : '',
                behalf_email: Helper.user_data.on_behalf_user && Helper.user_data.on_behalf_user.behalf_email ? Helper.user_data.on_behalf_user.behalf_email : '',
                // behalf_location: Helper.user_data.on_behalf_user && Helper.user_data.on_behalf_user.location ? Helper.user_data.on_behalf_user.location : '',
                behalf_street_no: Helper.user_data.on_behalf_user && Helper.user_data.on_behalf_user.street_no ? Helper.user_data.on_behalf_user.street_no : '',
                behalf_postcode: Helper.user_data.on_behalf_user && Helper.user_data.on_behalf_user.postcode ? Helper.user_data.on_behalf_user.postcode : '',
                behalf_city: Helper.user_data.on_behalf_user && Helper.user_data.on_behalf_user.city ? Helper.user_data.on_behalf_user.city : '',
                behalf_lat: Helper.user_data.lat ? Helper.user_data.lat : '',
                behalf_lng: Helper.user_data.lng ? Helper.user_data.lng : '',

                behalf_relation: Helper.user_data.on_behalf_user && Number(Helper.user_data.on_behalf_user.relation ? Helper.user_data.on_behalf_user.relation : ''),

                validators: {
                    behalf_name: { required: true, minLength: 2, maxLength: 45 },
                    behalf_mobile_number: { required: true, minLengthDigit: 9, maxLengthDigit: 9 },
                    behalf_email: { required: true, email: true },
                    // behalf_location: { required: true },
                    behalf_street_no: { required: true, minLength: 1, maxLength: 45 },
                    behalf_postcode: { required: true, minLengthDigit: 4, maxLengthDigit: 10 },
                    behalf_city: { required: true, minLength: 1, maxLength: 45 },
                    behalf_relation: { required: true }
                }
            },
            modalVisible:false,
                postalData:[],
                modalVisibleCity:false
        }
        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('EditProfile') })
    }

    componentDidMount() {
        this.getRelationshipsApiCall()
    }

    setValues(key, value) {
        let userSeniorEditForm = { ...this.state.userSeniorEditForm }
        // if (key == 'city') { value = value.replace(/[^a-zA-Z ]/g, ''); }
        // else { value = value; }
        if (key == 'mobile_number') {
            value = value.replace(/\D/g, '');
        }
        userSeniorEditForm[key] = value
        this.setState({ userSeniorEditForm })
    }

    setValuesOnBehalf(key, value) {
        let userSeniorBehalfEditForm = { ...this.state.userSeniorBehalfEditForm }
        // if (key == 'behalf_city') { value = value.replace(/[^a-zA-Z ]/g, ''); }
        // else { value = value; }
        if (key == 'behalf_mobile_number') {
            value = value.replace(/\D/g, '');
        }
        userSeniorBehalfEditForm[key] = value
        this.setState({ userSeniorBehalfEditForm })
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    getPostalsCode = () => {
        let data = {
            postcode:this.state.search_p
        };
        console.log('datadata',data)
        this.setState({postalData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    postalData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderList = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.setValues('postcode',item.code)
                            //this.setValues('city','')
                            
                            this.setModalVisible(false)
                            setTimeout(() => {
                                //this code use for clear city
                                this.setValues('city','')
                                this.setState({postalData:[]})
                                this.getCity()
                            },1000)
                            
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.code}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    postalModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisible(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By Postal')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            keyboardType={'numeric'}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_p:search}, () => {
                                this.getPostalsCode()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.postalData}
                        renderItem={this._renderList}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisible(false) }}
                        />
                    </View>
                </Modal>


        )
    }


    setModalVisibleCity = (visible) => {
        this.setState({ modalVisibleCity: visible });
    }

    getCity = () => {
        let data = {
            postcode:this.state.userSeniorEditForm.postcode,
            city:this.state.search_c
        };
        console.log('datadata',data)
        this.setState({cityData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    cityData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderListC = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            
                            //this.setValues('postcode',item.code)
                            this.setValues('city',item.city)
                            this.setModalVisibleCity(false)
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.city}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    cityModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisibleCity}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisibleCity(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By City')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_c:search}, () => {
                                this.getCity()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.cityData}
                        renderItem={this._renderListC}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisibleCity(false) }}
                        />
                    </View>
                </Modal>


        )
    }

    chooseImage = () => {
        CameraController.open((response) => {
            if (response) {
                this.UploadMediaMethod(response.path);
            }
        });
    } 

    UploadMediaMethod(uri) {
        this.setValues('profile_picture', uri)
        this.setState({ avatarSource: uri })
    }

    onEditProfile() {
        Keyboard.dismiss();
        let isValid = Helper.validate(this.state.userSeniorEditForm);
        if (isValid) {
            if (Helper.user_data.onbehalf) {
                let isValidNew = Helper.validate(this.state.userSeniorBehalfEditForm);
                if (isValidNew) {
                    this.EditSeniorUpdateProfile()
                }
            } else {
                this.EditSeniorUpdateProfile()
            }
        }
    }

    async EditSeniorUpdateProfile() {
        Helper.globalLoader.showLoader();
        await Helper.GetAddressFromLatLong(`${this.state.userSeniorEditForm.street_no} ${this.state.userSeniorEditForm.city} ${this.state.userSeniorEditForm.postcode}`, (resp) => {

            if (!resp) {
                Helper.globalLoader.hideLoader();
                return;
            }

            this.state.userSeniorEditForm.lat = resp.lat;
            this.state.userSeniorEditForm.lng = resp.lng;

            if (Helper.user_data.onbehalf) {
                Helper.GetAddressFromLatLong(`${this.state.userSeniorBehalfEditForm.behalf_street_no} ${this.state.userSeniorBehalfEditForm.behalf_city} ${this.state.userSeniorBehalfEditForm.behalf_postcode}`, (respNew) => {
                    if (!respNew) {
                        Helper.globalLoader.hideLoader();
                        return;
                    }
                    this.state.userSeniorBehalfEditForm.behalf_lat = respNew.lat;
                    this.state.userSeniorBehalfEditForm.behalf_lng = respNew.lng;
                    this.processUpdate()
                })

            } else {
                this.processUpdate()
            }
        });
    }

    processUpdate() {
        let data = new FormData();
        data.append('name', this.state.userSeniorEditForm.name);
        data.append('mobile_number', this.state.userSeniorEditForm.mobile_number);
        data.append('email', this.state.userSeniorEditForm.email);
        data.append('lat', this.state.userSeniorEditForm.lat);
        data.append('lng', this.state.userSeniorEditForm.lng);
        data.append('street_no', this.state.userSeniorEditForm.street_no);
        data.append('postcode', this.state.userSeniorEditForm.postcode);
        data.append('city', this.state.userSeniorEditForm.city);

        data.append('onbehalf', Helper.user_data.onbehalf);
        data.append('behalf_name', this.state.userSeniorBehalfEditForm.behalf_name);
        data.append('behalf_mobile_number', this.state.userSeniorBehalfEditForm.behalf_mobile_number);
        data.append('behalf_email', this.state.userSeniorBehalfEditForm.behalf_email);
        data.append('behalf_lat', this.state.userSeniorBehalfEditForm.behalf_lat);
        data.append('behalf_lng', this.state.userSeniorBehalfEditForm.behalf_lng);
        data.append('behalf_street_no', this.state.userSeniorBehalfEditForm.behalf_street_no);
        data.append('behalf_postcode', this.state.userSeniorBehalfEditForm.behalf_postcode);
        data.append('behalf_city', this.state.userSeniorBehalfEditForm.behalf_city);
        data.append('behalf_relation', this.state.userSeniorBehalfEditForm.behalf_relation);
        if (this.state.avatarSource) {
            data.append('profile_picture', {
                uri: this.state.avatarSource,
                name: 'test.jpg',
                type: 'image/jpeg'
            });
        }

       // console.log("data---data>>>>>",data)

        ApiCall.postMethodWithHeader(Constants.update_profile, data, Constants.APIImageUploadAndroid).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
               // console.log("response---response>>>>>",response)
                Helper.showToast(response.message)
                AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                Helper.user_data = response.data
                DeviceEventEmitter.emit(Constants.USER_DETAILS, response.data);
                setTimeout(() => {
                    handleNavigation({ type: 'pop', navigation: this.props.navigation });
                }, 200);
            }
            else {
                Helper.showToast(response.message)
            }
        })
    }

    getRelationshipsApiCall = () => {
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_relationships, data, Constants.APIPost).then((response) => {
            if (response.status == Constants.TRUE) {
                const newColumns = response.data.map(item => {
                    const { id: value, name: label, ...rest } = item;
                    return { value, label, ...rest }
                }
                );
                this.state.arrRelationship = [...this.state.arrRelationship, ...newColumns]
            }
            else {
                Helper.showToast(response.message)
            }
            this.setState({})
        }
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view} >
                {this.state.modalVisible ? this.postalModal() : null}
                {this.state.modalVisibleCity ? this.cityModal() : null}
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.pare_prof_view}>
                        <View style={styles.blue_radius_view}>

                        </View>
                        <View style={styles.imag_pro_view}>
                            <View style={styles.user_img_view}>
                                <Image
                                    resizeMode={'cover'}
                                    source={this.state.avatarSource ? { uri: this.state.avatarSource } : Helper.user_data.profile_picture ? { uri: Helper.user_data.profile_picture } : images.default}
                                    style={styles.img_user} />
                            </View>

                            <TouchableOpacity
                                onPress={() => { this.chooseImage() }}
                                style={styles.camera_touch}>
                                <Image resizeMode={'cover'}
                                    source={images.user_camera}
                                    style={styles.camera_img} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.input_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('Name')}
                            setFocus={() => { this.mobile_number.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(name) => this.setValues('name', name)}
                            value={this.state.userSeniorEditForm.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            getFocus={(input) => { this.mobile_number = input }}
                            setFocus={(input) => { this.email.focus(); }}
                            returnKeyType="next"
                            keyboardType={'number-pad'}
                            onChangeText={(mobile_number) => this.setValues('mobile_number', mobile_number)}
                            value={this.state.userSeniorEditForm.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('email')}
                            getFocus={(input) => { this.email = input }}
                            returnKeyType="next"
                            maxLength={30}
                            inputedit={Helper.user_data.email ? false : true}
                            keyboardType={'email-address'}
                            onChangeText={(email) => this.setValues('email', email)}
                            value={this.state.userSeniorEditForm.email}
                        />

                        {/* <TouchableOpacity
                            onPress={() => this.openModal('1')}
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginVertical: 10,
                                borderRadius: 15,
                                padding: 15,
                                backgroundColor: Colors.whiteThree,
                            }}>
                            <Text numberOfLines={1} style={{
                                fontSize: fonts.fontSize14,
                                color: Colors.warmGrey,
                                fontFamily: fonts.RoBoToMedium_1,
                            }}>{this.state.userSeniorEditForm.location ? this.state.userSeniorEditForm.location : LanguagesIndex.translate('Location')}</Text>
                        </TouchableOpacity> */}

                        {/* <TouchableOpacity onPress={() => this.openModal('1')}>
                            <IconInput
                                placeholder={LanguagesIndex.translate('Location')}
                                getFocus={(input) => { this.location = input }}
                                setFocus={(input) => { this.street_no.focus(); }}
                                returnKeyType="next"
                                maxLength={150}
                                inputedit={false}
                                keyboardType={'default'}
                                onChangeText={(location) => this.setValues('location', location)}
                                value={this.state.userSeniorEditForm.location}
                            />
                        </TouchableOpacity> */}

                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            getFocus={(input) => { this.street_no = input }}
                            setFocus={(input) => { this.postcode.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(street_no) => this.setValues('street_no', street_no)}
                            value={this.state.userSeniorEditForm.street_no}
                        />

                        <View style={styles.post_city_view}>
                            <View style={styles.post_view}>
                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    getFocus={(input) => { this.postcode = input }}
                                    etFocus={(input) => { this.city.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'number-pad'}
                                    onChangeText={(postcode) => this.setValues('postcode', postcode)}
                                    value={this.state.userSeniorEditForm.postcode}
                                /> */}
                                <TouchableOpacity 
                                onPress={() => this.setModalVisible(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userSeniorEditForm.postcode ? this.state.userSeniorEditForm.postcode : LanguagesIndex.translate('Post')}</Text>

                                </TouchableOpacity>
                            </View>

                            <View style={styles.city_view}>

                                {/* <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.userSeniorEditForm.city ? this.state.userSeniorEditForm.city : LanguagesIndex.translate('City')}</Text>
                                </View> */}



                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('City')}
                                    getFocus={(input) => { this.city = input }}
                                    returnKeyType="done"
                                    maxLength={12}
                                    // inputedit={false}
                                    keyboardType={'default'}
                                    onChangeText={(city) => this.setValues('city', city)}
                                    value={this.state.userSeniorEditForm.city}
                                /> */}
                                <TouchableOpacity 
                                onPress={() => this.setModalVisibleCity(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userSeniorEditForm.city ? this.state.userSeniorEditForm.city : LanguagesIndex.translate('City')}</Text>

                                </TouchableOpacity>
                            </View>
                        </View>

                        {Helper.user_data.onbehalf ?
                            <>
                                <View style={styles.on_behalf}>
                                    <Text style={styles.on_behalf_txt}>{LanguagesIndex.translate('OnBehalf')}</Text>
                                </View>

                                <IconInput
                                    placeholder={LanguagesIndex.translate('Name')}
                                    setFocus={() => { this.behalf_mobile_number.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'default'}
                                    onChangeText={(behalf_name) => this.setValuesOnBehalf('behalf_name', behalf_name)}
                                    value={this.state.userSeniorBehalfEditForm.behalf_name}
                                />

                                <IconInput
                                    phoneCode
                                    placeholder={LanguagesIndex.translate('ContactNumber')}
                                    getFocus={(input) => { this.behalf_mobile_number = input }}
                                    setFocus={(input) => { this.behalf_email.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'number-pad'}
                                    onChangeText={(behalf_mobile_number) => this.setValuesOnBehalf('behalf_mobile_number', behalf_mobile_number)}
                                    value={this.state.userSeniorBehalfEditForm.behalf_mobile_number}
                                />

                                <IconInput
                                    placeholder={LanguagesIndex.translate('email')}
                                    getFocus={(input) => { this.behalf_email = input }}
                                    // setFocus={(input) => { this.behalf_location.focus(); }}
                                    returnKeyType="next"
                                    maxLength={30}
                                    // inputedit={false}
                                    keyboardType={'email-address'}
                                    onChangeText={(behalf_email) => this.setValuesOnBehalf('behalf_email', behalf_email)}
                                    value={this.state.userSeniorBehalfEditForm.behalf_email}
                                />

                                {/* <TouchableOpacity
                                    onPress={() => this.openModal('2')}
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.userSeniorBehalfEditForm.behalf_location ? this.state.userSeniorBehalfEditForm.behalf_location : LanguagesIndex.translate('Location')}</Text>
                                </TouchableOpacity> */}



                                {/* <TouchableOpacity onPress={() => this.openModal('2')}>
                                    <IconInput
                                        placeholder={LanguagesIndex.translate('Location')}
                                        getFocus={(input) => { this.behalf_location = input }}
                                        setFocus={(input) => { this.behalf_street_no.focus(); }}
                                        returnKeyType="next"
                                        maxLength={150}
                                        inputedit={false}
                                        keyboardType={'default'}
                                        onChangeText={(behalf_location) => this.setValuesOnBehalf('behalf_location', behalf_location)}
                                        value={this.state.userSeniorBehalfEditForm.behalf_location}
                                    />
                                </TouchableOpacity> */}

                                <IconInput
                                    placeholder={LanguagesIndex.translate('Street/No')}
                                    getFocus={(input) => { this.behalf_street_no = input }}
                                    setFocus={(input) => { this.behalf_postcode.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'default'}
                                    onChangeText={(behalf_street_no) => this.setValuesOnBehalf('behalf_street_no', behalf_street_no)}
                                    value={this.state.userSeniorBehalfEditForm.behalf_street_no}
                                />

                                <View style={styles.post_city_relation_view}>
                                    <View style={styles.on_behalf_post_view}>
                                        <IconInput
                                            placeholder={LanguagesIndex.translate('Post')}
                                            getFocus={(input) => { this.behalf_postcode = input }}
                                            setFocus={(input) => { this.behalf_city.focus(); }}
                                            returnKeyType="next"
                                            keyboardType={'number-pad'}
                                            onChangeText={(behalf_postcode) => this.setValuesOnBehalf('behalf_postcode', behalf_postcode)}
                                            value={this.state.userSeniorBehalfEditForm.behalf_postcode}
                                        />
                                    </View>

                                    <View style={styles.on_behalf_city_view}>

                                        {/* <View
                                            style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                marginVertical: 10,
                                                borderRadius: 15,
                                                padding: 15,
                                                backgroundColor: Colors.whiteThree,
                                            }}>
                                            <Text numberOfLines={1} style={{
                                                fontSize: fonts.fontSize14,
                                                color: Colors.warmGrey,
                                                fontFamily: fonts.RoBoToMedium_1,
                                            }}>{this.state.userSeniorBehalfEditForm.behalf_city ? this.state.userSeniorBehalfEditForm.behalf_city : LanguagesIndex.translate('City')}</Text>
                                        </View> */}


                                        <IconInput
                                            placeholder={LanguagesIndex.translate('City')}
                                            getFocus={(input) => { this.behalf_city = input }}
                                            // setFocus={(input) => { this.password.focus(); }}
                                            returnKeyType="done"
                                            maxLength={30}
                                            // inputedit={false}
                                            keyboardType={'default'}
                                            onChangeText={(behalf_city) => this.setValuesOnBehalf('behalf_city', behalf_city)}
                                            value={this.state.userSeniorBehalfEditForm.behalf_city}
                                        />
                                    </View>
                                </View>

                                <View style={styles.relation_view}>
                                    <RNPickerSelect
                                        items={this.state.arrRelationship}
                                        placeHolder={{}}
                                        onValueChange={(value) => {
                                            this.setValuesOnBehalf('behalf_relation', value)
                                        }}
                                        selectValue={this.state.userSeniorBehalfEditForm.behalf_relation}
                                        useNativeAndroidPickerStyle={false}
                                        style={pickerSelectStyles}
                                    />
                                </View>
                            </>
                            : null

                        }
                    </View>

                    <View style={styles.update_btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('UPDATE')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.onEditProfile() }}
                        />
                    </View>
                </KeyboardScroll>

            </SafeAreaView >
        )
    }

};
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: fonts.fontSize14,
        height: 50,
        width: '100%',
        color: Colors.warmGrey,
        marginRight: 20,
        marginLeft: 8,
        // marginBottom: 10,
        fontFamily: fonts.RoBoToMedium_1,
    },
    inputAndroid: {
        fontSize: fonts.fontSize14,
        height: 50,
        width: '100%',
        color: Colors.warmGrey,
        marginRight: 20,
        marginLeft: 8,
        // marginBottom: 10,
        fontFamily: fonts.RoBoToMedium_1,
        textTransform: 'capitalize',

    },
});





