import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default EditProfileScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    pare_prof_view:{
        height: 120, 
        width: '100%'
    },
    blue_radius_view:{
        backgroundColor: Colors.cerulean, 
        height: 50, 
        width: '100%', 
        borderBottomLeftRadius: 25, 
        borderBottomRightRadius: 25
    },
    imag_pro_view:{
        alignSelf: 'center', 
        marginTop: -40, 
        height: 95, 
        width: 90
    },
    user_img_view:{
        height: 82, 
        width: 82, 
        borderRadius: 20,
    },
    img_user:{
        height: '100%', 
        width: '100%', 
        borderRadius: 20,
    },
    camera_touch:{
        height: 26, 
        width: 26, 
        position: 'absolute', 
        bottom: 0, 
        left: 30
    },
    camera_img:{
        height: '100%', 
        width: '100%',
    },
    input_view:{
        marginVertical: 10, 
        marginTop: 10, 
        marginHorizontal: 16
    },
    post_city_view:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    post_view:{
        flex: 4, 
        marginRight: 5
    },
    city_view:{
        flex: 4, 
        marginLeft: 5
    },
    on_behalf:{
        marginVertical: 5
    },
    on_behalf_txt:{
        color: Colors.black, 
        fontSize: fonts.fontSize14, 
        fontFamily: fonts.RoBoToMedium_1
    },
    post_city_relation_view:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    on_behalf_post_view:{
        flex: 4, 
        marginRight: 5
    },
    on_behalf_city_view:{
        flex: 4, 
        marginLeft: 5
    },
    relation_view:{
        marginVertical: 10
    },
    update_btn_view:{
        marginVertical: 10, 
        marginTop: 33, 
        marginHorizontal: 16
    },
    modal_color_view:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.white,
        paddingVertical: 100, 
        paddingHorizontal:40,
        //borderBottomRightRadius: 30, 
        //borderBottomLeftRadius: 30,
      },
   
});