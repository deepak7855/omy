import React from 'react';
import {
  Text,
  View,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  Keyboard,
  ScrollView,
  TextInput,
  Image,
  ImageBackground,
  DeviceEventEmitter,
} from 'react-native';
import styles from './HomeScreenStyles';
import Colors from '../../Assets/Colors';
import {images} from '../../Assets/imagesUrl';
import fonts from '../../Assets/fonts';
import AppHeader from '../../Comman/AppHeader';
import Helper from '../../Lib/Helper';
import {ApiCall, Constants} from '../../../app/Api';
import {handleNavigation} from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import SplashScreen from 'react-native-splash-screen';
import ImageLoadView from '../../Lib/ImageLoadView';
import {AirbnbRating} from 'react-native-ratings';
import {
  notifyCountStatus,
  sendToken,
  updateDeviceToken,
} from '../../Api/NotifyCountApi';
import messaging from '@react-native-firebase/messaging';
import SocketController, {events, socketInit} from '../../Lib/SocketController';
import * as RNLocalize from 'react-native-localize';

export default class HomeSenior extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ServiceActivities: [],
      search_value: '',
      arrSearchingActivities: [],
      arrActivityStudentList: [],
    };
    this.createHeader();
  }

  componentDidMount() {
    Helper.navigationRef = this.props.navigation;
    SocketController.socketInit();
    setTimeout(() => {
      SocketController.getUnreadCount();
    }, 2000);
    this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
      RNLocalize.addEventListener(
        'change',
        this.handleLocalizationChange(LanguagesIndex.MyLanguage),
      );
      this.getActivitiesServicesApiCall();
    });
    RNLocalize.addEventListener(
      'change',
      this.handleLocalizationChange(LanguagesIndex.MyLanguage),
    );

    setTimeout(() => {
      SplashScreen.hide();
    }, 500);
    this.listener = DeviceEventEmitter.addListener(
      Constants.USER_DETAILS,
      (value) => {
        this.createHeader();
      },
    );
    this.getActivitiesServicesApiCall();

    this._unsubscribeHomeSeniorFocus = this.props.navigation.addListener(
      'focus',
      () => {
        notifyCountStatus();
        updateDeviceToken();
        // messaging().onTokenRefresh(async (fcmToken) => {
        //     sendToken(fcmToken)
        // });
        this.getRecentStudentListApiCall();
      },
    );
    this.Book_Request_Schedule = DeviceEventEmitter.addListener(
      'Book_Request_Schedule',
      () => {
        this.getRecentStudentListApiCall();
      },
    );
    this.messageListener();
  }

  messageListener = async () => {
    messaging().setBackgroundMessageHandler(async (remoteMessage) => {});

    //When the application is running, but in the background
    messaging().onNotificationOpenedApp(async (notification) => {
      if (notification && notification.data) {
        this.handleNotificationNavigation(
          notification.data.type,
          notification.data.dictionary,
        );
      }
    });

    // notification app kill
    messaging()
      .getInitialNotification()
      .then(async (notification) => {
        if (notification && notification.data) {
          this.handleNotificationNavigation(
            notification.data.type,
            notification.data.dictionary,
          );
        }
      });

    //notification app forground
    messaging().onMessage(async (notification) => {
      let encryptedData = JSON.parse(notification.data.dictionary);
      if (Helper.currentPage != 'chat' || encryptedData.type != 'chat') {
        DeviceEventEmitter.emit('showLocalNotification', notification);
      }
    });
  };

  handleNotificationNavigation(type, dicData) {
    let encryptedData = JSON.parse(dicData);

    if (type != 'custom_notification') {
      if (type == 'chat') {
        SocketController.socketInit();
        setTimeout(() => {
          handleNavigation({
            type: 'push',
            page: 'ChatScreen',
            navigation: this.props.navigation,
            passProps: {
              other_user_id: encryptedData.sender_id,
              name: encryptedData.name,
              picture: encryptedData.profile_picture,
              fromWhere: 'booking',
              extraData: {
                bookingStatus: 'ACCEPT',
              },
            },
          });
        }, 1000);
      } else {
        handleNavigation({
          page: 'ActivityDetailScreen',
          type: 'push',
          navigation: this.props.navigation,
          passProps: {
            booking_id: encryptedData.booking_id,
            booking_service_id: encryptedData.booking_service_id,
            BookingStatus: encryptedData.status,
          },
        });
      }
    }
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    this._unsubscribeHomeSeniorFocus();
    if (this.listener) this.listener.remove();
    this.Book_Request_Schedule.remove();
  }

  handleLocalizationChange = (lang) => {
    LanguagesIndex.setI18nConfig(lang);
    this.forceUpdate();
  };

  createHeader() {
    AppHeader({
      ...this.props.navigation,
      leftTitle: Helper.user_data.name,
      bellIcon: true,
      settingsIcon: true,
      leftIcon: Helper.user_data.profile_picture
        ? {uri: Helper.user_data.profile_picture}
        : images.default,
      leftIconStyle: {
        height: 31,
        width: 31,
        borderRadius: 31 / 2,
        resizeMode: 'cover',
      },
      leftClick: () => this.leftClick(),
      bellIconClick: () => this.bellIconClick(),
      settingIconClick: () => this.settingIconClick(),
    });
  }

  leftClick() {
    handleNavigation({
      type: 'push',
      page: 'ProfileScreen',
      navigation: this.props.navigation,
    });
  }

  settingIconClick() {
    handleNavigation({
      type: 'push',
      page: 'SettingsScreen',
      navigation: this.props.navigation,
    });
  }

  bellIconClick() {
    handleNavigation({
      type: 'push',
      page: 'NotificationsScreen',
      navigation: this.props.navigation,
    });
  }

  goToStudentList(item) {
    handleNavigation({
      type: 'push',
      page: 'StudentListScreen',
      navigation: this.props.navigation,
      passProps: {
        service_id: item.id,
        service_name: item.name,
      },
    });
  }

  getRecentStudentListApiCall = () => {
    // Helper.globalLoader.showLoader();
    let data = {};
    ApiCall.postMethodWithHeader(
      Constants.recent_student_list,
      data,
      Constants.APIGet,
    ).then((response) => {
      // Helper.globalLoader.hideLoader();
      if (response.status == Constants.TRUE) {
        this.setState({arrActivityStudentList: response.data.data});
      } else {
        Helper.showToast(response.message);
      }
    });
  };

  getActivitiesServicesApiCall = () => {
    Helper.globalLoader.showLoader();
    let data = {};
    ApiCall.postMethodWithHeader(
      Constants.get_activities,
      data,
      Constants.APIPost,
    ).then((response) => {
      Helper.globalLoader.hideLoader();
      if (response.status == Constants.TRUE) {
        this.setState({ServiceActivities: response.data});
      } else {
        Helper.showToast(response.message);
      }
    });
  };

  activityItem = ({item}) => {
    return (
      <View style={{marginTop: 5}}>
        <Image
          resizeMode={'contain'}
          source={{uri: item.activity.icon}}
          style={{
            height: 16,
            width: 16,
            marginRight: 4,
          }}
        />
      </View>
    );
  };

  goToStudentProfile(item) {
    handleNavigation({
      type: 'push',
      page: 'StudentProfileScreen',
      navigation: this.props.navigation,
      passProps: {
        student_id: item.id,
      },
    });
  }

  _renderRecentActivity = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => this.goToStudentProfile(item)}
        style={{backgroundColor: Colors.white, marginTop: 5}}>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 15,
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <View style={{flex: 2.2}}>
            <View style={{height: 60, width: 60, borderRadius: 10}}>
              <ImageLoadView
                source={
                  item.profile_picture
                    ? {uri: item.profile_picture}
                    : images.default
                }
                // source={item.image}
                resizeMode={'cover'}
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 10,
                }}
              />
            </View>
          </View>

          <View style={{flex: 5.3}}>
            <Text
              style={{
                color: Colors.blackTwo,
                fontSize: fonts.fontSize14,
                fontFamily: fonts.RoBoToBold_1,
                marginBottom: 10,
              }}>
              {item.student_name}
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              {item.activities && item.activities.length > 0 ? (
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={item.activities}
                  numColumns={6}
                  renderItem={this.activityItem}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                />
              ) : null}

              {/* <Image source={item.imageActivity}
                                style={{
                                    height: 12.2, width: 13, marginRight: 6
                                }} /> */}

              <Text
                style={{
                  marginLeft: 5,
                  color: Colors.warmGrey,
                  fontSize: fonts.fontSize12,
                  fontFamily: fonts.RoBoToRegular_1,
                }}>
                {item.activity}
              </Text>
            </View>
          </View>
          <View style={{flex: 2.5}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-end',
                marginBottom: 12,
              }}>
              <AirbnbRating
                isDisabled={true}
                showRating={false}
                count={5}
                defaultRating={item.overall_rating}
                size={10}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Image
                source={images.distance_ic}
                resizeMode={'contain'}
                style={{
                  height: 12.6,
                  width: 12,
                  marginRight: 5,
                }}
              />
              <Text
                style={{
                  color: Colors.warmGrey,
                  fontSize: fonts.fontSize12,
                  fontFamily: fonts.RoBoToRegular_1,
                }}>
                {item.distance} km
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _serviceItems = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => this.goToStudentList(item)}
        style={styles.activities_touch}>
        <View
          style={{
            alignItems: 'center',
            marginHorizontal: 5,
          }}>
          <ImageBackground
            source={{uri: item.image}}
            resizeMode={'contain'}
            style={styles.background_img}>
            <Text style={[styles.service_name, {textTransform: 'capitalize'}]}>
              {item.name}
            </Text>
          </ImageBackground>
        </View>
      </TouchableOpacity>
    );
  };

  _renderSearchActivity = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => this.goToStudentList(item)}
        style={[{padding: 10}]}>
        <Text>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  searchActivity(val) {
    Keyboard.dismiss();
    this.setState({search_value: val}, () => {
      var results = [];
      if (val) {
        for (var i = 0; i < this.state.ServiceActivities.length; i++) {
          if (
            this.state.ServiceActivities[i].name
              .toLowerCase()
              .includes(val.toLowerCase())
          ) {
            results.push(this.state.ServiceActivities[i]);
          }
        }
      }
      this.setState({arrSearchingActivities: results});
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={'handled'}
          contentContainerStyle={{paddingBottom: 22}}>
          <View style={{backgroundColor: Colors.white}}>
            <View style={styles.search_box_blue}>
              <View style={{marginHorizontal: 16}}>
                <View style={styles.search_box_view}>
                  <TouchableOpacity
                    onPress={() => this.searchActivity()}
                    style={styles.search_touch}>
                    <Image
                      source={images.search_ic}
                      resizeMode={'contain'}
                      style={styles.search_img}
                    />
                  </TouchableOpacity>

                  <TextInput
                    style={styles.input_text}
                    placeholder={LanguagesIndex.translate(
                      'WhichServiceWouldYouLikeToHave',
                    )}
                    keyboardType={'default'}
                    returnKeyType="done"
                    placeholderTextColor={Colors.white}
                    underlineColorAndroid="transparent"
                    onChangeText={(val) => this.searchActivity(val)}
                    value={this.state.search_value}
                    // onSubmitEditing={() => { }}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                paddingHorizontal: 20,
                backgroundColor: '#fff',
                borderRadius: 5,
                width: '100%',
              }}>
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={this.state.arrSearchingActivities}
                keyboardShouldPersistTaps={'handled'}
                renderItem={this._renderSearchActivity}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>

            {this.state.arrActivityStudentList &&
            this.state.arrActivityStudentList.length > 0 ? (
              <View style={styles.recent_parent_view}>
                <View style={styles.recent_view}>
                  <Text style={styles.recent_txt}>
                    {LanguagesIndex.translate('RECENT_S')}
                  </Text>
                </View>
              </View>
            ) : null}
          </View>

          <FlatList
            style={{marginTop: -10}}
            showsVerticalScrollIndicator={false}
            data={this.state.arrActivityStudentList}
            renderItem={this._renderRecentActivity}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
          />

          <View style={styles.service_parent_view}>
            <View style={styles.service_view}>
              <Text style={styles.service_txt}>
                {LanguagesIndex.translate('services')}
              </Text>
            </View>
          </View>
          <View style={{paddingHorizontal: 5}}>
            <FlatList
              style={{marginTop: -10, backgroundColor: Colors.white}}
              showsHorizontalScrollIndicator={false}
              data={this.state.ServiceActivities}
              horizontal
              renderItem={this._serviceItems}
              extraData={this.state}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
