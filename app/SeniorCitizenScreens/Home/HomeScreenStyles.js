import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default HomeSeniorStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteFive
    },
    search_box_blue: {
        backgroundColor: Colors.cerulean,
        height:65,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25
    },
    search_box_view: {
        backgroundColor: '#3a9de9',
        width: '100%',
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 25,
    },
    search_touch: {
        marginLeft: 20
    },
    search_img: {
        height: 18,
        width: 18
    },
    input_text: {
        height: 50,
        width: 280,
        paddingLeft: 10,
        color: Colors.white,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    recent_parent_view:{
        backgroundColor: Colors.white, 
        marginTop: 10
    },
    recent_view:{
        marginHorizontal: 16,
        paddingVertical: 5,
    },
    recent_txt:{
        fontWeight: 'bold', 
        color: Colors.blackTwo, 
        fontFamily: fonts.RoBoToMedium_1, 
        fontSize: fonts.fontSize14,
    },
    service_parent_view:{
        backgroundColor: Colors.white, 
        marginTop: 10
    },
    service_view:{
        marginHorizontal: 16, 
        marginVertical: 10
    },
    service_txt:{
        fontWeight: 'bold', 
        color: Colors.black, 
        fontSize: fonts.fontSize14, 
        fontFamily: fonts.RoBoToMedium_1
    },


  
   activities_touch:{
    // alignItems: 'center', 
    // marginHorizontal: 5, 
    paddingVertical: 10 ,
   },
   background_img:{
    height: 311, 
    width: 141, 
    alignItems: 'center', 
    justifyContent: 'center',
    paddingHorizontal:5,
   },
   service_name:{
    color: Colors.white, 
    fontSize: fonts.fontSize14, 
    fontFamily: fonts.RoBoToBold_1, 
    fontWeight: 'bold'
   },



   
   
    
   
   


});