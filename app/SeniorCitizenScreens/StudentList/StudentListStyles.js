import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';

export default StudentListStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    list_main_view: {
        flexDirection: 'row',
        marginHorizontal: 15,
        // alignItems: 'center'
    },
    img_list_css: {
        height: '100%',
        width:'100%',
        borderRadius: 10
    },
    name_txt: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToBold_1,
        //marginBottom: 10
    },
    list_icon_view: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    text_of_css: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
        marginBottom: 10
    },
    sam_icon_of_css: {
        height: 22,
        width: 22,
        marginRight: 6
    },
    star_icon_of_css: {
        height: 9.3,
        width: 10,
        marginLeft: 3
    },
    dic_icon_of_css: {
        height: 12.6, width: 12,
        marginRight: 5
    },
    km_css: {
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToBold_1
    },
    btn_Pop_up_css: {
        height: 16,
        width: 16,
        marginRight: 6
    },
    btn_view_css: {
        flex: 0.5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8
    },
    main_vie_css_btt: {
        flexDirection: 'row',
        paddingVertical:10,
        justifyContent: 'center',
        backgroundColor: Colors.white,
    },
    model_view_add: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    model_bg_view_Oof_css: {
        backgroundColor: Colors.white,
    },
    sort_text_css: {
        color: Colors.cerulean,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
    },
    sam_text_sort_css: {
        color: Colors.warmGrey,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
        marginTop: 15
    },
    heading_view_filter: {
        backgroundColor: Colors.whiteFive,
        paddingVertical: 10,
        paddingHorizontal: 18,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    select_lang_pre_view: {
        marginHorizontal: 18,
        marginVertical: 10
    },
    lang_radio_touch_text: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 25
    },
    radio_img: {
        height: 16,
        width: 14
    },
    lang_select_text: {
        marginLeft: 10,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1,
        color: Colors.warmGreyThree
    },
    line_bottom_css: {
        borderBottomWidth: 7,
        borderBottomColor: Colors.greyBorder,
        marginVertical: 10
    },
    search_box_blue: {
        backgroundColor: Colors.cerulean,
        height: 65,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25
    },
    search_box_view: {
        backgroundColor: '#3a9de9',
        width: '100%',
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 25,
    },
    search_touch: {
        marginLeft: 20
    },
    search_img: {
        height: 18,
        width: 18
    },
    input_text: {
        height: 50,
        width: 280,
        paddingLeft: 10,
        color: Colors.white,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToBold_1
    },
    filter_btw_view: {
        borderLeftWidth: 1,
        borderLeftColor: Colors.greyBorder
    },
    apply_btn_touch: {
        backgroundColor: Colors.cerulean,
        borderRadius: 10,
        width: "48%",
        paddingVertical: 12
    },
    apply_btn_txt: {
        color: Colors.white,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToBold_1,
        textAlign: 'center'
    },
    cancel_btn_touch: {
        backgroundColor: Colors.white,
        borderWidth: 1,
        borderColor: Colors.cerulean,
        borderRadius: 10,
        width: "48%",
        paddingVertical: 12
    },
    cancel_btn_txt: {
        color: Colors.cerulean,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToBold_1,
        textAlign: 'center'
    },
    calender_view_date:{
        marginHorizontal: 18, 
        marginBottom: 10
    },
    check_img_lang:{
        width: 15, 
        height: 15, 
        borderRadius: 5
    },
    lang_view:{
        marginHorizontal: 18, 
        flexDirection: 'row'
    },




});