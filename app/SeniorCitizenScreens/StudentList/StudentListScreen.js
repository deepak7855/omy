import React, { Component } from 'react';
import { View, Text, Image, FlatList, TouchableOpacity, UIManager, LayoutAnimation, Keyboard, ScrollView, TextInput, Modal, Platform, RefreshControl } from 'react-native';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import styles from './StudentListStyles';
import fonts from '../../Assets/fonts';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants } from '../../Api';
import ImageLoadView from '../../Lib/ImageLoadView';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ListLoader from '../../Comman/ListLoader';
import NoData from '../../Comman/NoData';
import SliderBarView from '../../Comman/SliderBarView';
import moment from 'moment';
import { AirbnbRating } from 'react-native-ratings';


if (Platform.OS === "android" && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

export default class StudentListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            service_name: this.props.route.params.service_name,
            service_id: this.props.route.params.service_id,
            priceRangeValue: [0, 100],
            distanceRangeValue: [0, 10],

            isDatePickerVisible: false,
            sortModal: false,
            isFiltersModal: false,
            isGermanChosen: false,
            isFrenchChosen: false,
            isEnglishChosen: false,
            search_value: '',
            ActivityList: [],
            arrActivityStudentList: [],
            arrSearchingActivities: [],
            next_page_url: '',
            currentPage: 1,
            refreshing: false,
            isLoading: true,
            arrActivityList: [],
            available_date: '',
            app_languagepreference: [],
            language: [],
            filter_rating: [],
            sortBy: 'distance',
            filterValues: '',
            KeyboardHeight: 0,
            ServiceActivities: []

        };
        AppHeader({
            ...this.props.navigation, leftTitle: this.state.service_name,
            bellIcon: true, settingsIcon: true,
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })
    }

    priceRangeValuesChangeFinish = (values) => {
        this.setState({ priceRangeValue: values });
    }

    distanceRangeValuesChangeFinish = (values) => {
        this.setState({ distanceRangeValue: values });
    }


    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        this.getStudentListApiCall()
    }

    _keyboardDidShow = (e) => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ KeyboardHeight: e.endCoordinates.height });
    }
    _keyboardDidHide = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ KeyboardHeight: 0 })
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    onScroll = () => {
        if (this.state.next_page_url && !this.state.isLoading) {
            this.setState({ currentPage: this.state.currentPage + 1, isLoading: true }, () => {
                this.getStudentListApiCall();
            });
        }

    }
    _onRefresh = () => {
        if (this.flatStudentListRef) {
            this.flatStudentListRef.scrollToOffset({ animated: true, offset: 0 });
        }
        this.setState({ refreshing: true, currentPage: 1 }, () => {
            this.getStudentListApiCall();
        });
    }


    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToStudentProfile(item) {
        handleNavigation({
            type: 'push', page: 'StudentProfileScreen', navigation: this.props.navigation,
            passProps: {
                student_id: item.id,
                service_id: this.state.service_id
            }
        })
    }
    clearFilters() {
        let data = {
            language: [],
            min_price: 0,
            max_price: 100,
            min_distance: 0,
            max_distance: 10,
            available_date: '',
            rating: ''
        }

        this.setState({
            isFiltersModal: false, language: [], app_languagepreference: [],
            min_price: 0, max_price: 100,
            min_distance: 0, max_distance: 10,
            available_date: '', filter_rating: [],
            sortBy: 'distance', distanceRangeValue: [0, 10],
            priceRangeValue: [0, 100], filterValues: data
        }, () => {
            this._onRefresh()
        })
    }

    filterData() {
        let data = {
            language: this.state.language,
            min_price: this.state.priceRangeValue[0],
            max_price: this.state.priceRangeValue[1],
            min_distance: this.state.distanceRangeValue[0],
            max_distance: this.state.distanceRangeValue[1],
            avilable_date: this.state.available_date ? moment(this.state.available_date).format("YYYY-MM-DD") : '',
            rating: this.state.filter_rating.join(',')
        }
        this.setState({ filterValues: data }, () => {
            this._onRefresh()
        })
    }

    getStudentListApiCall = () => {

        this.sortModal(false)
        this.filtersModal(false)
        let data = {
            page: this.state.currentPage,
            sort_by: this.state.sortBy,
            activity_id: this.state.service_id,
            // filters: this.state.filterValues, 
            language: this.state.language,
            min_price: this.state.priceRangeValue[0] ? this.state.priceRangeValue[0] : '0',
            max_price: this.state.priceRangeValue[1],
            min_distance: this.state.distanceRangeValue[0] ? this.state.distanceRangeValue[0] : '0',
            max_distance: this.state.distanceRangeValue[1],
            avilable_date: this.state.available_date ? moment(this.state.available_date).format("YYYY-MM-DD") : '',
        }
        if (this.state.filter_rating.length > 0) {
            data.rating = this.state.filter_rating.join(',');
        }
       // console.log('data====data>>>>>',data)
        ApiCall.postMethodWithHeader(Constants.student_list, JSON.stringify(data), Constants.APIPost).then((response) => {
            if (response.status == Constants.TRUE) {
                if (response.data && response.data.current_page) {
                    this.setState({
                        arrActivityList: response.data.data[0].activities[0].activity,
                        arrActivityStudentList: this.state.currentPage == 1 ? response.data.data : [...this.state.arrActivityStudentList, ...response.data.data],
                        next_page_url: response.data.next_page_url,
                        refreshing: false,
                        isLoading: false,
                    })
                } else {
                    this.setState({
                        arrActivityStudentList: [],
                        refreshing: false,
                        isLoading: false,
                    })
                }
                this.getActivitiesServicesApiCall();
            } else {
                this.setState({ refreshing: false, isLoading: false })
                this.getActivitiesServicesApiCall();
            }
        })
    }


    activityItem = ({ item }) => {
        return (
            <View style={{ marginTop: 5, }}>
                <Image
                    resizeMode={'contain'} source={{ uri: item.activity.icon }}
                    style={{
                        height: 16,
                        width: 16,
                        marginRight: 4
                    }} />
            </View>
        )
    }


    _userActivityListItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => this.goToStudentProfile(item)}
                style={styles.list_main_view}>
                <View style={{ flex: 2.2, }}>
                    <View style={{ height: 60, width: 60, borderRadius: 10, }}>
                        <ImageLoadView
                            resizeMode={'cover'}
                            source={item.profile_picture ? { uri: item.profile_picture } : images.default}
                            style={styles.img_list_css} />
                    </View>

                </View>

                <View style={{ flex: 5, }}>
                    <Text style={styles.name_txt}>{item.student_name}</Text>
                    {item.activities && item.activities.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={item.activities}
                            numColumns={6}
                            renderItem={this.activityItem}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        : null}
                </View>
                <View style={{ flex: 2.8, }}>
                    <View style={{
                        alignItems: 'flex-end',
                        // flexDirection: 'row', 
                        // justifyContent: 'flex-end', 
                        // marginBottom: 12 
                    }}>

                        <AirbnbRating
                            isDisabled={true}
                            showRating={false}
                            count={5}
                            defaultRating={item.overall_rating}
                            size={10}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                        <Image
                            resizeMode={'contain'}
                            source={images.distance_ic}
                            style={styles.dic_icon_of_css} />
                        <Text style={{ color: Colors.warmGrey, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToRegular_1 }}>{item.distance} km</Text>
                    </View>
                    <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}> 
                        <Text style={{
                            textAlign: 'right', color: Colors.warmGrey, fontSize: fonts.fontSize12,
                            fontFamily: fonts.RoBoToRegular_1
                        }}>CHF {item.price}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    sortModal(visible) {
        this.setState({ sortModal: visible });
    }

    setSortBy(val) {
        this.setState({ sortBy: val }, () => {
            this._onRefresh();
        })
    }

    _modalSort = () => {
        let { sortBy } = this.state;

        return (
            <View>
                <Modal animationType={"slide"}
                    transparent={true}
                    visible={this.state.sortModal}
                    onRequestClose={() => { this.sortModal(false) }}>

                    <TouchableOpacity onPress={() => this.sortModal(false)} style={styles.model_view_add}>
                        <View style={styles.model_bg_view_Oof_css}>
                            <View style={{ backgroundColor: Colors.whiteFive, paddingVertical: 15, paddingHorizontal: 15 }}>
                                <Text style={styles.sort_text_css}>{LanguagesIndex.translate('SortBy')}</Text>
                            </View>
                            <View style={{ paddingHorizontal: 15, marginBottom: 10 }}>
                                <TouchableOpacity onPress={() => this.setSortBy('rating')}>
                                    <Text style={[styles.sam_text_sort_css, { color: sortBy == 'rating' ? Colors.cerulean : Colors.warmGrey }]}>{LanguagesIndex.translate('Rating')}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setSortBy('distance')}>
                                    <Text style={[styles.sam_text_sort_css, { color: sortBy == 'distance' ? Colors.cerulean : Colors.warmGrey }]}>{LanguagesIndex.translate('Distance')}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setSortBy('PLH')}>
                                    <Text style={[styles.sam_text_sort_css, { color: sortBy == 'PLH' ? Colors.cerulean : Colors.warmGrey }]}>{LanguagesIndex.translate('PriceLowToHigh')}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setSortBy('PHL')}>
                                    <Text style={[styles.sam_text_sort_css, { color: sortBy == 'PHL' ? Colors.cerulean : Colors.warmGrey }]}>{LanguagesIndex.translate('PriceHighToLow')}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setSortBy('acceptance_rate')}>
                                    <Text style={[styles.sam_text_sort_css, { color: sortBy == 'acceptance_rate' ? Colors.cerulean : Colors.warmGrey }]}>{LanguagesIndex.translate('AcceptanceRate')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Modal>
            </View >
        )
    }





    filtersModal(visible) {
        this.setState({ isFiltersModal: visible });
    }


    showDatePicker = () => {
        Keyboard.dismiss()
        this.setState({ isDatePickerVisible: true })
    };

    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false })
    };


    handleConfirm = (date) => {
        this.hideDatePicker();
        let startDate = moment(date).format("D MMMM, YYYY")
        this.setState({ available_date: startDate })
    };


    chooseLanguage(language) {
        let isExist = this.state.app_languagepreference.indexOf(language);
        if (isExist > -1) {
            this.state.app_languagepreference.splice(isExist, 1);
        } else {
            this.state.app_languagepreference.push(language);
        }
        this.state.language = this.state.app_languagepreference.toString();
        this.setState({})
    }


    chooseRating(rating) {
        let isExist = this.state.filter_rating.indexOf(rating);
        if (isExist > -1) {
            this.state.filter_rating.splice(isExist, 1);
        } else {
            this.state.filter_rating.push(rating);
        }

        this.setState({ change: !this.state.change })
    }




    _modalFilters = () => {
        return (
            <Modal animationType={"slide"}
                transparent={true}
                visible={this.state.isFiltersModal}
                onRequestClose={() => { this.filtersModal(false) }}>
                <View style={styles.model_view_add}>
                    <View style={styles.model_bg_view_Oof_css}>
                        <ScrollView>
                            <View style={styles.heading_view_filter}>
                                <Text style={styles.sort_text_css}>{LanguagesIndex.translate('FilterBy')}</Text>

                                <TouchableOpacity
                                    onPress={() => this.clearFilters()}>
                                    <Text style={styles.sort_text_css}>{LanguagesIndex.translate('ClearAll')}</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={styles.select_lang_pre_view}>
                                <Text style={[styles.text_of_css, { marginBottom: 0 }]}>{LanguagesIndex.translate('Language')}</Text>
                            </View>
                            <View style={styles.lang_view}>
                                <TouchableOpacity
                                    onPress={() => this.chooseLanguage('de')}
                                    style={styles.lang_radio_touch_text}>
                                    <Image
                                        resizeMode={'contain'}
                                        source={this.state.app_languagepreference.includes('de') ? images.check : images.unchecked}
                                        style={styles.check_img_lang}
                                    />
                                    <Text style={styles.lang_select_text}>{LanguagesIndex.translate('German')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.chooseLanguage('fr')}
                                    style={styles.lang_radio_touch_text}>
                                    <Image
                                        resizeMode={'contain'}
                                        source={this.state.app_languagepreference.includes('fr') ? images.check : images.unchecked}
                                        style={styles.check_img_lang}
                                    />
                                    <Text style={styles.lang_select_text}>{LanguagesIndex.translate('French')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.chooseLanguage('en')}
                                    style={styles.lang_radio_touch_text}>
                                    <TouchableOpacity>
                                        <Image
                                            resizeMode={'contain'}
                                            source={this.state.app_languagepreference.includes('en') ? images.check : images.unchecked}
                                            style={styles.check_img_lang}
                                        />
                                    </TouchableOpacity>

                                    <Text style={styles.lang_select_text}>{LanguagesIndex.translate('English')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.line_bottom_css} />
                            <SliderBarView
                                min={0}
                                max={100}
                                type={'price'}
                                label={LanguagesIndex.translate('PriceRange')}
                                value={this.state.priceRangeValue}
                                onChangeFinish={(value) => this.priceRangeValuesChangeFinish(value)}
                            />
                            <View style={[styles.line_bottom_css, { marginVertical: 0 }]} />
                            <View style={{ marginHorizontal: 18 }}>
                                <Text style={[styles.text_of_css, { marginBottom: 0 }]}>{LanguagesIndex.translate('Ratings')}</Text>

                                <View style={{ flexDirection: 'row', marginVertical: 5, marginTop: 10 }}>

                                    <TouchableOpacity
                                        onPress={() => this.chooseRating('5')}
                                        style={{ flexDirection: 'row', alignItems: 'center', marginRight: 30 }}>
                                        <Image style={styles.btn_Pop_up_css} source={this.state.filter_rating.includes('5') ? images.check : images.unchecked} />
                                        <Text style={[styles.km_css, { marginLeft: 5, }]}>5</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => this.chooseRating('4')}
                                        style={{ flexDirection: 'row', alignItems: 'center', marginRight: 30 }}>
                                        <Image style={styles.btn_Pop_up_css} source={this.state.filter_rating.includes('4') ? images.check : images.unchecked} />
                                        <Text style={[styles.km_css, { marginLeft: 5, }]}>4</Text>
                                    </TouchableOpacity>


                                    <TouchableOpacity
                                        onPress={() => this.chooseRating('3')}
                                        style={{ flexDirection: 'row', alignItems: 'center', marginRight: 30 }}>
                                        <Image style={styles.btn_Pop_up_css} source={this.state.filter_rating.includes('3') ? images.check : images.unchecked} />
                                        <Text style={[styles.km_css, { marginLeft: 5, }]}>3</Text>
                                    </TouchableOpacity>


                                    <TouchableOpacity
                                        onPress={() => this.chooseRating('2')}
                                        style={{ flexDirection: 'row', alignItems: 'center', marginRight: 30 }}>
                                        <Image style={styles.btn_Pop_up_css} source={this.state.filter_rating.includes('2') ? images.check : images.unchecked} />
                                        <Text style={[styles.km_css, { marginLeft: 5, }]}>2</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => this.chooseRating('1')}
                                        style={{ flexDirection: 'row', alignItems: 'center', marginRight: 30 }}>
                                        <Image style={styles.btn_Pop_up_css} source={this.state.filter_rating.includes('1') ? images.check : images.unchecked} />
                                        <Text style={[styles.km_css, { marginLeft: 5, }]}>1</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.line_bottom_css} />
                            <SliderBarView
                                min={0}
                                max={10}
                                type={'distance'}
                                label={LanguagesIndex.translate('Distance')}
                                value={this.state.distanceRangeValue}
                                onChangeFinish={(value) => this.distanceRangeValuesChangeFinish(value)}
                            />

                            <View style={[styles.line_bottom_css, { marginVertical: 0 }]} />

                            <View style={styles.calender_view_date}>
                                <Text style={[styles.text_of_css, { marginBottom: 0 }]}>{LanguagesIndex.translate('Date')}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                    <Text style={[styles.lang_select_text, { marginLeft: 0 }]}> {this.state.available_date ? this.state.available_date : ""}</Text>

                                    <TouchableOpacity
                                        onPress={() => this.showDatePicker()}
                                    >
                                        <Image
                                            resizeMode={'contain'}
                                            source={images.calendar_ic}
                                            style={styles.radio_img}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', marginHorizontal: 18, justifyContent: 'space-between', marginVertical: 20 }}>
                                <TouchableOpacity
                                    onPress={() => { this.filtersModal(false) }}
                                    style={styles.cancel_btn_touch}>
                                    <Text style={styles.cancel_btn_txt}>{LanguagesIndex.translate('CANCEL')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => { this.filterData() }}
                                    style={styles.apply_btn_touch}>
                                    <Text style={styles.apply_btn_txt}>{LanguagesIndex.translate('APPLY')}</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisible}
                    mode="date"
                    onConfirm={this.handleConfirm}
                    onCancel={this.hideDatePicker}
                    is24Hour={false}
                    minimumDate={new Date()}
                    date={new Date()}
                />
            </Modal>
        )
    }


    _renderRatingItem = ({ item, index }) => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 30 }}>
                <Image style={styles.btn_Pop_up_css} source={item.image} />
                <Text style={[styles.km_css, { marginLeft: 5, }]}>{item.labels}</Text>
            </View>
        )
    }

    _renderSearchActivity = ({ item }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                    this.setState({ service_id: item.id, arrSearchingActivities: [] }, () => {
                        this._onRefresh();
                    })
                }}
                style={[{ padding: 10 }]}>
                <Text>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    getActivitiesServicesApiCall = () => {
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_activities, data, Constants.APIPost).then((response) => {
            if (response.status == Constants.TRUE) {
                this.setState({ ServiceActivities: response.data })
            }
            else {
                Helper.showToast(response.message)
            }
        })
    }

    searchActivity(val) {
        this.setState({ search_value: val }, () => {
            var results = [];
            if (val) {
                for (var i = 0; i < this.state.ServiceActivities.length; i++) {
                    if (this.state.ServiceActivities[i].name.toLowerCase().includes(val.toLowerCase())) {
                        results.push(this.state.ServiceActivities[i]);
                    }
                }
            }
            this.setState({ arrSearchingActivities: results })
        })
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                <View style={styles.search_box_blue}>
                    <View style={{ marginHorizontal: 16 }}>
                        <View style={styles.search_box_view}>
                            <TouchableOpacity
                                onPress={() => this.searchActivity(this.state.search_value)}
                                style={styles.search_touch}>
                                <Image source={images.search_ic} resizeMode={'contain'} style={styles.search_img} />
                            </TouchableOpacity>

                            <TextInput
                                style={styles.input_text}
                                placeholder={LanguagesIndex.translate('WhichActivityWouldYouLikeToHave')}
                                keyboardType={'default'}
                                returnKeyType="done"
                                placeholderTextColor={Colors.white}
                                underlineColorAndroid='transparent'
                                onChangeText={(val) => this.searchActivity(val)}
                                value={this.state.search_value}


                            />
                        </View>
                    </View>


                </View>

                <View style={{ paddingHorizontal: 20, backgroundColor: '#fff', borderRadius: 5, width: '100%' }}>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        data={this.state.arrSearchingActivities}
                        keyboardShouldPersistTaps={'handled'}
                        renderItem={this._renderSearchActivity}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>



                <View style={{ flex: 1, marginTop: 20 }}>
                    {!this.state.isLoading && !this.state.refreshing && this.state.arrActivityStudentList.length < 1 ?
                        <NoData message={LanguagesIndex.translate('Nostudentfound')} />
                        : null
                    }
                    <FlatList
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                        ref={(value) => this.flatStudentListRef = value}
                        data={this.state.arrActivityStudentList}
                        renderItem={this._userActivityListItem}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={() => <View style={{ borderBottomWidth: 9, borderBottomColor: Colors.greyBorder, marginVertical: 15 }} />}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => {
                            return (
                                this.state.isLoading ? <ListLoader /> : null
                            )
                        }}
                        onEndReached={this.onScroll}
                        onEndReachedThreshold={0.5}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps={'handled'}
                    />
                </View>
                {this.state.KeyboardHeight == 0 ?
                    <View style={styles.main_vie_css_btt}>
                        <TouchableOpacity onPress={() => { this.sortModal(true) }}
                            style={styles.btn_view_css}>
                            <Image source={images.sort_ic}
                                style={styles.btn_Pop_up_css} />
                            <Text style={[styles.text_of_css, { marginBottom: 0 }]}>{LanguagesIndex.translate('SORT')}</Text>
                        </TouchableOpacity>
                        <View style={styles.filter_btw_view} />
                        <TouchableOpacity onPress={() => { this.filtersModal(true) }}
                            style={styles.btn_view_css}>
                            <Image source={images.filter_ic}
                                style={styles.btn_Pop_up_css} />
                            <Text style={[styles.text_of_css, { marginBottom: 0 }]}>{LanguagesIndex.translate('FILTER')}</Text>
                        </TouchableOpacity>
                    </View>
                    : null}
                {this._modalSort()}
                {this._modalFilters()}
            </View>
        );
    }
}
