
import React from 'react';
import { View, Image, Text, TouchableOpacity, DeviceEventEmitter, FlatList, RefreshControl } from 'react-native';
import styles from '../Wallet/WalletStyles';
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import { images } from '../../Assets/imagesUrl';
import ListLoader from '../../Comman/ListLoader';
import { Constants, ApiCall } from '../../Api';
import moment from 'moment';
import NoData from '../../Comman/NoData';

export default class Wallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form_date: "",
            to_date: "",
            arrWalletBalance: '',
            arrWallet: [],
            next_page_url: '',
            currentPage: 1,
            refreshing: false,
            isLoading: true,

        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('wallet'),
            borderBottomRadius: 25,
            // hideLeftBackIcon: true,
            settingsIcon: true,
            settingIconClick: () => this.settingIconClick()
        })
    }
    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('SelectDate', (data) => {
            this.setState({ form_date: data.form_date, to_date: data.to_date })
        })
        const unsubscribeFocus = this.props.navigation.addListener('focus', () => {
            this.setState({ currentPage: 1 }, () => {
                this.getPaymentHistory();
            });
        });
    }

    componentWillUnmount() {
        this.getDate.remove()
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }


    gotToAddMoney() {
        handleNavigation({ type: 'push', page: 'AddMoney', navigation: this.props.navigation })
    }

    gotToSelectMonth() {
        handleNavigation({ type: 'push', page: 'SelectMonth', navigation: this.props.navigation })
    }

    onScroll = () => {
        if (this.state.next_page_url && !this.state.isLoading) {
            this.setState({ currentPage: this.state.currentPage + 1, isLoading: true }, () => {
                this.getPaymentHistory();
            });
        }

    }
    _onRefresh = () => {
        this.setState({ refreshing: true, currentPage: 1 }, () => {
            this.getPaymentHistory();
        });
    }

    getPaymentHistory = () => {
        let data = {
            from: this.state.form_date ? moment(this.state.form_date).format("YYYY-MM-DD") : "",
            to: this.state.to_date ? moment(this.state.to_date).format("YYYY-MM-DD") : "",
            page: this.state.currentPage,
        }

        ApiCall.postMethodWithHeader(Constants.Transactions, JSON.stringify(data), Constants.APIPost).then((response) => {
            if (response.status == Constants.TRUE || response.status) {
                if (response.data && response.data.data) {
                    this.setState({
                        arrWalletBalance: response,
                        arrWallet: this.state.currentPage == 1 ? response.data.data : [...this.state.arrWallet, ...response.data.data],
                        next_page_url: response.data.next_page_url,
                        refreshing: false,
                        isLoading: false,
                    })
                }
                else {
                    this.setState({
                        arrWallet: [],
                        refreshing: false,
                        isLoading: false,
                    })
                }

            } else {
                this.setState({ refreshing: false, arrWallet: [], isLoading: false })

            }
        })

    }

    getStatusColor(item) {
        if (item.type == 'dr' || item.status == 'FAILED' || item.status == 'PENDING') { return "red" } else { return "#228C22" };
    }

    _renderWalletItem = ({ item }) => {
        return (
            <View style={{}}>
                <View style={styles.transactions_list_view}>
                    <View style={styles.title_date_view}>
                        <Text style={styles.title_txt}>{item.title}</Text>
                        <Text style={styles.date_time_txt}>
                            {moment.utc(item.created_at).local().format('DD/MM/YYYY h:mm a')}
                        </Text>
                    </View>
                    <View style={{ flex: 3 }}>
                        <View style={styles.amount_view}>
                            {item.status != 'FAILED' ?
                                <Text style={[styles.amount_txt, { color: this.getStatusColor(item), }]}>{item.type == "dr" ? '-' : '+'}</Text>
                                : null} 
                            <Text style={[styles.amount_txt, { color: this.getStatusColor(item), }]}>CHF {item.amount}</Text>
                        </View>
                        <Text style={[styles.amount_txt, { alignSelf: 'flex-end', color: this.getStatusColor(item), }]}>{item.status == 'FAILED' || item.status == 'PENDING' ? item.status : null}</Text>
                    </View>
                </View>
            </View >
        )
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                <View style={styles.earning_view}>
                    <View style={styles.earning_text_view}>
                        <Text style={styles.total_earning_text}>{LanguagesIndex.translate('WalletAmount')}</Text>
                    </View>

                    <View style={{
                        alignItems: 'center',
                        marginTop: 5, flexDirection: 'row', justifyContent: 'center'
                    }}> 
                        <Text style={styles.earning_amount_text}>CHF {this.state.arrWalletBalance.wallet_balance ? this.state.arrWalletBalance.wallet_balance : 0}</Text>
                    </View> 
                </View>

                <View style={styles.add_money_view}>
                    <TouchableOpacity
                        onPress={() => this.gotToAddMoney()} style={styles.add_money_touch}>
                        <Text style={styles.add_money_txt}>{LanguagesIndex.translate('ADDMONEY')}</Text>
                    </TouchableOpacity>
                </View>


                {!this.state.isLoading && !this.state.refreshing && this.state.arrWallet.length < 1 ?
                    <NoData
                        message={LanguagesIndex.translate('Notransactionavailableinyourwallet')} />
                    : null
                }
                <FlatList
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    ListFooterComponent={() => {
                        return (
                            this.state.isLoading ? <ListLoader /> : null
                        )
                    }}
                    onEndReached={this.onScroll}
                    onEndReachedThreshold={0.5}
                    showsVerticalScrollIndicator={false}
                    data={this.state.arrWallet}
                    renderItem={this._renderWalletItem}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}

                    style={{ flex: 1 }}
                />
 
                <TouchableOpacity onPress={() => { this.gotToSelectMonth() }} style={styles.filter_touch}>
                    <Image source={images.Filter}
                        resizeMode={'contain'}
                        style={styles.filter_img} />
                </TouchableOpacity>
            </View>
        )
    }

};





