import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default WalletStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    earning_view: {
        borderRadius: 10,
        marginHorizontal: 16,
        backgroundColor: Colors.cerulean,
        paddingVertical: 10,
        marginTop: 20
    },
    earning_text_view: {
        alignItems: 'center'
    },
    total_earning_text: {
        color: Colors.white,
        fontSize: fonts.fontSize14,
        lineHeight: 17,
        fontFamily: fonts.RoBoToMedium_1
    },
    earning_amount_text_view: {
        alignItems: 'center',
        marginTop: 5
    },
    earning_amount_text: {
        color: Colors.white,
        fontSize: fonts.fontSize22,
        // lineHeight: 27, 
        fontFamily: fonts.RoBoToBold_1
    },
    add_money_view: {
        marginVertical: 5,
        marginHorizontal: 16,
        alignItems: 'center',
        marginTop: 10
    },
    add_money_touch: {
        paddingVertical: 5,
        borderWidth: 1,
        borderColor: Colors.cerulean,
        paddingHorizontal: 15,
        borderRadius: 5
    },
    add_money_txt: {
        color: Colors.cerulean,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToBold_1
    },
    filter_touch: {
        position: 'absolute',
        right: 10,
        bottom: 30,
        backgroundColor: Colors.white
    },
    filter_img: {
        height: 50,
        width: 50
    },
    transactions_list_view: {
        flexDirection: 'row',
        borderBottomWidth: .5,
        flex: 1,
        padding: 20,
        borderBottomColor: Colors.warmGreyTwo
    },
    title_date_view: {
        flex: 7,
        justifyContent: 'center'
    },
    title_txt: {
        color: Colors.blackTwo,
        fontSize: fonts.fontSize14,
        fontWeight: 'bold',
        marginBottom: 5,
        fontFamily: fonts.RoBoToBold_1
    },
    date_time_txt: {
        color: Colors.greyish,
        fontSize: fonts.fontSize12,
    },
    amount_view: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    amount_txt: {
        fontSize: fonts.fontSize13
    },



});