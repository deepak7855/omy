import React from 'react';
import { Text, View, ScrollView, Image, FlatList, TouchableOpacity, SafeAreaView, Keyboard, } from 'react-native';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import { GButton } from '../../Comman/GButton';
import styles from './StudentProfileStyles';
import fonts from '../../Assets/fonts';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants } from '../../Api';
import ImageLoadView from '../../Lib/ImageLoadView';
import { AirbnbRating } from 'react-native-ratings';
import ListLoader from '../../Comman/ListLoader';
import RatingViewItem from '../../StudentScreens/RatingsReviewsScreen/RatingViewItem';
export default class StudentProfileScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            service_id: this.props.route && this.props.route.params && this.props.route.params.service_id,
            studentProfileData: '',
            arrStudentActivity: [],
            arrReviews: [],
            reviewsCount: '',
            currentPage: 1,
            next_page_url: '',
            isLoading: true,
        }

        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('StudentProfile'),
            bellIcon: true, settingsIcon: true,
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })
    }


    componentDidMount() {
        this.getStudentProfileDetailsCall()
        this.getReviews()
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToSchedule() {
        Keyboard.dismiss();
        handleNavigation({
            type: 'push', page: 'ScheduleBookingScreen', navigation: this.props.navigation, passProps: {
                studentId: this.state.studentProfileData.id,
                arrStudentActivity: this.state.arrStudentActivity,
                selected_service_id: this.state.service_id,
                user_slots: this.state.studentProfileData.userslots[0],
                unavailability: this.state.studentProfileData.unavailability
            }
        })
    }

    getReviews = () => {
        Helper.globalLoader.showLoader();
        let data = new FormData();
        data.append('student_id', this.props.route.params.student_id);
        data.append('page', this.state.currentPage);
        ApiCall.postMethodWithHeader(Constants.get_reviews, data, Constants.APIImageUploadAndroid).then((response) => {
            if (response.status == Constants.TRUE) {
                this.setState({
                    isLoading: false,
                    next_page_url: response.data.next_page_url,
                    reviewsCount: response.data.total,
                    arrReviews: this.state.currentPage == 1 ? response.data.data : [this.state.arrReviews, ...response.data.data]
                })
            }
            else {
                Helper.showToast(response.message)
                this.setState({
                    isLoading: false,
                })
            }
        })
    }

    getStudentProfileDetailsCall = () => {
        Helper.globalLoader.showLoader();
        let data = new FormData();
        data.append('student_id', this.props.route.params.student_id);
        ApiCall.postMethodWithHeader(Constants.student_detail, data, Constants.APIImageUploadAndroid).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                this.setState({
                    studentProfileData: response.data,
                    arrStudentActivity: response.data.user_activity,
                })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }


    _renderListItem = ({ item, index }) => {
        return (
            <View style={styles.activities_price_view}>
                <View style={styles.activities_img_view}>
                    <Image
                        source={{ uri: item.activity.icon }}
                        resizeMode={'contain'}
                        // source={item.icon}
                        style={styles.sam_img_css} />
                </View>

                <View style={styles.activities_name_view}>
                    <Text style={[styles.label_txt, { marginTop: 0, textTransform: 'capitalize' }]}>{item.myUserActivity}</Text>
                </View>

                <View style={[styles.activities_price_txt_view]}>
                    <Text style={[styles.rate_txt, { marginTop: 0 }]}>CHF {item.price}</Text>
                </View>
            </View>
        )
    }

    gotoMediaDetail(item) {
        this.props.navigation.navigate('MediaDetailPage', { item })
    }

    onScroll = () => {
        if (!this.state.isLoading && this.state.next_page_url)
            this.setState({ isLoading: true, currentPage: this.state.currentPage + 1 }, () => {
                this.getReviews();
            })
    }

    _renderUploadItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.gotoMediaDetail(item)} style={{ marginRight: 10, marginTop: 12, }}>
                <View style={{ height: 82, width: 82, }}>
                    <ImageLoadView
                        source={{ uri: item.media_type == 'video' ? item.media_thumb : item.media_url }}
                        resizeMode={'cover'}
                        style={{ width: '100%', height: '100%' }}
                    />
                </View>
                {item.media_type == 'video' ?
                    <View style={styles.blur_img_view_css}>
                        <Image source={images.play}
                            style={{ height: 25, width: 25 }} />
                    </View>
                    : null}
            </TouchableOpacity>

        )
    }

    _reviewsItem = ({ item, index }) => {
        return (
            <RatingViewItem from={'profile'} item={item} />
        )
    }

    render() {
        let { studentProfileData, } = this.state;
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.bg_css}>
                        <View style={styles.profile_view_img_css}>
                            <ImageLoadView
                                resizeMode={'cover'}
                                source={studentProfileData.profile_picture ? { uri: studentProfileData.profile_picture } : images.default}
                                style={styles.profile_img_css} />
                        </View>
                    </View>
                    <View style={styles.name_star_view}>
                        <Text style={styles.name_txt}>{studentProfileData.name}</Text>
                        <View style={styles.star_view_css}>

                            <AirbnbRating
                                isDisabled={true}
                                showRating={false}
                                count={5}
                                defaultRating={studentProfileData && studentProfileData.rating ? Number(studentProfileData.rating.overall_rating) : 0}
                                size={10}
                            />
                        </View>
                    </View>

                    <View style={styles.line_border_css}>
                        <Text style={styles.about_txt}>{LanguagesIndex.translate('About')} {studentProfileData.name}</Text>
                        <Text style={styles.about_de_txt}>{studentProfileData.about_me == 'null' ? '' : studentProfileData.about_me}</Text>
                    </View>

                    <View style={styles.line_border_css}>
                        <Text style={styles.activities_offered_txt}>{LanguagesIndex.translate('ActivitiesOfferedPrice')}</Text>
                        <FlatList
                            data={this.state.arrStudentActivity}
                            renderItem={this._renderListItem}
                            extraData={this.state}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>

                    <View style={{ padding: 15 }}>
                        <Text style={{
                            color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1
                        }}>{LanguagesIndex.translate('Availability')}</Text>

                    </View>

                    {studentProfileData && studentProfileData.userslots && studentProfileData.userslots[0] && studentProfileData.userslots[0].mon_slot ?
                        <View style={{ width: "100%", marginVertical: 5, }}>
                            <View
                                style={{ flexDirection: 'row', marginHorizontal: 15, }}>
                                <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('MondayToFriday')}</Text>
                            </View>

                            <View style={{ marginLeft: 20, marginTop: 10 }}>
                                <View
                                    style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.samGreyTextCss}>{studentProfileData.userslots[0].mon_slot}</Text>
                                </View>
                            </View>
                        </View>
                        : null}


                    {studentProfileData && studentProfileData.userslots && studentProfileData.userslots[0] && studentProfileData.userslots[0].sat_slot ?
                        <View style={{ width: "100%", marginVertical: 5 }}>
                            <View
                                style={{ flexDirection: 'row', marginHorizontal: 15, }}>
                                <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Saturday')}</Text>
                            </View>
                            <View style={{ marginLeft: 20, marginTop: 10 }}>
                                <View
                                    style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.samGreyTextCss}>{studentProfileData.userslots[0].sat_slot}</Text>
                                </View>
                            </View>
                        </View>

                        : null}
                    {studentProfileData && studentProfileData.userslots && studentProfileData.userslots[0] && studentProfileData.userslots[0].sun_slot ?
                        <View style={{ width: "100%", marginVertical: 5 }}>
                            <View
                                style={{ flexDirection: 'row', marginHorizontal: 15, }}>
                                <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Sunday')}</Text>
                            </View>
                            <View style={{ marginLeft: 20, marginTop: 10 }}>
                                <View
                                    style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.samGreyTextCss}>{studentProfileData.userslots[0].sun_slot}</Text>
                                </View>
                            </View>
                        </View>

                        : null}


                    <View style={{ padding: 15 }}>
                        <Text style={{
                            color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1
                        }}>{LanguagesIndex.translate('LanguagePreference')}</Text>

                        {studentProfileData.german_lang_pref ?
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                <View style={{ flex: 7.7, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image resizeMode={'contain'} source={images.check} style={styles.samImgCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('German')}</Text>
                                </View>
                                <View style={{ flex: 2.3, }}>
                                    <Text style={[styles.samGreyTextCss, { textTransform: 'capitalize' }]}>{studentProfileData.german_lang_pref}</Text>
                                </View>
                            </View>
                            : null
                        }


                        {studentProfileData.french_lang_pref ?
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                <View style={{ flex: 7.7, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image resizeMode={'contain'} source={images.check} style={styles.samImgCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('French')}</Text>
                                </View>
                                <View style={{ flex: 2.3, }}>
                                    <Text style={[styles.samGreyTextCss, { textAlign: 'left', textTransform: 'capitalize', }]}>{studentProfileData.french_lang_pref}</Text>
                                </View>

                            </View>
                            : null
                        }

                        {studentProfileData.english_lang_pref ?
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                <View style={{ flex: 7.7, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image resizeMode={'contain'} source={images.check} style={styles.samImgCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('English')}</Text>
                                </View>
                                <View style={{ flex: 2.3, }}>
                                    <Text style={[styles.samGreyTextCss, { textTransform: 'capitalize' }]}>{studentProfileData.english_lang_pref}</Text>
                                </View>
                            </View>

                            : null
                        }
                    </View>




                    <View style={styles.line_border_css}>
                        <View style={styles.sam_view_text_ic}>
                            <Text style={styles.past_experience_txt}>{LanguagesIndex.translate('PastExperiences')}</Text>
                        </View>

                        <FlatList
                            horizontal
                            data={studentProfileData.user_media}
                            renderItem={this._renderUploadItem}
                            showsHorizontalScrollIndicator={false}
                            extraData={this.state}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={{ padding: 15 }}>
                        <Text style={styles.review_txt}>{LanguagesIndex.translate('Reviews')}({this.state.reviewsCount})</Text>
                        <View style={{ marginTop: 10 }}>
                            <FlatList
                                data={this.state.arrReviews}
                                renderItem={this._reviewsItem}
                                showsHorizontalScrollIndicator={false}
                                extraData={this.state}
                                showsVerticalScrollIndicator={false}
                                ItemSeparatorComponent={() => <View style={{ marginVertical: 12, borderBottomWidth: 1, borderBottomColor: Colors.whiteTwo }} />}
                                onScroll={this.onScroll}
                                onEndReachedThreshold={0.5}
                                keyExtractor={(item, index) => index.toString()}
                                ListFooterComponent={() => {
                                    return (
                                        <>
                                            {this.state.isLoading ? <ListLoader /> : null}
                                        </>
                                    )
                                }}
                            />
                        </View>
                    </View>
                    {Helper.user_data && Helper.user_data.user_type === 'BEOMY' ?
                        <View style={styles.book_btn_view}>
                            <GButton
                                Text={LanguagesIndex.translate('Book')}
                                width={'100%'}
                                height={50}
                                borderRadius={10}

                                onPress={() => { this.goToSchedule() }}

                            />
                        </View>
                        : null}
                </ScrollView>
            </SafeAreaView>
        )
    }
};
