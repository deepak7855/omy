import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default StudentProfileStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    bg_css: {
        backgroundColor: Colors.cerulean,
        height: 60,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        marginBottom: 30
    },
    upIconCss: {
        height: 6,
        width: 10,
        tintColor: Colors.warmGreyTwo,
    },
    profile_view_img_css: {
        alignSelf: 'center',
        position: 'absolute',
        borderWidth: 1,
        borderRadius: 20,
        borderColor: Colors.white,
        bottom: -40
    },
    profile_img_css: {
        height: 82,
        width: 82,
        borderRadius: 20
    },
    name_star_view: {
        marginTop: 20,
        alignSelf: 'center',
    },
    name_txt: {
        textAlign: 'center',
        fontWeight: '700',
        color: Colors.black,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1
    },
    star_view_css: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 8,
        marginLeft: 4
    },
    star_icon_css: {
        height: 12,
        width: 12,
        marginRight: 7
    },
    line_border_css: {
        borderBottomWidth: 8,
        borderBottomColor: '#f8f8f8',
        padding: 15
    },
    about_txt: {
        fontWeight: '700',
        color: Colors.black,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1
    },
    about_de_txt: {
        marginTop: 10,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    label_txt: {
        marginTop: 10,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    activities_offered_txt: {
        fontWeight: '700',
        color: Colors.black,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1
    },
    past_experience_txt: {
        fontWeight: '700',
        color: Colors.black,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1
    },
    review_txt: {
        fontWeight: '700',
        color: Colors.black,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1
    },
    activities_price_view: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 12,
    },
    activities_img_view: {
        flex: 0.5
    },
    activities_name_view: {
        flex: 6.5
    },
    activities_price_txt_view: {
        flex: 2.5,
        justifyContent:'flex-end',
        alignItems: 'center',
        flexDirection:'row'
    },
    rate_txt: {
        marginTop: 10,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    star_icon_count_view: {
        height: 25,
        marginTop: 2,
        marginRight: 10,
        flexDirection: 'row',
        backgroundColor: Colors.cerulean,
        alignItems: 'center',
        paddingHorizontal: 7,
        paddingVertical: 2
    },
    count_start_txt: {
        color: Colors.white,
        marginRight: 6
    },
    star_img: {
        height: 10,
        width: 10
    },
    sam_img_css: {
        height: 12.2,
        width: 13,
        marginRight: 10
    },
    sam_view_text_ic: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    sub_des_txt: {
        marginTop: 5,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize10,
        fontFamily: fonts.RoBoToRegular_1
    },
    upload_img_css: {
        height: 82,
        width: 82,
    },
    blur_img_view_css: {
        position: 'absolute', alignSelf: 'center', height: "100%", width: "100%", alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba( 0, 0, 0, 0.5 )',
    },
    count_plus_text: {
        fontFamily: fonts.RoBoToMedium_1,
        fontSize: fonts.fontSize17,
        color: Colors.white
    },
    book_btn_view: {
        marginVertical: 10,
        marginTop: 30,
        marginHorizontal: 15
    },
    week_txt:{
        color: Colors.black,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1
    },
    date_text:{
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    check_img:{
        height: 20, 
        width: 20, 
        resizeMode: 'contain', 
        marginRight: 10
    }, 
    samGreyTextCss: { color: Colors.warmGrey, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToRegular_1 },
    samImgCss: { height: 20, width: 20, resizeMode: 'contain', marginRight: 10 },

});