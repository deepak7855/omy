import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';

export default SelectMonthStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    main_view:{
        marginHorizontal: 16, 
        marginTop: 20
    },
    apply_btn_view: {
        marginVertical: 5,
        marginHorizontal: 16,
        marginTop: 100
    },
    apply_btn_touch: {
        backgroundColor: Colors.cerulean,
        height: 50,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    apply_txt:{
        color: Colors.white, 
        fontSize: fonts.fontSize14, 
        fontWeight: '800', 
        fontFamily: fonts.RoBoToBold_1
    },
    date_text_view:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        marginVertical: 3,
    },
    from_To_date_txt:{
        color: Colors.black, 
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToBold_1, 
        fontWeight: 'bold'
    },
    from_To_date_text:{
        color: Colors.warmGrey, 
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1,
    },

    radio_img: {
        height: 16,
        width: 14
    },
    lang_select_text: {
        marginLeft: 10,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1,
        color: Colors.warmGreyThree
    },
    text_of_css: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
        marginBottom: 10
    },
    calender_view_date: {
        marginHorizontal: 18,
        marginBottom: 10
    },
});