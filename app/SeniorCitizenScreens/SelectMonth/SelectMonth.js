import React from 'react';
import { View, Image, Text, TouchableOpacity, DeviceEventEmitter, Keyboard } from 'react-native';
import styles from '../SelectMonth/SelectMonthStyles';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import { images } from '../../Assets/imagesUrl';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import Helper from '../../Lib/Helper';


export default class SelectMonth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form_date: '',
            to_date: '',
            isDatePickerVisible: false,
            isDatePickerVisibleEnd: false,

        }
        AppHeader({
            ...this.props.navigation, leftTitle:LanguagesIndex.translate('SelectDate'),
            borderBottomRadius: 25,
            // hideLeftBackIcon: true,
            settingsIcon: true,
            settingIconClick: () => this.settingIconClick()
        })
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }
    
    goToWallet() {
        if (this.state.form_date == "") {
            Helper.showToast(LanguagesIndex.translate('Pleaseselectstartdate'))
            return
        }
        else if (this.state.to_date == "") {
            Helper.showToast(LanguagesIndex.translate('Pleaseselectenddate'))
            return
        }

        let data = {
            form_date: this.state.form_date,
            to_date: this.state.to_date,
        }
        DeviceEventEmitter.emit("SelectDate", data)
        handleNavigation({
            type: 'push', page: 'Wallet', navigation: this.props.navigation,
            passProps: {
                form_date: this.state.form_date,
                to_date: this.state.to_date,
            }
        })
    }

    showDatePicker = () => {
        Keyboard.dismiss()
        this.setState({ isDatePickerVisible: true })
    };

    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false })
    };

    handleConfirm = (date) => {
        this.hideDatePicker();
        let startDate = moment(date).format("D MMMM, YYYY")
        this.setState({ form_date: startDate })
    };


    showDatePickerEnd = () => {
        Keyboard.dismiss()
        this.setState({ isDatePickerVisibleEnd: true })
    };

    hideDatePickerEnd = () => {
        this.setState({ isDatePickerVisibleEnd: false })
    };

    handleConfirmEnd = (date) => {
        this.hideDatePickerEnd();
        let endDate = moment(date).format("D MMMM, YYYY")
        this.setState({ to_date: endDate })
    };


    render() {
        return (
            <View style={styles.safe_area_view}>
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.main_view}>
                        <View style={styles.date_text_view}>
                            <View>
                                <Text style={styles.from_To_date_txt}>{LanguagesIndex.translate('Formdate')}</Text>
                                <Text style={styles.from_To_date_text}>{this.state.form_date ? this.state.form_date : ""}</Text>
                            </View>
                            <TouchableOpacity
                                hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
                                onPress={() => this.showDatePicker()}>
                                <Image
                                    resizeMode={'contain'}
                                    source={images.calendar_ic}
                                    style={styles.radio_img}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.date_text_view}>
                            <View>
                                <Text style={styles.from_To_date_txt}>{LanguagesIndex.translate('Todate')}</Text>
                                <Text style={styles.from_To_date_text}>{this.state.to_date ? this.state.to_date : ""}</Text>
                            </View>
                            <TouchableOpacity
                                hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
                                onPress={() => this.showDatePickerEnd()}>
                                <Image
                                    resizeMode={'contain'}
                                    source={images.calendar_ic}
                                    style={styles.radio_img}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.apply_btn_view}>
                        <TouchableOpacity
                            onPress={() => this.goToWallet(this.state.form_date, this.state.to_date)}
                            style={styles.apply_btn_touch}>
                            <Text style={styles.apply_txt}>{LanguagesIndex.translate('APPLY')}</Text>
                        </TouchableOpacity>
                    </View>

                </KeyboardScroll>
                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisible}
                    mode="date"
                    onConfirm={this.handleConfirm}
                    onCancel={this.hideDatePicker}
                    // is24Hour={false}
                    // minimumDate={new Date()}
                    date={new Date()}
                />
                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisibleEnd}
                    mode="date"
                    onConfirm={this.handleConfirmEnd}
                    onCancel={this.hideDatePickerEnd}
                    // is24Hour={false}
                    minimumDate={this.state.form_date ? new Date(moment(this.state.form_date).format()) : new Date()}
                    date={new Date()}
                />
            </View>
        )
    }

};





