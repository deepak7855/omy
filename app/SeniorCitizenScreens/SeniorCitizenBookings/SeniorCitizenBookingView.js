import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, DeviceEventEmitter } from 'react-native';
import styles from './SeniorCitizenBookingUpComingScreenStyles';
import { images } from '../../Assets/imagesUrl';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';
import Helper from '../../Lib/Helper';
import { Constants } from '../../Api';
import { changeBookingStatus } from '../../Api/BookingApis';
import { handleNavigation } from '../../navigation/Navigation';
import ImageLoadView from '../../Lib/ImageLoadView';
import LanguagesIndex from '../../Languages';


export default class SeniorCitizenBookingView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }

    }

    UpdateBooking = (type) => {
        Helper.confirm(LanguagesIndex.translate('Areyousureyouwantto') + " " + `${type == 'ACCEPT' ? LanguagesIndex.translate('Accept') : LanguagesIndex.translate('Reject')}` + " " + LanguagesIndex.translate('bookingrequest?'), (status) => {
            if (status) {
                let data = new FormData();
                data.append('booking_id', this.props.order.booking_id);
                data.append('booking_service_id', this.props.order.booking_service_id);
                data.append('status', type);

                changeBookingStatus(data, (res) => {
                    if (res.status == Constants.TRUE) {
                        DeviceEventEmitter.emit('changeBookingStatus', "done")
                        if (this.props.refreshList) {
                            this.props.refreshList()
                        }
                    }
                })
            }
        })
    }



    gotToActivityDetail() {
        handleNavigation({
            type: 'push', page: 'ActivityDetailScreen', navigation: this.props.navigation, passProps: {
                booking_id: this.props.order.booking_services[0].booking_id,
                booking_service_id: this.props.order.booking_services[0].id,
                BookingStatus: this.props.order.booking_services[0].user_booking[0].status

            }
        })
    }


    render() {

        let { order, type } = this.props;

        return (
            <TouchableOpacity
                onPress={() => this.gotToActivityDetail()}

                style={styles.up_coming_view}>
                <View style={styles.up_coming_profile_view}>
                    <View style={styles.profile_flex_view}>
                        <View style={styles.profile_img_view}>
                            <ImageLoadView
                                source={images.default}
                                source={order.booking_services[0].user_booking[0].user.profile_picture ? { uri: order.booking_services[0].user_booking[0].user.profile_picture } : images.default}
                                resizeMode={'cover'} style={[styles.profile_img, { borderRadius: 10 }]} />
                        </View>
                    </View>

                    <View style={styles.user_name_flex}>
                        <Text style={styles.user_name_text}>{order.booking_services[0].user_booking[0].user.name}
                        </Text>
                    </View>

                    <View style={styles.rate_view}>
                        {/* <Text numberOfLines={2} style={styles.rate_text}>${order.booking_services[0].user_booking[0].price}</Text> */}

                        {/* <Text numberOfLines={2} style={styles.rate_text}>${Number(order.price) * Number(order.booking_services.activity_duration).toFixed(2)}</Text> */}
                    </View>
                </View>

                <View style={styles.view_text_img_small}>
                    <View style={styles.dog_img_view}>
                        <Image
                            source={{ uri: order.booking_services[0].activity ? order.booking_services[0].activity.icon : null }}
                            resizeMode={'contain'} style={styles.dog_img} />
                    </View>

                    <View style={{ flex: 7.7 }}>
                        <Text style={[styles.title_name, { textTransform: 'capitalize' }]}>
                            {order.booking_services[0].activity ? order.booking_services[0].activity.name : null}
                        </Text>
                    </View>

                    <View style={{ flex: 1.6, flexDirection: 'row', alignItems: 'center' }}>

                        {/* <Text>{order.booking_services[0].user_booking[0].price}</Text> */}
                        <View>
                            <Image source={images.timer_ic} resizeMode={'contain'} style={{ height: 12, width: 10.5 }} />
                        </View>
                        <Text style={{
                            color: Colors.warmGrey, fontSize: fonts.fontSize12,
                            fontFamily: fonts.RoBoToRegular_1, lineHeight: 14, marginLeft: 5
                        }}>{order.booking_services[0].activity ? order.booking_services[0].activity_duration : null} hrs</Text>
                    </View>
                </View>

                <View style={styles.view_text_img_small}>
                    <View style={{ flex: .4 }}>
                        <Image source={images.calendar_date_ic} resizeMode={'contain'} style={styles.calender_img} />
                    </View>

                    <View style={{ flex: 7.6 }}>
                        <Text style={styles.date_text}>
                            {/* {Helper.formatDate(order.booking_date, "DD/MM/YYYY")} | {order.booking_time} */}
                            {Helper.formatDateUtcToLocal(order.booking_date,order.booking_time,'')}

                        </Text>
                    </View>

                    {type == 'pending' ?
                        <View style={styles.flex_card_btn_view}>
                            <TouchableOpacity onPress={() => this.UpdateBooking('REJECT')}>
                                <Image source={images.wrong_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.UpdateBooking('ACCEPT')}>
                                <Image source={images.right_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>
                        </View>

                        : type != 'home' ? <View style={{ flex: 3.4, alignItems: 'flex-end' }}>
                            
                            <Text style={{ color: Helper.getBookingStatus(order.booking_services[0].user_booking[0].status).color, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToRegular_1, textTransform: 'capitalize' }}>{Helper.getBookingStatus(order.booking_services[0].user_booking[0].status).value} {order.booking_services[0].user_booking[0].status == 'CANCEL' && order.booking_services[0].user_booking[0].cancelled_by_name ? `${LanguagesIndex.translate('by')} ${order.booking_services[0].user_booking[0].cancelled_by_name}` : null}</Text>
                           
                            
                        </View> : null}

                </View>

                <View style={styles.view_text_img_small}>
                    <View style={styles.location_img_view}>
                        <Image source={images.location_pin_ic} resizeMode={'contain'} style={styles.location_img} />
                    </View>

                    <View style={styles.location_text_view}>
                        <Text numberOfLines={2} style={styles.location_text}>
                            {order.booking_services[0].user_booking[0].user.street_no}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
