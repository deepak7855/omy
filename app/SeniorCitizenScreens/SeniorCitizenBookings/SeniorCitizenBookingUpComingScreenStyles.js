import {StyleSheet} from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';

export default SeniorCitizenBookingUpComingScreenStyles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
    backgroundColor: Colors.whiteThree,
  },
  up_his_btn_view: {
    marginHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
    marginTop: 15,
  },
  up_coming_touch: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 160,
    height: 40,
    borderWidth: 1,
    borderRadius: 10,
  },
  up_coming_text: {
    lineHeight: 17,
    fontFamily: fonts.RoBoToMedium_1,
    fontSize: fonts.fontSize14,
  },
  history_touch: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 160,
    height: 40,
    borderWidth: 1,
    borderRadius: 10,
  },
  history_text: {
    fontSize: fonts.fontSize14,
    lineHeight: 17,
    fontFamily: fonts.RoBoToMedium_1,
  },
  view_pager: {
    flex: 1,
  },
  up_coming_view: {
    marginTop: 10,
    backgroundColor: Colors.white,
    paddingVertical: 5,
  },
  up_coming_profile_view: {
    marginVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16,
    justifyContent: 'space-between',
  },
  profile_flex_view: {
    flex: 1.2,
  },
  profile_img_view: {
    width: 35,
    height: 35,
    borderRadius: 10,
  },
  profile_img: {
    width: '100%',
    height: '100%',
  },
  user_name_flex: {
    flex: 7,
  },
  user_name_text: {
    color: Colors.black,
    fontSize: fonts.fontSize15,
    fontFamily: fonts.RoBoToBold_1,
    marginLeft: 5,
  },
  rate_view: {
    flex: 2,
    alignItems: 'flex-end',
  },
  rate_text: {
    color: Colors.black,
    fontSize: fonts.fontSize15,
    fontFamily: fonts.RoBoToBold_1,
  },
  view_text_img_small: {
    marginHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
  },
  dog_img_view: {
    flex: 0.7,
  },
  dog_img: {
    width: 13,
    height: 12.2,
  },
  title_view: {
    flex: 9.2,
  },
  title_name: {
    fontSize: fonts.fontSize12,
    lineHeight: 14,
    fontFamily: fonts.RoBoToRegular_1,
    color: Colors.warmGreyThree,
  },
  calender_view: {
    flex: 0.4,
  },
  calender_img: {
    width: 10.7,
    height: 11.8,
  },
  date_view: {
    flex: 9.6,
  },
  date_text: {
    marginLeft: 8.3,
    fontSize: fonts.fontSize12,
    lineHeight: 14,
    fontFamily: fonts.RoBoToRegular_1,
    color: Colors.warmGreyThree,
  },
  location_img_view: {
    flex: 0.4,
  },
  location_img: {
    width: 8.7,
    height: 12.1,
  },
  location_text_view: {
    flex: 9.6,
  },
  location_text: {
    marginLeft: 8.3,
    fontSize: fonts.fontSize12,
    lineHeight: 14,
    fontFamily: fonts.RoBoToRegular_1,
    color: Colors.warmGreyThree,
  },
  history_view: {
    marginTop: 10,
    backgroundColor: Colors.white,
    paddingVertical: 5,
  },
  img_calender_location_list_view: {
    marginHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
  },
  flex_img_view: {
    flex: 0.4,
  },
  flex_text_view: {
    flex: 9.6,
  },
  history_dog_img: {
    width: 13,
    height: 12.2,
  },
  dog_title: {
    marginLeft: 8.3,
    fontSize: fonts.fontSize12,
    lineHeight: 14,
    fontFamily: fonts.RoBoToRegular_1,
    color: Colors.warmGreyThree,
  },
  history_calender_img: {
    width: 10.7,
    height: 11.8,
  },
  history_date_title: {
    marginLeft: 8.3,
    fontSize: fonts.fontSize12,
    lineHeight: 14,
    fontFamily: fonts.RoBoToRegular_1,
    color: Colors.warmGreyThree,
  },
  history_location_img: {
    width: 8.7,
    height: 12.1,
  },
  history_address: {
    marginLeft: 8.3,
    fontSize: fonts.fontSize12,
    lineHeight: 14,
    fontFamily: fonts.RoBoToRegular_1,
    color: Colors.warmGreyThree,
  },

  rej_com_view: {
    marginHorizontal: 17,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  radio_btn_view: {
    flexDirection: 'row',
  },
  rej_com_txt: {
    marginLeft: 10,
    fontSize: fonts.fontSize14,
    fontFamily: fonts.RoBoToMedium_1,
    lineHeight: 17,
    color: Colors.blackTwo,
  },
  radio_img: {
    height: 14,
    width: 14,
  },
  flex_card_btn_view: {
    flex: 2,
    alignSelf: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  booking_btn: {
    height: 30,
    width: 30,
  },
});
