import React from 'react';
import { Text, View, SafeAreaView, StyleSheet, Image, DeviceEventEmitter, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import styles from './MapTrackStudentStyles';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import moment from 'moment';
import { ApiCall, AsyncStorageHelper, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
const origin = { latitude: 37.3318456, longitude: -122.0296002 };
const destination = { latitude: 37.771707, longitude: -122.4053769 };
const GOOGLE_MAPS_APIKEY = 'AIzaSyBMTrV2b0afK-Jsmd7EVLv2UXKT-KNlP-E';

//AIzaSyCB_WC903B7zKg7pX1a2GXbCmDkl6dKHxI
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

var time = 0;
var distance = 0;

export default class MapTrackStudent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    
      bookingLatLong:this.props.route.params.bookingLatLong,
      student_id: this.props.route.params.student_id,
  
      coordinates: [
        {
          latitude: Number(this.props.route.params?.bookingLatLong?.bookLat),
          longitude: Number(this.props.route.params?.bookingLatLong?.bookLong),
        },
        {
          latitude: Number(this.props.route.params?.student_lat),
          longitude: Number(this.props.route.params?.student_lng),
        },
      ],
    }


    this.mapView = null;

    AppHeader({
      ...this.props.navigation, leftTitle: LanguagesIndex.translate('StudentTrack'),
      borderBottomRadius: 25,
      bellIcon: false, settingsIcon: false,
    })
    
     console.log("params_bookingLatLong>> bookLat", this.props.route.params?.student_lat);
    // console.log("params_bookingLatLong>> bookLong", this.props.route.params?.bookingLatLong?.bookLong);
  }


  componentDidMount() {
   
    this.getStudentLatLng();
    this.intervalId = setInterval(() => {
      this.getStudentLatLng();
    },
      9000);
    this.blurListener = this.props.navigation.addListener('blur', () => {
      clearInterval(this.intervalId);
    });
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }



  getStudentLatLng = () => {
   
    let data = new FormData();
   
    data.append('student_id', this.state.student_id)


    ApiCall.postMethodWithHeader(Constants.get_lat_lng, data, Constants.APIImageUploadAndroid).then((response) => {
      //alert('hi')
      //console.log("response--response--111>", response)
      if (response.status == Constants.TRUE && response.data) {
        console.log("response--response-->2222", response.data)
        // let arr = [...this.state.coordinates]
        // let dic = {
        //   latitude: Number(response?.data?.lat),
        //   longitude: Number(response?.data?.lng)
        // }
        // arr.splice(0, 1, dic)
        // console.log('arr', arr)
        // this.setState({ coordinates: [...arr] })

        let coordinates = [...this.state.coordinates];
        coordinates[1] = {
          ...coordinates[1],
          latitude: Number(response.data?.lat),
          longitude: Number(response.data?.lng),
        };
        this.setState({ coordinates });
      }
    })
  }



  render() {
   
    return (
      
      <View style={styles.container}>
         <MapView
          style={StyleSheet.absoluteFill}
          ref={(c) => (this.mapView = c)}
          //onPress={this.onMapPress}
        >



{this.state.coordinates.map((coordinate, index) =>
            index == 0 ? (
              <MapView.Marker
                key={`coordinate_${index}`}
                coordinate={coordinate}>
                <Image
                  source={images.location_user_pin}
                  style={{height: 35, width: 35}}
                  resizeMode={'contain'}
                />
              </MapView.Marker>
            ) : (
              <MapView.Marker
                key={`coordinate_${index}`}
                coordinate={coordinate}>
                <Image
                  resizeMode={'contain'}
                  source={images.location_student_pin}
                  style={{height: 35, width: 35}}
                />
              </MapView.Marker>
            ),
          )}

         
          {this.state.coordinates.length >= 2 && (
            <MapViewDirections
              origin={this.state.coordinates[0]}
              destination={
                this.state.coordinates[this.state.coordinates.length - 1]
              }
              apikey={GOOGLE_MAPS_APIKEY}
              strokeWidth={3}
              strokeColor="blue"
              // optimizeWaypoints={true}
              onStart={(params) => {}}
              onReady={(result) => {
                time = result.duration;
                distance = result.distance;
                this.setState({});

                this.mapView.fitToCoordinates(result.coordinates, {
                  edgePadding: {
                    right: width / 20,
                    bottom: height / 20,
                    left: width / 20,
                    top: height / 20,
                  },
                });
              }}
              onError={(errorMessage) => {}}
            />
          )}
        </MapView>
      </View>

    )
  }

};