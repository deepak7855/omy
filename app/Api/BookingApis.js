import ApiCall from "./ApiCall"
import Constants from "./Constants"
import Helper from "../Lib/Helper"

export function changeBookingStatus(data,cb){
    Helper.globalLoader.showLoader();
    ApiCall.postMethodWithHeader(Constants.update_booking_status, data, Constants.APIImageUploadAndroid).then((response) => {
        Helper.globalLoader.hideLoader();
        cb(response)
    })
}

export function getAdminSetting(cb){
    ApiCall.postMethodWithHeader(Constants.get_setting,{}, Constants.APIGet).then((response) => {
        if(cb){
            cb(response)
        }
    })
}