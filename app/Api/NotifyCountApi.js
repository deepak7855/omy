import ApiCall from "./ApiCall"
import Constants from "./Constants"
import { DeviceEventEmitter, Platform } from 'react-native';
import Helper from "../Lib/Helper";
import messaging from '@react-native-firebase/messaging';

export function notifyCountStatus(data, cb) {
    ApiCall.postMethodWithHeader(Constants.notify_count, data, Constants.APIGet).then((response) => {
        let Notification = response.count
        DeviceEventEmitter.emit('Notification_Count', Notification);
        if (cb) {
            cb(response)
        }
    })
}

export function updateDeviceToken() {
    if (Platform.OS === 'android') {
        getToken();
    }
    else {
        checkPermission()
    }
}


export async function getToken() {
    let fcmToken = null;
    // await messaging().deleteToken();
    fcmToken = await messaging().getToken()
    if (fcmToken) {
        sendToken(fcmToken);
    }
}

export function checkPermission() {
    messaging().hasPermission().then((enabled) => {
        if (enabled) {
            getToken();
        } else {
            requestPermission();
        }
    });
}


export function requestPermission() {
    try {
        messaging().requestPermission().then((enabled) => {
            if (enabled) {
                getToken();
            }
        });
    } catch (error) {
    }
}

export function sendToken(fcmToken) {
    Helper.device_id = fcmToken;
    if (Helper.user_data) {
        let form = new FormData();
        form.append('device_type', Helper.device_type);
        form.append('device_id', fcmToken);

        ApiCall.postMethodWithHeader(Constants.update_token, form, Constants.APIImageUploadAndroid).then((response) => {
        })
    }
}