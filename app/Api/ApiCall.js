import * as React from 'react';
import Helper from '../Lib/Helper';
import Constants from './Constants';
import NetworkUtils from "./Network";
import AsyncStorageHelper from './AsyncStorageHelper';
import LanguagesIndex from '../Languages';

export default class ApiCall extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    } 

    static async postMethod(url, SendData, apiType = Constants.APIPost, Base_Url = Constants.Base_Url) {
        return NetworkUtils.isNetworkAvailable().then(isConnected => {
            if (isConnected == true) {
                let completeUrl = Base_Url + url
                console.log("completeUrl====",completeUrl)
                let headersObj = {
                    Accept: 'application/json',
                    'Content-Type': apiType == Constants.APIImageUploadAndroid ? 'multipart/form-data' : 'application/json',
                }

                headersObj.language  = LanguagesIndex.MyLanguage
 
                return fetch(completeUrl, {
                    method: Constants.APIPost,
                    headers: headersObj,
                    body: SendData
                }).then(response => response.json())
                    .then(responseJson => {

                        if (responseJson.status == 'unauthenticated') {

                            ApiCall.logOut(responseJson.message)
                        } else {
                            return responseJson;
                        }
                    }).catch(err => {
                        Helper.globalLoader.hideLoader();
                        return { "message": LanguagesIndex.translate('kSorryError'), "status": "false" }
                    })
            } else {
                Helper.globalLoader.hideLoader();
                return { "message": LanguagesIndex.translate('YoureofflinePleasecheckinternetconnection'), "status": "false" }
            }
        }).catch((error, a) => {
            Helper.globalLoader.hideLoader();
            return { "message": LanguagesIndex.translate('YoureofflinePleasecheckinternetconnection'), "status": "false" }
        });


    }
 
    static async postMethodWithHeader(url, SendData, apiType = Constants.APIPost, Base_Url = Constants.Base_Url) {
        return NetworkUtils.isNetworkAvailable().then(isConnected => {
            if (isConnected == true) {
 
                let completeUrl = Base_Url + url
                console.log("completeUrl====",completeUrl)
               //console.log("Helper.token===",Helper.token)

                let headersObj = {
                    Accept: 'application/json',
                    'Content-Type': apiType == Constants.APIImageUploadAndroid ? 'multipart/form-data' : 'application/json',
                    "Authorization": 'Bearer ' + Helper.token
                } 
                let sendBody = {};  
                let sendApiType = '';
                    headersObj.language  = LanguagesIndex.MyLanguage
                if (apiType == Constants.APIImageUploadAndroid) {
                    sendApiType = Constants.APIPost;
                } else {
                    sendApiType = apiType;
                }
                if (sendApiType == Constants.APIPost) {
                    sendBody = {
                        method: sendApiType,
                        headers: headersObj,
                        body: SendData
                    }
                } else {
                    sendBody = {
                        method: sendApiType,
                        headers: headersObj,
                    }
                }
                 
                return fetch(completeUrl, sendBody).then(response => response.json())
                    .then(responseJson => {
                        if (responseJson.status == 'unauthenticated') {
                            ApiCall.logOut(responseJson.message)
                        } else {
                            return responseJson;
                        }
                    }).catch(err => {
                        Helper.globalLoader.hideLoader();
                        return { "message": LanguagesIndex.translate('kSorryError'), "status": "false" }
                    })
            } else {
                Helper.globalLoader.hideLoader();
                return { "message": LanguagesIndex.translate('YoureofflinePleasecheckinternetconnection'), "status": "false" }
            }
        }).catch((error, a) => {
            Helper.globalLoader.hideLoader();
            return { "message": LanguagesIndex.translate('YoureofflinePleasecheckinternetconnection'), "status": "false" }
        });
    }

    static logOut(message) {
        Helper.globalLoader.hideLoader();
        Helper.showToast(message)
        AsyncStorageHelper.removeItemValue(Constants.USER_DETAILS)
        Helper.user_data = {}
        Helper.navRef.switchNavigation('', 'LoginScreen');
    }

}
