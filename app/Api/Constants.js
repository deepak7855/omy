export default Constants = {

 

  ////Testing Server
  // Base_Url: "https://demo2server.com/omy/api/",
  // chatUrl: "http://23.239.31.218:9155/",
  // attachmentUrl:"https://demo2server.com/omy/",
  // geoCodeKey:'AIzaSyCB_WC903B7zKg7pX1a2GXbCmDkl6dKHxI',


  
  ////Live Server
  Base_Url: "https://omyweb.ch/api/",
  chatUrl: "https://omyweb.ch:9166/",
  attachmentUrl:"https://omyweb.ch/",
  geoCodeKey:'AIzaSyCB_WC903B7zKg7pX1a2GXbCmDkl6dKHxI',


  otpTxt: 1,
  emailMaxLength: 60,
  passMaxLength: 15,
  fullNameMax: 40,
  contactNumberMax: 15,
  addressMax: 100,
  cityMax: 50,
  amountMax: 15,
  countryMax: 50,
  zipCodeMax: 6,
  licenceNumberMax: 20,
  messageMax: 300,

  AllFilter: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  EmailFilter: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,

  //Keys
  APIPost: "POST",
  APIGet: "GET",
  APIImageUploadAndroid: "POST_UPLOAD",
  USER_DETAILS: "user_details",
  TRUE: "true",
  FALSE: "false",
  USER_PROFILE_UPDATE: "user_profile_update",
  SUCCESS: "Success",
  fcmToken: "fcmToken",
  ADMIN_USER_ID: '1',

  //userManagement
  LOGIN: "login",
  SIGNUP: "signup",
  get_relationships: "get-relationships",
  CHANGE_PASSWORD: "change_password",
  FORGOT_PASSWORD: "forgot",
  get_profile: "get_profile",
  get_activities: "get-activities",
  get_payment:"add-money",
  get_slots: "get-slots",
  update_profile: "update_profile",
  log_out: "logout",
  user_social: "userSocial",
  notification_permissions: "notification_permissions",
  default_lang_set: "default-lang-set",
  get_user_activity_list: "get-user-activity-list",
  search_activity: "search-activity",
  booking_request: "booking-request",
  booking_history: "booking-history",
  update_booking_status: "update-booking-status",
  booking_detail:"booking-detail",
  add_bank_details:"add-bank-details",
  pages_terms_and_condition:"pages/terms-and-condition",
  pages_privacy_policy:"pages/privacy-policy",
  recent_bookings:"recent-bookings",
  earnings:"earnings",
  year_wise_earnings:"year-wise-earnings",
  month_wise_earnings:"month-wise-earnings",
  get_notifications:"get-notifications",
  student_detail:"student-detail",
  student_list:"student-list",
  book_request:"book-request",
  recent_student_list:"recent-student-list",
  ratings:"ratings",
  redeem_request:"redeem-request",
  get_reviews:"get-reviews",
  user_tracking:"user-tracking",
  notify_count:"notify-count",
  Transactions:'transactions',
  cancel_booking:'cancel-booking',
  cancel_charges:"cancel-charges",
  chat_media:"chat-media",
  update_token:"update-device",
  get_setting:"get-commission",
  get_postcode:"get-postcode",

  cancellation_policy:"pages/cancellation-policy",
  new_cancellation_policy_url:"cancellation-policy",
  get_lat_lng:"get-latlng",
   
}
