import React, { PureComponent } from 'react'
import { Text, View, TouchableOpacity, LayoutAnimation, Platform, UIManager, StyleSheet, Image } from 'react-native'
import Colors from '../../Assets/Colors';
import { Rating, AirbnbRating } from 'react-native-ratings';
import fonts from '../../Assets/fonts';
import { images } from '../../Assets/imagesUrl';
import ImageLoadView from '../../Lib/ImageLoadView';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import moment from 'moment';

if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}

export default class RatingViewItem extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            showMore: false
        }
    }

    render() {
        let { item, from } = this.props;
        return (
            <View style={{ paddingVertical: 5, marginTop: 10, backgroundColor: Colors.white }}>
                <TouchableOpacity activeOpacity={1} onPress={() => {
                    LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
                    this.setState({ showMore: !this.state.showMore })
                }} style={styles.samCss}>
                    {from == 'profile' ?
                        <>
                            <View style={[styles.star_icon_count_view, { flex: 1 }]}>
                                <Text style={styles.count_start_txt}>{Number(item.avg_rate).toFixed(1)}</Text>
                                <Image
                                    resizeMode={'contain'} source={images.rating_white_ic}
                                    style={styles.star_img} />
                            </View>
                            <View style={{ flex: 8.3 }}>
                                <Text style={[styles.label_txt, { marginTop: 0 }]}>{item.note}</Text>
                                <Text style={styles.sub_des_txt}>{item.user.name}{' '}|{' '}{Helper.formatLanguageDate(item.created_at, "DD MMM YYYY")}{" "}|{" "}{moment.utc(item.created_at).local().format('h:mm a')}</Text>
                            </View>
                        </>
                        :
                        <>
                            <ImageLoadView
                                resizeMode={'cover'}
                                style={{ height: 65, width: 65, borderRadius: 10 }}
                                source={item.user && item.user.profile_picture ? { uri: item.user.profile_picture } : images.default} />
                            <View style={{ marginLeft: 20, flex: 1 }}>
                                <Text style={styles.textCss}>{item.user.name}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image
                                        resizeMode={'contain'}
                                        style={styles.samImgCss}
                                        source={{ uri: item.booking_services.activity.icon }} />
                                    <Text style={styles.greyTextCss}>{item.booking_services.activity.name}</Text>
                                </View>
                                <Text style={styles.student_date_and_time_txt}>{Helper.formatLanguageDate(item.created_at, "DD MMM YYYY")}{" "}|{" "}{moment.utc(item.created_at).local().format('h:mm a')}</Text>
        
                            </View>
                        </>
                    }
                    <View style={{ height: 20, width: 20, }}>
                        <Image style={styles.upIconCss}
                            resizeMode={'cover'}
                            source={this.state.showMore ? images.ups : images.downs} />
                    </View>
                </TouchableOpacity>

                {this.state.showMore &&
                    <View style={styles.mainViewRatingCss}>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('OverallExperience')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    isDisabled
                                    reviews={false}
                                    defaultRating={item.overall_exp}
                                    fontSize={10}
                                    size={14.4}
                                    reviewSize={0}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Punctual')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    isDisabled
                                    reviews={false}
                                    defaultRating={item.punctual}
                                    fontSize={10}
                                    size={14.4}
                                    reviewSize={0}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Friendly')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    isDisabled
                                    reviews={false}
                                    defaultRating={item.friendly}
                                    fontSize={10}
                                    size={14.4}
                                    reviewSize={0}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Performance')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    isDisabled
                                    reviews={false}
                                    defaultRating={item.performance}
                                    fontSize={10}
                                    size={14.4}
                                    reviewSize={0}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Value')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    isDisabled
                                    reviews={false}
                                    defaultRating={item.value}
                                    fontSize={10}
                                    size={14.4}
                                    reviewSize={0}
                                />
                            </View>
                        </View>
                        <View style={styles.yesViewCss}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('WouldYouRecommend')}</Text>
                            <Text style={styles.yesTextCss}>{Number(item.recommend) == 1 ? LanguagesIndex.translate('Yes') : LanguagesIndex.translate('No')}</Text>
                        </View>
                        <Text style={styles.yesTextCss}>{LanguagesIndex.translate('Note')}</Text>
                        <Text style={styles.pragrapCss}>{item.note}</Text>
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainViewRatingCss: {
        paddingBottom: 15,
        marginHorizontal: 16,
    },
    samCss: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 16,
        paddingVertical: 10,
    },
    textCss: {
        color: Colors.blackThree,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
    },
    samImgCss: {
        height: 12.2,
        width: 13,

    },
    greyTextCss: {
        marginLeft: 10,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        textAlign: 'center',
        marginVertical: 5,
        fontFamily: fonts.RoBoToRegular_1
    },
    upIconCss: {
        height: 6,
        width: 10,
        tintColor: Colors.warmGreyTwo,
    },
    ratingNameCss: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToRegular_1
    },
    ratingMainView: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between'
    },
    yesTextCss: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1
    },
    yesViewCss: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 15
    },
    pragrapCss: {
        marginTop: 10,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    star_icon_count_view: {
        height: 25,
        marginTop: 2,
        marginRight: 10,
        flexDirection: 'row',
        backgroundColor: Colors.cerulean,
        alignItems: 'center',
        paddingHorizontal: 7,
        paddingVertical: 2
    },
    count_start_txt: {
        color: Colors.white,
        marginRight: 6
    },
    star_img: {
        height: 10,
        width: 10
    }, 
    sub_des_txt: {
        marginTop: 5,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize10,
        fontFamily: fonts.RoBoToRegular_1
    },
    label_txt: {
        marginTop: 10,
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    student_date_and_time_txt:{
        color: Colors.warmGrey,
        fontSize: fonts.fontSize10,
        fontFamily: fonts.RoBoToRegular_1
    },
});