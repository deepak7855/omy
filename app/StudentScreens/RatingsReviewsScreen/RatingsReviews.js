import React, { Component } from 'react';
import { StyleSheet, FlatList, SafeAreaView, RefreshControl } from 'react-native';
import Colors from '../../Assets/Colors';
import { ApiCall, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import ListLoader from '../../Comman/ListLoader';
import NoData from '../../Comman/NoData';
import RatingViewItem from './RatingViewItem';
import LanguagesIndex from '../../Languages';

export default class RatingsReviews extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            ratingList: [],
            next_page_url: '',
            currentPage: 1
        };
        AppHeader({
            ...this.props.navigation, leftTitle:LanguagesIndex.translate('Ratings&Reviews'),
            borderBottomRadius: 25,
        })
    }

    componentDidMount() {
        this.getReviews()
    }

    onScroll = () => {
        if (!this.state.isLoading && this.state.next_page_url)
            this.setState({ isLoading: true, currentPage: this.state.currentPage + 1 }, () => {
                this.getReviews();
            })
    }

    onRefresh = () => {
        this.setState({ refreshing: true, currentPage: 1 }, () => {
            this.getReviews();
        })
    }

    getReviews = () => {
        let data = new FormData();
        data.append('student_id', Helper.user_data.id);
        data.append('page', this.state.currentPage);

        ApiCall.postMethodWithHeader(Constants.get_reviews, data, Constants.APIImageUploadAndroid).then((response) => {
            if (response.status == Constants.TRUE && response.data.current_page) {
                this.setState({
                    isLoading: false,
                    refreshing:false,
                    next_page_url: response.data.next_page_url,
                    ratingList: this.state.currentPage == 1 ? response.data.data : [...this.state.ratingList, ...response.data.data]
                })
            }
            else {
                Helper.showToast(response.message)
                this.setState({
                    isLoading: false,
                    refreshing:false,
                })
            }
        })
    }

    _renderListItem = ({ item }) => {
        return (
            <RatingViewItem item={item} />
        )
    }

    render() {
        let { ratingList,refreshing, isLoading } = this.state;
        return (
            <SafeAreaView style={styles.safe_area_view}>
                {ratingList.length < 1 && !isLoading ?
                    <NoData 
                    message={LanguagesIndex.translate('Youdonthaveanyratings&reviews')}/> :
                    <FlatList
                        data={ratingList}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                        renderItem={this._renderListItem}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        onScroll={this.onScroll}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => {
                            return (
                                <>
                                    {isLoading ? <ListLoader /> : null}
                                </>
                            )
                        }}
                    />
                }
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteThree
    }
});