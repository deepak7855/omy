import React from 'react';
import {
  FlatList,
  SafeAreaView,
  RefreshControl,
  DeviceEventEmitter,
} from 'react-native';
import styles from './BookingRequestsStyles';
import {Constants, ApiCall} from '../../Api';
import ListLoader from '../../Comman/ListLoader';
import NoData from '../../Comman/NoData';
import StudentBookingView from '../StudentBookings/StudentBookingView';
import AppHeader from '../../Comman/AppHeader';
import {handleNavigation} from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import * as RNLocalize from 'react-native-localize';

export default class BookingRequestsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requestList: [],
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: true,
    };
    AppHeader({
      ...this.props.navigation,
      leftTitle: LanguagesIndex.translate('BookingRequests'),
      borderBottomRadius: 25,
      bellIcon: true,
      settingsIcon: true,
      bellIconClick: () => this.bellIconClick(),
      settingIconClick: () => this.settingIconClick({}),
    });
  }

  settingIconClick() {
    handleNavigation({
      type: 'push',
      page: 'SettingsScreen',
      navigation: this.props.navigation,
    });
  }

  bellIconClick() {
    handleNavigation({
      type: 'push',
      page: 'NotificationsScreen',
      navigation: this.props.navigation,
    });
  }

  componentDidMount() {
    this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
      RNLocalize.addEventListener(
        'change',
        this.handleLocalizationChange(LanguagesIndex.MyLanguage),
      );
      AppHeader({
        ...this.props.navigation,
        leftTitle: LanguagesIndex.translate('BookingRequests'),
        borderBottomRadius: 25,
        bellIcon: true,
        settingsIcon: true,
        bellIconClick: () => this.bellIconClick(),
        settingIconClick: () => this.settingIconClick({}),
      });
    });
    RNLocalize.addEventListener(
      'change',
      this.handleLocalizationChange(LanguagesIndex.MyLanguage),
    );

    this.changeBookingStatus = DeviceEventEmitter.addListener(
      'changeBookingStatus',
      (data) => {
        this._onRefresh();
      },
    );
    this.getBookingRequestApiCall();
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    this.changeBookingStatus.remove();
  }

  handleLocalizationChange = (lang) => {
    LanguagesIndex.setI18nConfig(lang);
    this.forceUpdate();
  };

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState(
        {currentPage: this.state.currentPage + 1, isLoading: true},
        () => {
          this.getBookingRequestApiCall();
        },
      );
    }
  };
  _onRefresh = () => {
    this.setState({refreshing: true, currentPage: 1}, () => {
      this.getBookingRequestApiCall();
    });
  };

  getBookingRequestApiCall = () => {
    let data = new FormData();
    data.append('page', this.state.currentPage);
    data.append('type', 'pending');
    ApiCall.postMethodWithHeader(
      Constants.booking_history,
      data,
      Constants.APIImageUploadAndroid,
    ).then((response) => {
      //console.log('response---response',response)
      if (response.status == Constants.TRUE) {
        if (response.data && response.data.current_page) {
          this.setState({
            requestList:
              this.state.currentPage == 1
                ? response.data.data
                : [...this.state.requestList, ...response.data.data],
            next_page_url: response.data.next_page_url,
            refreshing: false,
            isLoading: false,
          });
        } else {
          this.setState({
            requestList: [],
            refreshing: false,
            isLoading: false,
          });
        }
      } else {
        this.setState({refreshing: false, isLoading: false});
      }
    });
  };

  _renderItem = ({item}) => {
    return (
      <StudentBookingView
        order={item}
        type={'pending'}
        refreshList={() => this._onRefresh()}
        navigation={this.props.navigation}
      />
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        {!this.state.isLoading &&
        !this.state.refreshing &&
        this.state.requestList.length < 1 ? (
          <NoData
            message={LanguagesIndex.translate('Youdonthaveanyrequests')}
          />
        ) : null}
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          data={this.state.requestList}
          renderItem={this._renderItem}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={() => {
            return this.state.isLoading ? <ListLoader /> : null;
          }}
          onEndReached={this.onScroll}
          onEndReachedThreshold={0.5}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={'handled'}
        />
      </SafeAreaView>
    );
  }
}
