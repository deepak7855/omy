import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default BookingRequestsStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteThree
    },
    card_view: {
        marginTop: 10,
        backgroundColor: Colors.white,
        paddingVertical: 5
    },
    card_profile_view: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 16,
        justifyContent: 'space-between'
    },
    profile_flex_view: {
        flex: 1.3,
    },
    profile_img_view: {
        width: 35,
        height: 35,
        borderRadius: 10,
    },
    profile_img: {
        width: '100%',
        height: '100%'
    },
    user_name_flex: {
        flex: 6,
    },
    rate_view: {
        flex: 2,
        alignItems: 'flex-end'
    },
    dog_img: {
        width: 13,
        height: 12.2
    },
    calender_img: {
        width: 10.7,
        height: 11.8
    },
    user_name_text:{
        color: Colors.black,
        fontSize: fonts.fontSize14,
        lineHeight: 17,
        fontFamily: fonts.RoBoToMedium_1
    },
    rate_text:{
        color: Colors.cerulean,
        fontSize: fonts.fontSize14,
        lineHeight: 17,
        fontFamily: fonts.RoBoToMedium_1
    },
    card_dog_view_btn:{
        marginHorizontal: 16, 
        flexDirection: 'row', 
        paddingVertical: 5
    },
    dog_flex_view:{
        flex: 8,
    },
    card_dog_view:{
        flexDirection: 'row', 
        alignItems: 'center',
    },
    dog_img_view:{
        flex: .8
    },
    dog_title_view:{
        flex: 9.2 
    },
    calender_img_view:{
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: 5
    },
    calender_flex_view:{
        flex: .8
    },
    date_flex_view:{
        flex: 9.2
    },
    dog_text:{
        fontSize: fonts.fontSize12, 
        color: Colors.warmGrey, 
        fontFamily: fonts.RoBoToRegular_1, 
        lineHeight: 14
    },
    date_text:{
        fontSize: fonts.fontSize12, 
        color: Colors.warmGrey, 
        fontFamily: fonts.RoBoToRegular_1, 
        lineHeight: 16
    },
    redeem_txt:{
        fontSize: fonts.fontSize12, 
        color: Colors.warmGrey, 
        fontFamily: fonts.RoBoToRegular_1, 
        lineHeight: 14
    },
    flex_card_btn_view:{
        flex: 2,
        alignSelf: 'flex-end',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    booking_btn:{
        height: 30, 
        width: 30
    },


});