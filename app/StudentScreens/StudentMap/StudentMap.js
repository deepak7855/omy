import React from 'react';
import { Text, View, SafeAreaView,StyleSheet, Image, DeviceEventEmitter, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import styles from './StudentMapStyles';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import moment from 'moment';
import { ApiCall, AsyncStorageHelper, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
const origin = { latitude: 37.3318456, longitude: -122.0296002 };
const destination = { latitude: 37.771707, longitude: -122.4053769 };
const GOOGLE_MAPS_APIKEY = 'AIzaSyBMTrV2b0afK-Jsmd7EVLv2UXKT-KNlP-E';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

var time = 0;
var distance = 0;

export default class StudentMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // booking_id:this.props.route.params.booking_id,
            // booking_service_id:this.props.route.params.booking_service_id,
            // booking_time:this.props.route.params.booking_time,
            // student_lat:this.props.route.params.senior_lat,
            // student_lng:this.props.route.params.senior_lng,

            coordinates: [
                {
                    latitude: Number(this.props.route.params.senior_lat),
                    longitude: Number(this.props.route.params.senior_lng),
                    // latitude: Number("47.3808"),
                    // longitude: Number("8.5173"),
                },
                {
                    latitude: Number(Helper?.user_data?.lat),
                    longitude: Number(Helper?.user_data?.lng),
                },
            ],
        }
        this.mapView = null;

        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('MapDetails'),
            borderBottomRadius: 25,
            bellIcon: false, settingsIcon: false,
        })
        // console.log("this.props.route.params.booking_id--->",this.props.route.params.booking_id),
        // console.log("this.props.route.params.booking_service_id---->>",this.props.route.params.booking_service_id),
        // console.log("this.props.route.params.booking_time-->",this.props.route.params.booking_time)
        // console.log("this.props.route.params.student_lat-->",this.props.route.params.student_lat)
        // console.log("this.props.route.params.student_lng-->",this.props.route.params.student_lng)

        // console.log("Helper?.user_data?.lat>>>>>>>>>>>",Helper?.user_data?.lat),
        // console.log("Helper?.user_data?.lng>>>>>>>>>>>",Helper?.user_data?.lng)
    }


    componentDidMount() {
        //this.getTrackStudent();
    }

    getTrackStudent = () => {
        let booking_time = moment.utc(this.state.booking_time, "hh:mm a").local().format("hh:mm a");
        //console.log("booking_time-booking_time---",booking_time);
    
        let data = new FormData();
        data.append('booking_id', this.state.booking_id)
        data.append('booking_service_id', this.state.booking_service_id)
        data.append('lat', Helper?.user_data?.lat)
        data.append('lng', Helper?.user_data?.lng)
        data.append('start_time', booking_time)
        data.append('tracking_type', "0")
        
        console.log("user tracking---->>>====++++",data)
        ApiCall.postMethodWithHeader(Constants.user_tracking, data, Constants.APIImageUploadAndroid).then((response) => {

            console.log("response--response-->",response)
            if (response.status == Constants.TRUE && response.data && response.data.status) {


            } else {
                //Helper.showToast(response.message);
                // this.props.navigation.goBack(null);
            }
        })
    }



    render() {
        
        return (
            // <SafeAreaView style={styles.safe_area_view}>
                <View style={styles.container}>
                 <MapView
          style={StyleSheet.absoluteFill}
          ref={(c) => (this.mapView = c)}
          //onPress={this.onMapPress}
        >
          {this.state.coordinates.map((coordinate, index) =>
            index == 0 ? (
              <MapView.Marker
                key={`coordinate_${index}`}
                coordinate={coordinate}>
                <Image
                  source={images.location_user_pin}
                  style={{height: 35, width: 35}}
                  resizeMode={'contain'}
                />
              </MapView.Marker>
            ) : (
              <MapView.Marker
                key={`coordinate_${index}`}
                coordinate={coordinate}>
                <Image
                  resizeMode={'contain'}
                  source={images.location_student_pin}
                  style={{height: 35, width: 35}}
                />
              </MapView.Marker>
            ),
          )}
          {this.state.coordinates.length >= 2 && (
            <MapViewDirections
              origin={this.state.coordinates[0]}
              destination={
                this.state.coordinates[this.state.coordinates.length - 1]
              }
              apikey={GOOGLE_MAPS_APIKEY}
              strokeWidth={3}
              strokeColor="hotpink"
              // optimizeWaypoints={true}
              onStart={(params) => {}}
              onReady={(result) => {
                time = result.duration;
                distance = result.distance;
                this.setState({});

                this.mapView.fitToCoordinates(result.coordinates, {
                  edgePadding: {
                    right: width / 20,
                    bottom: height / 20,
                    left: width / 20,
                    top: height / 20,
                  },
                });
              }}
              onError={(errorMessage) => {}}
            />
          )}
        </MapView>

        {/* <View style={styles.bottomView}>
          <View style={{flex: 0.2}}>
            <Image
              style={styles.listIcon}
              source={
                data?.vandetails?.profile_image
                  ? {uri: data?.vandetails?.profile_image}
                  : Images.profile_inactive
              }
            />
          </View>
          <View style={{flex: 0.8, marginLeft: 20}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text style={styles.nameText}>{data?.vandetails?.full_name}</Text>

              <Text style={styles.kmText}>{distance.toFixed(2)} Km</Text>
            </View>
            <View style={[styles.kkmView, {marginTop: 10}]}>
              <View style={{flex: 1}}>
                <Text style={styles.earText}>{data.package_title}</Text>
              </View>
              <Text style={styles.timeText}>{time.toFixed(0)} min</Text>
            </View>
          </View>
        </View> */}
    </View>
               
        )
    }

};