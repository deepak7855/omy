import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default StudentMaptStyles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
    backgroundColor: Colors.whiteFive
  },
  container: {
   flex:1, backgroundColor:'#0fff'
  },
  mapStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  //
  bottom_view:{
    position: 'absolute',
    bottom: 50,
    padding: 12,
    marginBottom: 30,
    backgroundColor: 'white',
    borderRadius: 5,
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  user_img_view:{
    height: 60,
    width: 60,
    resizeMode: 'contain',
    borderRadius: 100 / 2,
    backgroundColor:'yellow'
  },
  full_name_view:{
    flexDirection: 'row',
    alignItems: 'center',
  },
  full_name_txt:{
    fontSize: 14,
      top: 5
  },
  km_txt:{
    marginLeft: 'auto',
    fontSize: 18,
  },
  package_details_view:{
    marginTop: 10,
    flexDirection: 'row',
    bottom: 7,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  service_title:{
    fontSize: 10,
    color: 'rgb(77, 77, 77)',
  },
  service_time:{
    fontSize: 14,
    top: 4,
    color: 'rgb(77, 77, 77)',
  }

});