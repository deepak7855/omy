import React from 'react';
import { Text, View, ScrollView, Keyboard, SafeAreaView, } from 'react-native';
import styles from './AccountDetailsStyles';
import { GButton } from '../../Comman/GButton';
import IconInput from '../../Comman/GInput';
import AppHeader from '../../Comman/AppHeader';
import Helper from '../../Lib/Helper';
import { handleNavigation } from '../../navigation/Navigation';
import { ApiCall, AsyncStorageHelper, Constants } from '../../Api';
import LanguagesIndex from '../../Languages';

export default class AccountDetailsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            userAccountDetailForm: {
                name: Helper.user_data.name,
                IBAN_number: "",
                address: `${Helper.user_data.street_no} ${Helper.user_data.postcode} ${Helper.user_data.city}`,
                validators: {
                    name: { required: true, minLength: 2, maxLength: 45, },
                    IBAN_number: { required: true, minLengthDigit: 19, maxLengthDigit: 19,numeric:true },
                    address: { required: true, minLength: 2, maxLength: 100, },

                }
            }
        }
        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('AccountDetails'), borderBottomRadius: 25 })
    }

    componentDidMount() {
        console.log('Helper.user_data',Helper.user_data)
        if (Helper.user_data && Helper.user_data.bank_details) {
            console.log('Helper.user_data.bank_details',Helper.user_data.bank_details)
            this.setState((prevState) => ({
                userAccountDetailForm: {
                    ...prevState.userAccountDetailForm,
                    name: Helper.user_data.bank_details.bank_name,
                    IBAN_number: Helper.user_data.bank_details.bank_code,
                    address: Helper.user_data.bank_details.address,
                }
            }))
        }
    }

    setValues(key, value) {
        let userAccountDetailForm = { ...this.state.userAccountDetailForm }
        if (key == 'IBAN_number') {
            let num = value.replace(".", '');
            value = num;
        }
        userAccountDetailForm[key] = value;
        this.setState({ userAccountDetailForm })
    }

    account_Submit() {
        Keyboard.dismiss()
        let isValid = Helper.validate(this.state.userAccountDetailForm);
        if (isValid) {
            Helper.globalLoader.showLoader();
            let data = new FormData();
            data.append('bank_name', this.state.userAccountDetailForm.name)
            data.append('bank_code', this.state.userAccountDetailForm.IBAN_number)
            data.append('address', this.state.userAccountDetailForm.address)

            ApiCall.postMethodWithHeader(Constants.add_bank_details, data, Constants.APIImageUploadAndroid).then((response) => {
                Helper.globalLoader.hideLoader();
                if (response.status == Constants.TRUE) {
                    Helper.showToast(response.message);
                    let user_data = { ...Helper.user_data }
                    user_data.bank_details = {};
                    user_data.bank_details.bank_name = this.state.userAccountDetailForm.name;
                    user_data.bank_details.bank_code = this.state.userAccountDetailForm.IBAN_number;
                    user_data.bank_details.address = this.state.userAccountDetailForm.address;
                    Helper.user_data = user_data;
                    AsyncStorageHelper.setData(Constants.USER_DETAILS, Helper.user_data)
                    setTimeout(() => {
                        handleNavigation({ type: 'pop', navigation: this.props.navigation });
                    }, 200);
                }
                else {
                    Helper.showToast(response.message)
                }
            }
            ).catch(err => {
                Helper.globalLoader.hideLoader();
            })
        }
    }
 
    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.account_details_txt_view}>
                        <Text style={styles.enter_account_txt}>{LanguagesIndex.translate('EnterBelowToAddAccountDetails')}</Text>
                    </View>
                    <View style={styles.input_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('NameOnly')}
                            setFocus={() => { this.iban_number.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(name) => this.setValues('name', name)}
                            value={this.state.userAccountDetailForm.name}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('IBANNumber')}
                            fixedValue={'CH'}
                            getFocus={(input) => { this.iban_number = input }}
                            setFocus={(input) => { this.address.focus(); }}
                            returnKeyType="next"
                            keyboardType={'numeric'}
                            maxLength={19}
                            onChangeText={(iban_number) => this.setValues('IBAN_number', iban_number)}
                            value={this.state.userAccountDetailForm.IBAN_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('Address')}
                            getFocus={(input) => { this.address = input }}
                            // setFocus={() => { this.account_Submit() }}
                            // returnKeyType="done"
                            keyboardType={'default'}
                            multiline={true}
                            onChangeText={(address) => this.setValues('address', address)}
                            value={this.state.userAccountDetailForm.address}
                            style={{height:80}}
                        />
                    </View>

                    <View style={styles.save_btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('SAVE')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.account_Submit() }}
                        // onPress={() => { this.account_Submit() }}
                        />
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }

};


