import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default AccountDetailsStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1, 
        backgroundColor: Colors.white
    },
    account_details_txt_view:{
        marginHorizontal:17,
        marginTop:20
    },
    enter_account_txt:{
        color:Colors.black,
        fontSize:fonts.fontSize14,
        fontFamily:fonts.RoBoToBold_1,
    },
    input_view:{
        marginVertical: 10,  
        marginHorizontal: 17
    },
    save_btn_view:{
        marginVertical: 10, 
        marginTop: 20, 
        marginHorizontal: 17
    },
   

});