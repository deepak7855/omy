import React, { Component } from 'react';
import { View, Text, FlatList, DeviceEventEmitter, RefreshControl } from 'react-native';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import { GButton } from '../../Comman/GButton';
import styles from './WelcomeHomeScreenStyles';
import Helper from '../../Lib/Helper';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import SplashScreen from 'react-native-splash-screen';
import ImageLoadView from '../../Lib/ImageLoadView';
import { Constants, ApiCall, AsyncStorageHelper } from '../../Api';
import ListLoader from '../../Comman/ListLoader';
import NoData from '../../Comman/NoData';
import StudentBookingView from '../StudentBookings/StudentBookingView';
import { notifyCountStatus, sendToken, updateDeviceToken } from '../../Api/NotifyCountApi';
import { configureBackgroundTracking, stopUserTracking } from '../../Comman/BackgroundUserTracking';
import moment from 'moment';
import messaging from '@react-native-firebase/messaging';
import * as RNLocalize from "react-native-localize";
import SocketController, { events } from '../../Lib/SocketController';

export default class WelcomeHomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            upcomingList: [],
            next_page_url: '',
            currentPage: 1,
            refreshing: false,
            isLoading: true,
        };
        this.createHeader();
    }


    createHeader() {
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('Welcome_back'),
            bellIcon: true, settingsIcon: true,
            hideLeftBackIcon: true,
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })
    }


    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    componentDidMount() {
        Helper.navigationRef = this.props.navigation;
        SocketController.socketInit();
        setTimeout(() => {
            SocketController.getUnreadCount();
        }, 2000);
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            AppHeader({
                ...this.props.navigation, leftTitle: LanguagesIndex.translate('Welcome_back'),
                bellIcon: true, settingsIcon: true,
                hideLeftBackIcon: true,
                bellIconClick: () => this.bellIconClick(),
                settingIconClick: () => this.settingIconClick()
            })

        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));

        setTimeout(() => {
            SplashScreen.hide();
        }, 500);
        this.listener = DeviceEventEmitter.addListener(Constants.USER_DETAILS, (value) => {
            this.setState({ change: !this.state.change })
        });
        this.unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getUpComingBookingApiCall()
            updateDeviceToken()
            // messaging().onTokenRefresh(async (fcmToken) => {
            //     sendToken(fcmToken)
            // });
            notifyCountStatus({}, (resData) => {
               // console.log("=======notic",resData)
                if (resData.data && resData.data.length > 0 && resData.data[0].status == "START") {
                    let currentBookingTime = moment(resData.data[0].booking_request.booking_date + " " + resData.data[0].booking_request.booking_time).format('YYYY-MM-DD HH:mm');
                    let bookingEndTime = moment(currentBookingTime).add(Number(resData.data[0].booking_services.activity_duration), 'hours').format('YYYY-MM-DD HH:mm');

                    let currentTime = moment().format('YYYY-MM-DD HH:mm');

                    if (moment(currentTime).isAfter(bookingEndTime)) {
                        stopUserTracking();
                        AsyncStorageHelper.removeItemValue('runningBooking');
                    } else {
                        AsyncStorageHelper.getData('runningBooking').then((runningBooking) => {
                            let data = {
                                booking_id: resData.data[0].booking_id,
                                booking_service_id: resData.data[0].booking_service_id,
                                start_time: runningBooking?.start_time ? runningBooking?.start_time :  moment().utc().format('YYYY-MM-DD HH:mm')
                            }
                            AsyncStorageHelper.setData('runningBooking', data).then(() => {
                                configureBackgroundTracking();
                            })
                        })

                    }
                } else {
                    stopUserTracking();
                }
            })
        });
        this.messageListener()
    }

    messageListener = async () => {

        messaging().setBackgroundMessageHandler(async remoteMessage => {

        });

        //notification app forground
        messaging().onMessage(async notification => {
            let encryptedData = JSON.parse(notification.data.dictionary);
            if (Helper.currentPage != 'chat' || encryptedData.type != "chat") {
                DeviceEventEmitter.emit('showLocalNotification', notification);
            }
        });

        //When the application is running, but in the background
        messaging().onNotificationOpenedApp(async notification => {
            if (notification && notification.data) {
                this.handleNotificationNavigation(notification.data.type, notification.data.dictionary)
            }
        })

        // notification app kill
        messaging().getInitialNotification().then(async notification => {
            if (notification && notification.data) {
                this.handleNotificationNavigation(notification.data.type, notification.data.dictionary)
            }
        });


    }

    handleNotificationNavigation(type, dicData) {
        let encryptedData = JSON.parse(dicData);
        if (type != 'custom_notification') {
            if (type == "chat") {
                SocketController.socketInit();
                setTimeout(() => {
                    handleNavigation({
                        type: 'push', page: 'ChatScreen', navigation: this.props.navigation,
                        passProps: {
                            other_user_id: encryptedData.sender_id,
                            name: encryptedData.name,
                            picture: encryptedData.profile_picture,
                            extraData: {
                                bookingStatus: "ACCEPT",
                            }
                        }
                    })
                }, 1000);
            } else {
                handleNavigation({
                    page: 'BookingDetails', type: 'push', navigation: this.props.navigation, passProps: {
                        booking_id: encryptedData.booking_id
                    }
                })
            }
        }
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
        this.unsubscribe();
        if (this.listener)
            this.listener.remove();
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    goToBookingRequest() {
        handleNavigation({ type: 'push', page: 'BookingRequestsScreen', navigation: this.props.navigation })
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    onScroll = () => {
        if (this.state.next_page_url && !this.state.isLoading) {
            this.setState({ currentPage: this.state.currentPage + 1, isLoading: true }, () => {
                this.getUpComingBookingApiCall();
            });
        }

    }
    _onRefresh = () => {
        this.setState({ refreshing: true, currentPage: 1 }, () => {
            this.getUpComingBookingApiCall();
        });
    }

    getUpComingBookingApiCall = () => {
        let data = new FormData();
        data.append('page', this.state.currentPage);
        data.append('type', 'upcomming');
        ApiCall.postMethodWithHeader(Constants.booking_history, data, Constants.APIImageUploadAndroid).then((response) => {
            if (response.status == Constants.TRUE) {
                this.createHeader();
                if (response.data && response.data.current_page) {
                    this.setState({
                        upcomingList: this.state.currentPage == 1 ? response.data.data : [...this.state.upcomingList, ...response.data.data],
                        next_page_url: response.data.next_page_url,
                        refreshing: false,
                        isLoading: false,
                    })
                } else {
                    this.setState({
                        upcomingList: [],
                        refreshing: false,
                        isLoading: false,
                    })
                }
            } else {
                this.setState({ refreshing: false, isLoading: false })
            }
        })
    }

    _renderItem = ({ item }) => {
        return (
            <StudentBookingView
                order={item}
                type={'home'}
                navigation={this.props.navigation}
            />
        )
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                <View style={styles.profile_view}>
                    <View style={styles.blue_radius_view} />
                    <View style={styles.pro_img_view}>
                        <ImageLoadView style={styles.profile_img}
                            resizeMode={'cover'}
                            source={Helper.user_data.profile_picture ? { uri: Helper.user_data.profile_picture } : images.profile_picture}
                        />
                    </View>
                </View>
                <Text style={styles.name_txt}>{Helper.user_data.name}</Text>

                <View style={styles.up_text_view}>
                    <Text style={styles.up_text}>{LanguagesIndex.translate('YourNextUpcomingBookings')}</Text>
                </View>

                {!this.state.isLoading && !this.state.refreshing && this.state.upcomingList.length < 1 ?
                    <NoData
                        message={LanguagesIndex.translate('Youdonthaveanyupcomingbookings')}
                    />
                    : null
                }
                <FlatList
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    data={this.state.upcomingList}
                    renderItem={this._renderItem}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                    ListFooterComponent={() => {
                        return (
                            this.state.isLoading ? <ListLoader /> : null
                        )
                    }}
                    ItemSeparatorComponent={() => <View style={{ borderBottomWidth: 6, borderBottomColor: Colors.whiteThree }} />}
                    onEndReached={this.onScroll}
                    onEndReachedThreshold={0.5}
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps={'handled'}
                />


                <View style={styles.login_btn_view}>
                    <GButton
                        Text={LanguagesIndex.translate('ViewAllRequests')}
                        width={'100%'}
                        height={50}
                        borderRadius={10}
                        onPress={() => { this.goToBookingRequest() }}
                    />
                </View>
            </View>
        );
    }
}

