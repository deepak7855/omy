import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default WelcomeHomeScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    profile_view: {
        alignItems: 'center',
        marginBottom: 45
    },
    blue_radius_view: {
        backgroundColor: Colors.cerulean,
        height: 60,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        width: "100%"
    },
    pro_img_view: {
        height: 82,
        width: 82,
        borderRadius: 20,
        position: 'absolute',
        bottom: -40,
        alignSelf: 'center',
    },
    profile_img: {
        height: '100%',
        width: '100%',
        borderRadius: 20,
    },
    name_txt: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        textAlign: 'center',
        marginVertical: 5,
        fontFamily: fonts.RoBoToMedium_1
    },
    up_text_view: {
        marginHorizontal: 16,
        marginTop: 5
    },
    up_text: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1
    },
    all_grey_txt: {
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    all_black_name_text: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1
    },
    login_btn_view: {
        marginVertical: 10,
        marginTop: 30,
        marginHorizontal: 16
    },
    location_img: {
        width: 8.7,
        height: 12.1
    },
    loc_cal_dog_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 5
    },
    dog_img: {
        width: 13,
        height: 12.2
    },
    timer_img: {
        width: 10.5,
        height: 12,
        marginRight: 10
    },
    booking_user_img_view: {
        height: 32,
        width: 32,
        borderRadius: 10
    },
    booking_user_img: {
        width: '100%',
        height: '100%'
    },
    calendar_img: {
        width: 10.7,
        height: 11.8
    },
    time_flex: {
        flex: 1.5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    booking_view:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    card_touch:{
        marginHorizontal: 16,
        paddingVertical: 12
    },



});