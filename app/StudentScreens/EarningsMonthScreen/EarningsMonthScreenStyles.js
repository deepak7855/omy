import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default EarningsMonthScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteThree
    },
    earning_view: {
        borderRadius: 10,
        marginHorizontal: 16,
        backgroundColor: Colors.cerulean,
        paddingVertical: 10,
        marginTop: 20
    },
    earning_text_view:{
        alignItems: 'center'
    },
    total_earning_text:{
        color: Colors.white,
         fontSize: fonts.fontSize14, 
         lineHeight: 17, 
         fontFamily: fonts.RoBoToMedium_1
    },
    earning_amount_text_view:{
        alignItems: 'center', 
        marginTop: 5
    },
    earning_amount_text:{
        color: Colors.white, 
        fontSize: fonts.fontSize22, 
        lineHeight: 27, 
        fontFamily: fonts.RoBoToBold_1
    },
    latest_text_view:{
        marginHorizontal: 16, 
        marginTop: 15
    },
    latest_text:{
        color:Colors.black,
        fontSize:fonts.fontSize14,
        lineHeight:17,
        fontFamily:fonts.RoBoToMedium_1
    },
    card_view: {
        marginTop: 10,
        backgroundColor: Colors.white,
        paddingVertical: 5
    },
    card_profile_view: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 16,
        justifyContent: 'space-between'
    },
    profile_flex_view: {
        flex: 1.3,
    },
    profile_img_view: {
        width: 35,
        height: 35,
        borderRadius: 10,
    },
    profile_img: {
        width: '100%',
        height: '100%'
    },
    user_name_flex: {
        flex: 6,
    },
    rate_view: {
        flex: 2,
        alignItems: 'flex-end'
    },
    dog_img: {
        width: 13,
        height: 12.2
    },
    calender_img: {
        width: 10.7,
        height: 11.8
    },
    user_name_text:{
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToBold_1
    },
    rate_text:{
        color: Colors.cerulean,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToBold_1
    },
    card_dog_view_btn:{
        marginHorizontal: 16, 
        flexDirection: 'row', 
        paddingVertical: 5
    },
    dog_flex_view:{
        flex: 8,
    },
    card_dog_view:{
        flexDirection: 'row', 
        alignItems: 'center',
    },
    dog_img_view:{
        flex: .8
    },
    dog_title_view:{
        flex: 9.2 
    },
    calender_img_view:{
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: 5
    },
    calender_flex_view:{
        flex: .8
    },
    date_flex_view:{
        flex: 9.2
    },
    redeem_flex_view:{
        flex: 8, 
        marginTop: 5
    },
    redeem_flex_btn_view:{
        flex: 2, 
        alignSelf: 'flex-end'
    },
    redeem_btn_touch:{
        paddingVertical: 3, 
        paddingHorizontal: 10, 
        borderColor: Colors.greyish, 
        borderWidth: 1, 
        borderRadius: 5
    },
    redeem_text:{
        fontSize: fonts.fontSize10,
        fontFamily: fonts.RoBoToMedium_1,
        color: Colors.greyish,
        lineHeight: 13,
    },
    dog_text:{
        fontSize: fonts.fontSize12, 
        color: Colors.warmGrey, 
        fontFamily: fonts.RoBoToRegular_1, 
        lineHeight: 14
    },
    date_text:{
        fontSize: fonts.fontSize12, 
        color: Colors.warmGrey, 
        fontFamily: fonts.RoBoToRegular_1, 
        lineHeight: 16
    },
    redeem_txt:{
        fontSize: fonts.fontSize12, 
        color: Colors.warmGrey, 
        fontFamily: fonts.RoBoToRegular_1, 
        lineHeight: 14
    },
    year_next_pre_view:{
        alignItems: 'center', 
        marginTop: 10, 
        marginHorizontal: 16,
        backgroundColor: Colors.cerulean, 
        flexDirection: 'row', 
        justifyContent: 'space-between',
        paddingVertical: 15, 
        paddingHorizontal: 15, 
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    pre_next_img:{
        height: 16.3,
        width: 8.2
    },
    year_text:{
        fontSize: fonts.fontSize16, 
        color: Colors.white, 
        fontFamily: fonts.RoBoToMedium_1, 
        lineHeight: 19
    },
    earning_card:{
        marginHorizontal: 16, 
        backgroundColor: Colors.white
    },
    earning_card_view:{
        marginHorizontal: 15, 
        flexDirection: 'row',
        justifyContent: 'space-between', 
        marginVertical: 15
    },
    month_name_rate_text:{
        color: Colors.black, 
        fontSize: fonts.fontSize14, 
        fontFamily: fonts.RoBoToMedium_1, 
        lineHeight: 17
    },
    separator_view:{
        height: 1, 
        width: '100%', 
        backgroundColor: Colors.whiteTwo
    },
    






  


});