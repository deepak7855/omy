import React from 'react';
import { Text, View, ScrollView, FlatList, SafeAreaView,Image, DeviceEventEmitter } from 'react-native';
import styles from './EarningsMonthScreenStyles';
import LanguagesIndex from '../../Languages';
import AppHeader from '../../Comman/AppHeader';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants } from '../../Api';
import { handleNavigation } from '../../navigation/Navigation';
import EarningShowView from '../EarningsScreen/EarningShowView';
import NoData from '../../Comman/NoData';

export default class EarningsMonthScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            totalMonthEarning: {},
            monthUserTotal: [],
        }
        AppHeader({ ...this.props.navigation, leftTitle: this.props.route.params.item.month_name + " " +  LanguagesIndex.translate('Earnings'), borderBottomRadius: 25, })

    }

    componentDidMount() {
        this.getMonthWiseEarningsCall()
        this.updateRedum = DeviceEventEmitter.addListener('updateRedum', (data) => {
            this.getMonthWiseEarningsCall()
         })

    }
    componentWillUnmount(){
        this.updateRedum.remove()
    }

    getMonthWiseEarningsCall = () => {
        Helper.globalLoader.showLoader();
        let data = new FormData();
        data.append('year', this.props.route.params.item.year);
        data.append('month', this.props.route.params.item.month);
        //console.log('data----data--->>>',data)
        ApiCall.postMethodWithHeader(Constants.month_wise_earnings, data, Constants.APIImageUploadAndroid).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
               // console.log("response---",response.data)
                this.setState({ totalMonthEarning: response, monthUserTotal: response.data.data })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }

    goToBookingDetails(item) {
        handleNavigation({
            type: 'push', page: 'BookingDetails',
            navigation: this.props.navigation,
            passProps: { booking_id: item.booking_id, booking_service_id: item.booking_service_id }
        })
    }  

    _renderEarDetailsItem = ({ item, index }) => {
        //console.log("item-month---item---->>>>",item)
        return ( 
            <EarningShowView
            navigation={this.props.navigation}
            item={item}
            
            />
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.earning_view}>
                        <View style={styles.earning_text_view}>
                            <Text style={styles.total_earning_text}>{this.props.route.params.item.month_name}{' '}{LanguagesIndex.translate('TotalEarnings')}</Text>
                        </View>
                        <View style={styles.earning_amount_text_view}>
                            <Text style={styles.earning_amount_text}>CHF{" "}{this.state.totalMonthEarning.total_earning}</Text>
                        </View>
                    </View>
                    {this.state.monthUserTotal.length < 1 ?
                        <NoData
                        />
                        : null
                    }
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.monthUserTotal}
                        renderItem={this._renderEarDetailsItem}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </ScrollView>
            </SafeAreaView>
        )
    }

};
