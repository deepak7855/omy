import React from 'react';
import { FlatList, View, Text, Image, SafeAreaView, TouchableOpacity, RefreshControl, DeviceEventEmitter } from 'react-native';
import styles from './BookingUpComingScreenStyles';
import { Constants, ApiCall } from '../../Api';
import ListLoader from '../../Comman/ListLoader';
import NoData from '../../Comman/NoData';
import StudentBookingView from './StudentBookingView';
import LanguagesIndex from '../../Languages';
import { images } from '../../Assets/imagesUrl';
import * as RNLocalize from "react-native-localize";


export default class BookingHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedStatus: 'REJECT',
            index: 0,
            historyList: [],
            next_page_url: '',
            currentPage: 1,
            refreshing: false,
            isLoading: true,
        }
    }

    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));

        this.getBookingRequestApiCall();
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    
    onScroll = () => {
        if (this.state.next_page_url && !this.state.isLoading) {
            this.setState({ currentPage: this.state.currentPage + 1, isLoading: true }, () => {
                this.getBookingRequestApiCall();
            });
        }

    }
    _onRefresh = () => {
        this.setState({ refreshing: true, currentPage: 1 }, () => {
            this.getBookingRequestApiCall();
        });
    }

    changeStatus(status) {
        this.setState({ selectedStatus: status }, () => {
            this._onRefresh();
        })
    }

    getBookingRequestApiCall = () => {
        let data = new FormData();
        data.append('page', this.state.currentPage);
        data.append('type', 'history');
        data.append('status', this.state.selectedStatus);
        ApiCall.postMethodWithHeader(Constants.booking_history, data, Constants.APIImageUploadAndroid).then((response) => {
            if (response.status == Constants.TRUE) {
                if (response.data && response.data.current_page) {
                    this.setState({
                        historyList: this.state.currentPage == 1 ? response.data.data : [...this.state.historyList, ...response.data.data],
                        next_page_url: response.data.next_page_url,
                        refreshing: false,
                        isLoading: false,
                    })
                } else {
                    this.setState({
                        historyList: [],
                        refreshing: false,
                        isLoading: false,
                    })
                }
            } else {
                this.setState({ refreshing: false, isLoading: false })
            }
        })
    }

    _renderItem = ({ item }) => {
        return (
            <StudentBookingView
                order={item}
                type={'history'}
                navigation={this.props.navigation}
            />
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <View style={styles.rej_com_view}>
                    <TouchableOpacity onPress={() => this.changeStatus('REJECT')} style={styles.radio_btn_view}>
                        <Image
                            source={this.state.selectedStatus == 'REJECT' ? images.radio_btn_selected : images.radio_btn_un_selected}
                            style={styles.radio_img} />
                        <Text style={styles.rej_com_txt}>{LanguagesIndex.translate('Rejected')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.changeStatus('COMPLETE')} style={styles.radio_btn_view}>
                        <Image
                            source={this.state.selectedStatus == 'COMPLETE' ? images.radio_btn_selected : images.radio_btn_un_selected}
                            style={styles.radio_img} />
                        <Text style={styles.rej_com_txt}>{LanguagesIndex.translate('Completed')}</Text>
                    </TouchableOpacity>
                </View>
                {!this.state.isLoading && !this.state.refreshing && this.state.historyList.length < 1 ?
                   <View>
                   {this.state.selectedStatus == 'REJECT' ?
                       <NoData message={LanguagesIndex.translate('Youdonthaveanyrejectedbookings')} />
                       : null}
                   {this.state.selectedStatus == 'COMPLETE' ?
                       <NoData message={LanguagesIndex.translate('Youdonthaveanycompletedbookings')} />
                       : null
                   }
               </View>
                    : null
                }
                <FlatList
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    data={this.state.historyList}
                    renderItem={this._renderItem}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                    ListFooterComponent={() => {
                        return (
                            this.state.isLoading ? <ListLoader /> : null
                        )
                    }}
                    onEndReached={this.onScroll}
                    onEndReachedThreshold={0.5}
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps={'handled'}
                />

            </SafeAreaView>
        )
    }

};
