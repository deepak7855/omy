import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, DeviceEventEmitter } from 'react-native';
import styles from './BookingUpComingScreenStyles';
import { images } from '../../Assets/imagesUrl';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';
import Helper from '../../Lib/Helper';
import { Constants } from '../../Api';
import { changeBookingStatus } from '../../Api/BookingApis';
import { handleNavigation } from '../../navigation/Navigation';
import ImageLoadView from '../../Lib/ImageLoadView';
import LanguagesIndex from '../../Languages';
import * as RNLocalize from "react-native-localize";
import TimeShow from '../../Comman/TimeShow';
import moment from 'moment';

export default class StudentBookingView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            maxCountDownTime: 0
        }

    }

    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        if (this.props.type == 'pending') {
            this.checkToShowbtn();
        }
    }

    checkToShowbtn() {
        let { order } = this.props;

        let utcCreateDate = order.booking_request.created_at
        let bookingDate = order.booking_request.booking_date
        let bookingTime = order.booking_request.booking_time
        let combinedBookingDate = bookingDate + ' ' + bookingTime
        let createDate = moment.utc(utcCreateDate, 'YYYY-MM-DD HH:mm:ss').local()

        let strCreateDate = moment(createDate).format('YYYY-MM-DD HH:mm:ss')
        let strBookingDate = moment(combinedBookingDate, 'YYYY-MM-DD hh:mm a').format('YYYY-MM-DD HH:mm:ss')


        let duration = moment.duration(moment(strBookingDate).diff(moment(strCreateDate)));
        let day = duration.days();
        let hours = duration.hours();
        let minutes = duration.minutes();



        let maxCreateDate = null
        if (day >= 7) {
            // add 6 hours
            maxCreateDate = moment(strCreateDate).add(6, 'hour').format('YYYY-MM-DD HH:mm:ss')
        }

        else if (day > 1 || (day == 1 && (hours >= 1 || minutes >= 1))) {
            // add 3 hours
            maxCreateDate = moment(strCreateDate).add(3, 'hour').format('YYYY-MM-DD HH:mm:ss')
        }
        else if (day == 1 || hours >= 6) {
            // add 1 hours
            maxCreateDate = moment(strCreateDate).add(1, 'hour').format('YYYY-MM-DD HH:mm:ss')
        }
        else if (day == 0 || hours >= 24) {
            // add 30 minutes
            maxCreateDate = moment(strCreateDate).add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss')
            console.log("maxCreateDate---", maxCreateDate)
        }

        if (maxCreateDate) {
            let currentDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
            let maxDuration = moment.duration(moment(maxCreateDate).diff(moment(currentDate)));

            if (maxDuration && maxDuration > 0) {
                this.setState({ maxCountDownTime: maxDuration })
            }
        }
    }

    methodCountDownComplete() {
        this.setState({ maxCountDownTime: 0 })
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };


    UpdateBooking = (type) => {
        Helper.confirm(LanguagesIndex.translate('Areyousureyouwantto') + " " + `${type == 'ACCEPT' ? LanguagesIndex.translate('Accept') : LanguagesIndex.translate('Reject')}` + " " + LanguagesIndex.translate('bookingrequest?'), (status) => {
            if (status) {
                let data = new FormData();
                data.append('booking_id', this.props.order.booking_id);
                data.append('booking_service_id', this.props.order.booking_service_id);
                data.append('status', type);

                changeBookingStatus(data, (res) => {
                    if (res.status == Constants.TRUE) {
                        Helper.showToast(res.message)
                        // Helper.showToast(`Booking request ${type == 'ACCEPT' ? 'accepted' : 'rejected'} successfully.`)
                        DeviceEventEmitter.emit('changeBookingStatus', "done")
                        DeviceEventEmitter.emit('CancelBookingStudent', "done")
                        if (this.props.refreshList) {
                            this.props.refreshList()
                        }
                    }
                })
            }
        })
    }


    goToBookingDetails() {
        handleNavigation({ type: 'push', page: 'BookingDetails', navigation: this.props.navigation, passProps: { booking_id: this.props.order.booking_id, booking_service_id: this.props.order.booking_service_id } })
    }

    render() {
        let { order, type } = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.goToBookingDetails()}
                style={styles.up_coming_view}>
                <View style={styles.up_coming_profile_view}>
                    <View style={styles.profile_flex_view}>
                        <View style={styles.profile_img_view}>
                            <ImageLoadView
                                source={order.booking_request.user.profile_picture ? { uri: order.booking_request.user.profile_picture } : images.default}
                                resizeMode={'cover'} style={[styles.profile_img, { borderRadius: 10 }]} />
                        </View>
                    </View>

                    <View style={styles.user_name_flex}>
                        <Text style={styles.user_name_text}>{order.booking_request.user.name}</Text>
                    </View>
                    <View style={styles.rate_view}>
                        {/* <Text numberOfLines={2} style={styles.rate_text}>${Number(order.price) * Number(order.booking_services.activity_duration).toFixed(2)}</Text> */}
                    </View>
                </View>

                <View style={styles.view_text_img_small}>
                    <View style={styles.dog_img_view}>
                        <Image source={{ uri: order.booking_services.activity?.icon }} resizeMode={'contain'} style={styles.dog_img} />
                    </View>

                    <View style={{ flex: 7.7 }}>
                        <Text style={[styles.title_name, { textTransform: 'capitalize' }]}>{order.booking_services.activity?.name}</Text>
                    </View>

                    <View style={{ flex: 1.6, flexDirection: 'row', alignItems: 'center' }}>
                        <View>
                            <Image source={images.timer_ic} resizeMode={'contain'} style={{ height: 12, width: 10.5 }} />
                        </View>
                        <Text style={{ color: Colors.warmGrey, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToRegular_1, lineHeight: 14, marginLeft: 5 }}>{order.booking_services.activity_duration} hrs</Text>
                    </View>
                </View>

                <View style={styles.view_text_img_small}>
                    <View style={{ flex: .4 }}>
                        <Image source={images.calendar_date_ic} resizeMode={'contain'} style={styles.calender_img} />
                    </View>
                    <View style={{ flex: 7.6 }}>
                        {/* <Text style={styles.date_text}>{Helper.formatDate(order.booking_request.booking_date, "DD/MM/YYYY")} | {order.booking_request.booking_time}</Text> */}
                        <Text style={styles.date_text}>{Helper.formatDateUtcToLocal(order.booking_request.booking_date, order.booking_request.booking_time, '')}</Text>
                    </View>
                    {/* {type != 'home' || order?.booking_request.user.user_type == "ENTERPRISE" ?
                    <View style={styles.flex_card_btn_view}>
                            <TouchableOpacity onPress={() => this.UpdateBooking('REJECT')}>
                                <Image source={images.wrong_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.UpdateBooking('ACCEPT')}>
                                <Image source={images.right_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>

                        </View>:
                        null
        } */}
                    {type == 'pending' && order.booking_request.user.user_type == "ENTERPRISE" ?
                        <View style={styles.flex_card_btn_view}>
                            <TouchableOpacity onPress={() => this.UpdateBooking('REJECT')}>
                                <Image source={images.wrong_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>
                 
                            <TouchableOpacity onPress={() => this.UpdateBooking('ACCEPT')}>
                                <Image source={images.right_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>

                        </View> :
                        null}

                    {type == 'pending' && order.booking_request.user.user_type == "BEOMY" && this.state.maxCountDownTime ?
                        <View style={styles.flex_card_btn_view}>
                            <TouchableOpacity onPress={() => this.UpdateBooking('REJECT')}>
                                <Image source={images.wrong_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.UpdateBooking('ACCEPT')}>
                                <Image source={images.right_ic} resizeMode={'contain'}
                                    style={styles.booking_btn} />
                            </TouchableOpacity>

                        </View>
                        : type != 'pending' && type != 'home' ? <View style={{ flex: 3.4, alignItems: 'flex-end', }}>

                            <Text style={{ color: Helper.getBookingStatus(order.status).color, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToRegular_1, textTransform: 'capitalize' }}>{Helper.getBookingStatus(order.status).value} {order.status == 'CANCEL' && order.cancelled_by_name ? `${LanguagesIndex.translate('by')} ${order.cancelled_by_name}` : null}</Text>

                        </View> : null}

                </View>

                <View style={styles.view_text_img_small}>
                    <View style={styles.location_img_view}>
                        <Image source={images.location_pin_ic} resizeMode={'contain'} style={styles.location_img} />
                    </View>
        
                    <View style={styles.location_text_view}>
                        <Text numberOfLines={2} style={styles.location_text}>{order.booking_request.location}</Text>
                    </View>
                    {this.state.maxCountDownTime && order?.booking_request.user.user_type == "BEOMY" ?
                        <View>
                            <TimeShow type={'list'} maxCountDownTime={this.state.maxCountDownTime} timeOutComplete={() => this.methodCountDownComplete()} />
                        </View>
                        : null
                    }
                </View>
            </TouchableOpacity>
        )
    }
}
