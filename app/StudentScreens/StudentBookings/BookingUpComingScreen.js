import React from 'react';
import {
  FlatList,
  SafeAreaView,
  RefreshControl,
  DeviceEventEmitter,
} from 'react-native';
import styles from './BookingUpComingScreenStyles';
import {Constants, ApiCall} from '../../Api';
import ListLoader from '../../Comman/ListLoader';
import NoData from '../../Comman/NoData';
import StudentBookingView from './StudentBookingView';
import LanguagesIndex from '../../Languages';
import * as RNLocalize from 'react-native-localize';

export default class BookingUpComingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      upcomingList: [],
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: true,
    };
  }

  componentDidMount() {
    this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
      RNLocalize.addEventListener(
        'change',
        this.handleLocalizationChange(LanguagesIndex.MyLanguage),
      );
    });
    RNLocalize.addEventListener(
      'change',
      this.handleLocalizationChange(LanguagesIndex.MyLanguage),
    );

    this.getDate = DeviceEventEmitter.addListener(
      'CancelBookingStudent',
      (data) => {
        this.getBookingRequestApiCall();
      },
    );
    this.getBookingRequestApiCall();
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = (lang) => {
    LanguagesIndex.setI18nConfig(lang);
    this.forceUpdate();
  };

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState(
        {currentPage: this.state.currentPage + 1, isLoading: true},
        () => {
          this.getBookingRequestApiCall();
        },
      );
    }
  };
  _onRefresh = () => {
    this.setState({refreshing: true, currentPage: 1}, () => {
      this.getBookingRequestApiCall();
    });
  };

  getBookingRequestApiCall = () => {
    let data = new FormData();

    data.append('page', this.state.currentPage);
    data.append('type', 'upcomming');

    ApiCall.postMethodWithHeader(
      Constants.booking_history,
      data,
      Constants.APIImageUploadAndroid,
    ).then((response) => {
      if (response.status == Constants.TRUE) {
        if (response.data && response.data.current_page) {
          this.setState({
            upcomingList:
              this.state.currentPage == 1
                ? response.data.data
                : [...this.state.upcomingList, ...response.data.data],
            next_page_url: response.data.next_page_url,
            refreshing: false,
            isLoading: false,
          });
        } else {
          this.setState({
            upcomingList: [],
            refreshing: false,
            isLoading: false,
          });
        }
      } else {
        this.setState({refreshing: false, isLoading: false});
      }
    });
  };

  _renderItem = ({item}) => {
    return (
      <StudentBookingView
        order={item}
        type={'upcoming'}
        navigation={this.props.navigation}
      />
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        {!this.state.isLoading &&
        !this.state.refreshing &&
        this.state.upcomingList.length < 1 ? (
          <NoData
            message={LanguagesIndex.translate('Youdonthaveanyexperiences')}
          />
        ) : null}
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          data={this.state.upcomingList}
          renderItem={this._renderItem}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={() => {
            return this.state.isLoading ? <ListLoader /> : null;
          }}
          onEndReached={this.onScroll}
          onEndReachedThreshold={0.5}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={'handled'}
        />
      </SafeAreaView>
    );
  }
}
