import React, { Component } from 'react'
import { Text, View, ScrollView, FlatList, Image, TouchableOpacity, SafeAreaView,DeviceEventEmitter } from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import styles from './BookingUpComingScreenStyles';
import BookingUpComingScreen from './BookingUpComingScreen';
import AppHeader from '../../Comman/AppHeader';
import Colors from '../../Assets/Colors';
import LanguagesIndex from '../../Languages';
import BookingHistory from './BookingHistory';
import { handleNavigation } from '../../navigation/Navigation';
import * as RNLocalize from "react-native-localize";

export default class StudentBookings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            arrComingRequest: [],
            page: 1,
        }

        AppHeader({
            ...this.props.navigation, leftTitle:LanguagesIndex.translate('Experiences'), borderBottomRadius: 25,
            bellIcon: true, settingsIcon: true,
            hideLeftBackIcon: true,
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })

    }
    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    componentDidMount(){
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            AppHeader({
                ...this.props.navigation, leftTitle:LanguagesIndex.translate('Experiences'), borderBottomRadius: 25,
                bellIcon: true, settingsIcon: true,
                hideLeftBackIcon: true,
                bellIconClick: () => this.bellIconClick(),
                settingIconClick: () => this.settingIconClick()
            })
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    
    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <View style={styles.up_his_btn_view}>
                    <TouchableOpacity
                        onPress={() => { this.setState({ index: 0 }), this.viewpager.setPage(0) }}
                        style={[styles.up_coming_touch, {
                            borderColor: this.state.index == 0 ? Colors.white : Colors.cerulean,
                            backgroundColor: this.state.index == 0 ? Colors.cerulean : Colors.white
                        }]}>
                        <Text style={[styles.up_coming_text, {
                            color: this.state.index == 0 ? Colors.white : Colors.cerulean,
                        }]}>{LanguagesIndex.translate('Upcoming')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { this.setState({ index: 1 }), this.viewpager.setPage(1) }}
                        style={[styles.history_touch, {
                            backgroundColor: this.state.index == 1 ? Colors.cerulean : Colors.white,
                            borderColor: this.state.index == 0 ? Colors.cerulean : Colors.white
                        }]}>
                        <Text style={[styles.history_text, {
                            color: this.state.index == 0 ? Colors.cerulean : Colors.white,
                        }]}>{LanguagesIndex.translate('History')}</Text>
                    </TouchableOpacity>
                </View>
                <ViewPager
                    ref={(viewpager) => this.viewpager = viewpager}
                    style={styles.view_pager}
                    initialPage={0}
                    scrollEnabled={false}
                    onPageSelected={(e) => this.setState({ index: e.nativeEvent.position })}>

                    <View style={styles.view_pager} key="1">
                        <BookingUpComingScreen
                            navigation={this.props.navigation}
                        />
                    </View>

                    <View style={styles.view_pager} key="2">
                        <BookingHistory
                            navigation={this.props.navigation}
                        />
                    </View>
                </ViewPager>
            </SafeAreaView>
        )
    }
}
