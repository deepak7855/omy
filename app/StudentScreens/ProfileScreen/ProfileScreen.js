import React from 'react';
import { View, Image, Text, FlatList, Dimensions,DeviceEventEmitter } from 'react-native';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import styles from './ProfileScreenStyles';
import IconInput from '../../Comman/GInput';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import AppHeader from '../../Comman/AppHeader';
import { TouchableOpacity } from 'react-native-gesture-handler';
import fonts from '../../Assets/fonts';
import Helper from '../../Lib/Helper';
import moment from 'moment';
import ImageLoadView from '../../Lib/ImageLoadView';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import { ApiCall, Constants } from '../../Api';
import * as RNLocalize from "react-native-localize";

const ScreenWidth = Dimensions.get('screen').width;
export default class ProfileScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            availableSlots: [],
            StudentDetails: {},
            slots: [],

        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('Profile'),
            hideLeftBackIcon: true,
            bellIcon: true, settingsIcon: true, profileIcon: true,
            profileIconClick: () => this.profileIconClick(),
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })
    }

    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            AppHeader({
                ...this.props.navigation, leftTitle: LanguagesIndex.translate('Profile'),
                hideLeftBackIcon: true,
                bellIcon: true, settingsIcon: true, profileIcon: true,
                profileIconClick: () => this.profileIconClick(),
                bellIconClick: () => this.bellIconClick(),
                settingIconClick: () => this.settingIconClick()
            })
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            Helper.updateUserData();
            this.methodFillForm();
            this.getSlotsApiCall();
        });
    }

    getSlotsApiCall = () => {
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_slots, data, Constants.APIGet).then((response) => {
            if (response.status == Constants.TRUE) {
                this.setState({ availableSlots: response.data })
            }
        })
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
        this._unsubscribe();
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };
    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    profileIconClick() {
        handleNavigation({ type: 'push', page: 'EditProfileScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToSetupProfile() {
        handleNavigation({ type: 'push', page: 'EditProfileScreen', navigation: this.props.navigation })
    }

    methodFillForm() {
        if (Helper.user_data) {
            this.setState({ StudentDetails: Helper.user_data });
        }
    }

    _renderCheckItem = ({ item, index }) => {
        return (
            <View style={styles.checkboxViewcss}>
                <View style={styles._samViewTextIc}>
                    <View
                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                        onPress={() => { }}>
                        <Image source={images.check} style={styles.samImgCss} />
                    </View>
                    <Text style={styles.samGreyTextCss}>{item.myUserActivity}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text numberOfLines={1} style={styles.samGreyTextCss}>CHF {item.price}</Text>
                </View>
            </View>
        )
    }

    gotoMediaDetail(item) {
        this.props.navigation.navigate('MediaDetailPage', { item })
    }

    _renderUploadItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.gotoMediaDetail(item)} style={{ marginRight: 10, marginTop: 12, }}>
                <View style={{ height: 82, width: 82, }}>
                    <ImageLoadView
                        source={{ uri: item.media_type == 'video' ? item.media_thumb : item.media_url }}
                        resizeMode={'cover'}
                        style={{ width: '100%', height: '100%' }}
                    />
                </View>
                {item.media_type == 'video' ?
                    <View style={styles.blurImgViewCss}>
                        <Image source={images.play}
                            style={{ height: 25, width: 25 }} />
                    </View>
                    : null}
            </TouchableOpacity>
        )
    }

    _renderDateItem = ({ item, index }) => {
        return (
            <View style={styles.main_slot_view}>
                <View
                    style={{ flexDirection: 'row', marginVertical: 5 }}>
                    <Image source={this.state.showSun ? images.unchecked : images.check} style={styles.samImgCss} />
                    <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{item.mon_to_fri == 2 ? "Manoday To Friday" : ""}</Text>
                </View>
                <View style={styles.slot_view}>
                    <View>
                        <Image source={images.check}
                            style={styles.samImgCss} />
                    </View>
                    <Text style={styles.slot_text}>{item?.slot?.slot}</Text>
                </View>
            </View>
        )
    }

    getSlotName(id) {
        let slotName = '';
        this.state.availableSlots.forEach(elementNew => {
            if (Number(id) == Number(elementNew.id)) {
                slotName = elementNew.slot;
            }
        });
        return slotName;
    }

    render() {

        return (
            <View style={styles.safe_area_view}>
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.profile_view}>
                        <View style={styles.profile_img_view}>
                            <ImageLoadView
                                resizeMode={'cover'}
                                source={this.state.StudentDetails.profile_picture ? { uri: this.state.StudentDetails.profile_picture } : images.default}
                                style={styles.profile_img} />
                        </View>
                    </View>

                    <View style={styles.input_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('Name')}
                            placeholderTextColor={Colors.greyishBrown}
                            inputedit={false}
                            value={this.state.StudentDetails.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            placeholderTextColor={Colors.greyishBrown}
                            inputedit={false}
                            value={this.state.StudentDetails.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('EmailUniversityEmailOnly')}
                            placeholderTextColor={Colors.greyishBrown}
                            inputedit={false}
                            value={this.state.StudentDetails.email}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('NameOfInstitution/Department')}
                            placeholderTextColor={Colors.greyishBrown}
                            inputedit={false}
                            value={this.state.StudentDetails.university}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('Department')}
                            placeholderTextColor={Colors.greyishBrown}
                            inputedit={false}
                            value={this.state.StudentDetails.departement}
                        />

                        {/* <IconInput
                            placeholder={LanguagesIndex.translate('Location')}
                            placeholderTextColor={Colors.greyishBrown}
                            inputedit={false}
                            value={this.state.StudentDetails.location}
                        /> */}

                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            placeholderTextColor={Colors.greyishBrown}
                            inputedit={false}
                            value={this.state.StudentDetails.street_no}
                        />

                        <View style={styles.input_zip_city_view}>
                            <View style={styles.input_zip_view}>
                                <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    placeholderTextColor={Colors.greyishBrown}
                                    inputedit={false}
                                    value={this.state.StudentDetails.postcode}
                                />
                            </View>

                            <View style={styles.input_city_view}>
                                <View
                                    style={{
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.StudentDetails.city ? this.state.StudentDetails.city : LanguagesIndex.translate('City')}</Text>
                                </View>


                                {/* <IconInput
                                    maxLength={15}
                                    placeholder={LanguagesIndex.translate('City')}
                                    placeholderTextColor={Colors.greyishBrown}
                                    inputedit={false}
                                    value={this.state.StudentDetails.city}
                                /> */}
                            </View>
                        </View>
                    </View>

                    {this.state.StudentDetails.about_me ?

                        <View style={{
                            borderBottomWidth: 8, borderBottomColor: '#f8f8f8', padding: 15, marginTop: 10
                        }}>
                            <Text style={{
                                color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1
                            }}>{LanguagesIndex.translate('AboutMe')}</Text>
                            <View style={{
                                borderRadius: 8,
                                borderWidth: 1,
                                borderColor: '#dbdbdb',
                                marginTop: 10
                            }}>
                                <View>
                                    <Text
                                        style={{
                                            paddingVertical: 5,
                                            minHeight: 75,
                                            //height: 75, 
                                            paddingHorizontal: 10,
                                            color: Colors.warmGrey,
                                            fontSize: fonts.fontSize12,
                                            fontFamily: fonts.RoBoToRegular_1
                                        }}>
                                        {this.state.StudentDetails.about_me == 'null' ? "" : this.state.StudentDetails.about_me}

                                    </Text>
                                </View>
                            </View>
                        </View> : null
                    }

                    {/* {this.state.StudentDetails.about_me == "undefined" || this.state.StudentDetails.about_me == null || this.state.StudentDetails.about_me == "null" ?
                        null
                        : <View style={styles.lineBorderCss}>
                            <Text style={styles.samTextCss}>{LanguagesIndex.translate('AboutMe')}</Text>
                            <View style={{ marginTop: 10 }}>
                                <Text style={styles.samGreyTextCss}>{this.state.StudentDetails.about_me}</Text>
                            </View>
                        </View>} */}
                    {this.state.StudentDetails && this.state.StudentDetails.user_activity && this.state.StudentDetails.user_activity.length > 0 ?
                        <View style={styles.lineBorderCss}>
                            <Text style={styles.samTextCss}>{LanguagesIndex.translate('Activities&Price')}</Text>
                            <FlatList
                                data={this.state.StudentDetails.user_activity}
                                renderItem={this._renderCheckItem}
                                extraData={this.state}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                        : null}

                    {/* {this.state.StudentDetails && this.state.StudentDetails.location ?
                        <View style={styles.lineBorderCss}>
                            <Text style={styles.samTextCss}>{LanguagesIndex.translate('Location')}</Text>
                            <View style={styles._samViewTextIc}>
                                <Text style={[styles.samGreyTextCss, { marginTop: 10 }]}>{this.state.StudentDetails.location}</Text>
                            </View>
                        </View>
                        : null} */}
                    {this.state.StudentDetails && this.state.StudentDetails.user_media && this.state.StudentDetails.user_media.length > 0 ?
                        <View style={styles.lineBorderCss}>
                            <View style={styles._samViewTextIc}>
                                <Text style={[styles.samTextCss]}>{LanguagesIndex.translate('UploadImages/Videos')}</Text>
                            </View>

                            <FlatList
                                horizontal
                                data={this.state.StudentDetails.user_media}
                                renderItem={this._renderUploadItem}
                                showsHorizontalScrollIndicator={false}
                                extraData={this.state}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                        : null}


                    {this.state.StudentDetails.german_lang_pref || this.state.StudentDetails.french_lang_pref || this.state.StudentDetails.english_lang_pref ?
                        <View style={styles.lineBorderCss}>
                            {this.state.StudentDetails.german_lang_pref || this.state.StudentDetails.french_lang_pref || this.state.StudentDetails.english_lang_pref ?
                                <Text style={styles.samTextCss}>{LanguagesIndex.translate('LanguagePreference')}</Text> : null}
                            {this.state.StudentDetails.german_lang_pref ?
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                    <View style={{ flex: 7.7, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image resizeMode={'contain'} source={images.check} style={styles.samImgCss} />
                                        <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('German')}</Text>
                                    </View>
                                    <View style={{ flex: 2.3, }}>
                                        <Text style={[styles.samGreyTextCss, { textTransform: 'capitalize' }]}>{this.state.StudentDetails.german_lang_pref}</Text>
                                    </View>
                                </View>
                                : null
                            }


                            {this.state.StudentDetails.french_lang_pref ?
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                    <View style={{ flex: 7.7, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image resizeMode={'contain'} source={images.check} style={styles.samImgCss} />
                                        <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('French')}</Text>
                                    </View>
                                    <View style={{ flex: 2.3, }}>
                                        <Text style={[styles.samGreyTextCss, { textAlign: 'left', textTransform: 'capitalize', }]}>{this.state.StudentDetails.french_lang_pref}</Text>
                                    </View>

                                </View>
                                : null
                            }

                            {this.state.StudentDetails.english_lang_pref ?

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                                    <View style={{ flex: 7.7, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image resizeMode={'contain'} source={images.check} style={styles.samImgCss} />
                                        <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('English')}</Text>
                                    </View>
                                    <View style={{ flex: 2.3, }}>
                                        <Text style={[styles.samGreyTextCss, { textTransform: 'capitalize' }]}>{this.state.StudentDetails.english_lang_pref}</Text>
                                    </View>
                                </View>
                                : null
                            }

                        </View> : null}


                    <View style={styles.lineBorderCss}>
                        <Text style={styles.samTextCss}>{LanguagesIndex.translate('Visibility')}</Text>
                        <View style={{ marginTop: 10 }}>
                            <Text style={styles.samGreyTextCss}>{Number(this.state.StudentDetails.visibility) ? LanguagesIndex.translate('Visible') : LanguagesIndex.translate('NotVisible')}</Text>
                        </View>
                    </View>

                    {Helper.user_data.userslots && Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].mon_to_fri ||
                        Helper.user_data.userslots && Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].sat ||
                        Helper.user_data.userslots && Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].sun
                        ?
                        <View style={{ padding: 15 }}>
                            <Text style={styles.samTextCss}>{LanguagesIndex.translate('Availability')}</Text>
                            {Helper.user_data.userslots && Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].mon_to_fri ?
                                <View style={{ width: "100%", marginVertical: 5, }}>
                                    <View
                                        style={{ flexDirection: 'row', marginHorizontal: 15, }}>
                                        <Image source={images.check} style={styles.samImgCss} />
                                        <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('MondayToFriday')}</Text>
                                    </View>
                                    <View style={{ marginLeft: 20 }}>
                                        <FlatList
                                            style={{ padding: 15 }}
                                            data={this.state.availableSlots}
                                            renderItem={(newitem) => {
                                                return (
                                                    <View
                                                        style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                        <Image source={Helper.user_data.userslots[0].mon_to_fri.split(',').map(n => parseInt(n, 10)).includes(newitem.item.id) ? images.check : images.unchecked} style={styles.samImgCss} />
                                                        <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                                    </View>
                                                )
                                            }}
                                            extraData={this.state}
                                            showsVerticalScrollIndicator={false}
                                            keyExtractor={(item, index) => index.toString()}
                                        />
                                    </View>
                                </View>
                                : null}
                            {Helper.user_data.userslots && Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].sat ?
                                <View style={{ width: "100%", marginVertical: 5 }}>
                                    <View
                                        style={{ flexDirection: 'row', marginHorizontal: 15, }}>
                                        <Image source={images.check} style={styles.samImgCss} />
                                        <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Saturday')}</Text>
                                    </View>
                                    <View style={{ marginLeft: 20 }}>
                                        <FlatList
                                            style={{ padding: 15 }}
                                            data={this.state.availableSlots}
                                            renderItem={(newitem) => {
                                                return (
                                                    <View
                                                        style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                        <Image source={Helper.user_data.userslots[0].sat.split(',').map(n => parseInt(n, 10)).includes(newitem.item.id) ? images.check : images.unchecked} style={styles.samImgCss} />
                                                        <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                                    </View>
                                                )
                                            }}
                                            extraData={this.state}
                                            showsVerticalScrollIndicator={false}
                                            keyExtractor={(item, index) => index.toString()}
                                        />
                                    </View>
                                </View>
                                : null}
                            {Helper.user_data.userslots && Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].sun ?
                                <View style={{ width: "100%", marginVertical: 5 }}>
                                    <View
                                        style={{ flexDirection: 'row', marginHorizontal: 15, }}>
                                        <Image source={images.check} style={styles.samImgCss} />
                                        <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Sunday')}</Text>
                                    </View>
                                    <View style={{ marginLeft: 20 }}>
                                        <FlatList
                                            style={{ padding: 15 }}
                                            data={this.state.availableSlots}
                                            renderItem={(newitem) => {
                                                return (
                                                    <View
                                                        style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                        <Image source={Helper.user_data.userslots[0].sun.split(',').map(n => parseInt(n, 10)).includes(newitem.item.id) ? images.check : images.unchecked} style={styles.samImgCss} />
                                                        <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                                    </View>
                                                )
                                            }}
                                            extraData={this.state}
                                            showsVerticalScrollIndicator={false}
                                            keyExtractor={(item, index) => index.toString()}
                                        />
                                    </View>
                                </View>
                                : null}
                        </View> : null}





                    {/* <View style={{ width: "100%", marginVertical: 5 }}>
                        <TouchableOpacity
                            // onPress={() => this.enableWeekView('showMonFri')}
                            style={{ flexDirection: 'row', marginHorizontal: 15, marginVertical: 5 }}>
                            <Image source={this.state.showMonFri ? images.check : images.unchecked} style={styles.samImgCss} />
                            <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('MondayToFriday')}</Text>
                        </TouchableOpacity>
                        {this.state.showMonFri ?
                            <View style={{ marginLeft: 20 }}>
                                <FlatList
                                    style={{ padding: 15 }}
                                    data={Helper.user_data.userslots}
                                    renderItem={(newitem) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => this.chooseWeekSlot('mon_to_fri', newitem.item.id)}
                                                style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                <View>
                                                    <Image source={this.state.mon_to_fri == newitem.item.id ? images.check : images.unchecked} style={styles.samImgCss} />
                                                </View>
                                                <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                    extraData={this.state}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null}
                    </View>

                    <View style={{ width: "100%", marginVertical: 5 }}>
                        <TouchableOpacity
                            // onPress={() => this.enableWeekView('showSat')}
                            style={{ flexDirection: 'row', marginHorizontal: 15, marginVertical: 5 }}>
                            <Image source={this.state.showSat ? images.check : images.unchecked} style={styles.samImgCss} />
                            <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Saturday')}</Text>
                        </TouchableOpacity>
                        {this.state.showSat ?
                            <View style={{ marginLeft: 20 }}>
                                <FlatList
                                    style={{ padding: 15 }}
                                    data={this.state.availableSlots}
                                    renderItem={(newitem) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => this.chooseWeekSlot('sat', newitem.item.id)}
                                                style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                <View>
                                                    <Image source={this.state.sat == newitem.item.id ? images.check : images.unchecked} style={styles.samImgCss} />
                                                </View>
                                                <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                    extraData={this.state}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null}
                    </View>

                    <View style={{ width: "100%", marginVertical: 5, }}>
                        <TouchableOpacity
                            // onPress={() => this.enableWeekView('showSun')}
                            style={{ flexDirection: 'row', marginHorizontal: 15, marginVertical: 5 }}>
                            <Image source={this.state.showSun ? images.check : images.unchecked} style={styles.samImgCss} />
                            <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Sunday')}</Text>
                        </TouchableOpacity>
                        {this.state.showSun ?
                            <View style={{ marginLeft: 20 }}>
                                <FlatList
                                    style={{ padding: 15 }}
                                    data={this.state.availableSlots}
                                    renderItem={(newitem) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => this.chooseWeekSlot('sun', newitem.item.id)}
                                                style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                <View>
                                                    <Image source={this.state.sun == newitem.item.id ? images.check : images.unchecked} style={styles.samImgCss} />
                                                </View>
                                                <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                    extraData={this.state}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null}
                    </View> */}







                    <View style={{ marginHorizontal: 16, marginTop: 30 }}>
                        <TouchableOpacity
                            onPress={() => this.goToSetupProfile()}
                            style={{ width: '100%', height: 50, borderRadius: 10, borderWidth: 1, borderColor: Colors.cerulean, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: Colors.cerulean, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1, lineHeight: 17 }}>
                                {LanguagesIndex.translate('SETUP_PROFILE')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardScroll>
            </View >
        )
    }

};





