import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default ProfileScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    profile_view: {
        backgroundColor: Colors.cerulean,
        height: 50,
        // paddingVertical: 35, 
        width: '100%',
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25
    },
    profile_img_view: {
        height: 82,
        width: 82,
        borderRadius: 20,
        position: 'absolute',
        bottom: -40,
        alignSelf: 'center'
    },
    profile_img: {
        height: '100%',
        width: '100%',
        borderRadius: 20,
    },
    input_view: {
        // marginVertical: 10,
        marginTop: 60,
        marginHorizontal: 16,paddingBottom:8, borderBottomWidth: 1, borderBottomColor: Colors.greyBorder
    },
    input_zip_city_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input_zip_view: {
        flex: 4,
        marginRight: 5
    },
    input_city_view: {
        flex: 4,
        marginLeft: 5
    },
    slot_text:{
        color: Colors.warmGrey, 
        fontSize: fonts.fontSize12, 
        fontFamily: fonts.RoBoToRegular_1, 
        marginBottom: 10
    },
    slot_view:{
        flexDirection: 'row', 
        marginVertical: 5, 
        marginTop: 10,
        marginHorizontal:10,
    },
    main_slot_view:{
        paddingVertical: 10, 
        flex: 1
    },
    lineBorderCss: { borderBottomWidth: 8, borderBottomColor: '#f8f8f8', padding: 15 },
    samTextCss: { color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1 },
    samGreyTextCss: { color: Colors.warmGrey, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToRegular_1 },
    checkboxViewcss: { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 12 },
    _samViewTextIc: { flexDirection: 'row', alignItems: 'center' },
    samImgCss: { height: 20, width: 20, resizeMode: 'contain', marginRight: 10 },
    uploadImgCss: { height: 82, width: 82,  },
    blurImgViewCss: { position: 'absolute', alignSelf: 'center', height: "100%", width: "100%", alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba( 0, 0, 0, 0.5 )', },
    sliderMainVewCss: { flexDirection: 'row', alignItems: 'center', },
});