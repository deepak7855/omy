import React, { PureComponent } from 'react'
import { Text, View, Image, TouchableOpacity, DeviceEventEmitter } from 'react-native'
import { images } from '../../Assets/imagesUrl'
import styles from './EarningsScreenStyles';
import ImageLoadView from '../../Lib/ImageLoadView';
import moment from 'moment';
import Colors from '../../Assets/Colors';
import LanguagesIndex from '../../Languages';
import { Constants, ApiCall } from '../../Api';
import Helper from '../../Lib/Helper';
import { handleNavigation } from '../../navigation/Navigation';

export default class EarningShowView extends PureComponent {


    constructor(props) {
        super(props)
        this.state = {
            showMore: false
        }
    }

    checkRemainingTime(booking_date) {
        let formatBookingDate = moment(booking_date).format('YYYY-MM-DD');
        let afterDays_date = moment(formatBookingDate, "YYYY-MM-DD").add(15, 'days');
        let afterDays_dateFormat = moment(afterDays_date).format('YYYY-MM-DD');
        let currentDate = moment(new Date()).format('YYYY-MM-DD');

        let admission = moment(afterDays_dateFormat, 'YYYY-MM-DD');
        let discharge = moment(currentDate, 'YYYY-MM-DD');
        return admission.diff(discharge, 'days');
    }

    methodRedeemRequest(val) {
       // console.log('val-------val======val+++val',val)
        Helper.confirm(LanguagesIndex.translate('Areyousureyouwanttorequestforredeem'), (statusVal) => {
            if (statusVal) {
                this.redeemRequest(val)
            }
        })
    }

    redeemRequest = (val) => {
        let { item } = this.props;
        let data = {
            booking_id: item.booking_id,
            booking_service_id: item.booking_service_id
        }
       // console.log("data===___data+++++",data)
        Helper.globalLoader.showLoader();
        ApiCall.postMethodWithHeader(Constants.redeem_request, JSON.stringify(data), Constants.APIPost).then((response) => {
           Helper.globalLoader.hideLoader();
           DeviceEventEmitter.emit("updateRedum", "done")
            if (response.status == Constants.TRUE) {

                Helper.showToast(response.message)
            }
            else {
               // Helper.globalLoader.hideLoader()
                Helper.showToast(response.message)
            }
        })
    }

    goToCancelChargesStudent() {
        handleNavigation({ type: 'push', page: 'CancelChargesStudent', navigation: this.props.navigation })
    }

    render() {
        let { item } = this.props;
       // console.log("item_booking_data====booking---data", item)
        return (
            <View style={styles.card_view}>
                <View style={styles.card_profile_view}>
                    <View style={styles.profile_flex_view}>
                        <View style={styles.profile_img_view}>
                            <ImageLoadView
                                source={item.booking_request.user.profile_picture ? { uri: item.booking_request.user.profile_picture } : images.default}
                                resizeMode={'cover'} style={[styles.profile_img, { borderRadius: 10 }]} />
                        </View>
                    </View>

                    <View style={styles.user_name_flex}>
                        <Text style={styles.user_name_text}>{item.booking_request.user.name}</Text>
                    </View>

                    <View style={styles.rate_view}>
                        <Text style={styles.rate_text}>CHF {item.price}</Text>
                    </View>
                </View>

                <View style={styles.card_dog_view_btn}>
                    <View style={styles.dog_flex_view}>
                        <View style={styles.card_dog_view}>
                            <View style={styles.dog_img_view}>
                                <Image
                                    source={{ uri: item.booking_services.activity.icon }}
                                    resizeMode={'contain'} style={styles.dog_img} />
                            </View>
                            <View style={styles.dog_title_view}>
                                <Text style={[styles.dog_text, { textTransform: 'capitalize' }]}>{item.booking_services.activity.name}</Text>
                            </View>
                        </View>

                        <View style={styles.calender_img_view}>
                            <View style={styles.calender_flex_view}>
                                <Image source={images.calendar_date_ic} resizeMode={'contain'} style={styles.calender_img} />
                            </View>
                            <View style={styles.date_flex_view}>
                                <Text style={styles.date_text}>
                                    {Helper.formatDateUtcToLocal(item.booking_request.booking_date,item.booking_request.booking_time,'')
}
                                    {/* {Helper.formatDate(item.booking_request.booking_date, "DD/MM/YYYY")} | {item.booking_request.booking_time} */}
                                    </Text>
                            </View>
                        </View>

                        <View style={styles.calender_img_view}>
                            <View style={styles.date_flex_view}>
                                <Text style={styles.date_text}>{Number(item.price) > 0 ?`${LanguagesIndex.translate('Completedby')} ${item.booking_request?.user.name}`:`${LanguagesIndex.translate('Cancelledby')} ${item.cancelled_by_name}`}</Text>
                            </View>
                        </View>

                        <View style={styles.calender_img_view}>
                            <View style={styles.date_flex_view}>
                                <Text style={styles.date_text}>{LanguagesIndex.translate('ActivityDuration')}:- {item.booking_services?.activity_duration}hr</Text>
                            </View>
                        </View>

                        {/* <View style={styles.calender_img_view}>
                            <View style={styles.date_flex_view}>
                                <Text style={styles.date_text}>{LanguagesIndex.translate('BookingAmount')}:- CHF {item.booking_request.transaction?.amount}</Text>
                            </View>
                        </View> */}

                        {Number(item.price) < 0 ?
                            <View style={styles.calender_img_view}>
                                <View style={styles.date_flex_view}>
                                    <Text style={styles.date_text}>{LanguagesIndex.translate('CancelTime')}:- {moment.utc(item.updated_at).local().format('DD/MM/YYYY | h:m A')}</Text>
                                </View>
                            </View>
                            : null}

                        {Number(item.price) > 0 ?
                            <View style={styles.redeem_flex_view}>
                                <Text style={styles.redeem_txt}>{this.checkRemainingTime(item.booking_date) > 0 ? `${this.checkRemainingTime(item.booking_date)}` + " " + LanguagesIndex.translate('dayslefttoredeem') : LanguagesIndex.translate('Redeemyourmoneynow')}</Text>
                            </View>
                            :
                            <View style={styles.redeem_flex_view}>
                                <TouchableOpacity
                                    onPress={() => this.goToCancelChargesStudent()} >
                                    <Text style={styles.redeem_txt}>{LanguagesIndex.translate('Forcancellationpolicyandcharges')}{" "}<Text style={{ color: Colors.cerulean, lineHeight: 15, textDecorationLine: 'underline' }}>{LanguagesIndex.translate('clickhere')}</Text></Text>
                                </TouchableOpacity>
                            </View>
                        }
                    </View>
                    {Number(item.price) > 0 ?
                        <View style={styles.redeem_flex_btn_view}>
                            <TouchableOpacity
                                onPress={() => this.checkRemainingTime(item.booking_date) > 0 || item.redeem_status == 1 ? null : this.methodRedeemRequest(item.booking_date)}
                                //onPress={() => this.methodRedeemRequest(item)}
                                style={[styles.redeem_btn_touch, { borderColor: this.checkRemainingTime(item.booking_date) > 0 || item.redeem_status == 1 ? Colors.greyish : Colors.cerulean }]}>
                                <Text style={[styles.redeem_text, { color: this.checkRemainingTime(item.booking_date) > 0 || item.redeem_status == 1 ? Colors.greyish : Colors.cerulean }]}>{LanguagesIndex.translate('REDEEM')}</Text>
                            </TouchableOpacity>

                        </View>
                        : null}
                </View>
            </View>
        )
    }
}
