import React from 'react';
import { Text, View, ScrollView, Image, RefreshControl, TouchableOpacity, FlatList, SafeAreaView, DeviceEventEmitter } from 'react-native';
import styles from './EarningsScreenStyles';
import { images } from '../../Assets/imagesUrl';
import LanguagesIndex from '../../Languages';
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import { ApiCall, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import NoData from '../../Comman/NoData'; 
import EarningShowView from './EarningShowView';
import * as RNLocalize from "react-native-localize"; 
import moment from 'moment/min/moment-with-locales';

export default class EarningsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            currentYear: moment().year(),
            earnings: {},
            bookingCompletedUser: [],
            yearEarning: [],
            arrYear: []
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('Earnings'),
            borderBottomRadius: 25, bellIcon: true, settingsIcon: true,
            hideLeftBackIcon: true,
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })

    }

    componentDidMount() {
        
        this.setListofYear()
        this.updateRedum = DeviceEventEmitter.addListener('updateRedum', (data) => {
           this.getEarningsApiCall()
        })
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            AppHeader({
                ...this.props.navigation, leftTitle: LanguagesIndex.translate('Earnings'),
                borderBottomRadius: 25, bellIcon: true, settingsIcon: true,
                hideLeftBackIcon: true,
                bellIconClick: () => this.bellIconClick(),
                settingIconClick: () => this.settingIconClick()
            })
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        this.getEarningsApiCall()
        this.getYearWiseEarningsCall()
    }

    setListofYear() {
        let allYears = [];
        let dateStart = moment().subtract(5, 'y')
        let dateEnd = moment().add(0, 'y')
        while (dateEnd.diff(dateStart, 'years') >= 0) {
            allYears.push({ year: dateStart.format('YYYY') })
            dateStart.add(1, 'year')
        }
        this.setState({ arrYear: allYears }, () => {
             setTimeout(() => {
                this.yearFlatList.scrollToIndex({ animated: true, index: 5 });
             }, 500);
        })
    }

    _onRefresh = () => {
        this.getEarningsApiCall()
        this.getYearWiseEarningsCall()
    }

    componentWillUnmount() {
        this.updateRedum.remove()
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };
    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToEarningMonths(item) {
        handleNavigation({
            type: 'push', page: 'EarningsMonthScreen', navigation: this.props.navigation,
            passProps: {
                item: {
                    month: item.month,
                    month_name: moment(item.month_name,'MMMM').locale(LanguagesIndex.MyLanguage).format('MMM'),
                    year: this.state.currentYear,
                }
            }
        }
        )
    }


    getEarningsApiCall = () => {
        Helper.globalLoader.showLoader();
        let data = {}
        ApiCall.postMethodWithHeader(Constants.earnings, data, Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                this.setState({ refreshing: false, earnings: response, bookingCompletedUser: response.data.data })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }


    getYearWiseEarningsCall = () => {
        Helper.globalLoader.showLoader();
        let data = new FormData();
        data.append('year', this.state.currentYear);
        ApiCall.postMethodWithHeader(Constants.year_wise_earnings, data, Constants.APIImageUploadAndroid).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                this.setState({ refreshing: false, yearEarning: response.data })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }

    changeView(index) {
        this.yearFlatList.scrollToIndex({ animated: true, index: index });
        this.setState({ currentYear: this.state.arrYear[index].year }, () => {
            this.getYearWiseEarningsCall()
        })
    }


    _yearItem = ({ item, index }) => {
        return (
            <View style={styles.year_next_pre_view}>
                {index > 0 ?
                    <TouchableOpacity hitSlop={{ left: 30, right: 30, top: 30, bottom: 30 }} style={{ flex: 3.33, alignItems: 'flex-start' }} onPress={() => this.changeView(index - 1)}>
                        <Image source={images.previous_img} resizeMode={'contain'}
                            style={styles.pre_next_img} />
                    </TouchableOpacity>
                    : <View style={{ flex: 3.33 }}></View>}
                <View style={{ flex: 3.33, alignItems: 'center' }}>
                    <Text style={styles.year_text}>{item.year}</Text>
                </View>

                {index != this.state.arrYear.length - 1 ?
                    <TouchableOpacity style={{ flex: 3.33, alignItems: 'flex-end' }} hitSlop={{ left: 30, right: 30, top: 30, bottom: 30 }} onPress={() => this.changeView(index + 1)}>
                        <Image source={images.next_img} resizeMode={'contain'}
                            style={styles.pre_next_img} />
                    </TouchableOpacity>
                    : <View style={{ flex: 3.33 }}></View>}
            </View>
        )

    }



    _renderEarDetailsItem = ({ item, index }) => {
        //console.log("item---item------->>>>======>>>>",item)
        return (
            <EarningShowView
                item={item}
                navigation={this.props.navigation} />
        )
    }

    _renderEarningItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => this.goToEarningMonths(item)}
                style={styles.earning_card}>
                <View style={styles.earning_card_view}>
                    <Text style={styles.month_name_rate_text}>{moment(item.month_name,'MMMM').locale(LanguagesIndex.MyLanguage).format('MMM')}</Text>
                    <Text style={styles.month_name_rate_text}>CHF {item.total}</Text>
                </View>

                <View style={styles.separator_view}></View>
            </TouchableOpacity>
        )
    }


    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                    showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.earning_view}>
                        <View style={styles.earning_text_view}>
                            <Text style={styles.total_earning_text}>{LanguagesIndex.translate('TotalEarnings')}</Text>
                        </View>
                        <View style={styles.earning_amount_text_view}>
                            <Text style={styles.earning_amount_text}>CHF{" "}{this.state.earnings.total_earning}</Text>
                        </View>
                    </View>

                    <View style={styles.latest_text_view}>
                        <Text style={styles.latest_text}>{LanguagesIndex.translate('YourLatestCompletedBookings')}</Text>
                    </View>
                    
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.bookingCompletedUser}
                        renderItem={this._renderEarDetailsItem}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    <FlatList
                        ref={(getRef) => this.yearFlatList = getRef}
                        showsHorizontalScrollIndicator={false}
                        data={this.state.arrYear}
                        renderItem={this._yearItem}
                        extraData={this.state}
                        initialNumToRender={100}
                        scrollEnabled={false}
                        pagingEnabled
                        horizontal
                        keyExtractor={(item, index) => index.toString()}
                    />

                    {this.state.yearEarning.length < 1 ?
                        <NoData

                            message={LanguagesIndex.translate('Youdonthaveearnings')}
                        />
                        : null
                    }

                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.yearEarning}
                        renderItem={this._renderEarningItem}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />

                </ScrollView>
            </SafeAreaView>
        )
    }

};
