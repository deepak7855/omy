import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default EditProfileScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    pare_view: {
        height: 120,
        width: '100%'
    },
    blue_radius_view: {
        backgroundColor: Colors.cerulean,
        height: 50,
        width: '100%',
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25
    },
    profile_view: {
        alignSelf: 'center',
        marginTop: -40,
        height: 95,
        width: 90
    },
    pro_img_view: {
        height: 82,
        width: 82,
        borderRadius: 20,
    },
    profile_img: {
        height: '100%',
        width: '100%',
        borderRadius: 20,
    },
    camera_touch:{
        height: 26, 
        width: 26, 
        position: 'absolute', 
        bottom: 0, 
        left: 30
    },
    camera_img:{
        height: '100%', 
        width: '100%',
    },
    input_view:{
        marginVertical: 10, 
        marginTop: 10, 
        marginHorizontal: 16
    },
    post_city_view:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    post_view:{
        flex: 4, 
        marginRight: 5
    },
    city_view:{
        flex: 4, 
        marginLeft: 5
    },
    save_btn_view:{
        marginVertical: 10, 
        marginTop: 20, 
        marginHorizontal: 16
    },
    about_input_view:{
        height: 75, 
        textAlignVertical: 'top', 
        paddingHorizontal: 10,
        color:Colors.warmGrey,
    },


    samTextCss: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1
    },
    _textInputCss: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#dbdbdb',
        marginTop: 10
    },
    lineBorderCss: {
        borderBottomWidth: 8,
        borderBottomColor: '#f8f8f8',
        padding: 15
    },
    checkboxViewcss: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 12
    },
    samImgCss: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
        marginRight: 10
    },
    radioIconCss: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
        marginRight: 10
    },
    editIconCss: {
        height: 17,
        width: 17,
        resizeMode: 'contain',
        marginLeft: 10
    },
    samGreyTextCss: {
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    _samViewTextIc: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    uploadImgCss: {
        height: 82,
        width: 82, 
    },
    blurImgViewCss: {
        position: 'absolute',
        alignSelf: 'center',
        height: "100%",
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba( 0, 0, 0, 0.5 )',
    },
    sliderMainVewCss: {
        marginTop:10
    },
    login_btn_view: {
        marginVertical: 10,
        marginTop: 20,
        marginHorizontal: 15
    },
    beginner_txt_btn:{
        marginRight: 20, 
        flexDirection: 'row', 
        alignItems: 'center'
    },
    advanced_txt_btn:{
        flexDirection: 'row', 
        alignItems: 'center'
    },
    modal_color_view:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.white,
        paddingVertical: 100, 
        paddingHorizontal:40,
        //borderBottomRightRadius: 30, 
        //borderBottomLeftRadius: 30,
      },
});