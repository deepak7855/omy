import React from 'react';
import {
    Text, View, ScrollView, TextInput, Image, FlatList, StyleSheet,
    TouchableOpacity, SafeAreaView, Dimensions, Keyboard, DeviceEventEmitter, Modal
} from 'react-native';
import styles from './EditProfileScreenStyles';
import { GButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import IconInput from '../../Comman/GInput';
import fonts from '../../Assets/fonts';
import AppHeader from '../../Comman/AppHeader';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import ImageLoadView from '../../Lib/ImageLoadView';
import ActivitiesRow from '../../Comman/ActivitiesRow';
import LanguagesIndex from '../../Languages';
import CameraController from '../../Lib/CameraController';
import { handleNavigation } from '../../navigation/Navigation';
import KeyboardScroll from '../../Comman/KeyboardScroll';


const ScreenWidth = Dimensions.get('screen').width;
const CustomCalendarArrow = (direction) => {

    return (
        <Image source={direction == "left" ? images.previous_img : images.next_img} resizeMode={'contain'} style={{ height: 16.3, width: 8.2, }} />
    )
}

export default class EditProfileScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            unavailableDate: [],
            activeMon: moment().format('YYYY-MM'),
            availableSlots: [],
            dicMarkDates: {},
            // selectedDates: [],
            selectedSendDates: [],
            selectedLanguages: [],
            avatarSource: '',
            StudentActivities: [],
            selectedActivities: [],
            selectedActivityIds: [],
            deletedMediaIds: [],
            selectedMedia: [],
            selectedUploadedMedia: [],
            userEditForm: {
                profile_picture: '',
                name: '',
                mobile_number: '',
                email: '',
                university: '',
                departement: '',
                // location: '',
                location_lat: '',
                location_lng: '',
                street_no: '',
                postcode: '',
                city: '',
                about_me: '',
                // visibility: '1',
                price: '',
                social_type: true,
                validators: {
                    name: { required: true, minLength: 2, maxLength: 45 },
                    mobile_number: { required: true, minLengthDigit: 9, maxLengthDigit: 9 },
                    university: { required: true, minLength: 1, maxLength: 100 },
                    departement: { required: true, minLength: 1, maxLength: 100 },
                    street_no: { required: true, minLength: 1, maxLength: 100 },
                    postcode: { required: true, minLengthDigit: 4, maxLengthDigit: 10 },
                    // location: { required: true },
                    city: { required: true, minLength: 1, maxLength: 45 },
                    // price: { required: true, },
                }

            },
            german_lang_pref: '',
            french_lang_pref: '',
            english_lang_pref: '',
            visibility: '',

            mon_to_fri: [],
            sat: [],
            sun: [],
            showMonFri: false,
            showSat: false,
            showSun: false,
            modalVisible: false,
            postalData: [],
            modalVisibleCity: false

        }

        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('EditProfile') })
    }

    componentDidMount() {

        this.methodFillEditForm()
        this.getActivitiesApiCall();
        this.getSlotsApiCall();
    }

    chooseWeekSlot(week, value) {
        let isExist = this.state[week].indexOf(value);
        if (isExist > -1) {
            this.state[week].splice(isExist, 1);
        } else {
            this.state[week].push(value);
        }

        if (week == 'sun') {
            this.state.showSun = this.state[week].length == 0 ? false : true
        } else if (week == 'mon_to_fri') {
            this.state.showMonFri = this.state[week].length == 0 ? false : true
        } else if (week == 'sat') {
            this.state.showSat = this.state[week].length == 0 ? false : true
        }

        this.setState({ change: !this.state.change }, () => {
            this.showSelectedDates();
        })
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    getPostalsCode = () => {
        let data = {
            postcode: this.state.search_p
        };
        // console.log('datadata',data)
        this.setState({ postalData: [] })
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            //console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    postalData: response.data
                })


            } else {
                Helper.showToast(response.message);

            }
        })
    }

    _renderList = ({ item }) => {
        return (
            <View style={{ width: '100%' }}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.setValues('postcode', item.code)
                            this.setModalVisible(false)
                            setTimeout(() => {
                                //this code use for clear city
                                this.setValues('city', '')
                                this.setState({ postalData: [] })
                                this.getCity()
                            }, 1000)

                        }}
                        style={{ padding: 15, borderBottomColor: Colors.warmGrey, borderBottomWidth: 1 }}>
                        <Text >{item.code}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    postalModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => { }}>
                <View style={styles.modal_color_view}>
                    <TouchableOpacity
                        onPress={() => this.setModalVisible(false)} style={{ position: 'absolute', width: 30, height: 30, right: 15, top: 35 }}>
                        <Image source={images.cross} resizeMode={'contain'} style={{ width: 35, height: 35, tintColor: Colors.cerulean }} />
                    </TouchableOpacity>
                    <View style={{ width: '100%' }}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('Search By Postal')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            keyboardType={'numeric'}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({ search_p: search }, () => {
                                this.getPostalsCode()
                            })}
                        />
                    </View>
                    <View style={{ width: '100%' }}>
                        <FlatList
                            style={{}}
                            showsVerticalScrollIndicator={false}
                            data={this.state.postalData}
                            renderItem={this._renderList}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>

                <View style={styles.btn_view}>
                    <GButton
                        Text='Cancel'
                        width={'100%'}
                        height={50}
                        borderRadius={10}
                        onPress={() => { this.setModalVisible(false) }}
                    />
                </View>
            </Modal>


        )
    }


    setModalVisibleCity = (visible) => {
        this.setState({ modalVisibleCity: visible });
    }

    getCity = () => {
        let data = {
            postcode: this.state.userEditForm.postcode,
            city: this.state.search_c
        };
        // console.log('datadata',data)
        this.setState({ cityData: [] })
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            // console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    cityData: response.data
                })


            } else {
                Helper.showToast(response.message);

            }
        })
    }

    _renderListC = ({ item }) => {
        return (
            <View style={{ width: '100%' }}>
                <View>
                    <TouchableOpacity
                        onPress={() => {

                            //this.setValues('postcode',item.code)
                            this.setValues('city', item.city)
                            this.setModalVisibleCity(false)
                        }}
                        style={{ padding: 15, borderBottomColor: Colors.warmGrey, borderBottomWidth: 1 }}>
                        <Text >{item.city}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    cityModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                visible={this.state.modalVisibleCity}
                onRequestClose={() => { }}>
                <View style={styles.modal_color_view}>
                    <TouchableOpacity
                        onPress={() => this.setModalVisibleCity(false)} style={{ position: 'absolute', width: 30, height: 30, right: 15, top: 35 }}>
                        <Image source={images.cross} resizeMode={'contain'} style={{ width: 35, height: 35, tintColor: Colors.cerulean }} />
                    </TouchableOpacity>
                    <View style={{ width: '100%' }}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('Search By City')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({ search_c: search }, () => {
                                this.getCity()
                            })}
                        />
                    </View>
                    <View style={{ width: '100%' }}>
                        <FlatList
                            style={{}}
                            showsVerticalScrollIndicator={false}
                            data={this.state.cityData}
                            renderItem={this._renderListC}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>

                <View style={styles.btn_view}>
                    <GButton
                        Text='Cancel'
                        width={'100%'}
                        height={50}
                        borderRadius={10}
                        onPress={() => { this.setModalVisibleCity(false) }}
                    />
                </View>
            </Modal>


        )
    }

    enableWeekView(week) {
        this.setState({ [week]: !this.state[week] }, () => {
            if (!this.state[week]) {
                switch (week) {
                    case 'showMonFri':
                        this.setState({ mon_to_fri: [] }, () => {
                            this.showSelectedDates();
                        })
                        break;
                    case 'showSat':
                        this.setState({ sat: [] }, () => {
                            this.showSelectedDates();
                        })
                        break;
                    case 'showSun':
                        this.setState({ sun: [] }, () => {
                            this.showSelectedDates();
                        })
                        break;
                    default:
                        break;
                }
            }
        })
    }


    setValues(key, value) {
        let userEditForm = { ...this.state.userEditForm }
        // if (key == 'city') { value = value.replace(/[^a-zA-Z ]/g, ''); }
        // else { value = value; }
        if (key == 'mobile_number') {
            value = value.replace(/\D/g, '');
        }
        userEditForm[key] = value
        this.setState({ userEditForm })
    }

    UploadMediaMethod(uri) {
        this.setValues('profile_picture', uri)
        this.setState({ avatarSource: uri })
    }

    goToProfile() {
        let isValid = Helper.validate(this.state.userEditForm);
        if (isValid) {
            Helper.showToast('')
            handleNavigation({ type: 'push', page: 'ProfileScreen', navigation: this.props.navigation })
        }
    }

    methodFillEditForm() {
        if (Helper.user_data) {

            this.state.userEditForm.name = Helper.user_data.name
            this.state.userEditForm.mobile_number = Helper.user_data.mobile_number
            this.state.userEditForm.email = Helper.user_data.email
            this.state.userEditForm.university = Helper.user_data.university
            this.state.userEditForm.departement = Helper.user_data.departement
            this.state.userEditForm.street_no = Helper.user_data.street_no
            this.state.userEditForm.postcode = Helper.user_data.postcode
            this.state.userEditForm.city = Helper.user_data.city
            this.state.userEditForm.postcode = Helper.user_data.postcode
            this.state.userEditForm.about_me = Helper.user_data.about_me
            // this.state.userEditForm.location = Helper.user_data.location
            this.state.userEditForm.location_lat = Helper.user_data.lat
            this.state.userEditForm.location_lng = Helper.user_data.lng
            this.state.userEditForm.profile_picture = Helper.user_data.profile_picture
            this.state.german_lang_pref = Helper.user_data.german_lang_pref
            this.state.french_lang_pref = Helper.user_data.french_lang_pref
            this.state.english_lang_pref = Helper.user_data.english_lang_pref
            this.state.visibility = Helper.user_data.visibility

            if (Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].mon_to_fri) {
                this.state.showMonFri = true;
                if (Array.isArray(Helper.user_data.userslots[0].mon_to_fri)) {
                    this.state.mon_to_fri = Helper.user_data.userslots[0].mon_to_fri;
                } else {
                    this.state.mon_to_fri = Helper.user_data.userslots[0].mon_to_fri.split(',').map(n => parseInt(n, 10));
                }
            }
            if (Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].sat) {
                this.state.showSat = true;
                if (Array.isArray(Helper.user_data.userslots[0].sat)) {
                    this.state.sat = Helper.user_data.userslots[0].sat;
                } else {
                    this.state.sat = Helper.user_data.userslots[0].sat.split(',').map(n => parseInt(n, 10));
                }
            }
            if (Helper.user_data.userslots.length > 0 && Helper.user_data.userslots[0].sun) {
                this.state.showSun = true;
                if (Array.isArray(Helper.user_data.userslots[0].sun)) {
                    this.state.sun = Helper.user_data.userslots[0].sun;
                } else {
                    this.state.sun = Helper.user_data.userslots[0].sun.split(',').map(n => parseInt(n, 10));
                }
            }
            if (Helper.user_data && Helper.user_data.unavailability && Helper.user_data.unavailability.length > 0) {
                Helper.user_data.unavailability.forEach(element => {
                    this.state.unavailableDate.push(element.date);
                });
            }

            this.setState({ selectedUploadedMedia: Helper.user_data.user_media }, () => {
                this.showSelectedDates();
            })
        }
    }

    async EditUpdateProfile() {

        Keyboard.dismiss();
        let isValid = Helper.validate(this.state.userEditForm);
        if (isValid) {
            let checking = false;
            this.state.selectedActivities.forEach(element => {
                if (!element.price || Number(element.price) < 1) {
                    checking = true;
                    Helper.showToast(LanguagesIndex.translate('Pleaseaddpriceforselectedactivities'))
                }
            });

            if (checking) return;

            Helper.globalLoader.showLoader();
            await Helper.GetAddressFromLatLong(`${this.state.userEditForm.street_no} ${this.state.userEditForm.city} ${this.state.userEditForm.postcode}`, (resp) => {
                if (!resp) {
                    Helper.globalLoader.hideLoader();
                    return;
                }
                this.state.userEditForm.location_lat = resp.lat;
                this.state.userEditForm.location_lng = resp.lng;
                this.setState({})

                let data = new FormData();
                data.append('name', this.state.userEditForm.name);
                data.append('mobile_number', this.state.userEditForm.mobile_number);
                data.append('email', this.state.userEditForm.email);
                data.append('university', this.state.userEditForm.university);
                data.append('departement', this.state.userEditForm.departement);

                // data.append('location', this.state.userEditForm.location);
                data.append('lat', this.state.userEditForm.location_lat);
                data.append('lng', this.state.userEditForm.location_lng);
                data.append('street_no', this.state.userEditForm.street_no);
                data.append('postcode', this.state.userEditForm.postcode);
                data.append('city', this.state.userEditForm.city);
                data.append('about_me', this.state.userEditForm.about_me);
                data.append('german_lang_pref', this.state.german_lang_pref);
                data.append('french_lang_pref', this.state.french_lang_pref);
                data.append('english_lang_pref', this.state.english_lang_pref);
                data.append('visibility', this.state.visibility);
                // data.append('slots', JSON.stringify(this.state.selectedSendDates));

                if (this.state.showMonFri) {
                    data.append('mon_to_fri', this.state.mon_to_fri.join(','));
                } else {
                    data.append('mon_to_fri', '');
                }
                if (this.state.showSat) {
                    data.append('sat', this.state.sat.join(','));
                } else {
                    data.append('sat', '');
                }
                if (this.state.showSun) {
                    data.append('sun', this.state.sun.join(','));
                } else {
                    data.append('sun', '');
                }

                if (this.state.deletedMediaIds.length > 0) {
                    data.append('media_ids', this.state.deletedMediaIds.join(','));
                }

                if (this.state.selectedActivities.length > 0) {
                    data.append('activities', JSON.stringify(this.state.selectedActivities));
                }

                data.append('media_count', this.state.selectedMedia.length);

                if (this.state.selectedMedia.length > 0) {
                    for (let index = 1; index <= this.state.selectedMedia.length; index++) {
                        data.append(`media_${index}`, {
                            uri: this.state.selectedMedia[index - 1].uri,
                            name: this.state.selectedMedia[index - 1].media_type == 'video' ? 'test.mp4' : 'test.jpg',
                            type: this.state.selectedMedia[index - 1].media_type == 'video' ? 'video/mp4' : 'image/jpeg',
                        });
                        data.append(`media_type_${index}`, this.state.selectedMedia[index - 1].media_type);
                    }
                }

                if (this.state.avatarSource) {
                    data.append('profile_picture', {
                        uri: this.state.avatarSource,
                        name: 'test.jpg',
                        type: 'image/jpeg'
                    });
                }

                if (this.state.unavailableDate.length > 0) {
                    data.append('unavailable_date', this.state.unavailableDate.join(','));
                } else {
                    data.append('unavailable_date', 0);
                }
                console.log("data----+++", data)

                // return
                ApiCall.postMethodWithHeader(Constants.update_profile, data, Constants.APIImageUploadAndroid).then((response) => {
                    Helper.globalLoader.hideLoader();
                    console.log("response ==========> ", response)
                    if (response.status == Constants.TRUE) {

                        Helper.showToast(response.message)
                        AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                        Helper.user_data = response.data
                        DeviceEventEmitter.emit(Constants.USER_DETAILS, response.data);
                        setTimeout(() => {
                            handleNavigation({ type: 'pop', navigation: this.props.navigation });
                        }, 100);

                    }
                    else {
                        Helper.showToast(response.message || LanguagesIndex.translate('kSorryError'))
                        Helper.globalLoader.hideLoader();
                    }
                })
            })
        }
    }



    getSlotsApiCall = () => {
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_slots, data, Constants.APIGet).then((response) => {
            // console.log("solots----+++",response)
            if (response.status == Constants.TRUE) {
                this.setState({ availableSlots: response.data })
            }
            else {
                Helper.showToast(response.message)
            }
        })
    }

    getActivitiesApiCall = () => {
        //Helper.globalLoader.showLoader();
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_activities, data, Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                Helper.user_data.user_activity.forEach(element => {
                    this.state.selectedActivityIds.push(Number(element.activities));
                    this.state.selectedActivities.push({
                        id: Number(element.activities),
                        price: element.price
                    })

                    response.data.forEach(elementNew => {
                        if (Number(element.activities) == Number(elementNew.id)) {
                            elementNew.price = element.price;
                        }
                    });
                });
                this.setState({ StudentActivities: response.data })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }

    chooseActivity(id) {
        let isExist = this.state.selectedActivityIds.indexOf(Number(id));
        if (isExist > -1) {
            this.state.selectedActivityIds.splice(isExist, 1);
            this.state.selectedActivities.splice(isExist, 1);
            for (let index = 0; index < this.state.StudentActivities.length; index++) {
                if (Number(this.state.StudentActivities[index].id) == Number(id)) {
                    this.state.StudentActivities[index].price = '';
                }
            }
        } else {
            this.state.selectedActivityIds.push(Number(id));
            this.state.selectedActivities.push({
                id: Number(id),
                price: ''
            })
        }
        this.setState({})
    }

    selectLanguages(lan) {
        let isExist = this.state.selectedLanguages.indexOf(lan);
        if (isExist > -1) {
            this.state.selectedLanguages.splice(isExist, 1);
        } else {
            this.state.selectedLanguages.push(lan);
        }
        this.setState({})
    }

    onAddActivity(id, price) {
        let getIndex = this.state.selectedActivityIds.indexOf(id);

        if (getIndex > -1) {
            this.state.selectedActivities[getIndex].price = price;
            for (let index = 0; index < this.state.StudentActivities.length; index++) {
                if (Number(this.state.StudentActivities[index].id) == Number(id)) {
                    this.state.StudentActivities[index].price = price;
                }
            }
            this.setState({})
        }
    }

    _renderActivitiesOrPrice = ({ item, index }) => {
        return (
            <ActivitiesRow
                item={item}
                onSelectActivities={(id) => this.chooseActivity(id)}
                selected={this.state.selectedActivityIds.includes(item.id)}
                onAdd={(id, price) => this.onAddActivity(id, price)}
            />
        )
    }

    deleteMedia(item, index) {
        Helper.confirm(LanguagesIndex.translate('Areyousureyouwanttodelete'), (status) => {
            if (status) {
                if (item.new) {
                    this.state.selectedMedia.splice(index, 1);
                    this.setState({})
                } else {
                    this.state.deletedMediaIds.push(item.id);
                    this.state.selectedUploadedMedia.splice(index - this.state.selectedMedia.length, 1);
                    this.setState({})
                }
            }

        })
    }

    _renderUploadItem = ({ item, index }) => {
        return (
            <View style={{ marginRight: 10, marginTop: 12, }}>
                <View style={{ height: 82, width: 82, }}>
                    <ImageLoadView
                        source={{ uri: item.media_type == 'video' ? item.media_thumb : item.uri ? item.uri : item.media_url }} resizeMode={'cover'}
                        style={{ width: '100%', height: '100%' }}
                    />

                    {item.media_type == 'video' ?
                        <View style={styles.blurImgViewCss}>
                            <Image source={images.play}
                                style={{ height: 25, width: 25 }} />
                        </View>
                        : null}
                </View>

                <TouchableOpacity onPress={() => this.deleteMedia(item, index)} style={{ flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
                    <Image source={images.cross} style={{ height: 10, width: 10, marginLeft: 5 }} resizeMode={'contain'} />
                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Remove')}</Text>
                </TouchableOpacity>
            </View>
        )
    }




    _renderDateItem = ({ item, index }) => {
        return (
            <View style={{ paddingVertical: 10, width: "50%" }}>
                <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{Helper.formatDate(item.date, 'D MMM YYYY')}</Text>
                <FlatList
                    style={{ padding: 15 }}
                    data={this.state.availableSlots}
                    renderItem={(newitem) => {
                        return (
                            <TouchableOpacity onPress={() => this.selectSlot(index, newitem.item.id)} style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                <View>
                                    <Image source={item.slot_id == newitem.item.id ? images.check : images.unchecked} style={styles.samImgCss} />
                                </View>
                                <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                            </TouchableOpacity>
                        )
                    }}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                />

            </View>
        )
    }

    prefValuesChangeFinish = (key, values) => {

        this.setState({ [key]: values, });
    }


    changeClick = (v) => {
        this.setState({ visibility: v })
    }

    selectDates(day) {
        let pressDate = moment(day.dateString).format("YYYY-MM-DD")
        let arrMarkDates = { ...this.state.dicMarkDates }
        let unavailableDate = [...this.state.unavailableDate]

        // let arrDatesArr = [...this.state.selectedDates]

        // if (arrDates[pressDate]) {
        //     delete arrDates[pressDate];
        //     let getIndex = arrDatesArr.map(function (e) { return e.date; }).indexOf(pressDate);
        //     arrDatesArr.splice(getIndex, 1);

        //     this.state.selectedSendDates[getIndex].slot_id = '';

        // } else {
        //     arrDates[pressDate] = { selected: true, selectedColor: Colors.darkSkyBlue, }
        //     arrDatesArr.push({
        //         date: pressDate,
        //         slot_id: 1
        //     })
        //     this.setState({ selectedSendDates: arrDatesArr })
        // }

        // this.setState({ dicMarkDates: arrDates, selectedDates: arrDatesArr })


        if (arrMarkDates[pressDate]) {
            Helper.confirm(LanguagesIndex.translate('Unselectadatewillsetanunavailabilityforthisdate'), (status) => {
                if (status) {
                    delete arrMarkDates[pressDate];
                    unavailableDate.push(pressDate)
                    this.setState({ dicMarkDates: arrMarkDates, unavailableDate: unavailableDate })
                }
            })
        } else {
            arrMarkDates[pressDate] = { selected: true, selectedColor: Colors.darkSkyBlue, }
            let getIndex = unavailableDate.map(function (e) { return e.date; }).indexOf(pressDate);
            unavailableDate.splice(getIndex, 1);
            this.setState({ dicMarkDates: arrMarkDates, unavailableDate: unavailableDate })
        }
    }

    checkUnavailableDate(date) {

        let isExist = false
        if (Helper.user_data && Helper.user_data.unavailability && Helper.user_data.unavailability.length > 0) {
            Helper.user_data.unavailability.forEach(element => {
                if (element.date == date) {
                    isExist = true
                }
            });
        }
        return isExist
    }


    showSelectedDates() {
        let arrDates = {};
        var startOfMonth = moment(this.state.activeMon).startOf('month');

        this.setState({ dicMarkDates: {} }, () => {

            for (let index = 0; index < moment(this.state.activeMon, "YYYY-MM").daysInMonth() + 1; index++) {
                if (this.state.mon_to_fri.length > 0) {
                    if (moment(startOfMonth).weekday() != 6 && moment(startOfMonth).weekday() != 0 && !this.checkUnavailableDate(moment(startOfMonth).format('YYYY-MM-DD'))) {
                        arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = { selected: true, selectedColor: Colors.darkSkyBlue, }
                    }
                }
                if (this.state.sat.length > 0) {
                    if (moment(startOfMonth).weekday() == 6 && !this.checkUnavailableDate(moment(startOfMonth).format('YYYY-MM-DD'))) {
                        arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = { selected: true, selectedColor: Colors.darkSkyBlue, }
                    }
                }
                if (this.state.sun.length > 0) {
                    if (moment(startOfMonth).weekday() == 0 && !this.checkUnavailableDate(moment(startOfMonth).format('YYYY-MM-DD'))) {
                        arrDates[moment(startOfMonth).format('YYYY-MM-DD')] = { selected: true, selectedColor: Colors.darkSkyBlue, }
                    }
                }
                startOfMonth.add(1, 'days')
            }
            this.setState({ dicMarkDates: arrDates })
        })
    }

    selectSlot(index, slotid) {
        let arrDatesArr = [...this.state.selectedDates]
        arrDatesArr[index].slot_id = slotid;
        this.state.selectedSendDates[index].slot_id = slotid;
        this.setState({ selectedDates: arrDatesArr })
    }

    chooseImage() {
        CameraController.open((response) => {
            if (response) {
                 //console.log("response------------",response);
                this.UploadMediaMethod(response.path);
            }
        });
    }


    // selectMedia(isProfileImageSelect) {

    //    // CameraController.isMixed = false
    //     CameraController.open((response) => {
    //         if (response.uri) {


    //         }
    //     });
    // }
    selectMedia() {

        CameraController.chooseMediaType((response) => {
            //console.log("media type response-------", response);
            if (response.path) {
                if (response.media_type == 'video') {
                    console.log("in video condition");
                    Helper.globalLoader.showLoader();
                    CameraController.createThumbnail(response.path, (res) => {
                        //console.log("response after thunmnail 123 -------", res);
                        Helper.globalLoader.hideLoader();
                        if (res) {
                           // console.log("response after thunmnail 456 -------", res);
                            response.media_thumb = res;
                            response.new = 1;
                            response.uri = response.path;
                            this.state.selectedMedia.push(response);
                            this.setState({});
                        }
                    })
                } else {
                    response.new = 1;
                    response.uri = response.path;
                    this.state.selectedMedia.push(response);
                    this.setState({});
                }
            }
        });
    }


    render() {
        //console.log("user_data",Helper.user_data)
        return (
            <SafeAreaView style={styles.safe_area_view}>
                {this.state.modalVisible ? this.postalModal() : null}
                {this.state.modalVisibleCity ? this.cityModal() : null}
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.pare_view}>
                        <View style={styles.blue_radius_view}>

                        </View>
                        <View style={styles.profile_view}>
                            <View style={styles.pro_img_view}>
                                <ImageLoadView
                                    resizeMode={'cover'}
                                    source={this.state.avatarSource ? { uri: this.state.avatarSource } : Helper.user_data.profile_picture ? { uri: Helper.user_data.profile_picture } : images.default}
                                    // source={this.state.userEditForm.profile_picture ? { uri: this.state.userEditForm.profile_picture } : images.student_Profile}
                                    // source={images.user_box}
                                    style={styles.profile_img} />
                            </View>

                            <TouchableOpacity
                                onPress={() => { this.chooseImage() }}
                                style={styles.camera_touch}>
                                <Image resizeMode={'cover'}
                                    source={images.user_camera}
                                    style={styles.camera_img} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.input_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('Name')}
                            setFocus={() => { this.mobile_number.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(name) => this.setValues('name', name)}
                            value={this.state.userEditForm.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            getFocus={(input) => { this.mobile_number = input }}
                            setFocus={(input) => { this.email.focus(); }}
                            returnKeyType="next"
                            keyboardType={'number-pad'}
                            onChangeText={(mobile_number) => this.setValues('mobile_number', mobile_number)}
                            value={this.state.userEditForm.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('EmailUniversityEmailOnly')}
                            getFocus={(input) => { this.email = input }}
                            setFocus={(input) => { this.university.focus(); }}
                            returnKeyType="next"
                            keyboardType={'email-address'}
                            inputedit={Helper.user_data.email ? false : true}
                            onChangeText={(email) => this.setValues('email', email)}
                            value={this.state.userEditForm.email}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('NameOfInstitution/Department')}
                            getFocus={(input) => { this.university = input }}
                            setFocus={(input) => { this.departement.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(university) => this.setValues('university', university)}
                            value={this.state.userEditForm.university}
                        />


                        <IconInput
                            placeholder={LanguagesIndex.translate('Department')}
                            getFocus={(input) => { this.departement = input }}
                            setFocus={(input) => { this.street_no.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(departement) => this.setValues('departement', departement)}
                            value={this.state.userEditForm.departement}
                        />


                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            getFocus={(input) => { this.street_no = input }}
                            setFocus={(input) => { this.postcode.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(street_no) => this.setValues('street_no', street_no)}
                            value={this.state.userEditForm.street_no}
                        />


                        <View style={styles.post_city_view}>
                            <View style={styles.post_view}>
                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    getFocus={(input) => { this.postcode = input }}
                                    setFocus={(input) => { this.city.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'number-pad'}
                                    onChangeText={(postcode) => this.setValues('postcode', postcode)}
                                    value={this.state.userEditForm.postcode}
                                /> */}
                                <TouchableOpacity
                                    onPress={() => this.setModalVisible(true)}
                                    style={{ backgroundColor: Colors.whiteThree, height: 50, borderRadius: 15, justifyContent: 'center', paddingHorizontal: 15 }}>
                                    <Text style={{ color: Colors.warmGrey }}>{this.state.userEditForm.postcode ? this.state.userEditForm.postcode : LanguagesIndex.translate('Post')}</Text>

                                </TouchableOpacity>
                            </View>

                            <View style={styles.city_view}>



                                {/*                                 
                                <View
                                    style={{
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.userEditForm.city ? this.state.userEditForm.city : LanguagesIndex.translate('City')}</Text>
                                </View> */}


                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('City')}
                                    getFocus={(input) => { this.city = input }}
                                    // setFocus={(input) => { this.password.focus(); }}
                                    returnKeyType="done"
                                    keyboardType={'default'}
                                    onChangeText={(city) => this.setValues('city', city)}
                                    value={this.state.userEditForm.city}
                                /> */}
                                <TouchableOpacity
                                    onPress={() => this.setModalVisibleCity(true)}
                                    style={{ backgroundColor: Colors.whiteThree, height: 50, borderRadius: 15, justifyContent: 'center', paddingHorizontal: 15 }}>
                                    <Text style={{ color: Colors.warmGrey }}>{this.state.userEditForm.city ? this.state.userEditForm.city : LanguagesIndex.translate('City')}</Text>

                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>


                    <View style={styles.lineBorderCss}>
                        <Text style={styles.samTextCss}>{LanguagesIndex.translate('AboutMe')}</Text>
                        <View style={styles._textInputCss}>
                            <TextInput
                                placeholder={LanguagesIndex.translate('WriteHere')}
                                multiline={true}
                                returnKeyType={'done'}
                                onSubmitEditing={() => { this.EditUpdateProfile() }}
                                placeholderTextColor={Colors.warmGrey}
                                onChangeText={(about_me) => this.setValues('about_me', about_me)}
                                value={this.state.userEditForm.about_me == 'null' ? "" : this.state.userEditForm.about_me}
                                style={styles.about_input_view}
                            />
                        </View>
                    </View>
                    <View style={styles.lineBorderCss}>
                        <Text style={styles.samTextCss}>{LanguagesIndex.translate('AddActivitiesAndPrice')}</Text>
                        <FlatList
                            data={this.state.StudentActivities}
                            renderItem={this._renderActivitiesOrPrice}
                            extraData={this.state}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>

                    <View style={styles.lineBorderCss}>
                        <TouchableOpacity onPress={() => this.selectMedia()} style={styles._samViewTextIc}>
                            <Text style={[styles.samTextCss, { flex: 1 }]}>{LanguagesIndex.translate('UploadImages/Videos')}</Text>
                            <Image source={images.cloud_upload_ic}
                                style={styles.editIconCss} />
                        </TouchableOpacity>
                        <FlatList
                            horizontal
                            data={[...this.state.selectedMedia, ...this.state.selectedUploadedMedia]}
                            renderItem={this._renderUploadItem}
                            showsHorizontalScrollIndicator={false}
                            extraData={this.state}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>


                    <View style={styles.lineBorderCss}>
                        <Text style={styles.samTextCss}>{LanguagesIndex.translate('LanguagePreference')}</Text>
                        <View style={styles.sliderMainVewCss}>
                            <TouchableOpacity
                                onPress={() => { this.prefValuesChangeFinish('german_lang_pref', this.state.german_lang_pref ? '' : 'beginner') }}
                                style={{ flexDirection: "row", alignItems: "center", width: ScreenWidth / 4 }}>
                                <View>
                                    <Image source={this.state.german_lang_pref ? images.check : images.unchecked} style={styles.samImgCss} />
                                </View>
                                <View style={{ marginRight: 25 }}>
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('German')}</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={[styles._samViewTextIc, { marginTop: 10, justifyContent: 'flex-end' }]}>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('german_lang_pref', 'beginner') }}
                                    style={styles.beginner_txt_btn}>
                                    <Image source={(this.state.german_lang_pref == 'beginner') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Beginner')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('german_lang_pref', 'intermediate') }}
                                    style={styles.beginner_txt_btn}>
                                    <Image source={(this.state.german_lang_pref == 'intermediate') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Intermediate')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('german_lang_pref', 'advanced') }}
                                    style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={(this.state.german_lang_pref == 'advanced') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Advanced')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={styles.sliderMainVewCss}>
                            <TouchableOpacity
                                onPress={() => { this.prefValuesChangeFinish('french_lang_pref', this.state.french_lang_pref ? '' : 'beginner') }}
                                style={{ flexDirection: "row", alignItems: "center", width: ScreenWidth / 4 }}>
                                <View>
                                    <Image source={this.state.french_lang_pref ? images.check : images.unchecked} style={styles.samImgCss} />
                                </View>
                                <View style={{ marginRight: 25 }}>
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('French')}</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={[styles._samViewTextIc, { marginTop: 10, justifyContent: 'flex-end' }]}>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('french_lang_pref', 'beginner') }}
                                    style={styles.beginner_txt_btn}>
                                    <Image source={(this.state.french_lang_pref == 'beginner') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Beginner')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('french_lang_pref', 'intermediate') }}
                                    style={styles.beginner_txt_btn}>
                                    <Image source={(this.state.french_lang_pref == 'intermediate') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Intermediate')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('french_lang_pref', 'advanced') }}
                                    style={styles.advanced_txt_btn}>
                                    <Image source={(this.state.french_lang_pref == 'advanced') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Advanced')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={styles.sliderMainVewCss}>
                            <TouchableOpacity
                                onPress={() => { this.prefValuesChangeFinish('english_lang_pref', this.state.english_lang_pref ? '' : 'beginner') }}
                                style={{ flexDirection: "row", alignItems: "center", width: ScreenWidth / 4 }}>
                                <View>
                                    <Image source={this.state.english_lang_pref ? images.check : images.unchecked} style={styles.samImgCss} />
                                </View>
                                <View style={{ marginRight: 25 }}>
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('English')}</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={[styles._samViewTextIc, { marginTop: 10, justifyContent: 'flex-end' }]}>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('english_lang_pref', 'beginner') }}
                                    style={styles.beginner_txt_btn}>
                                    <Image source={(this.state.english_lang_pref == 'beginner') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Beginner')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('english_lang_pref', 'intermediate') }}
                                    style={styles.beginner_txt_btn}>
                                    <Image source={(this.state.english_lang_pref == 'intermediate') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Intermediate')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.prefValuesChangeFinish('english_lang_pref', 'advanced') }}
                                    style={styles.advanced_txt_btn}>
                                    <Image source={(this.state.english_lang_pref == 'advanced') ? images.radio_btn_selected : images.radio_btn_un_selected}
                                        style={styles.radioIconCss} />
                                    <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Advanced')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={styles.lineBorderCss}>
                        <Text style={styles.samTextCss}>{LanguagesIndex.translate('Visibility')}</Text>
                        <View style={[styles._samViewTextIc, { marginTop: 12 }]}>
                            <TouchableOpacity onPress={() => { this.changeClick(1) }}
                                style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center' }}>
                                <Image source={Number(this.state.visibility) ? images.radio_btn_selected : images.radio_btn_un_selected}
                                    style={styles.radioIconCss} />
                                <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('Visible')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.changeClick(0) }}
                                style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center' }}>
                                <Image source={!Number(this.state.visibility) ? images.radio_btn_selected : images.radio_btn_un_selected}
                                    style={styles.radioIconCss} />
                                <Text style={styles.samGreyTextCss}>{LanguagesIndex.translate('NotVisible')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={styles.lineBorderCss}>
                        <Text style={styles.samTextCss}>{LanguagesIndex.translate('Availability')}</Text>
                        <View style={{ marginTop: 10 }}>
                            <Calendar
                                style={{
                                    borderWidth: .5,
                                    borderColor: Colors.white,
                                    borderRadius: 10,
                                    elevation: 1.5,
                                    backgroundColor: Colors.cerulean
                                }}
                                minDate={new Date()}
                                hideArrows={false}
                                onMonthChange={(month) => {
                                    this.setState({ activeMon: moment(month.dateString).format('YYYY-MM') }, () => {
                                        this.showSelectedDates();
                                    })
                                }}
                                onDayPress={(day) => this.selectDates(day)}
                                renderArrow={CustomCalendarArrow}
                                disableArrowLeft={false}
                                firstDay={1}
                                theme={{
                                    "stylesheet.calendar.main": {
                                        container: {
                                            paddingLeft: 0,
                                            paddingRight: 0,
                                        },
                                    },
                                    'stylesheet.calendar.header': {
                                        week: {
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            backgroundColor: Colors.veryLightBlue,
                                            color: Colors.black,
                                        },
                                        headerContainer: {
                                            flexDirection: 'row',
                                        },
                                        arrow: {
                                            padding: 10,
                                            color: Colors.white,
                                        },
                                        arrowImage: {
                                            tintColor: Colors.white
                                        }
                                    },
                                    monthTextColor: Colors.white,
                                    textDayFontWeight: 'bold',
                                    textMonthFontWeight: 'bold',
                                    textDayHeaderFontWeight: '300',
                                    textDayFontSize: 16,
                                    textMonthFontSize: 14,
                                    textDayHeaderFontSize: 12,
                                }}
                                markingType={'custom'}
                                markedDates={this.state.dicMarkDates}
                            />
                        </View>
                    </View>

                    <View style={{ width: "100%", marginVertical: 5 }}>
                        <TouchableOpacity
                            onPress={() => this.enableWeekView('showMonFri')}
                            style={{ flexDirection: 'row', marginHorizontal: 15, marginVertical: 5 }}>
                            <Image source={this.state.showMonFri ? images.check : images.unchecked} style={styles.samImgCss} />
                            <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('MondayToFriday')}</Text>
                        </TouchableOpacity>
                        {this.state.showMonFri ?
                            <View style={{ marginLeft: 20 }}>
                                <FlatList
                                    style={{ padding: 15 }}
                                    data={this.state.availableSlots}
                                    renderItem={(newitem) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => this.chooseWeekSlot('mon_to_fri', newitem.item.id)}
                                                style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                <View>
                                                    <Image source={this.state.mon_to_fri.includes(newitem.item.id) ? images.check : images.unchecked} style={styles.samImgCss} />
                                                </View>
                                                <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                    extraData={this.state}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null}
                    </View>

                    <View style={{ width: "100%", marginVertical: 5 }}>
                        <TouchableOpacity
                            onPress={() => this.enableWeekView('showSat')}
                            style={{ flexDirection: 'row', marginHorizontal: 15, marginVertical: 5 }}>
                            <Image source={this.state.showSat ? images.check : images.unchecked} style={styles.samImgCss} />
                            <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Saturday')}</Text>
                        </TouchableOpacity>
                        {this.state.showSat ?
                            <View style={{ marginLeft: 20 }}>
                                <FlatList
                                    style={{ padding: 15 }}
                                    data={this.state.availableSlots}
                                    renderItem={(newitem) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => this.chooseWeekSlot('sat', newitem.item.id)}
                                                style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                <View>
                                                    <Image source={this.state.sat.includes(newitem.item.id) ? images.check : images.unchecked} style={styles.samImgCss} />
                                                </View>
                                                <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                    extraData={this.state}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null}
                    </View>

                    <View style={{ width: "100%", marginVertical: 5, }}>
                        <TouchableOpacity
                            onPress={() => this.enableWeekView('showSun')}
                            style={{ flexDirection: 'row', marginHorizontal: 15, marginVertical: 5 }}>
                            <Image source={this.state.showSun ? images.check : images.unchecked} style={styles.samImgCss} />
                            <Text style={{ color: Colors.black, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('Sunday')}</Text>
                        </TouchableOpacity>
                        {this.state.showSun ?
                            <View style={{ marginLeft: 20 }}>
                                <FlatList
                                    style={{ padding: 15 }}
                                    data={this.state.availableSlots}
                                    renderItem={(newitem) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => this.chooseWeekSlot('sun', newitem.item.id)}
                                                style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}>
                                                <View>
                                                    <Image source={this.state.sun.includes(newitem.item.id) ? images.check : images.unchecked} style={styles.samImgCss} />
                                                </View>
                                                <Text style={styles.samGreyTextCss}>{newitem.item.slot}</Text>
                                            </TouchableOpacity>
                                        )
                                    }}
                                    extraData={this.state}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null}
                    </View>

                    {/* <FlatList
                        style={{ padding: 15 }}
                        data={this.state.selectedDates}
                        renderItem={this._renderDateItem}
                        showsHorizontalScrollIndicator={false}
                        extraData={this.state}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => <View style={{ borderBottomWidth: 1, borderBottomColor: Colors.whiteTwo }} />}
                    /> */}



                    <View style={styles.save_btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('SAVE')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.EditUpdateProfile() }}
                        />
                    </View>

                </KeyboardScroll>

            </SafeAreaView>
        )
    }

};
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: fonts.fontSize14,
        height: 50,
        color: Colors.warmGrey,
        fontFamily: fonts.RoBoToMedium_1,
        width: '100%',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 10,

        marginLeft: 10
    },
    inputAndroid: {
        fontSize: fonts.fontSize14,
        height: 50,
        width: '100%',
        color: Colors.warmGrey,
        marginRight: 20,
        marginLeft: 8,
        marginBottom: 10,
        fontFamily: fonts.RoBoToMedium_1,

    },
});





