import React from 'react';
import { Text, View, SafeAreaView, FlatList, } from 'react-native';
import styles from './CancelChargesStudentStyles';
import Colors from '../../Assets/Colors';
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import { ApiCall, AsyncStorageHelper, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import fonts from '../../Assets/fonts';
import ListLoader from '../../Comman/ListLoader';
import PDFView from 'react-native-view-pdf';
import WebView from 'react-native-webview';

export default class CancelChargesStudent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cancellationPolicyData:'',

            ChargesPolicy: [],
            isLoading: true,
            userLanguage:'',
            
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('CancelCharges'),
            borderBottomRadius: 25,
        })
    }


    goToStartBooking() {
        handleNavigation({ type: 'push', page: 'StartBooking', navigation: this.props.navigation })
    }


    componentDidMount() {
        AsyncStorageHelper.getData('lan').then((lan) => {
            this.setState({userLanguage:lan})
            //console.log("lan ==lan", lan);
        })
        //this.getCancelChargesApiCall()
       // this.getChargesPolicy()
    }

    // getChargesPolicy() {
    //     let data = {};
    //     ApiCall.postMethodWithHeader(Constants.cancellation_policy, data, Constants.APIImageUploadAndroid).then((response) => {
    //         console.log('response--response--->',response.data)
    //         if (response.status == Constants.TRUE) {
    //             this.setState({ cancellationPolicyData: response.data.content_file })
    //         }
    //         else {
    //             Helper.showToast(response.message)
    //         }
    //     }
    //     ).catch(err => {
    //         this.setState({ isLoading: false })
    //     })
    // }

    getCancelChargesApiCall = () => {
        let data = {
        }
        ApiCall.postMethodWithHeader(Constants.new_cancellation_policy_url, data, Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                console.log("response---",response)
                this.setState({ ChargesPolicy: response.data, isLoading: false })
            }
            else {
                Helper.showToast(response.message)
            }
        });
    }


    _renderEarDetailsItem = ({ item, index }) => {
        return (
            <View style={{ marginTop: 5 }}>
                <Text style={{ color: Colors.black, fontFamily: fonts.RoBoToRegular_1, fontSize: fonts.fontSize14 }}>{item.charges_title}</Text>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                 {/* {this.state.isLoading ? <ListLoader /> : null} */}
                {/* {this.state.cancellationPolicyData ?
                    <PDFView
                        style={{ flex: 1 }}
                        resource={this.state.cancellationPolicyData}
                        resourceType={'url'}
                        onLoad={() => this.setState({ isLoading: false })}
                        onError={(error) => console.log('Cannot render PDF', error)}
                    />
                    : null} */}
{/* {this.state.ChargesPolicy ? 
                <FlatList
                    style={{ paddingHorizontal: 16, marginTop: 10 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.ChargesPolicy}
                    renderItem={this._renderEarDetailsItem}
                    extraData={this.state}
                    ListFooterComponent={() => {
                        return (
                            this.state.isLoading ? <ListLoader /> : null
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />:
                null
    } */}

                <WebView
                    source={{ uri:`${Constants.Base_Url}${Constants.new_cancellation_policy_url}/${this.state.userLanguage}`}} 
                    javaScriptEnabled={true}
                    originWhitelist={['*']}
                    domStorageEnabled={true}
                    scrollEnabled
                    scalesPageToFit
                    contentMode={'mobile'}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    onLoadProgress={this._onLoadProgress}
                    onLoadEnd={() => this.setState({ isLoading: false })}
                    onNavigationStateChange={this.onNavigationStateChange}
                    containerStyle={{width:'100%', paddingHorizontal: 0, backgroundColor: Colors.white, paddingTop: 5 ,paddingHorizontal:15}}
                    style={{}}
                />
                {this.state.isLoading ?
                    <View style={{ position: 'absolute', width: '100%', top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.white }}>
                        <ListLoader />
                    </View>
                    : null}

            </SafeAreaView>
        )
    }
};
