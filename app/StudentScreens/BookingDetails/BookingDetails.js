import React from 'react';
import { Text, View, SafeAreaView, Image, DeviceEventEmitter, ScrollView, TouchableOpacity } from 'react-native';
import styles from './BookingDetailsStyles';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import { ApiCall, AsyncStorageHelper, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import ListLoader from '../../Comman/ListLoader';
import { changeBookingStatus } from '../../Api/BookingApis';
import ImageLoadView from '../../Lib/ImageLoadView';
import moment from 'moment';
import { configureBackgroundTracking } from '../../Comman/BackgroundUserTracking';
import CountDown from 'react-native-countdown-component';
import TimeShow from '../../Comman/TimeShow';
import fonts from '../../Assets/fonts';



export default class BookingDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            booking_id: this.props.route.params.booking_id,
            arrBookingDetails: '',
            loading: true,
            buttonHide: false,
            Seconds: "",
            maxCountDownTime: 0
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('BookingDetail'),
            borderBottomRadius: 25,
            bellIcon: true, settingsIcon: true,
            settingIconClick: () => this.settingIconClick(),
            bellIconClick: () => this.bellIconClick(),

        })
    }

    componentDidMount() {
        // AsyncStorageHelper.getData('runningBooking').then((runningBooking) => {
        //     console.log("booking ==runningBooking", runningBooking);
        // })
        this.getBookingDetailApiCall();
        configureBackgroundTracking();
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToBookingRequests() {
        handleNavigation({ type: 'push', page: 'BookingRequestsScreen', navigation: this.props.navigation })
    }

    goToStartBooking() {
        handleNavigation({ type: 'push', page: 'StartBooking', navigation: this.props.navigation })
    }

    goToCancelChargesStudent() {
        handleNavigation({ type: 'push', page: 'CancelChargesStudent', navigation: this.props.navigation })
    }

    getCancelBookingApiCall = () => {
        Helper.backConfirm(LanguagesIndex.translate('Areyousureyouwanttocancelthisbooking'), (status) => {
            if (status) {
                Helper.globalLoader.showLoader();
                let data = {
                    booking_id: this.state.booking_id,
                    booking_service_id: this.state.arrBookingDetails.booking_service_id
                }

                ApiCall.postMethodWithHeader(Constants.cancel_booking, JSON.stringify(data), Constants.APIPost).then((response) => {
                    Helper.globalLoader.hideLoader();
                    if ((response.status == Constants.TRUE) || (response.user_status == Constants.TRUE)) {
                        DeviceEventEmitter.emit('CancelBookingStudent', "done")
                        this.props.navigation.goBack(null)
                        Helper.showToast(response.message)
                    }
                });
            }
        });
    }

    getBookingDetailApiCall = () => {
        let data = new FormData();
        data.append('booking_id', this.state.booking_id);
        ApiCall.postMethodWithHeader(Constants.booking_detail, data, Constants.APIImageUploadAndroid).then((response) => {
            if (response.status == Constants.TRUE && response.data && response.data.status) {

                this.setState({ arrBookingDetails: response.data, loading: false })
                let BookingAt = moment(response.data.booking_request.booking_date + " " + moment(response.data.booking_request.booking_time, ["hh:mm:ss A"]).format("HH:mm")).format();

                let CreateAt = moment(response.data.booking_request.created_at).format();


                let duration = moment.duration(moment(BookingAt).diff(CreateAt));
                this.setState({
                    Seconds: parseInt(duration.asSeconds())
                })

                let data = response.data.booking_request
                let c = moment(new Date()).format('YYYY-MM-DD')
                let buttonStart = moment(data.booking_date).isSame(c);

                let bTime = moment(data.booking_time, "HH:mm a").format('HH:mm');
                //console.log('bTime--bTime',bTime)
                let currentTime = moment(new Date()).format('YYYY-MM-DD HH:mm')
                let bookingTime = c + ' ' + bTime

                console.log('bookingTime--bookingTime', Helper.formatDateUtcToLocal(bookingTime))
                // console.log('currentTime--currentTime',currentTime);

                // console.log('===bookingTime=======>',bookingTime)
                // console.log('===currentTime=======>',currentTime)
                //if (buttonStart == true && Helper.formatDateUtcToLocal.bookingTime < currentTime ) {    
                if (buttonStart == true && bookingTime < currentTime) {
                    this.setState({ buttonHide: true })
                } else {
                    this.setState({ buttonHide: false })
                }


                this.methodGetMaxTime()

            } else {
                Helper.showToast(response.message);
                this.props.navigation.goBack(null);
            }
        })
    }

    UpdateBooking = (type) => {
        Helper.confirm(LanguagesIndex.translate('Areyousureyouwantto') + " " + `${type == 'ACCEPT' ? LanguagesIndex.translate('Accept') : LanguagesIndex.translate('Reject')}` + " " + LanguagesIndex.translate('bookingrequest?'), (status) => {
            if (status) {
                let data = new FormData();
                data.append('booking_id', this.state.booking_id);
                data.append('booking_service_id', this.state.arrBookingDetails.booking_service_id);
                data.append('status', type);

                changeBookingStatus(data, (res) => {
                    if (res.status == Constants.TRUE) {
                        Helper.showToast(res.message)
                        DeviceEventEmitter.emit('changeBookingStatus', "done")
                        this.getBookingDetailApiCall();
                        if (this.props.refreshList) {
                            this.props.refreshList()
                        }
                    }
                })
            }
        })
    }

    goToService() {
        handleNavigation({
            type: 'push', page: 'StudentMap', navigation: this.props.navigation,
            passProps: {
                booking_id:
                    this.state.arrBookingDetails?.booking_id,
                booking_service_id: this.state.arrBookingDetails?.booking_service_id,
                booking_time: this.state.arrBookingDetails?.booking_request?.booking_time,
                senior_lat: this.state.arrBookingDetails?.booking_request?.user?.lat,
                senior_lng: this.state.arrBookingDetails?.booking_request?.user?.lng,

            }
        })
    }

    startServiceRequest() {
        Helper.confirm(LanguagesIndex.translate('Areyousureyouwanttostartservice'), (status) => {
            if (status) {
                let data = new FormData();
                data.append('booking_id', this.state.booking_id);
                data.append('booking_service_id', this.state.arrBookingDetails.booking_service_id);
                data.append('status', 'START');

                changeBookingStatus(data, (res) => {
                    if (res.status == Constants.TRUE) {
                        let data = {
                            booking_id: this.state.booking_id,
                            booking_service_id: this.state.arrBookingDetails.booking_service_id,
                            start_time: moment().utc().format('YYYY-MM-DD HH:mm')
                        }
                        AsyncStorageHelper.setData('runningBooking', data).then(() => {
                            configureBackgroundTracking();
                        })
                        Helper.showToast(res.message)
                        DeviceEventEmitter.emit('changeBookingStatus', "done")
                        this.getBookingDetailApiCall();
                        if (this.props.refreshList) {
                            this.props.refreshList()
                        }
                    }
                })
            }
        })
    }

    methodGetMaxTime() {

        if (this.state.arrBookingDetails && this.state.arrBookingDetails.booking_request) {
            let utcCreateDate = this.state.arrBookingDetails.booking_request.created_at
            let bookingDate = this.state.arrBookingDetails.booking_request.booking_date
            let bookingTime = this.state.arrBookingDetails.booking_request.booking_time
            let combinedBookingDate = bookingDate + ' ' + bookingTime
            let createDate = moment.utc(utcCreateDate, 'YYYY-MM-DD HH:mm:ss').local()

            let strCreateDate = moment(createDate).format('YYYY-MM-DD HH:mm:ss')
            let strBookingDate = moment(combinedBookingDate, 'YYYY-MM-DD hh:mm a').format('YYYY-MM-DD HH:mm:ss')


            let duration = moment.duration(moment(strBookingDate).diff(moment(strCreateDate)));
            let day = duration.days();
            let hours = duration.hours();
            let minutes = duration.minutes();



            var maxCreateDate = null
            if (day >= 7) {
                // add 6 hours
                maxCreateDate = moment(strCreateDate).add(6, 'hour').format('YYYY-MM-DD HH:mm:ss')
            }

            else if (day > 1 || (day == 1 && (hours >= 1 || minutes >= 1))) {
                // add 3 hours
                maxCreateDate = moment(strCreateDate).add(3, 'hour').format('YYYY-MM-DD HH:mm:ss')
            }
            else if (day == 1 || hours >= 6) {
                // add 1 hours
                maxCreateDate = moment(strCreateDate).add(1, 'hour').format('YYYY-MM-DD HH:mm:ss')
            }

            else if (day == 0 || hours >= 24) {
                // add 30 minutes
                maxCreateDate = moment(strCreateDate).add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss')
            }
            if (maxCreateDate) {
                let currentDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                let maxDuration = moment.duration(moment(maxCreateDate).diff(moment(currentDate)));

                if (maxDuration && maxDuration > 0) {
                    this.setState({ maxCountDownTime: maxDuration })
                }
            }
        }


    }
    methodCountDownComplete() {
        this.setState({ maxCountDownTime: 0 })
    }

    goToChatScreen(item) {
        handleNavigation({
            type: 'push', page: 'ChatScreen', navigation: this.props.navigation,
            passProps: {
                other_user_id: item.id,
                name: item.name,
                picture: item.profile_picture,
                fromWhere: 'booking',
                extraData: {
                    bookingStatus: this.state.arrBookingDetails.status,
                    cancelBy: this.state.arrBookingDetails.cancelled_by_name,
                    givenRating: Number(this.state.arrBookingDetails.rating_status),
                    serviceName: this.state.arrBookingDetails.booking_services.activity?.name,
                }
            }
        })
    }

    render() {
        let { arrBookingDetails, loading } = this.state;
        // console.log("arrBookingDetails---arrBookingDetails------->>>>>=====>>>",this.state.arrBookingDetails?.booking_id);
        //console.log('arrBookingDetails?.status == && this.state.maxCountDownTime',arrBookingDetails?.status == 'PENDING',this.state.maxCountDownTime)
        // console.log("arrBookingDetails---arrBookingDetails------->>>>>=====>>>",this.state.arrBookingDetails?.booking_request?.booking_date);
        //  console.log("arrBookingDetails---arrBookingDetails------->>>>>=====>>>", this.state.arrBookingDetails?.booking_request?.booking_time);

        let tackTime = moment.utc(this.state.arrBookingDetails?.booking_request?.booking_time, "hh:mm a").local().format("hh:mm a");
        let subTractTime = moment(tackTime, "hh:mm a").subtract(15, "minutes").format("hh:mm a");
        let check = moment(new Date()).format("hh:mm a")
        var beginningTime = moment(check, 'h:mma');
        var endTime = moment(subTractTime, 'h:mma');

        // console.log("subTractTime---------------", beginningTime);
        // console.log("beginningTime---------------", endTime);

        // console.log("isAfter----------", beginningTime.isAfter(endTime)); // true

        // console.log("this.state.arrBookingDetails.booking_date",this.state.arrBookingDetails.booking_date)

        let val = moment(this.state.arrBookingDetails?.booking_request?.booking_date).isSameOrBefore(moment()) && beginningTime.isAfter(endTime);
        //console.log('val----val---val', val)

        console.log("arrBookingDetails.status---arrBookingDetails.status===>>>", arrBookingDetails)
        return (

            <SafeAreaView style={styles.safe_area_view}>
                {console.log('this.state.buttonHide', this.state.buttonHide)}
                <ScrollView bounces={false} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    {loading ?
                        <ListLoader /> :
                        arrBookingDetails && arrBookingDetails.status ?
                            <>
                                <TouchableOpacity
                                    // onPress={() => this.goToRatingsReviews()}
                                    style={{ backgroundColor: Colors.white, }}>
                                    <View style={styles.status_text_view}>
                                        <Text style={styles.status_text}>{LanguagesIndex.translate('status')}</Text>
                                        {/* {console.log('arrBookingDetails.status===....', arrBookingDetails)} */}
                                        {arrBookingDetails.booking_request.user.user_type == "ENTERPRISE" ?
                                            <Text style={[styles.completed_text, { color: Helper.getBookingStatus(arrBookingDetails.status).color }]}>{Helper.getBookingStatus(arrBookingDetails.status == "IN PROGRESS" ? '' : arrBookingDetails.status).value} {arrBookingDetails.status == 'CANCEL' && arrBookingDetails.cancelled_by_name ? `${LanguagesIndex.translate('by')} ${arrBookingDetails.cancelled_by_name}` : null}</Text>
                                            :
                                            <Text style={[styles.completed_text, { color: Helper.getBookingStatus(arrBookingDetails.status).color }]}>{Helper.getBookingStatus(arrBookingDetails.status).value} {arrBookingDetails.status == 'CANCEL' && arrBookingDetails.cancelled_by_name ? `${LanguagesIndex.translate('by')} ${arrBookingDetails.cancelled_by_name}` : null}</Text>
                                        }
                                    </View>
                                </TouchableOpacity>

                                <View style={styles.senior_person_view}>
                                    <View style={styles.senior_person_child_view}>
                                        <View style={styles.senior_person_profile_txt_view}>
                                            <Text style={styles.senior_person_prof_txt}>{arrBookingDetails?.booking_request.user.user_type == "BEOMY" ? LanguagesIndex.translate('SeniorPersonProfile') : LanguagesIndex.translate('ManagerProfile')}</Text>
                                        </View>
                                        <View style={[styles.senior_person_img_parent_view, { justifyContent: 'space-between' }]}>
                                            <View style={styles.senior_person_pro_flex_view}>
                                                <View style={styles.senior_profile_img_view}>
                                                    <ImageLoadView
                                                        source={arrBookingDetails.booking_request.user.profile_picture ? { uri: arrBookingDetails.booking_request.user.profile_picture } : images.default}
                                                        resizeMode={'cover'} style={[styles.senior_person_img, { borderRadius: 10 }]} />
                                                </View>
                                            </View>

                                            <View style={styles.senior_person_name_flex}>
                                                <Text style={styles.senior_person_name_txt}>{arrBookingDetails.booking_request.user.name}</Text>
                                            </View>
                                            {arrBookingDetails.booking_request.user.user_type == 'BEOMY' && (arrBookingDetails.status == "ACCEPT" || arrBookingDetails.status == "COMPLETE") ?
                                                <TouchableOpacity
                                                    onPress={() => this.goToChatScreen(arrBookingDetails.booking_request.user)}
                                                    style={{
                                                        backgroundColor: Colors.cerulean, paddingHorizontal: 5,
                                                        alignItems: 'center', marginTop: 5, paddingVertical: 5, borderRadius: 5
                                                    }}>
                                                    <Text style={[styles.name_txt, { color: Colors.white }]}>{LanguagesIndex.translate('Chat')}</Text>
                                                </TouchableOpacity>
                                                : null}
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.date_time_parent_view}>
                                    <View style={styles.date_time_text_view}>
                                        <Text style={styles.date_time_text}>{LanguagesIndex.translate('Date&Time')}</Text>
                                    </View>
                                    <View style={styles.date_time_view}>
                                        <View style={{ flex: 5, }}>
                                            <Text style={styles.date_text}>
                                                {/* {Helper.formatDate(arrBookingDetails.booking_request.booking_date, "DD/MM/YYYY")} */}
                                                {Helper.formatDateUtcToLocal(arrBookingDetails.booking_request.booking_date, arrBookingDetails.booking_request.booking_time, 'date')}
                                            </Text>
                                        </View>
                                        <View style={styles.flex_view}>
                                            <Text style={styles.time_text}>
                                                {Helper.formatDateUtcToLocal(arrBookingDetails.booking_request.booking_date, arrBookingDetails.booking_request.booking_time, 'time')}

                                                {/* {arrBookingDetails.booking_request.booking_time} */}
                                            </Text>
                                        </View>
                                    </View>
                                </View>


                                <View style={styles.location_parent_view}>
                                    <View style={styles.location_text_view}>
                                        <Text style={styles.location_text}>{LanguagesIndex.translate('Address')}</Text>
                                    </View>
                                    <View style={styles.address_view}>
                                        <Text style={styles.address_text}>
                                            {arrBookingDetails.booking_request.location}
                                        </Text>
                                    </View>
                                </View>


                                <View style={styles.date_time_parent_view}>
                                    <View style={styles.date_time_text_view}>
                                        <Text style={styles.date_time_text}>{LanguagesIndex.translate('Activity&Duration')}</Text>
                                    </View>
                                    <View style={styles.date_time_view}>
                                        <View style={{ flex: 5, }}>
                                            <Text style={styles.date_text}>
                                                {arrBookingDetails.booking_services.activity?.name}
                                            </Text>
                                        </View>
                                        <View style={styles.flex_view}>
                                            <Text style={styles.time_text}>
                                                {arrBookingDetails.booking_services.activity_duration} {LanguagesIndex.translate('hrs')}</Text>
                                        </View>
                                    </View>
                                </View>

                                {arrBookingDetails.booking_request && arrBookingDetails.booking_request.transaction ?
                                    <View style={styles.date_time_parent_view}>
                                        <View style={styles.date_time_text_view}>
                                            <Text style={styles.date_time_text}>{LanguagesIndex.translate('BookingAmount')}</Text>
                                        </View>
                                        <View style={styles.date_time_view}>
                                            <View style={{ flex: 5, }}>
                                                <Text style={styles.date_text}>
                                                    {LanguagesIndex.translate('HourlyRate')}
                                                </Text>
                                            </View>
                                            <View style={styles.flex_view}>
                                                <Text style={styles.time_text}>CHF {(Number(arrBookingDetails.booking_request.amount)) / Number(arrBookingDetails.booking_services.activity_duration)}/{LanguagesIndex.translate('hr')}</Text>
                                            </View>
                                        </View>
                                        {/* <View style={styles.date_time_view}>
                                            <View style={{ flex: 5, }}>
                                                <Text style={styles.date_text}>
                                                    {LanguagesIndex.translate('ServiceAmount')}
                                                </Text>
                                            </View>
                                            <View style={styles.flex_view}>
                                                <Text style={styles.time_text}>CHF {Number(arrBookingDetails.booking_request.transaction.amount)-Number(arrBookingDetails.booking_request.commission)}</Text>
                                            </View>
                                        </View> */}
                                        {/* <View style={styles.date_time_view}>
                                            <View style={{ flex: 5, }}>
                                                <Text style={styles.date_text}>
                                                    {LanguagesIndex.translate('Commission')}
                                                </Text>
                                            </View>
                                            <View style={styles.flex_view}>
                                                <Text style={styles.time_text}>CHF {Number(arrBookingDetails.booking_request.commission)}</Text>
                                            </View>
                                        </View> */}
                                        <View style={styles.date_time_view}>
                                            <View style={{ flex: 5, }}>
                                                <Text style={styles.date_text}>
                                                    {LanguagesIndex.translate('TotalAmount')}
                                                </Text>
                                            </View>
                                            <View style={styles.flex_view}>
                                                <Text style={styles.time_text}>CHF {Number(arrBookingDetails.booking_request.amount)}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    : null}

                                {arrBookingDetails?.booking_services.language ?
                                    <View style={styles.language_view}>
                                        <View style={styles.language_view_text}>
                                            <Text style={styles.student_language_text}>{LanguagesIndex.translate('LanguagePreference')}</Text>
                                        </View>
                                        <View style={styles.selected_language_view}>
                                            <Text style={styles.language_text}>
                                                {arrBookingDetails.booking_services.language}
                                            </Text>
                                        </View>
                                    </View> :
                                    null
                                }



                                <View style={styles.short_note_view}>
                                    <View style={styles.short_view_note}>
                                        <Text style={styles.short_text}>{LanguagesIndex.translate('ShortNote')}</Text>
                                    </View>
                                    <View style={styles.short_desc_view}>
                                        <Text style={styles.short_desc_text}>
                                            {arrBookingDetails?.booking_services.short_note}
                                        </Text>
                                    </View>
                                </View>

                                {arrBookingDetails?.status == 'PENDING' && arrBookingDetails?.booking_request.user.user_type == "ENTERPRISE" ?
                                    <View style={styles.btn_view_rej_acc}>
                                        <View style={styles.btn_child_view}>
                                            <TouchableOpacity
                                                onPress={() => this.UpdateBooking('REJECT')}
                                                // onPress={() => this.goToBookingRequests()} 
                                                style={styles.rej_touch}>
                                                <Text style={styles.rej_text}>{LanguagesIndex.translate('REJECT')}</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity
                                                onPress={() => this.UpdateBooking('ACCEPT')}
                                                // onPress={() => this.goToStartBooking()} 
                                                style={styles.acce_touch}>
                                                <Text style={styles.acce_text}>{LanguagesIndex.translate('ACCEPT')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    : null}

                                {arrBookingDetails?.status == 'PENDING' && this.state.maxCountDownTime && arrBookingDetails?.booking_request.user.user_type == "BEOMY" ?
                                    <View style={styles.btn_view_rej_acc}>
                                        <View style={styles.btn_child_view}>
                                            <TouchableOpacity
                                                onPress={() => this.UpdateBooking('REJECT')}
                                                // onPress={() => this.goToBookingRequests()} 
                                                style={styles.rej_touch}>
                                                <Text style={styles.rej_text}>{LanguagesIndex.translate('REJECT')}</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity
                                                onPress={() => this.UpdateBooking('ACCEPT')}
                                                // onPress={() => this.goToStartBooking()} 
                                                style={styles.acce_touch}>
                                                <Text style={styles.acce_text}>{LanguagesIndex.translate('ACCEPT')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    : null}
                            </>
                            : null
                    }

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 16 }}>
                        {arrBookingDetails?.status == "ACCEPT" && arrBookingDetails?.booking_request.user.user_type == "BEOMY" ?
                            <View style={styles.start_view_btn}>
                                <TouchableOpacity
                                    onPress={() => this.getCancelBookingApiCall()}
                                    style={styles.start_btn_touch}>
                                    <Text style={styles.start_txt}>{LanguagesIndex.translate('CANCEL')}</Text>
                                </TouchableOpacity>
                            </View> :
                            null
                        }

                            {this.state.buttonHide && val && arrBookingDetails?.status == "ACCEPT" ?
                            <View style={styles.start_view_btn}>
                                <TouchableOpacity
                                    onPress={() => { this.startServiceRequest() }}
                                    style={styles.start_btn_touch}>
                                    <Text style={styles.start_txt}>{LanguagesIndex.translate('START')}</Text>
                                </TouchableOpacity>
                            </View>:null}

                        {/* {this.state.buttonHide && val && arrBookingDetails?.status == "ACCEPT" ?
                            <View style={styles.start_view_btn}>
                                <TouchableOpacity
                                    onPress={() => { this.startServiceRequest() }}
                                    style={styles.start_btn_touch}>
                                    <Text style={styles.start_txt}>{LanguagesIndex.translate('START')}</Text>
                                </TouchableOpacity>
                            </View>
                            : (arrBookingDetails?.status == "ACCEPT" && arrBookingDetails?.booking_request.user.user_type == "ENTERPRISE") ?
                                <View style={styles.start_view_btn}>
                                    <TouchableOpacity
                                        onPress={() => { this.startServiceRequest() }}
                                        style={styles.start_btn_touch}>
                                        <Text style={styles.start_txt}>{LanguagesIndex.translate('START')}</Text>
                                    </TouchableOpacity>
                                </View> :
                                null} */}
                    </View>


                    {/* <View>
                        <View style={{
                            alignItems:'center',
                            marginVertical: 20
                        }}>
                            <TouchableOpacity
                                onPress={() => { this.goToService() }}
                                style={{
                                    paddingHorizontal:35,
                                    paddingVertical:10,
                                    backgroundColor:Colors.cerulean,
                                    borderRadius:10
                                }}>
                                <Text style={styles.start_txt}>{LanguagesIndex.translate('Direction')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View> */}



                    {arrBookingDetails?.status == "ACCEPT" && arrBookingDetails?.booking_request?.user?.user_type == "BEOMY" ?
                        <View style={{ paddingHorizontal: 16, paddingVertical: 15, alignItems: 'flex-start', marginTop: 10 }}>
                            <TouchableOpacity
                                onPress={() => this.goToCancelChargesStudent()} >
                                <Text style={{ color: Colors.black, fontSize: fonts.fontSize14 }}>{LanguagesIndex.translate('Forcancellationpolicyandcharges')}{" "}<Text style={{ color: Colors.cerulean, lineHeight: 15, textDecorationLine: 'underline' }}>{LanguagesIndex.translate('clickhere')}</Text></Text>
                            </TouchableOpacity>
                        </View> : null}




                    {/* 21600 seconds means 6 hours */}
                    {arrBookingDetails?.status == 'PENDING' && this.state.maxCountDownTime && arrBookingDetails?.booking_request.user.user_type == "BEOMY" ?
                        <View style={{ backgroundColor: Colors.white, paddingHorizontal: 16, paddingVertical: 15, alignItems: 'flex-start', marginTop: 10 }}>
                            <TimeShow maxCountDownTime={this.state.maxCountDownTime} timeOutComplete={() => this.methodCountDownComplete()} />
                        </View>
                        : null
                    }

                </ScrollView>
            </SafeAreaView>
        )
    }

};