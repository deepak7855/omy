import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default BookingDetailsStyles = StyleSheet.create({
  safe_area_view: {
    flex: 1,
    backgroundColor: Colors.whiteFive
  },
  date_time_parent_view: {
    marginTop: 5,
    backgroundColor: Colors.white,
    paddingVertical: 5
  },
  date_time_text_view: {
    marginHorizontal: 16,
    paddingVertical: 5
  },
  date_time_text: {
    fontSize: fonts.fontSize14,
    color: Colors.blackTwo,
    fontFamily: fonts.RoBoToMedium_1,
    lineHeight: 17
  },
  date_time_view: {
    marginHorizontal: 16,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  date_text: {
    fontSize: fonts.fontSize12,
    color: Colors.warmGreyThree,
    fontFamily: fonts.RoBoToRegular_1,
    lineHeight: 14
  },
  flex_view: {
    flex: 5,
    alignItems: 'flex-end'
  },
  time_text: {
    fontSize: fonts.fontSize12,
    color: Colors.warmGreyThree,
    fontFamily: fonts.RoBoToRegular_1,
    lineHeight: 14
  },
  location_parent_view: {
    marginTop: 5,
    backgroundColor: Colors.white,
    paddingVertical: 5
  },
  location_text_view: {
    marginHorizontal: 16,
    paddingVertical: 5
  },
  location_text: {
    fontSize: fonts.fontSize14,
    color: Colors.blackTwo,
    fontFamily: fonts.RoBoToMedium_1,
    lineHeight: 17
  },
  address_view: {
    marginHorizontal: 16,
    paddingVertical: 10,
    justifyContent: 'center'
  },
  address_text: {
    fontSize: fonts.fontSize12,
    color: Colors.warmGreyThree,
    fontFamily: fonts.RoBoToRegular_1,
    lineHeight: 14
  },
  student_language_text: {
    fontSize: fonts.fontSize14,
    color: Colors.blackTwo,
    fontFamily: fonts.RoBoToMedium_1,
    lineHeight: 17
  },
  language_view: {
    marginTop: 5,
    backgroundColor: Colors.white,
    paddingVertical: 5
  },
  language_view_text: {
    marginHorizontal: 16,
    paddingVertical: 5
  },
  selected_language_view: {
    marginHorizontal: 16,
    paddingVertical: 5,
    justifyContent: 'center'
  },
  language_text: {
    fontSize: fonts.fontSize12,
    color: Colors.warmGreyThree,
    fontFamily: fonts.RoBoToRegular_1,
    lineHeight: 14
  },
  short_note_view: {
    marginTop: 5,
    backgroundColor: Colors.white,
    paddingVertical: 5
  },
  short_view_note: {
    marginHorizontal: 16,
    paddingVertical: 5
  },
  short_text: {
    fontSize: fonts.fontSize14,
    color: Colors.blackTwo,
    fontFamily: fonts.RoBoToMedium_1,
    lineHeight: 17
  },
  short_desc_view: {
    marginHorizontal: 16,
    paddingVertical: 5,
    justifyContent: 'center'
  },
  short_desc_text: {
    fontSize: fonts.fontSize12,
    color: Colors.warmGreyThree,
    fontFamily: fonts.RoBoToRegular_1,
    lineHeight: 14
  },
  btn_view: {
    marginVertical: 10,
    marginTop: 35,
    marginHorizontal: 16
  },
  status_text_view:{
  marginHorizontal: 16, 
  flexDirection: 'row', 
  justifyContent: 'space-between', 
  paddingVertical: 10
  },
  status_text:{
    color: Colors.black, 
    fontSize: fonts.fontSize14, 
    fontFamily: fonts.RoBoToBold_1, 
    lineHeight: 17
  },
  completed_text:{
    fontSize: fonts.fontSize14, 
    fontFamily: fonts.RoBoToMedium_1, 
  },
  senior_person_view:{
    backgroundColor: Colors.white,
  },
  senior_person_child_view:{
    marginHorizontal: 16, 
    paddingVertical: 10
  },
  senior_person_profile_txt_view:{
    marginVertical: 5 
  },
  senior_person_prof_txt:{
    color: Colors.black, 
    fontSize: fonts.fontSize14, 
    fontFamily: fonts.RoBoToMedium_1, 
    lineHeight: 17
  },
  senior_person_img_parent_view:{
    flexDirection: 'row', 
    alignItems: 'center', 
    marginVertical: 5
  },
  senior_person_pro_flex_view:{
    flex: 1.5,
  },
  senior_profile_img_view:{
    height: 40, 
    width: 40, 
    borderRadius: 10
  },
  senior_person_img:{
    height: '100%', 
    width: '100%'
  },
  senior_person_name_flex:{
    flex: 8,
  },
  senior_person_name_txt:{
    color: Colors.warmGrey, 
    fontSize: fonts.fontSize14, 
    lineHeight: 17, 
    fontFamily: fonts.RoBoToMedium_1
  },
  total_amount_parent_view:{
    alignItems: 'center', 
    height: 50, 
    marginHorizontal: 16, 
    flexDirection: 'row', 
    justifyContent: 'space-between',
  },
  total_amount_flex_view:{
    flex: 5, 
    alignItems: 'flex-start'
  },
  total_amount_txt_rate:{
    color: Colors.cerulean, 
    fontSize: fonts.fontSize14, 
    fontFamily: fonts.RoBoToMedium_1, 
    lineHeight: 17
  },
  count_rate_flex_view:{
    flex: 5, 
    alignItems: 'flex-end'
  },
  btn_view_rej_acc:{
    backgroundColor: Colors.white,
    paddingVertical:20
  },
  btn_child_view:{
    marginHorizontal:16,
    flexDirection:'row',
    justifyContent:'space-between',
  },
  rej_touch:{
    justifyContent:'center',
    paddingHorizontal:35,
    paddingVertical:15, 
    borderWidth: 1, 
    borderColor: Colors.cerulean, 
    alignItems: 'center',
    borderRadius:10
  },
  rej_text:{
    color: Colors.cerulean, 
    fontSize: fonts.fontSize14, 
    fontFamily: fonts.RoBoToMedium_1, 
    lineHeight: 17
  },
  acce_touch:{
    paddingHorizontal:35,
    paddingVertical:15,
    backgroundColor: Colors.cerulean, 
    justifyContent:'center',
    alignItems: 'center',
    borderRadius:10
  },
  acce_text:{
    color: Colors.white, 
    fontSize: fonts.fontSize14, 
    fontFamily: fonts.RoBoToMedium_1, 
    lineHeight: 17
  },
  start_view_btn:{
    alignItems:'center',
    marginVertical:20
  },
  start_btn_touch:{
    paddingHorizontal:35,
    paddingVertical:10,
    backgroundColor:Colors.cerulean,
    borderRadius:10
  },
  start_txt:{
    color:Colors.white,
    fontSize:fonts.fontSize13,
    fontFamily:fonts.RoBoToBold_1
  },



});