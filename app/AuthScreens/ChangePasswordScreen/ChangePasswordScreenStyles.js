import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default ChangePasswordScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
  enter_txt_view:{
    marginHorizontal: 16 ,
    marginTop:20
  },
   enter_title_txt:{
    color:Colors.black,
    fontSize:fonts.fontSize14,
    fontFamily:fonts.RoBoToBold_1
   },
   input_view:{
    marginVertical: 10, 
    marginTop: 10, 
    marginHorizontal: 16
   },
   btn_view:{
    marginVertical: 10, 
    marginTop: 25, 
    marginHorizontal: 16
   },


});