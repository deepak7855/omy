import React from 'react';
import { Text, View, ScrollView, Dimensions, SafeAreaView, Keyboard } from 'react-native';
import styles from './ChangePasswordScreenStyles';
import { GButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import IconInput from '../../Comman/GInput';
import AppHeader from '../../Comman/AppHeader';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, AsyncStorageHelper, Constants } from '../../../app/Api';
import { handleNavigation } from '../../navigation/Navigation';

export default class ChangePasswordScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userChangePassForm: {
                current_password: '',
                new_password: '',
                confirm_password: '',
                validators: Number(Helper.user_data.is_password)?{
                    current_password : { required: true, minLength: 8, maxLength: 15 },
                    new_password: { required: true, minLength: 8, maxLength: 15 },
                    confirm_password: { required: true, matchWith: 'new_password' },
                }:{
                    new_password: { required: true, minLength: 8, maxLength: 15 },
                    confirm_password: { required: true, matchWith: 'new_password' },
                }
            }

        }
        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('ChangePassword') })
    }

    setValues(key, value) {
        let userChangePassForm = { ...this.state.userChangePassForm }
        if (key == 'current_password') { value = value.replace(/[^\S*$/]/, ''); }
        else if (key == 'new_password') { value = value.replace(/[^\S*$/]/, ''); }
        else if (key == 'confirm_password') { value = value.replace(/[^\S*$/]/, ''); }
        else { value = value; }
        userChangePassForm[key] = value;
        this.setState({ userChangePassForm })
    }

    change_password_Submit() {
        Keyboard.dismiss()
        let isValid = Helper.validate(this.state.userChangePassForm);
        if (isValid) {
            Helper.globalLoader.showLoader();
            let data = new FormData();
            data.append('current_password', this.state.userChangePassForm.current_password)
            data.append('new_password', this.state.userChangePassForm.new_password)
            ApiCall.postMethodWithHeader(Constants.CHANGE_PASSWORD, data, Constants.APIImageUploadAndroid).then((response) => {
                Helper.showToast(response.message)
                Helper.globalLoader.hideLoader();
                if (response.status == Constants.TRUE) {
                    let copyUserData = { ...Helper.user_data };
                    copyUserData.is_password = 1;
                    AsyncStorageHelper.setData(Constants.USER_DETAILS, copyUserData)
                    Helper.user_data = copyUserData
                    handleNavigation({ type: 'pop', navigation: this.props.navigation })
                }
                else {
                    Helper.showToast(response.message)
                }
            }
            )
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.enter_txt_view}>
                        <Text style={styles.enter_title_txt}>{LanguagesIndex.translate('EnterBelowToChangeYourPassword')}</Text>
                    </View>

                    <View style={styles.input_view}>
                        {Number(Helper.user_data.is_password) ?
                            <IconInput
                                placeholder={LanguagesIndex.translate('CurrentPassword')}
                                placeholderTextColor={Colors.warmGrey}
                                secureTextEntry={true}
                                setFocus={() => { this.new_password.focus(); }}
                                returnKeyType="next"
                                keyboardType={'default'}
                                onChangeText={(current_password) => this.setValues('current_password', current_password)}
                                value={this.state.userChangePassForm.current_password}
                            />
                            : null}
                        <IconInput
                            placeholder={LanguagesIndex.translate('NewPassword')}
                            placeholderTextColor={Colors.warmGrey}
                            secureTextEntry={true}
                            getFocus={(input) => { this.new_password = input }}
                            setFocus={(input) => { this.confirm_password.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(new_password) => this.setValues('new_password', new_password)}
                            value={this.state.userChangePassForm.new_password}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('ConfirmPassword')}
                            placeholderTextColor={Colors.warmGrey}
                            secureTextEntry={true}
                            getFocus={(input) => { this.confirm_password = input }}
                            setFocus={() => { this.change_password_Submit() }}
                            returnKeyType="done"
                            keyboardType={'default'}
                            onChangeText={(confirm_password) => this.setValues('confirm_password', confirm_password)}
                            value={this.state.userChangePassForm.confirm_password}
                        />
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('ResetPassword')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.change_password_Submit() }}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }

};



