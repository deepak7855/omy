import React from 'react';
import { ImageBackground, View, Dimensions, Image, Text, BackHandler, TouchableOpacity } from 'react-native';
import styles from './SplashScreenStyles';
import { images } from '../../Assets/imagesUrl';
import Colors from '../../Assets/Colors';
import { handleNavigation } from '../../navigation/Navigation';
import { Modalize } from 'react-native-modalize';
import AfterSplash from '../AfterSplash/AfterSplash';
import Helper from '../../Lib/Helper';


export default class SplashScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            modalPosition: ''
        }
    }

    componentDidMount() {
        Helper.navigationRef = this.props.navigation;
    }

    goToAfterSplash() {
        handleNavigation({ type: 'setRoot', page: 'AfterSplash', navigation: this.props.navigation })
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                {/* <ImageBackground
                    resizeMode={'stretch'}
                    source={images.new_splash_img}
                    style={styles.image_back_ground}
                > */}
                <View style={{ flex: 1, backgroundColor: Colors.cerulean }}>
                    <View style={{ alignItems: 'center', marginTop: 150 }}>
                        <Image source={images.splash_logo} resizeMode={'contain'}

                            style={{ width: 205, height: 205 }} />
                    </View>


                    {/* <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => this.goToAfterSplash()}
                        style={{ alignSelf: 'center', bottom: 20, position: 'absolute' }}>
                        <Image source={images.swipeup} resizeMode={'contain'}

                            style={{ height: 70, width: 70, }} />


                        <View style={{ alignSelf: 'center' }}>
                            <Text style={{ color: Colors.cerulean, fontFamily: fonts.RoBoToMedium_1, fontSize: fonts.fontSize14 }}>Swipe up</Text>
                        </View>
                    </TouchableOpacity> */}



                </View>
                <Modalize
                    modalHeight={Dimensions.get('window').height}
                    modalStyle={{ flex: 1 }}
                    scrollViewProps={{
                        showsVerticalScrollIndicator: false,
                        bounces: false,
                        scrollEnabled: false
                    }}
                    // ref={(refVal) => this.modalRef = refVal}
                    alwaysOpen={100}
                    dragToss={1}
                    panGestureEnabled={this.state.modalPosition == 'top' ? false : true}
                    onPositionChange={(val) => this.setState({ modalPosition: val })}
                    withHandle={false}
                // handleStyle
                >
                    <AfterSplash
                        navigation={this.props.navigation}
                        modalPosition={this.state.modalPosition}
                        type={'modal'}

                    />
                </Modalize>
            </View>
        )
    }

};
