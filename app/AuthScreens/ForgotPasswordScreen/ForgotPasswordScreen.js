import React from 'react';
import { Text, View, TouchableOpacity, Image, ImageBackground, Keyboard } from 'react-native';
import styles from './ForgotPasswordScreenStyles';
import { GButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import IconInput from '../../Comman/Input';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants } from '../../../app/Api';

export default class ForgotPasswordScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userForgotPassForm: {
                email: '',
                validators: {
                    email: { required: true, email: true },
                }
            }
        }
    }

    setValues(key, value) {
        let userForgotPassForm = { ...this.state.userForgotPassForm }
        userForgotPassForm[key] = value;
        this.setState({ userForgotPassForm })
    }

    submit_forgot_password = () => {
        let isValid = Helper.validate(this.state.userForgotPassForm);
        if (isValid) {
            Keyboard.dismiss()
            Helper.globalLoader.showLoader();
            let data = {
                email: this.state.userForgotPassForm.email,
            }
            ApiCall.postMethodWithHeader(Constants.FORGOT_PASSWORD, JSON.stringify(data), Constants.APIPost).then((response) => {
                Helper.globalLoader.hideLoader();
                Helper.showToast(response.message)
                if (response.status == true) {
                    handleNavigation({ type: 'push', page: 'LoginScreen', navigation: this.props.navigation })
                }
                else {
                    Helper.showToast(response.message)
                }
            }
            ).catch(err => {
                Helper.globalLoader.hideLoader();
            })
        }
    }


    render() {
        return (
            <View style={styles.safe_area_view}>
                <ImageBackground source={images.Login} style={styles.imag_back_ground} resizeMode="cover">
                    <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                        <TouchableOpacity hitSlop={{ left: 30, right: 30, bottom: 30, top: 30 }} onPress={() => this.props.navigation.goBack(null)}>
                            <Image
                                resizeMode={'contain'}
                                source={images.back_btn}
                                style={[{ height: 15, width: 15, resizeMode: 'contain', left:20,top:30 }]} />
                        </TouchableOpacity>
                        <View style={styles.log_view}>
                            <Image source={images.login_logo} style={styles.logo_img} resizeMode={'contain'} />
                        </View>

                        <View style={styles.forgot_pass_txt_view}>
                            <Text style={styles.forgot_txt}>{LanguagesIndex.translate('ForgotPassword')}</Text>
                        </View>

                        <View style={styles.enter_email_txt_view}>
                            <Text style={styles.enter_txt}>{LanguagesIndex.translate('EnterYourEmailAddressF')}</Text>
                        </View>

                        <View style={styles.pare_view}>
                            <View style={styles.input_view}>
                                <IconInput
                                    placeholder={LanguagesIndex.translate('email')}
                                    imagePath={images.mail_ic}
                                    height={12}
                                    width={15}
                                    placeholderTextColor={Colors.warmGrey}
                                    keyboardType={'email-address'}
                                    returnKeyType="done"
                                    setFocus={() => { this.submit_forgot_password() }}
                                    onChangeText={(email) => this.setValues('email', email)}
                                    value={this.state.userForgotPassForm.email}
                                    maxLength={50}
                                />
                            </View>

                            <View style={styles.btn_view}>
                                <GButton
                                    Text={LanguagesIndex.translate('RecoverPassword')}
                                    width={'100%'}
                                    height={50}
                                    borderRadius={10}
                                    onPress={() => { this.submit_forgot_password() }}
                                />
                            </View>
                        </View>
                    </KeyboardScroll>
                </ImageBackground>
            </View>
        )
    }

};






