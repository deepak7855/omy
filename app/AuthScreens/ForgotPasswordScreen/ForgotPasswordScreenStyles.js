import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default ForgotPasswordScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1
    },
    imag_back_ground: {
        flex: 1
    },
    log_view: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 50
    },
    logo_img: {
        width: 133, 
        height: 133
    },
    forgot_pass_txt_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 150
    },
    forgot_txt:{
        color: Colors.blackTwo, 
        fontSize: fonts.fontSize26, 
        fontFamily:fonts.RoBoToBold_1,
        fontWeight:'bold'
    },
    enter_email_txt_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 10
    },
    enter_txt:{
        color: Colors.blackTwo, 
        fontSize: fonts.fontSize12, 
        fontFamily:fonts.RoBoToRegular_1
    },
    pare_view:{
        marginTop: 71
    },
    input_view:{
        marginHorizontal: 16 
    },
    btn_view:{
        marginVertical: 10, 
        marginTop: 30, 
        marginHorizontal: 16
    },
});