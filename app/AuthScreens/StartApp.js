import React, { Component } from 'react';
import { View, FlatList, Dimensions, StatusBar, Platform } from 'react-native';
import AfterSplash from './AfterSplash/AfterSplash';
import SplashScreen from './SplashScreen/SplashScreen';

export default class StartApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderVertical = ({ item, index }) => {

        return (
            <>
                { index == 1 ?
                    <View style={{ height: Platform.OS === 'android' ? Dimensions.get('window').height - StatusBar.currentHeight : Dimensions.get('window').height, width: Dimensions.get('window').width }}>
                        <AfterSplash />
                    </View>
                    :
                    <View style={{ height: Platform.OS === 'android' ? Dimensions.get('window').height - StatusBar.currentHeight : Dimensions.get('window').height, width: Dimensions.get('window').width }}>
                        <SplashScreen />
                    </View>
                }
            </>
        )
    }

    render() {
        return (
            <>
                <FlatList
                    data={['1', '2']}
                    renderItem={this.renderVertical}
                    showsVerticalScrollIndicator={false}
                    pagingEnabled
                    extraData={this.state}
                />
            </>
        );
    }
}
