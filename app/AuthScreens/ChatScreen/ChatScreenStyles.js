import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default ChatScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.white
    },
    today_txt_view: {
        alignItems: 'center',
        marginVertical: 5
    },
    today_txt_status: {
        color: Colors.black,
        fontSize: fonts.fontSize13
    },
    chat_textInput_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 15,
        paddingHorizontal: 16,
        bottom: 10
    },
    chat_textInput_view_child: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical:5,
        flex: 5,
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: Colors.warmGrey,
        borderRadius: 30
    },
    input: {
        marginLeft: 10,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1,
        width: '70%'
    },
    icons_View: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 15
    },
    attach_touch: {
        marginHorizontal: 5
    },
    attach_camera_icon: {
        height: 25,
        width: 25
    },
    camera_touch: {
        marginHorizontal: 5
    },
    send_touch: {
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 10
    },
    blockText: {
        color: Colors.black, fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToMedium_1,
        textAlign: 'center'
    },
    image: {
        height: 80,
        width: 80,
        resizeMode:'contain'
    }

});