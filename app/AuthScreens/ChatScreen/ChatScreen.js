import React from 'react';
import { View, Image, SafeAreaView, Text, TouchableOpacity, FlatList, TextInput, DeviceEventEmitter, UIManager, LayoutAnimation, Keyboard, Platform } from 'react-native';
import Colors from '../../Assets/Colors';
import styles from './ChatScreenStyles';
import AppHeader from '../../Comman/AppHeader';
import fonts from '../../Assets/fonts';
import { images } from '../../Assets/imagesUrl';
import Helper from '../../Lib/Helper';
import ActionSheet from 'react-native-actionsheet';
import SocketController, { events } from '../../Lib/SocketController';
import ListLoader from '../../Comman/ListLoader';
import CameraController from '../../Lib/CameraController';
import { ApiCall, Constants } from '../../Api';
import ImageLoadView from '../../Lib/ImageLoadView';
import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import LanguagesIndex from '../../Languages';
import DeviceInfo from 'react-native-device-info';


if (Platform.OS === "android" && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

export default class ChatScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isBookingOpen: this.props.route.params.isBookingOpen,
            fromWhere: this.props.route.params.fromWhere,
            extraData: this.props.route.params.extraData,
            other_user_id: this.props.route.params.other_user_id,
            other_user_name: this.props.route.params.name,
            message: '',
            allChatList: [],
            first_message_id: 0,
            showLoader: true,
            lastMsgId: -1,
            change: false,
            sending: false,
            isBlockedByOther: false,
            KeyboardHeight: 0,
            is_blockedByMe: false,
            inputHeight: 0,

        }
        AppHeader({
            ...this.props.navigation,
            isAlsoBack: true,
            leftTitle: this.props.route.params.name,
            leftIcon: this.props.route.params.picture ? { uri: this.props.route.params.picture } : images.default,
            leftIconStyle: { height: 30, width: 30, borderRadius: 10, resizeMode: 'cover' },
            borderBottomRadius: 25,
            moreOptionsIcon: this.props.route.params.extraData.bookingStatus == 'ACCEPT' ? true : false,
            moreOptionsIconClick: () => this.moreOptionsIconClick(),
        })
    }

    moreOptionsIconClick = () => {
        this.ActionSheet.show()
    }

    blockUnblockUser() {
        Helper.confirm(LanguagesIndex.translate('Areyousureyouwantto') + " " + `${this.state.is_blockedByMe ? LanguagesIndex.translate('unblock') : LanguagesIndex.translate('block') + " "}?`, (status) => {
            if (status) {
                let form = {
                    user_id: Helper.user_data.id,
                    other_user_id: this.state.other_user_id,
                }
                SocketController.emitSocketEvent(events.user_block_unblock, form);
            }
        });
    }

    componentDidMount() {
        Helper.currentPage = 'chat';
        SocketController.checkConnection((cb) => {
            if (cb) {
                this.getMessageHistory('after')
            }
        });
        this.checkUserStatus();
        if (Platform.OS == 'ios') {
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
        }

        this.get_user_thread_response = DeviceEventEmitter.addListener(events.get_user_thread_response, (data) => {
            this.setState({
                first_message_id: data?.first_message_id,
                lastMsgId: data && data?.responseData&& data?.responseData.length > 0 ? data?.responseData[data?.responseData.length - 1].id : '',
                showLoader: false,
            });
            if(data && data?.responseData&& data.responseData.length > 0){
                this.setState({
                    allChatList: [...this.state.allChatList, ...data.responseData] 
                })
            }
        })

        this.multi_userlistener = DeviceEventEmitter.addListener(events.multi_user, (data) => {
            let getIndex = this.state.allChatList.findIndex(x => x.id === data.responseData.id);
            if (getIndex == -1) {
                this.state.allChatList.unshift(data.responseData);
                this.setState({ change: true, sending: false });
            }
        })


        this.check_User_Status = DeviceEventEmitter.addListener(events.check_user_status_response, (data) => {
            this.setState({ is_blockedByMe: data?.responseData?.is_blocked_by_me, isBlockedByOther: data?.responseData?.is_blocked })
        });

        this.receive_messagelistener = DeviceEventEmitter.addListener(events.receive_message, (data) => {
            if (Number(data.responseData.other_user_info.userid) == this.state.other_user_id) {
                let getIndex = this.state.allChatList.findIndex(x => x.id === data.responseData.id);
                if (getIndex > -1) {
                    return
                }
                let formdata = {
                    is_read: 1,
                    msg_id: data.responseData.id
                }
                SocketController.emitSocketEvent('read_message_update', formdata);
                this.state.allChatList.unshift(data.responseData);
                this.setState({ change: true });
            }
        })

        this.blockedUnblock = DeviceEventEmitter.addListener(events.user_block_unblock_response, (data) => {
            if (data.user_id == this.state.other_user_id) {
                this.setState({ isBlockedByOther: data.block_status })
            }
            if (data.user_id == Helper.user_data.id) {
                this.setState({ is_blockedByMe: data.block_status })
            }
        })

        this.backChat = DeviceEventEmitter.addListener('backChat', (data) => {
            this.props.navigation.goBack(null);
        })
    }

    checkUserStatus = () => {
        let data = {
            user_id: Helper.user_data.id,
            other_user_id: this.state.other_user_id,
        };
        SocketController.emitSocketEvent(events.check_user_status, data);
    };

    componentWillUnmount() {
        if (this.get_user_thread_response) {
            this.get_user_thread_response.remove();
        }

        if (this.multi_userlistener) {
            this.multi_userlistener.remove();
        }

        if (this.check_User_Status) {
            this.check_User_Status.remove();
        }

        if (this.receive_messagelistener) {
            this.receive_messagelistener.remove();
        }
        if (this.blockedUnblock) {
            this.blockedUnblock.remove();
        }
        if (this.backChat) {
            this.backChat.remove();
        }
        if (this.keyboardDidShowListener) {
            this.keyboardDidShowListener.remove();
        }
        if (this.keyboardDidHideListener) {
            this.keyboardDidHideListener.remove();
        }
        Helper.currentPage = '';
    }

    _keyboardDidShow(e) {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ KeyboardHeight: e.endCoordinates.height });
    }
    _keyboardDidHide() {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ KeyboardHeight: 0 })
    }

    sendMessage = () => {
        if (this.state.message.trim() != '') {
            this.setState({ sending: true }, () => {
                var data = {
                    user_id: Helper.user_data.id,
                    other_user_id: this.state.other_user_id,
                    msg: this.state.message.trim(),
                    message_type: "TEXT",
                }
                this.setState({ message: '' });
                SocketController.emitSocketEvent(events.send_message, data);
            })
        }
    }

    onEndReached = () => {
        if (!this.state.showLoader && this.state.first_message_id != 0 && this.state.first_message_id != this.state.allChatList[this.state.allChatList.length - 1].id) {
            this.setState({ showLoader: true }, () => {
                this.getMessageHistory('before');
            })
        }
    }

    selectMedia() {
        CameraController.open((response) => {
            if (response) {
                let formdata = new FormData();
                formdata.append("attachment", {
                   // uri: response.uri,
                    uri: response.path,
                    name: "test.jpg",
                    type: "image/jpg",
                });
                this.uploadMedia('IMAGE', formdata);
            }
        });
    }

    selectAttachment = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [
                    DocumentPicker.types.pdf, DocumentPicker.types.doc, DocumentPicker.types.docx,
                    DocumentPicker.types.xls, DocumentPicker.types.xlsx, DocumentPicker.types.plainText],
            });

            let formdata = new FormData();
            formdata.append("attachment", {
                uri: res.uri,
                type: res.type,
                name: res.name
            });
            this.uploadMedia('DOCUMENT', formdata);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    uploadMedia(type, formdata) {
        Helper.globalLoader.showLoader();
        ApiCall.postMethodWithHeader(Constants.chat_media, formdata, Constants.APIImageUploadAndroid).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == true || response.status == "true") {
                let data = {
                    user_id: Helper.user_data.id,
                    other_user_id: this.state.other_user_id,
                    msg: response.data.attachment,
                    message_type: type,
                }
                if (response.data.thumb_attachment) {
                    data.thumb_image = response.data.thumb_attachment;
                }
                this.setState({ message: '' });
                SocketController.emitSocketEvent(events.send_message, data);

            } else {
                Helper.showToast(response.message || LanguagesIndex.translate('Somethingwentwrong'));
            }
        });
    }

    getMessageHistory = (type) => {
        let data = {
            last_id: this.state.lastMsgId,
            limit: 20,
            other_user_id: this.state.other_user_id,
            type: type,
            user_id: Helper.user_data.id,
        }
        SocketController.emitSocketEvent(events.get_user_thread, data);
    }

    showFullImage(item) {
        let form = {
            media_type: 'image',
            media_url: Constants.attachmentUrl + item.msg
        }
        this.props.navigation.navigate('MediaDetailPage', { item: form })
    }

    showAttachment(item) {
        Helper.globalLoader.showLoader()
        let fileName = Constants.attachmentUrl + item.msg;
        let extension = fileName.split('.').pop();
        let localFile = `${RNFS.DocumentDirectoryPath}/file.${extension}`;
        let options = {
            fromUrl: fileName,
            toFile: localFile
        };

        RNFS.downloadFile(options).promise
            .then(() => {
                FileViewer.open(localFile, {
                    onDismiss: () => Helper.globalLoader.hideLoader()
                })
                Helper.globalLoader.hideLoader()
            })
            .then(() => {
                Helper.globalLoader.hideLoader()
            })
            .catch(error => {
                Helper.globalLoader.hideLoader()
            });
    }

    renderMsg(from, item) {
        return (
            <>
                <View style={{ alignSelf: from == 'right' ? 'flex-end' : 'flex-start', maxWidth: '60%' }}>
                    <View style={{ backgroundColor: from == 'right' ? Colors.veryLightBlue : Colors.whiteFour, padding: 10, alignItems: 'center', borderRadius: 20, borderBottomLeftRadius: from == 'left' ? 0 : 20, borderBottomRightRadius: from == 'right' ? 0 : 20 }}>
                        {item.message_type == "IMAGE" ?
                            <TouchableOpacity onPress={() => this.showFullImage(item)} activeOpacity={0.6}>
                                <ImageLoadView
                                    source={item?.msg ? { uri: Constants.attachmentUrl + item?.thumb_image } : images.default}
                                    resizeMode={'cover'}
                                    style={styles.image}
                                />
                            </TouchableOpacity> :
                            item.message_type == "DOCUMENT" ?
                                <TouchableOpacity onPress={() => this.showAttachment(item)} activeOpacity={0.6}>
                                    <Image resizeMode={"contain"} source={images.typeFile} style={styles.image} />
                                </TouchableOpacity>
                                :
                                <Text style={{ color: Colors.black, fontSize: fonts.fontSize13 }}>
                                    {item?.msg}
                                </Text>
                        }
                    </View>
                </View>
                <View>
                    <Text style={{ color: Colors.warmGrey, fontSize: fonts.fontSize12, textAlign: from, marginTop: 5 }}>{Helper.formatLanguageDate(item.created_at, 'DD MMM YYYY, h:mm a')}</Text>
                </View>
            </>
        )
    }

    renderChatListItem = ({ item }) => {
        return (
            <View style={{ backgroundColor: Colors.white, paddingVertical: 10, }}>
                <View style={{ flexDirection: 'column', paddingHorizontal: 15 }}>
                    {item.userid == Helper.user_data.id ?
                        <View>
                            {this.renderMsg('right', item)}
                        </View>
                        :
                        <View>
                            {this.renderMsg('left', item)}
                        </View>
                    }
                </View>
            </View>
        )
    }

    render() {
        let { extraData } = this.state;

        return (
            <SafeAreaView style={styles.safe_area_view}>
                {/* <View style={styles.today_txt_view}>
                    <Text style={styles.today_txt_status}>Today</Text>
                </View> */}
                {this.state.showLoader ? <ListLoader /> : null}
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.allChatList}
                    renderItem={this.renderChatListItem}
                    onEndReached={this.onEndReached}
                    onEndReachedThreshold={0.01}
                    extraData={this.state}
                    inverted
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => <View style={{ height: 13, }} />}
                />

                {this.state.isBookingOpen || (!this.state.isBlockedByOther && !this.state.is_blockedByMe && this.state.extraData.bookingStatus == 'ACCEPT') ?

                    <View style={[styles.chat_textInput_view, { marginBottom: Platform.OS == 'ios' && DeviceInfo.hasNotch() && this.state.KeyboardHeight ? this.state.KeyboardHeight - 34 : Platform.OS == 'ios' && this.state.KeyboardHeight ? this.state.KeyboardHeight : 0 }]}>
                        <View style={styles.chat_textInput_view_child}>
                            <TextInput
                                placeholder={LanguagesIndex.translate('Typeamessage')}
                                placeholderTextColor={Colors.warmGrey}
                                multiline={true}
                                underlinecolorandroid='transparent'
                                value={this.state.message}
                                onContentSizeChange={(event) =>
                                    this.setState({ inputHeight: event.nativeEvent.contentSize.height })
                                }
                                onChangeText={(message) => this.setState({ message })}
                                style={[styles.input, {
                                    height: Math.min(60, Math.max(35, this.state.inputHeight)),
                                    marginTop: Platform.OS == 'ios' ? 5 : 0
                                }]}
                            />
                            <View style={styles.icons_View}>
                                <TouchableOpacity activeOpacity={0.6} onPress={() => this.selectAttachment()} style={styles.attach_touch}>
                                    <Image resizeMode={"contain"} source={images.attach}
                                        style={styles.attach_camera_icon}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity activeOpacity={0.6} onPress={() => this.selectMedia()}
                                    style={styles.camera_touch}>
                                    <Image resizeMode={"contain"} source={images.camera_chat}
                                        style={styles.attach_camera_icon}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={() => !this.state.sending ? this.sendMessage() : null}
                            style={styles.send_touch}>
                            <Image resizeMode={"contain"} source={images.send_icon1} style={styles.attach_camera_icon} />
                        </TouchableOpacity>
                    </View>
                    : null}
                {this.state.fromWhere == 'booking' && this.state.extraData.bookingStatus == 'COMPLETE' ?
                    <View style={{ paddingHorizontal: 10, marginBottom: 5 }}>
                        <Text style={styles.blockText}>{LanguagesIndex.translate('thisserviceiscompleted')}</Text>
                        {Number(extraData.givenRating) && Number(extraData.givenRating) > 0 ?
                            <Text style={styles.blockText}>{Helper.user_data.user_type === 'BEOMY' ?
                                `${LanguagesIndex.translate('YouRated')} ${this.state.other_user_name} ${extraData.givenRating} ${LanguagesIndex.translate('star')} ${LanguagesIndex.translate('for')} ${extraData.serviceName} ${LanguagesIndex.translate('service')}`
                                :
                                `${this.state.other_user_name} ${LanguagesIndex.translate('ratedyou')} ${extraData.givenRating} ${LanguagesIndex.translate('star')} ${LanguagesIndex.translate('for')} ${extraData.serviceName} ${LanguagesIndex.translate('service')}`}
                            </Text>
                            : null}
                    </View>
                    : this.state.fromWhere == 'booking' && this.state.extraData.bookingStatus == 'CANCEL' ?
                        <View style={{ paddingHorizontal: 10, marginBottom: 5 }}>
                            <Text style={styles.blockText}>{LanguagesIndex.translate('thisserviceiscancelledby')} {this.state.extraData.cancelBy}</Text>
                        </View>
                        : this.state.is_blockedByMe ?
                            <View style={{ paddingHorizontal: 10, marginBottom: 5 }}>
                                <Text style={styles.blockText}>{LanguagesIndex.translate('Youcantsendmessagesbecauseyoublocked')} {this.state.other_user_name} </Text>
                            </View>
                            : this.state.isBlockedByOther ?
                                <View style={{ paddingHorizontal: 10, marginBottom: 5 }}>
                                    <Text style={styles.blockText}>{LanguagesIndex.translate('Youcantsendmessagesbecause')} {this.state.other_user_name} {LanguagesIndex.translate('hasblockedyou')}</Text>
                                </View>
                                : null
                }


                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={LanguagesIndex.translate('SelectAction')}
                    options={[`${this.state.is_blockedByMe ? LanguagesIndex.translate('Unblock') : LanguagesIndex.translate('Block')}` + " " + LanguagesIndex.translate('user'), LanguagesIndex.translate('Cancel')]}
                    onPress={(index) => {
                        if (index == 0) {
                            this.blockUnblockUser(index)
                        }
                    }}
                />
            </SafeAreaView>
        )
    }

};





