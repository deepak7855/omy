import { StyleSheet,Dimensions } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default AfterSplashStyles = StyleSheet.create({
    safe_area_view: { 
        height:Dimensions.get('window').height,
        // marginTop:-100
    },
    image_back_ground: {
        flex: 1,
    },
    top_button_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 30,
        marginHorizontal: 16
    },
    de_view:{
        width: 83, 
        height: 34, 
        backgroundColor: Colors.white, 
        borderRadius: 26, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        paddingHorizontal: 10, 
        alignItems: 'center'
    },
    de_text:{
        fontSize: fonts.fontSize14, 
        fontFamily: fonts.RoBoToMedium_1, 
        lineHeight: 17, 
        color: Colors.blackThree
    },
    switzer_img:{
        height: 20, 
        width: 30
    },
    en_view:{
        //width: 95, 
        height: 34, 
        backgroundColor: Colors.white, 
        borderRadius: 26, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        paddingHorizontal: 10, 
        alignItems: 'center'
    },
    en_text:{
        fontSize: fonts.fontSize10, 
        lineHeight: 13, 
        color: Colors.greyishBrown, 
        fontFamily: fonts.RoBoToMedium_1
    },
    en_img:{
        marginLeft:5,
        height: 15, 
        width: 20
    },
    after_splash_logo_view:{
        alignItems: 'center', 
        marginTop: 70
    },
    after_splash_logo:{
        height: 213, 
        width: 213
    },
    have_be_parent_view:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginHorizontal: 32.8, 
        marginTop: 90
    },
    have_be_round_view:{
        height: 120, 
        width: 120, 
        // padding: 10,
        backgroundColor: Colors.white, 
        borderRadius: 120 / 2, 
        borderWidth: 2, 
        borderColor: Colors.waterBlue, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    have_be_img:{
        height: 70, 
        width: 70
    },
    have_be_omy_text:{
        fontSize: fonts.fontSize10, 
        fontFamily: fonts.RoBoToMedium_1, 
        lineHeight: 13, 
        color: Colors.greyishBrown
    },
    have_account_view:{
        marginVertical: 10, 
        marginTop: 50, 
        marginHorizontal: 16, 
        alignItems: 'center'
    },
    have_account_text:{
        color: Colors.blackTwo, 
        fontSize: fonts.fontSize14, 
        lineHeight: 17, 
        fontFamily: fonts.RoBoToMedium_1
    },
    btn_view:{
        marginVertical: 10, 
        marginHorizontal: 16
    },


    lang_main_view:{
        flex: 1, 
        justifyContent: 'center',
        marginHorizontal:25,
        marginTop:15
    },
    lang_touch:{
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingVertical:10,
        alignItems:'center',
    },
    lang_icon:{
        height: 15, 
        width: 25
    },
    lang_txt:{
        fontSize: fonts.fontSize14, 
        fontFamily: fonts.RoBoToMedium_1, 
        lineHeight: 17, 
        color: Colors.blackThree
    },
});