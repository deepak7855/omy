import React from 'react';
import { Text, useRef, View, ScrollView, Image, Dimensions, ImageBackground, BackHandler, TouchableOpacity, Linking } from 'react-native';
import styles from './AfterSplashStyles';
import { GButton } from '../../Comman/GButton';
import { images } from '../../Assets/imagesUrl';
import { handleNavigation } from '../../navigation/Navigation';
import { Modalize } from 'react-native-modalize';
import LanguagesIndex from '../../Languages';
import * as RNLocalize from "react-native-localize";
import { AsyncStorageHelper } from '../../Api';
import Helper from '../../Lib/Helper';
// import {socketInit} from '../../Lib/SocketController';

export default class AfterSplash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    logInClick = () => {
        handleNavigation({ type: 'push', page: 'LoginScreen', navigation: this.props.navigation })
    }


    signupSenior = () => {
        handleNavigation({ type: 'push', page: 'SeniorSignUpScreen', navigation: this.props.navigation })
    }

    sigInUpInClick = () => {
        handleNavigation({ type: 'push', page: 'StudentSignUpScreen', navigation: this.props.navigation })
    }

    signUpEnterprise = () => {
        handleNavigation({ type: 'push', page: 'SignUpScreen', navigation: this.props.navigation })
    }

    async gotoEnterprise() {
        const supported = await Linking.canOpenURL('http://omyweb.ch/');
        if (supported) {
            // Opening the link with some app, if the URL scheme is "http" the web link should be opened
            // by some browser in the mobile
            await Linking.openURL('http://omyweb.ch/');
        }
    }

    componentDidMount() {
        AsyncStorageHelper.getData('lan').then((data) => {
            if(data){
                LanguagesIndex.MyLanguage = data
            }else{
                LanguagesIndex.MyLanguage = 'de'
            }
           
          })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
    }

    componentWillUnmount() {
        // BackHandler.removeEventListener("hardwareBackPress", this.backAction);
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    languageChange(type) {
        LanguagesIndex.MyLanguage = type;
        this.handleLocalizationChange(LanguagesIndex.MyLanguage)
        AsyncStorageHelper.setData("lan", LanguagesIndex.MyLanguage)
        this.modalizeRef.close()
    }

    handleBackButtonClick = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation })
    };

    render() {
        return (
            <View style={styles.safe_area_view}>
                <ImageBackground source={images.After_splash} style={styles.image_back_ground}
                    resizeMode={'cover'}>
                    {this.props.modalPosition != 'top' && this.props.type == 'modal' ?
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{ alignSelf: 'center', top: 0 }}
                        >
                            <Image source={images.swipeup} resizeMode={'contain'}
                                style={{ height: 70, width: 70 }}
                            />

                        </TouchableOpacity>
                        : null}
                    <ScrollView bounces={false} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} style={{ marginBottom: 25 }}>

                        <View style={styles.top_button_view}>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                onPress={() => this.modalizeRef.open()}
                                style={styles.de_view}>
                                <Text style={[styles.de_text, { textTransform: 'uppercase' }]}>{LanguagesIndex.MyLanguage}</Text>
                                <Image source={LanguagesIndex.MyLanguage == "en" ? images.English : LanguagesIndex.MyLanguage == "fr" ? images.French : images.German}
                                    resizeMode={'contain'}
                                    style={styles.switzer_img}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity

                                onPress={() => this.signUpEnterprise()}
                                // onPress={() => this.gotoEnterprise()} 

                                style={styles.en_view}>
                                <Text style={styles.en_text}>{LanguagesIndex.translate('enterprise')}</Text>
                                <Image source={images.enterprise_ic__blue} resizeMode={'contain'} style={styles.en_img} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.after_splash_logo_view}>
                            <Image source={images.after_splash_logo} resizeMode={'contain'} style={styles.after_splash_logo} />
                        </View>

                        <View style={styles.have_be_parent_view}>
                            <TouchableOpacity
                                onPress={() => this.sigInUpInClick()} style={styles.have_be_round_view}>
                                <Image source={images.have_omy_black} resizeMode={'contain'} style={styles.have_be_img} />
                                <Text style={styles.have_be_omy_text}>{LanguagesIndex.translate('haveOmy')}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.signupSenior()} style={styles.have_be_round_view}>
                                <Image source={images.be_omy_blue} resizeMode={'contain'} style={styles.have_be_img} />
                                <Text style={styles.have_be_omy_text}>{LanguagesIndex.translate('beOMY')}</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.have_account_view}>
                            <Text style={styles.have_account_text}>{LanguagesIndex.translate('alreadyAccount')}</Text>
                        </View>
                        <View style={styles.btn_view}>
                            <GButton
                                Text={LanguagesIndex.translate('LOGIN')}
                                width={'40%'}
                                height={40}
                                borderRadius={5}
                                onPress={() => { }}
                                onPress={() => { this.logInClick() }}
                            />
                        </View>
                    </ScrollView>
                </ImageBackground>

                <Modalize
                    modalHeight={150}
                    ref={(getref) => this.modalizeRef = getref}
                >
                    <View style={styles.lang_main_view}>
                        <TouchableOpacity
                            activeOpacity={0.6}
                            onPress={() => this.languageChange('en')}
                            style={styles.lang_touch}>
                            <Text style={styles.lang_txt}>{LanguagesIndex.translate('English')}</Text>

                            <Image source={images.English}
                                resizeMode={'contain'}
                                style={styles.lang_icon}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.languageChange('de')}
                            activeOpacity={0.6}
                            style={styles.lang_touch}>
                            <Text style={styles.lang_txt}>{LanguagesIndex.translate('German')}</Text>

                            <Image source={images.German}
                                resizeMode={'contain'}
                                style={styles.lang_icon}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.languageChange('fr')}
                            activeOpacity={0.6}
                            style={styles.lang_touch}>
                            <Text style={styles.lang_txt}>{LanguagesIndex.translate('French')}</Text>

                            <Image source={images.French}
                                resizeMode={'contain'}
                                style={styles.lang_icon}
                            />
                        </TouchableOpacity>
                    </View>
                </Modalize>
            </View>
        )
    }

};



