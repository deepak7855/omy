import React from 'react';
import { View, Image, Text, TouchableOpacity, FlatList, DeviceEventEmitter, Platform } from 'react-native';
import Colors from '../../Assets/Colors';
import styles from './MessageScreenStyles';
import AppHeader from '../../Comman/AppHeader';
import fonts from '../../Assets/fonts';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import { images } from '../../Assets/imagesUrl';
import Helper from '../../Lib/Helper';
import SocketController, { events } from '../../Lib/SocketController';
import moment from 'moment';
import ImageLoadView from '../../Lib/ImageLoadView';
import NoData from '../../Comman/NoData';
import ListLoader from '../../Comman/ListLoader';

export default class MessageScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            messageList: []
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('Messages'),
            borderBottomRadius: 25,
            hideLeftBackIcon: true,
            bellIcon: true,
            settingsIcon: true,
            settingIconClick: () => this.settingIconClick(),
            bellIconClick: () => this.bellIconClick(),
        })
    }


    componentDidMount() {
        Helper.currentPage = 'inbox';
        this._inboxunsubscribe = this.props.navigation.addListener('focus', () => {
            this.getInboxList();
            SocketController.getUnreadCount();
        });
        this.inboxList = DeviceEventEmitter.addListener(events.get_message_history_response, (data) => {
            this.setState({ isLoading: false, messageList: data.responseData })
        })

        this.receive_messageInbox = DeviceEventEmitter.addListener(events.receive_message, (data) => {
            this.getInboxList();

//             let userIndex = this.state.messageList.findIndex(x => Number(x.conversation_id) === Number(data.responseData.conversation_id));

//             if (userIndex > -1) {
//                 let unread_message = Number(this.state.messageList[userIndex].unread_message);
//                 this.state.messageList.splice(userIndex, 1);
//                 if (unread_message) {
//                     data.responseData.unread_message = unread_message + 1;
//                 } else {
//                     data.responseData.unread_message = 1;
//                 }
//             } else {
//                 data.responseData.unread_message = 1;
//             }
// console.log(data.responseData,"data.responseData")
//             this.state.messageList.unshift(data.responseData);
//             this.setState({ change: !this.state.change });
        })
    }

    componentWillUnmount() {
        this._inboxunsubscribe();
        if (this.inboxList) {
            this.inboxList.remove();
        }
        if (this.receive_messageInbox) {
            this.receive_messageInbox.remove();
        }
        Helper.currentPage = '';
    }

    getInboxList = () => {
        let data = {
            user_id: Helper.user_data.id,
        }
        SocketController.emitSocketEvent(events.get_message_history, data);
    } 

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToChatScreen(item) {
        handleNavigation({
            type: 'push', page: 'ChatScreen', navigation: this.props.navigation,
            passProps: {
                other_user_id: item.other_user_info.userid,
                name: item.other_user_info.name,
                picture: item.other_user_info.picture,
                fromWhere: 'message',
                isBookingOpen: Number(item.is_booking) > 0,
                extraData: {

                }
            }
        })
    }

    renderMessageItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => this.goToChatScreen(item)}
                style={styles.message_list_touch}>
                <View style={styles.list_view}>
                    <View style={{ flex: 2, paddingHorizontal: 5 }}>
                        <View style={styles.profile_img_view}>
                            <ImageLoadView
                                source={item.other_user_info?.picture ? { uri: item.other_user_info?.picture } : images.default}
                                resizeMode={'cover'}
                                style={styles.profile_img}
                            />
                        </View>
                    </View>
                    <View style={styles.name_message_view}>
                        <Text style={styles.name_text}>{item.other_user_info?.name}</Text>
                        <View style={{ marginTop: 5, }}>

                            <Text numberOfLines={1} style={styles.message_text}>{item.message_type == "IMAGE" ? "IMAGE" : item.message_type == "DOCUMENT" ? "DOCUMENT" : item.msg}</Text>
                        </View>
                    </View>
                    <View style={styles.time_and_Count_view}>
                        <Text style={styles.time_status_txt}>{moment.utc(item.created_at).local().fromNow()}</Text>
                        {Number(item.unread_message) > 0 ?
                            <View style={styles.count_view}>
                                <Text style={styles.count_text}>{item.unread_message}</Text>
                            </View> : null
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                {this.state.isLoading ? <View style={{ marginTop: 10 }}><ListLoader /></View> : null}
                {!this.state.isLoading && this.state.messageList.length < 1 ?
                    <NoData message={LanguagesIndex.translate('Youdonothaveanymessages')} />
                    : null
                }
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.messageList}
                    renderItem={this.renderMessageItem}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => <View style={{ borderBottomWidth: 6, borderBottomColor: Colors.whiteThree }} />}
                />
            </View>
        )
    }
};





