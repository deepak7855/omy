import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default MessageScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        //backgroundColor: Colors.white
    },
    message_list_touch: {
        backgroundColor: Colors.white,
        paddingVertical: 10,
    },
    list_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    profile_img_view: {
        height: 60,
        width: 60,
        borderRadius: 18,
        backgroundColor: Colors.white
    },
    profile_img: {
        height: 60,
        width: 60,
        borderRadius:10
    },
    name_message_view: {
        flex: 5.0,
        marginTop: 5,
       // marginLeft:20
    },
    name_text: {
        color: Colors.black,
        fontSize: fonts.fontSize15,
        fontFamily: fonts.RoBoToBold_1,
        fontWeight: 'bold'
    },
    message_text: {
        color: Colors.warmGrey,
        fontSize: fonts.fontSize13,
        fontFamily: fonts.RoBoToRegular_1
    },
    time_and_Count_view: {
        //backgroundColor:'red',
        flex: 3,
        alignItems: 'flex-end',
        marginTop: 5
    },
    time_status_txt: {
        color: Colors.warmGrey,
        fontSize: fonts.fontSize10,
        fontFamily: fonts.RoBoToRegular_1
    },
    count_view: {
        backgroundColor: Colors.cerulean,
        borderRadius: 24/2,
        height: 24,
        width: 24,
        marginTop: 10,
        alignItems: 'center',justifyContent: 'center',
    },
    count_text: {
        color: Colors.white,
        fontSize: fonts.fontSize12
    },
});