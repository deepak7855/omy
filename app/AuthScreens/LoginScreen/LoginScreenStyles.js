import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default LoginScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1
    },
    imag_back_ground: {
        flex: 1
    },
    log_view: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40
    },
    logo_img: {
        width: 105,
        height: 105
    },
    welcome_text_view:{
        justifyContent: 'center', 
        alignItems: 'center',
        marginTop:5 
    },
    welcome_text: {
        color: Colors.white, 
        lineHeight: 32, 
        fontSize: fonts.fontSize26,
        fontFamily:fonts.RoBoToBold_1
    },
    input_btn_parent_view:{
        marginTop:120
    },
    input_view:{
        marginHorizontal: 16
    },
    forgot_text_view:{
        marginHorizontal: 16 
    },
    forgot_touch:{
        alignSelf: 'flex-end'
    },
    forgot_text:{
        fontSize: fonts.fontSize12, 
        lineHeight: 14, 
        color: Colors.blackTwo,
        fontFamily:fonts.RoBoToMedium_1
    },
    login_btn_view:{
        marginVertical: 10, 
        marginTop: 30, 
        marginHorizontal: 16
    },
    or_view:{
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'row', 
        marginVertical: 10
    },
    or_line_view:{
        backgroundColor: Colors.warmGreyTwo, 
        height: 1, 
        width: 15
    },
    or_text:{
        marginHorizontal: 5, 
        fontSize: fonts.fontSize12, 
        color: Colors.warmGreyTwo,
        fontFamily:fonts.RoBoToBold_1
    },
    social_fb_apple_view:{
        marginVertical: 10, 
        marginHorizontal: 16
    },
    do_not_text_view:{
        marginVertical: 10, 
        marginHorizontal: 16,
        alignItems:'center'
    },
    do_not_text:{
        lineHeight:14,
        fontSize:fonts.fontSize12,
        color:Colors.black,
        fontFamily:fonts.RoBoToRegular_1
    },
    sign_up_text:{
        fontSize:fonts.fontSize12,
        color:Colors.azul,
        lineHeight:14,
        fontFamily:fonts.RoBoToMedium_1
    },
    modal_color_view:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.cerulean,
        paddingVertical: 100, 
        paddingHorizontal:40,
        //borderBottomRightRadius: 30, 
        //borderBottomLeftRadius: 30,
      },
      have_be_round_view:{
        height: 120, 
        width: 120, 
        // padding: 10,
        backgroundColor: Colors.white, 
        borderRadius: 120 / 2, 
        borderWidth: 2, 
        borderColor: Colors.waterBlue, 
        alignItems: 'center', 
        justifyContent: 'center',
        marginVertical:20
    },
    have_be_img:{
        height: 70, 
        width: 70
    },
    have_be_omy_text:{
        fontSize: fonts.fontSize10, 
        fontFamily: fonts.RoBoToMedium_1, 
        lineHeight: 13, 
        color: Colors.greyishBrown
    },


   
});