import React from 'react';
import { Text, View, Image, ImageBackground, TouchableOpacity, Keyboard,Modal } from 'react-native';
import styles from './LoginScreenStyles';
import { GButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import IconInput from '../../Comman/Input';
import { handleNavigation } from '../../navigation/Navigation';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import { FacebookLogin, } from '../../Comman/SocialLogin';
import { SocialButton } from '../../Comman/GButton';

import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import LanguagesIndex from '../../Languages';
import * as RNLocalize from "react-native-localize";

export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            PasswordIcon: true,
            userLoginForm: {
                // udaysanth@yopmail.com
                // 12345678
                email: '',
                password: '',
                device_type: '',
                device_id: '',
                validators: {
                    email: { required: true, email: true },
                    password: { required: true },
                }
            },
            modalVisible: false,
            userType:''
        }


    }

    setModalVisible(visible){
        this.setState({ modalVisible: visible });
    }


    setValues(key, value) {
        let userLoginForm = { ...this.state.userLoginForm }
        userLoginForm[key] = value;
        this.setState({ userLoginForm })
    }

    ShowPassword = () => {
        this.setState({ PasswordIcon: !this.state.PasswordIcon })
    }

    login_Submit = () => {
        let isValid = Helper.validate(this.state.userLoginForm);
        if (isValid) {
            Helper.globalLoader.showLoader();
            Keyboard.dismiss()
            let data = new FormData();
            data.append('device_type', Helper.device_type)
            data.append('device_id', Helper.device_id)
            data.append('email', this.state.userLoginForm.email)
            data.append('password', this.state.userLoginForm.password)
            data.append('lan', LanguagesIndex.MyLanguage)

            ApiCall.postMethod(Constants.LOGIN, data, Constants.APIImageUploadAndroid).then((response) => {
                Helper.globalLoader.hideLoader();
                if (response.status == Constants.TRUE) {
                    Helper.token = response.token
                    //console.log('response.dataresponse.data',JSON.stringify(response.data))
                    AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                    AsyncStorageHelper.setData("token", response.token)

                    LanguagesIndex.MyLanguage = response.data.default_lang
                    AsyncStorageHelper.setData("lan", LanguagesIndex.MyLanguage)

                    Helper.user_data = response.data
                    if (response.data.user_type == 'HAVEOMY') {
                        Helper.navRef.switchNavigation('2');
                    } else if (response.data.user_type == 'BEOMY') {
                        Helper.navRef.switchNavigation('3');
                    } else if (response.data.user_type == 'ENTERPRISE') {
                        Helper.navRef.switchNavigation('1');
                    }
                }
                else {
                    Helper.showToast(response.message)
                }
            }
            ).catch(err => {
                Helper.globalLoader.hideLoader();
            })
        }
    }


    goToSingUp() {
        if (this.props.navigation.canGoBack()) {
            handleNavigation({ type: 'pop', navigation: this.props.navigation })
        } else {
            handleNavigation({ type: 'setRoot', page: 'AfterSplash', navigation: this.props.navigation })
        }
    }

    goToForgotPass() {
        this.props.navigation.navigate('ForgotPasswordScreen')

    }

    gotoFacebookSocialSide = () => {
        this.setState({SocialTypeLogin:'FACEBOOK'})
        
        FacebookLogin((result) => {
            if (result) {
                Helper.globalLoader.showLoader();
                this.gotoSocialNext(result)
            } else {
                //  Helper.showToast(Constants.SorryMessageError);
            }
        })
        
    }

    gotoSocialNext = (result) => {
        this.setState({socialdata:result})
        this.setModalVisible(true)
    }

    appleSignIn = (result) => {
        this.setState({SocialTypeLogin:'APPLE'})
        if (!result.user) return
        try {
            let formdata = {
                device_id: Helper.device_id,
                device_type: Helper.device_type,
                social_id: result.user,
                social_type: 'APPLE',
                name: '',
                email: '',
                profile_picture: '',
                //user_type: this.state.userType
            }

            if (result.email) {
                formdata.email = result.email;
            }
            if (result.fullName && result.fullName.givenName) {
                formdata.name = result.fullName.givenName;
            }

            Helper.globalLoader.showLoader();
            this.gotoSocialNext(formdata)
            //this.callSocialLogin(formdata);

        } catch (error) {
        }
    }


    callSocialLogin = (result) => {
        console.log('resultresultresultresultresult',result)
        ApiCall.postMethod(Constants.user_social, JSON.stringify(result), Constants.APIPost,).then((response) => {
            if (response.status == Constants.TRUE) {

                Helper.globalLoader.hideLoader();
                Helper.token = response.token
                AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                AsyncStorageHelper.setData("token", response.token)
                AsyncStorageHelper.setData("MyLanguage", response.data.default_lang)
                Helper.user_data = response.data

                if (response.data.user_type == 'HAVEOMY') {
                    Helper.navRef.switchNavigation('2');
                } else if (response.data.user_type == 'BEOMY') {
                    Helper.navRef.switchNavigation('3');
                } else if (response.data.user_type == 'ENTERPRISE') {
                    Helper.navRef.switchNavigation('1');
                    //handleNavigation({ type: 'setRoot', page: 'BottomTab', navigation: this.props.navigation })
                }

            } else {
                Helper.showToast(response.message)
                Helper.globalLoader.hideLoader();
            }
        }).catch(err => {
            Helper.globalLoader.hideLoader();
        })
    }

    setUserType = (type) => {
        this.setState({userType:type},() => {

            this.setModalVisible(false)
            var result = this.state.socialdata
            result.user_type = this.state.userType

            this.callSocialLogin(result);

            // if(this.state.SocialTypeLogin == 'FACEBOOK'){
            //     this.gotoFacebookSocialSideNext()
            // }   
        })
    }


    userTypeModal = () => {
        return (
            <Modal animationType={"slide"} transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                    <TouchableOpacity
                        onPress={() => this.setModalVisible(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                        <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.white}} />
                        
                    </TouchableOpacity>


                        <Text style={styles.welcome_text}>Please Select User Type</Text>
                    <TouchableOpacity
                        onPress={() => this.setUserType('HAVEOMY')} style={styles.have_be_round_view}>
                        <Image source={images.have_omy_black} resizeMode={'contain'} style={styles.have_be_img} />
                        <Text style={styles.have_be_omy_text}>{LanguagesIndex.translate('haveOmy')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.setUserType('BEOMY')} style={styles.have_be_round_view}>
                        <Image source={images.be_omy_blue} resizeMode={'contain'} style={styles.have_be_img} />
                        <Text style={styles.have_be_omy_text}>{LanguagesIndex.translate('beOMY')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.setUserType('ENTERPRISE')} style={styles.have_be_round_view}>
                        <Image source={images.enterprise_ic__blue} resizeMode={'contain'} style={styles.have_be_img} />
                        <Text style={styles.have_be_omy_text}>{LanguagesIndex.translate('enterprise')}</Text>
                    </TouchableOpacity>
                        
                    </View>

                    {/* <View style={styles.btn_view}>
                        <GButton
                            Text='Go To Home'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisible(false) }}
                        />
                    </View> */}
                </Modal>


        )
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                {this.userTypeModal()}
                <ImageBackground source={images.Login} style={styles.imag_back_ground} resizeMode={'stretch'}>
                    <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                        <View style={styles.log_view}>
                            <Image source={images.login_logo} style={styles.logo_img} resizeMode={'contain'} />
                        </View>

                        
                        <View style={styles.welcome_text_view}>
                            <Text style={styles.welcome_text}>{LanguagesIndex.translate('WelcomeBack')}</Text>
                        </View>

                        <View style={styles.input_btn_parent_view}>
                            <View style={styles.input_view}>
                                <IconInput
                                    placeholder={LanguagesIndex.translate('email')}
                                    imagePath={images.mail_ic}
                                    height={12}
                                    width={15}
                                    placeholderTextColor={Colors.warmGrey}
                                    keyboardType={'email-address'}
                                    setFocus={() => { this.password.focus(); }}
                                    returnKeyType="next"
                                    onChangeText={(email) => this.setValues('email', email)}
                                    value={this.state.userLoginForm.email}
                                />

                                <IconInput
                                    placeholder={LanguagesIndex.translate('Password')}
                                    imagePath={images.lock_ic}
                                    height={15}
                                    width={11.4}
                                    secureTextEntry={this.state.PasswordIcon}
                                    imagePathRight={this.state.PasswordIcon ? images.eye_off : images.eye_on}
                                    show={() => { this.ShowPassword() }}
                                    rightHeight={14.7}
                                    rightWidth={14.7}
                                    placeholderTextColor={Colors.warmGrey}
                                    keyboardType={'default'}
                                    getFocus={(input) => { this.password = input }}
                                    setFocus={() => { this.login_Submit(); }}
                                    returnKeyType="done"
                                    onChangeText={(password) => this.setValues('password', password)}
                                    value={this.state.userLoginForm.password}
                                />

                            </View>
                            <View style={styles.forgot_text_view}>
                                <TouchableOpacity
                                    onPress={() => { this.goToForgotPass() }}
                                    style={styles.forgot_touch}>
                                    <Text style={styles.forgot_text}>{LanguagesIndex.translate('ForgotPassword')}</Text>
                                </TouchableOpacity>
                            </View>


                            <View style={styles.login_btn_view}>
                                <GButton
                                    Text={LanguagesIndex.translate('InLogin')}
                                    width={'100%'}
                                    height={50}
                                    borderRadius={10}
                                    onPress={() => { this.login_Submit() }}
                                />
                            </View>

                            <View style={styles.or_view}>
                                <View style={styles.or_line_view}></View>
                                <Text style={styles.or_text}>{LanguagesIndex.translate('or')}</Text>
                                <View style={styles.or_line_view}></View>
                            </View>
                            <View style={[styles.social_btn_view,{marginHorizontal:17}]}>
                                <SocialButton
                                    facebookSignIn={this.gotoFacebookSocialSide}
                                    appleSignIn={this.appleSignIn}
                                />
                            </View>

 

                            <TouchableOpacity
                                onPress={() => { this.goToSingUp() }}
                                style={styles.do_not_text_view}>
                                <Text style={styles.do_not_text}>{LanguagesIndex.translate('DoNotHaveAnAccount?')} <Text style={styles.sign_up_text}>{LanguagesIndex.translate('SignUp')}</Text></Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardScroll>
                </ImageBackground>
            </View>
        )
    }

};






