import React from 'react';
import AppHeader from '../../Comman/AppHeader';
import { ApiCall, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import Colors from '../../Assets/Colors';
import LanguagesIndex from '../../Languages';
import ListLoader from '../../Comman/ListLoader';
import { View } from 'react-native';
import PDFView from 'react-native-view-pdf';

export default class PrivacyPolicy extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            privacyData: '',
            isLoading: true
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('PrivacyPolicy'),
            borderBottomRadius: 25,
        })
    }


    componentDidMount() {
        this.getPrivacyPolicy();
    }

    getPrivacyPolicy() {
        let data = {};
        ApiCall.postMethodWithHeader(Constants.pages_privacy_policy, data, Constants.APIImageUploadAndroid).then((response) => {
            if (response.status == Constants.TRUE) {
                this.setState({ privacyData: response.data.content_file })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        ).catch(err => {
            this.setState({ isLoading: false })
        })
    }

    render() {
        return (
            <View style={{ backgroundColor: Colors.white, flex: 1 }}>
                {this.state.isLoading ? <ListLoader /> : null}
                {this.state.privacyData ?
                    <PDFView
                        style={{ flex: 1 }}
                        resource={this.state.privacyData}
                        resourceType={'url'}
                        onLoad={() => this.setState({ isLoading: false })}
                        onError={(error) => console.log('Cannot render PDF', error)}
                    />
                    : null}
            </View>
        )
    }

};