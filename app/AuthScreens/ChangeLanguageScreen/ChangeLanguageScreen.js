import React from 'react';
import { Text, View, ScrollView, Image,DeviceEventEmitter, TouchableOpacity, SafeAreaView, } from 'react-native';
import styles from './ChangeLanguageScreenStyles';
import { GButton } from '../../Comman/GButton';
import { images } from '../../Assets/imagesUrl';
import AppHeader from '../../Comman/AppHeader';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import { handleNavigation } from '../../navigation/Navigation';
import * as RNLocalize from "react-native-localize";


export default class ChangeLanguageScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userLanguage: {},
            language_code: Helper.user_data.default_lang,
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('ChangeLanguage'),
            borderBottomRadius: 25
        })
    }



    componentDidMount() {
        this.getProfileApiCall()
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
    }

    componentWillUnmount() {
        // BackHandler.removeEventListener("hardwareBackPress", this.backAction);
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };


    getProfileApiCall = () => {
        ApiCall.postMethodWithHeader(Constants.get_profile, Constants.APIGet).then((response) => {
            if (response.status == Constants.TRUE) {
                AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                Helper.user_data = response.data
                LanguagesIndex.MyLanguage = response.data.default_lang;
                this.handleLocalizationChange(LanguagesIndex.MyLanguage)
                AsyncStorageHelper.setData("lan", LanguagesIndex.MyLanguage)
                this.setState({
                    userLanguage: response.data
                })

            }
            else {
                this.setState({})
            }
        })
    }

    chooseLanguage = (language) => {

        this.setState({ language_code: language })
    };

    onChangeLanguage() {
        Helper.globalLoader.showLoader();
        let data = { 
            language_code: this.state.language_code,
        }
        ApiCall.postMethodWithHeader(Constants.default_lang_set, JSON.stringify(data), Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                LanguagesIndex.MyLanguage = this.state.language_code;
                this.handleLocalizationChange(LanguagesIndex.MyLanguage)
                AsyncStorageHelper.setData("lan", LanguagesIndex.MyLanguage)

                Helper.showToast(response.message)
                 DeviceEventEmitter.emit('upDateLanguage', "done")
                 this.getProfileApiCall()
                handleNavigation({ type: 'pop', navigation: this.props.navigation })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }


    renderLanguageOption = (language, code) => {
        return (
            <View style={styles.lang_view}>
                <TouchableOpacity onPress={() => this.chooseLanguage(code)} style={styles.lang_radio_touch_text}>
                    <Image
                        source={this.state.language_code == code ? images.radio_btn_selected : images.radio_btn_un_selected}
                        style={styles.radio_img}
                    />
                    <Text style={styles.lang_select_text}>{language}</Text>
                </TouchableOpacity>
            </View>
        );
    };



    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.select_lang_pre_view}>
                        <Text style={styles.select_lang_pre_text}>{LanguagesIndex.translate('SelectLanguagePreference')}</Text>
                    </View>
                    {this.renderLanguageOption(LanguagesIndex.translate('German'), 'de')}
                    {this.renderLanguageOption(LanguagesIndex.translate('French'), 'fr')}
                    {this.renderLanguageOption(LanguagesIndex.translate('English'), 'en')}
                    <View style={styles.update_btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('UPDATE')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => this.onChangeLanguage()}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }

};
