import React from 'react';
import { Text, View, FlatList, SafeAreaView, RefreshControl } from 'react-native';
import styles from './NotificationsScreenStyles';
import AppHeader from '../../Comman/AppHeader';
import { ApiCall, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import moment from 'moment';
import ListLoader from '../../Comman/ListLoader';
import NoData from '../../Comman/NoData';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/fonts';
import { RotationGestureHandler, TouchableOpacity } from 'react-native-gesture-handler';
import { handleNavigation } from '../../navigation/Navigation';
import { notifyCountStatus } from '../../Api/NotifyCountApi';
import LanguagesIndex from '../../Languages';


export default class NotificationsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            getNotification: [],
            next_page_url: '',
            currentPage: 1
        }
        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('Notifications'), borderBottomRadius: 25 })


    }

    componentDidMount() {
        this.getNotificationsApiCall();
    }

    onScroll = () => {
        if (!this.state.isLoading && this.state.next_page_url)
            this.setState({ isLoading: true, currentPage: this.state.currentPage + 1 }, () => {
                this.getNotificationsApiCall();
            })
    }

    onRefresh = () => {
        this.setState({ refreshing: true, currentPage: 1 }, () => {
            this.getNotificationsApiCall();
        })
    }

    getNotificationsApiCall = () => {
        ApiCall.postMethodWithHeader(`${Constants.get_notifications}?page=${this.state.currentPage}`, {}, Constants.APIGet).then((response) => {
            console.log('response---response--->>',response.data)
            if (response.status == Constants.TRUE && response.data.current_page) {
                this.setState({
                    isLoading: false,
                    refreshing: false,
                    next_page_url: response.data.next_page_url,
                    getNotification: this.state.currentPage == 1 ? response.data.data : [...this.state.getNotification, ...response.data.data]
                })
                notifyCountStatus()
            }
            else {
                this.setState({
                    isLoading: false,
                    refreshing: false,
                })
            }
        })
    }

    gotoDetail(booking_id) {
        handleNavigation({
            page: 'BookingDetails', type: 'push', navigation: this.props.navigation, passProps: {
                booking_id: booking_id
            }
        })
    }


    gotoActivityDetail(allDataObj) {
        handleNavigation({
            page: 'ActivityDetailScreen', type: 'push', navigation: this.props.navigation, passProps: allDataObj
        })
    }

    onNavigation(item) {
        let convertData = JSON.parse(item.data);
        if (item.type != 'custom_notification' && item.type != 'custom_booking') {
            if (Helper.user_data.user_type === 'HAVEOMY') {
                if (convertData.booking_id)
                    this.gotoDetail(convertData.booking_id)
            } else if (Helper.user_data.user_type === 'BEOMY') {
                if (convertData.booking_id)
                    this.gotoActivityDetail({
                        booking_id: convertData.booking_id,
                        booking_service_id: convertData.booking_service_id,
                        BookingStatus: convertData.status
                    })
            } else if (Helper.user_data.user_type === 'ENTERPRISE') {
                if (convertData.booking_id)
                    this.gotoActivityDetail({
                        booking_id: convertData.booking_id,
                        userType: "homeCare"
                    })
            }
        }
    }

    _renderNotificationItem = ({ item }) => {
        return (
            <TouchableOpacity activeOpacity={0.5} onPress={() => this.onNavigation(item)} style={styles.card_view}>
                <View style={styles.notification_view}>
                    <View style={styles.status_time_view}>
                        <View style={styles.flex_view_status}>
                            <Text style={[styles.status_txt, { fontFamily: fonts.RoBoToRegular_1, color: item.type == 'booking_request' ? Colors.caramel : Colors.black }]}>{item.title}</Text>
                        </View>
                        <View style={styles.flex_view_time}>
                            <Text style={styles.time_txt}>{moment.utc(item.created_at).local().fromNow()}</Text>
                        </View>
                    </View>

                    <View style={styles.notification_view_txt}>
                        <Text style={styles.notification_msg_text}>{item.message}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }


    render() {
        let { getNotification, refreshing, isLoading } = this.state;
        return (
            <SafeAreaView style={styles.safe_area_view}>
                {getNotification.length < 1 && !isLoading ?
                    <NoData
                        message={LanguagesIndex.translate('Youdontthaveanynotifications')} /> :
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={getNotification}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                        renderItem={this._renderNotificationItem}
                        extraData={this.state}
                        onScroll={this.onScroll}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={() => {
                            return (
                                <>
                                    {isLoading ? <ListLoader /> : null}
                                </>
                            )
                        }}
                    />
                }
            </SafeAreaView>
        )
    }

};
