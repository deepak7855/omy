import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';

export default NotificationsScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteFive
    },
    card_view:{
        backgroundColor: Colors.white, 
        paddingVertical: 5,
        marginTop:8
    },
   notification_view:{
    marginTop: 5, 
    backgroundColor: Colors.white, 
    paddingVertical: 5
   },
   status_time_view:{
    marginHorizontal: 16, 
    flexDirection: 'row', 
    justifyContent: 'space-between'
   },
   flex_view_status:{
    flex: 5,
   },
   flex_view_time:{
    flex: 5,
    alignItems: 'flex-end'
   },
   status_txt:{
    fontSize: fonts.fontSize16,  
   },
   time_txt:{
    fontSize: fonts.fontSize10, 
    color: Colors.blackTwo, 
    fontFamily: fonts.RoBoToMedium_1, 
    lineHeight: 17
   },
   notification_view_txt:{
    marginHorizontal: 16,
    marginTop:5, 
    justifyContent: 'center'
   },
   notification_msg_text:{
    fontSize: fonts.fontSize12, 
    color: Colors.warmGreyThree, 
    fontFamily: fonts.RoBoToMedium_1, 
    lineHeight: 17
   },




});