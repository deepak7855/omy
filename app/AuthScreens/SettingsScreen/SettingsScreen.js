import React from 'react';
import { Text, View, ScrollView, Image, TouchableOpacity, SafeAreaView, Alert, DeviceEventEmitter } from 'react-native';
import styles from './SettingsScreenStyles';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import fonts from '../../Assets/fonts';
import SwitchToggle from "react-native-switch-toggle";
import AppHeader from '../../Comman/AppHeader';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import { stopUserTracking } from '../../Comman/BackgroundUserTracking';
import * as RNLocalize from "react-native-localize";
import SocketController from '../../Lib/SocketController';

export default class SettingsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            switchOnNotification: Number(Helper.user_data.notification_permissions),
            status: 0,

        }
        this.createHeader();
    }


    createHeader() {
        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('Settings'), borderBottomRadius: 25 })
    }

    goToChangePassword() {
        handleNavigation({ type: 'push', page: 'ChangePasswordScreen', navigation: this.props.navigation })
    }

    gotToChangeLanguage() {
        handleNavigation({ type: 'push', page: 'ChangeLanguageScreen', navigation: this.props.navigation })
    }

    goToAccountDetails() {
        handleNavigation({ type: 'push', page: 'AccountDetailsScreen', navigation: this.props.navigation })
    }

    goToRatingsReviews() {
        handleNavigation({ type: 'push', page: 'RatingsReviews', navigation: this.props.navigation })
    }

    gotToNotification() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    gotToPrivacyPolicy() {
        handleNavigation({ type: 'push', page: 'PrivacyPolicy', navigation: this.props.navigation })
    }

    gotToTermsAndCondition() {
        handleNavigation({ type: 'push', page: 'TermsAndCondition', navigation: this.props.navigation })
    }


    gotToWallet() {
        handleNavigation({ type: 'push', page: 'Wallet', navigation: this.props.navigation })
    }

    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('Settings'), borderBottomRadius: 25 })
            // this.getBookingRequestApiCall();
            // alert(JSON.stringify(data))
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
    }

    componentWillUnmount() {
        // BackHandler.removeEventListener("hardwareBackPress", this.backAction);
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };


    allowNotificationApiCall() {
        Helper.globalLoader.showLoader();
        let statusOnOf = !this.state.switchOnNotification
        this.setState({ switchOnNotification: !this.state.switchOnNotification })
        let data = {
            status: statusOnOf == true ? '1' : '0'
        }
        ApiCall.postMethodWithHeader(Constants.notification_permissions, JSON.stringify(data), Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                let copyData = { ...Helper.user_data };
                copyData.notification_permissions = response.data.notification_permissions;
                AsyncStorageHelper.setData(Constants.USER_DETAILS, copyData)
                Helper.user_data = copyData;
            }
            else {
                this.setState({ switchOnNotification: !this.state.switchOnNotification })
                Helper.showToast(response.message)
            }
        })
    }

    methodLogOut() {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            LanguagesIndex.translate('kLogout'),
            [
                { text: LanguagesIndex.translate('No'), onPress: () => { console.log("Logout no") } },
                { text: LanguagesIndex.translate('Yes'), onPress: () => { this.methodLogoutApiCall() } },
            ],
            { cancelable: false }
        )
    }

    methodLogoutApiCall() {
        Helper.globalLoader.showLoader();
        let data = {
            device_type: Helper.device_type,
            device_id: Helper.device_id,
        }
        ApiCall.postMethodWithHeader(Constants.log_out, JSON.stringify(data), Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            Helper.showToast(response.message)
            if (response.status == Constants.TRUE) {
                AsyncStorageHelper.removeItemValue(Constants.USER_DETAILS)
                AsyncStorageHelper.removeItemValue("token")
                Helper.user_data = ''
                stopUserTracking();
                if (Helper.user_data.user_type === 'HAVEOMY') {
                    SocketController.disconnetSocket();
                } else if (Helper.user_data.user_type === 'BEOMY') {
                    SocketController.disconnetSocket();
                }

                Helper.navRef.switchNavigation('', 'LoginScreen');
                setTimeout(() => {
                    handleNavigation({ type: 'setRoot', page: 'LoginScreen', navigation: this.props.navigation })
                }, 500);
            }
        }
        )
    }


    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.main_view_touch}>
                        <TouchableOpacity
                            onPress={() => this.allowNotificationApiCall('status')}
                            style={styles.settings_title_touch}>
                            <Text style={styles.settings_title_text}>{LanguagesIndex.translate('Notification')}</Text>
                            <View>
                                <SwitchToggle
                                    containerStyle={{ width: 24, height: 15.3, borderRadius: 10, padding: 1.5, borderColor: Colors.cerulean, borderWidth: 1 }}
                                    backgroundColorOn={Colors.cerulean}
                                    backgroundColorOff={Colors.greyishBrown}
                                    circleStyle={{ width: 12, height: 12, borderRadius: 6, }}
                                    switchOn={this.state.switchOnNotification}
                                    onPress={() => this.allowNotificationApiCall('status')}
                                    circleColorOff={Colors.white}
                                    circleColorOn="#e5e1e0"
                                    duration={500}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>

                    {/* {!Helper.user_data.social_type ? */}
                        <View style={styles.main_view_touch}>
                            <TouchableOpacity
                                onPress={() => this.goToChangePassword()} style={styles.settings_title_touch}>
                                <Text style={styles.settings_title_text}>{LanguagesIndex.translate('ChangePassword')}</Text>
                                <Image source={images.setting_next} resizeMode={'contain'} style={styles.arrow_img} />
                            </TouchableOpacity>
                        </View>
                        {/* : null} */}


                    <View style={styles.main_view_touch}>
                        <TouchableOpacity
                            onPress={() => this.gotToChangeLanguage()}
                            style={styles.settings_title_touch}>
                            <Text style={styles.settings_title_text}>{LanguagesIndex.translate('ChangeLanguage')}</Text>
                            <Image source={images.setting_next} resizeMode={'contain'} style={styles.arrow_img} />
                        </TouchableOpacity>
                    </View>

                    {Helper.user_data.user_type === 'HAVEOMY' ?
                        <View style={styles.main_view_touch}>
                            <TouchableOpacity
                                onPress={() => this.goToAccountDetails()}
                                style={styles.settings_title_touch}>
                                <Text style={styles.settings_title_text}>{LanguagesIndex.translate('AccountDetails')}</Text>
                                <Image source={images.setting_next} resizeMode={'contain'} style={styles.arrow_img} />
                            </TouchableOpacity>
                        </View>
                        : null
                    }
                    {Helper.user_data.user_type === 'HAVEOMY' ?
                        <View style={styles.main_view_touch}>
                            <TouchableOpacity
                                onPress={() => this.goToRatingsReviews()}
                                style={styles.settings_title_touch}>
                                <Text style={styles.settings_title_text}>{LanguagesIndex.translate('Ratings&Reviews')}</Text>
                                <Image source={images.setting_next} resizeMode={'contain'} style={styles.arrow_img} />
                            </TouchableOpacity>
                        </View>
                        : null
                    }

                    {Helper.user_data.user_type === 'BEOMY' ?
                        <View style={styles.main_view_touch}>
                            <TouchableOpacity
                                onPress={() => this.gotToWallet()}
                                style={styles.settings_title_touch}>
                                <Text style={styles.settings_title_text}>{LanguagesIndex.translate('wallet')}</Text>
                                <Image source={images.setting_next} resizeMode={'contain'} style={{ height: 12, width: 7 }} />
                            </TouchableOpacity>
                        </View> :
                        null}

                    <View style={styles.main_view_touch}>
                        <TouchableOpacity
                            onPress={() => this.gotToTermsAndCondition()}
                            style={styles.settings_title_touch}>
                            <Text style={styles.settings_title_text}>{LanguagesIndex.translate('Terms&Conditions')}</Text>
                            <Image source={images.setting_next} resizeMode={'contain'} style={{ height: 12, width: 7 }} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.main_view_touch}>
                        <TouchableOpacity
                            onPress={() => this.gotToPrivacyPolicy()}
                            style={styles.settings_title_touch}>
                            <Text style={styles.settings_title_text}>{LanguagesIndex.translate('PrivacyPolicy')}</Text>
                            <Image source={images.setting_next} resizeMode={'contain'} style={styles.arrow_img} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.main_view_touch}>
                        <TouchableOpacity
                            onPress={() => this.methodLogOut()}
                            style={styles.settings_title_touch}>
                            <Text style={{ fontSize: fonts.fontSize14, color: Colors.cerulean, fontFamily: fonts.RoBoToMedium_1, }}>{LanguagesIndex.translate('Logout')}</Text>
                            {/* <Image source={images.setting_next} resizeMode={'contain'} style={styles.arrow_img} /> */}
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }

};
