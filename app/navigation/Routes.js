import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack"; 


import SplashScreen from "../AuthScreens/SplashScreen/SplashScreen";
import AfterSplash from '../AuthScreens/AfterSplash/AfterSplash';
import LoginScreen from '../AuthScreens/LoginScreen/LoginScreen';
import ForgotPasswordScreen from '../AuthScreens/ForgotPasswordScreen/ForgotPasswordScreen';
import SignUpScreen from '../Screens/SignUpScreen/SignUpScreen';
import StudentSignUpScreen from "../StudentScreens/StudentSignUpScreen/StudentSignUpScreen";
import SeniorSignUpScreen from "../SeniorCitizenScreens/SeniorSignUpScreen/SeniorSignUpScreen";
import PrivacyPolicy from "../AuthScreens/PrivacyPolicy /PrivacyPolicy";
import TermsAndCondition from "../AuthScreens/TermsAndCondition/TermsAndCondition";



const Stack = createStackNavigator();
function Routes(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={props.AuthDefault}>
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AfterSplash"
          component={AfterSplash}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="SignUpScreen"
          component={SignUpScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="StudentSignUpScreen"
          component={StudentSignUpScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SeniorSignUpScreen"
          component={SeniorSignUpScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ForgotPasswordScreen"
          component={ForgotPasswordScreen}
          options={{ headerShown: false }}
        />
        

        <Stack.Screen
          name="PrivacyPolicy"
          component={PrivacyPolicy}
        />

        <Stack.Screen
          name="TermsAndCondition"
          component={TermsAndCondition}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default Routes;
