import React from 'react';
import { View, Text, Image, Dimensions, DeviceEventEmitter } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import HomeScreen from '../Screens/HomeScreen/HomeScreen';
// import ManagerBookingUpComingScreen from '../Screens/BookingsUpComingScreen/ManagerBookingUpComingScreen';
// import MessageScreen from '../AuthScreens/MessageScreen/MessageScreen';
import ProfileScreen from '../Screens/ProfileScreen/ProfileScreen';
import Colors from '../Assets/Colors';
import fonts from '../Assets/fonts';
import { images } from '../Assets/imagesUrl';
import LanguagesIndex from '../Languages';
import ManagerBookings from '../Screens/ManagerBookings/ManagerBookings';
import * as RNLocalize from "react-native-localize";

const Tabs = createBottomTabNavigator();
const DeviceW = Dimensions.get('screen').width

const RenderTabIcons = (props) => {
    const { icon, lable, name, activeIcon, isFocused } = props;
    return (
        <View style={[{ alignItems: "center", justifyContent: "center", width: DeviceW / 5, height: 57, }, (isFocused) ? {} : '']}>
            <Image
                source={(isFocused) ? activeIcon : icon}
                style={{ height: 20, width: 20, resizeMode: 'contain', marginTop: 10 }}
            />
            <Text style={[{ lineHeight: 13, color: Colors.warmGreyFour, fontSize: fonts.fontSize10, fontFamily: fonts.RoBoToRegular_1 }, (isFocused) ? { color: Colors.darkSkyBlue, fontSize: fonts.fontSize10, fontFamily: fonts.RoBoToRegular_1, lineHeight: 13 } : '']}>{name}</Text>

        </View>
    );
}


const ReturnNavigator = createStackNavigator();
function HomeStackNavigator() {
    return (
        <ReturnNavigator.Navigator>
            <ReturnNavigator.Screen name="HomeScreen" component={HomeScreen} />
        </ReturnNavigator.Navigator>
    )
}
const ExperiencesNavigator = createStackNavigator();
function ExperiencesStackNavigator() {
    return (
        <ExperiencesNavigator.Navigator >
            <ReturnNavigator.Screen name="ManagerBookings" component={ManagerBookings} />
        </ExperiencesNavigator.Navigator>
    )
}


// const MessagesNavigator = createStackNavigator();
// function MessagesStackNavigator() {
//     return (
//         <MessagesNavigator.Navigator >
//             <ReturnNavigator.Screen name="MessageScreen" component={MessageScreen} />
//         </MessagesNavigator.Navigator>
//     )
// }

const ProfileNavigator = createStackNavigator();
function ProfileStackNavigator() {
    return (
        <ProfileNavigator.Navigator >
            <ReturnNavigator.Screen name="ProfileScreen" component={ProfileScreen} />
        </ProfileNavigator.Navigator>
    )
}

export default class BottomTab extends React.Component {
    
    componentDidMount(){
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage)); 
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    render() {
        return (
            <Tabs.Navigator
                tabBarOptions={{
                    keyboardHidesTabBar:true,
                    style: {
                        // height: 50,
                    },
                }}
            >
                <Tabs.Screen
                    name="HomeScreen"
                    component={HomeStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    icon={images.home_ic}
                                    activeIcon={images.home_active_ic}
                                    name={LanguagesIndex.translate('Home')}
                                    isFocused={focused}
                                />
                            );
                        },
                    }}
                />
                <Tabs.Screen
                    name="ManagerBookings"
                    component={ExperiencesStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    icon={images.experiences_ic}
                                    activeIcon={images.experiences_active_ic}
                                    name={LanguagesIndex.translate('Experiences')}
                                    isFocused={focused}
                                />
                            );
                        },

                    }}
                />

                {/* <Tabs.Screen
                    name="MessageScreen"
                    component={MessagesStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    icon={images.messages_ic}
                                    activeIcon={images.messages_active_ic}
                                    name={LanguagesIndex.translate('Messages')}
                                    isFocused={focused}
                                />
                            );
                        },

                    }}
                /> */}
                <Tabs.Screen
                    name="ProfileScreen"
                    component={ProfileStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    icon={images.profile_ic}
                                    activeIcon={images.profile_ic}
                                    name={LanguagesIndex.translate('Profile')}
                                    isFocused={focused}
                                />
                            );
                        },
                    }}
                />
            </Tabs.Navigator>

        )
    }
}



