import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

//Senior Citizen
import SeniorCitizenBottomTab from '../navigation/SeniorCitizenBottomTab';

import HomeSenior from '../SeniorCitizenScreens/Home/HomeSenior';
import ProfileScreen from '../SeniorCitizenScreens/ProfileScreen/ProfileScreen';
import StudentListScreen from "../SeniorCitizenScreens/StudentList/StudentListScreen";
import ScheduleBookingScreen from "../SeniorCitizenScreens/ScheduleBookingScreen/ScheduleBookingScreen";

import EditProfileScreen from "../SeniorCitizenScreens/EditProfileScreen/EditProfileScreen";
import StudentProfileScreen from "../SeniorCitizenScreens/StudentProfile/StudentProfileScreen";
import RateExperienceScreen from "../Screens/RateExperience/RateExperienceScreen";

import ChangePasswordScreen from "../AuthScreens/ChangePasswordScreen/ChangePasswordScreen";
import SettingsScreen from "../AuthScreens/SettingsScreen/SettingsScreen";
import ForgotPasswordScreen from "../AuthScreens/ForgotPasswordScreen/ForgotPasswordScreen";
import SplashScreen from "../AuthScreens/SplashScreen/SplashScreen";
import AfterSplash from "../AuthScreens/AfterSplash/AfterSplash";
import NotificationsScreen from "../AuthScreens/NotificationsScreen/NotificationsScreen";
import ChangeLanguageScreen from "../AuthScreens/ChangeLanguageScreen/ChangeLanguageScreen";
import PrivacyPolicy from "../AuthScreens/PrivacyPolicy /PrivacyPolicy";
import TermsAndCondition from "../AuthScreens/TermsAndCondition/TermsAndCondition";
import MediaDetailPage from "../Comman/MediaDetailPage";

import ActivityDetailScreen from '../Screens/ActivityDetailScreen/ActivityDetailScreen';
import Wallet from '../SeniorCitizenScreens/Wallet/Wallet';
import SelectMonth from '../SeniorCitizenScreens/SelectMonth/SelectMonth';
import AddMoney from '../SeniorCitizenScreens/PaymentScreen/AddMoney'
import Payment from "../SeniorCitizenScreens/PaymentScreen/Payment";
import CancelChargesStudent from "../StudentScreens/CancelChargesStudent/CancelChargesStudent";
import ChatScreen from '../AuthScreens/ChatScreen/ChatScreen';

import MapTrackStudent from '../SeniorCitizenScreens/MapTrackStundent/MapTrackStudent';


const Stack = createStackNavigator();
function SeniorCitizenRoutes() {
  return (
    <NavigationContainer>

      <Stack.Navigator
        initialRouteName="SeniorCitizenBottomTab">

        <Stack.Screen
          name="HomeSenior"
          component={HomeSenior}
        />




        <Stack.Screen
          name="ForgotPasswordScreen"
          component={ForgotPasswordScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SettingsScreen"
          component={SettingsScreen}
        />
        <Stack.Screen
          name="ChangeLanguageScreen"
          component={ChangeLanguageScreen}
        />

        <Stack.Screen
          name="ChangePasswordScreen"
          component={ChangePasswordScreen}
        />
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="AfterSplash"
          component={AfterSplash}
          options={{ headerShown: false }}
        />


        <Stack.Screen
          name="SeniorCitizenBottomTab"
          component={SeniorCitizenBottomTab}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ProfileScreen"
          component={ProfileScreen}
        />

        <Stack.Screen
          name="StudentProfileScreen"
          component={StudentProfileScreen}
        />

        <Stack.Screen
          name="ScheduleBookingScreen"
          component={ScheduleBookingScreen}
        />



        <Stack.Screen
          name="EditProfileScreen"
          component={EditProfileScreen}
        />


        <Stack.Screen
          name="NotificationsScreen"
          component={NotificationsScreen}
        />



        <Stack.Screen
          name="StudentListScreen"
          component={StudentListScreen}
        />

        <Stack.Screen
          name="RateExperienceScreen"
          component={RateExperienceScreen}
        />

        <Stack.Screen
          name="MediaDetailPage"
          component={MediaDetailPage}
        />

        <Stack.Screen
          name="PrivacyPolicy"
          component={PrivacyPolicy}
        />

        <Stack.Screen
          name="TermsAndCondition"
          component={TermsAndCondition}
        />


        <Stack.Screen
          name="ActivityDetailScreen"
          component={ActivityDetailScreen}
        />

        <Stack.Screen
          name="Wallet"
          component={Wallet}
        />

        <Stack.Screen
          name="SelectMonth"
          component={SelectMonth}
        />

        <Stack.Screen
          name="AddMoney"
          component={AddMoney}
        />

        <Stack.Screen
          name="Payment"
          component={Payment}
        />

        <Stack.Screen
          name="CancelChargesStudent"
          component={CancelChargesStudent}
        />
        <Stack.Screen
          name="ChatScreen"
          component={ChatScreen}
        />

        <Stack.Screen
          name="MapTrackStudent"
          component={MapTrackStudent}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default SeniorCitizenRoutes;
