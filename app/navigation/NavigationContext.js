import React, { Component } from 'react'
import { StatusBar,SafeAreaView, Platform, View } from 'react-native'
import Colors from '../Assets/Colors'
import Routes from './Routes'
import StudentRoutes from './StudentRoutes'
import HomeCareRoutes from './HomeCareRoutes'
import SeniorCitizenRoutes from './SeniorCitizenRoutes'
import Helper from '../Lib/Helper'

import ActivityIndicatorApp from '../Api/ActivityIndicatorApp';
import { AsyncStorageHelper } from '../Api'
import SplashScreen from 'react-native-splash-screen'
import LocalNotification from '../Comman/LocalNotification'


export default class NavigationProvider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            AuthDefault: 'SplashScreen',
            userData: {},
            userType: '',
            // userType 1 means home care manager
            // userType 2 means student
            // userType 3 senior citizen
        }
    }

    componentDidMount() { 
        Helper.navRef = this;

        AsyncStorageHelper.getData(Constants.USER_DETAILS).then((response) => {
            if (response) {
                Helper.user_data = response;
                AsyncStorageHelper.getData('token').then((token) => {
                    Helper.token = token;
                })
                if (response.user_type == 'HAVEOMY') {
                    this.setState({ userType: '2' }, () => {

                    })
                } else if (response.user_type == 'BEOMY') {
                    this.setState({ userType: '3' }, () => {

                    })
                } else if (response.user_type == 'ENTERPRISE') {
                    this.setState({ userType: '1' }, () => {

                    })
                }

            } else {

                setTimeout(() => {
                    SplashScreen.hide();
                }, 500);
            }
        })

    }

    switchNavigation = (value, screen) => {
        this.setState({ userType: value, AuthDefault: screen })
    }

    render() {
        const { userType, AuthDefault } = this.state

        return (
            <View style={{ flex: 1 }}>
                <View style={{ backgroundColor: Colors.cerulean, height: Platform.OS === 'ios' ? 20 : 0 }}>
                    <StatusBar barStyle={'dark-content'} backgroundColor={Colors.cerulean} />
                </View>
                {userType == '2' ?
                    <StudentRoutes />
                    : userType == '3' ?
                        <SeniorCitizenRoutes />
                        : userType == '1' ?
                            <HomeCareRoutes />
                            : <Routes AuthDefault={AuthDefault} />
                }
                <ActivityIndicatorApp
                    onRef={ref => { Helper.globalLoader = ref }}
                />
                <LocalNotification />
                {/* <SafeAreaView/> */}
            </View>
        )
    }
} 