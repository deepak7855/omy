import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
//student screen 
import StudentBottomTab from '../navigation/StudentBottomTab';

import ProfileScreen from '../StudentScreens/ProfileScreen/ProfileScreen';
import EditProfileScreen from '../StudentScreens/EditProfileScreen/EditProfileScreen';

import EarningsScreen from '../StudentScreens/EarningsScreen/EarningsScreen';
import EarningsMonthScreen from '../StudentScreens/EarningsMonthScreen/EarningsMonthScreen';
import BookingRequestsScreen from '../StudentScreens/BookingRequestsScreen/BookingRequestsScreen';
import AccountDetailsScreen from '../StudentScreens/AccountDetailsScreen/AccountDetailsScreen';
import SettingsScreen from '../AuthScreens/SettingsScreen/SettingsScreen';
import RatingsReviews from '../StudentScreens/RatingsReviewsScreen/RatingsReviews';
import WelcomeHomeScreen from '../StudentScreens/WelcomeHomeScreen/WelcomeHomeScreen';
import StudentProfileScreen from "../SeniorCitizenScreens/StudentProfile/StudentProfileScreen";
import BookingDetails from "../StudentScreens/BookingDetails/BookingDetails";
import ChangePasswordScreen from "../AuthScreens/ChangePasswordScreen/ChangePasswordScreen";
import ForgotPasswordScreen from "../AuthScreens/ForgotPasswordScreen/ForgotPasswordScreen";
import SplashScreen from "../AuthScreens/SplashScreen/SplashScreen";
import AfterSplash from "../AuthScreens/AfterSplash/AfterSplash";
import LoginScreen from "../AuthScreens/LoginScreen/LoginScreen";
import NotificationsScreen from "../AuthScreens/NotificationsScreen/NotificationsScreen";
import ChangeLanguageScreen from "../AuthScreens/ChangeLanguageScreen/ChangeLanguageScreen";
import MediaDetailPage from "../Comman/MediaDetailPage";
import PrivacyPolicy from "../AuthScreens/PrivacyPolicy /PrivacyPolicy";
import TermsAndCondition from "../AuthScreens/TermsAndCondition/TermsAndCondition";
import CancelChargesStudent from "../StudentScreens/CancelChargesStudent/CancelChargesStudent";
import ChatScreen from "../AuthScreens/ChatScreen/ChatScreen";

import StudentMap from "../StudentScreens/StudentMap/StudentMap";


const Stack = createStackNavigator();
function StudentRoutes() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="StudentBottomTab">

        <Stack.Screen
          name="ForgotPasswordScreen"
          component={ForgotPasswordScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SettingsScreen"
          component={SettingsScreen}
        />
        <Stack.Screen
          name="ChangeLanguageScreen"
          component={ChangeLanguageScreen}
        />

        <Stack.Screen
          name="ChangePasswordScreen"
          component={ChangePasswordScreen}
        />
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="AfterSplash"
          component={AfterSplash}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ProfileScreen"
          component={ProfileScreen}
        />

        <Stack.Screen
          name="EditProfileScreen"
          component={EditProfileScreen}
        />

        <Stack.Screen
          name="NotificationsScreen"
          component={NotificationsScreen}
        />

        <Stack.Screen
          name="EarningsScreen"
          component={EarningsScreen}
        />

        <Stack.Screen
          name="EarningsMonthScreen"
          component={EarningsMonthScreen}
        />

        <Stack.Screen
          name="BookingRequestsScreen"
          component={BookingRequestsScreen}
        />

        <Stack.Screen
          name="AccountDetailsScreen"
          component={AccountDetailsScreen}
        />

        <Stack.Screen
          name="RatingsReviews"
          component={RatingsReviews}
        />

        <Stack.Screen
          name="WelcomeHomeScreen"
          component={WelcomeHomeScreen}
        />

        <Stack.Screen
          name="StudentProfileScreen"
          component={StudentProfileScreen}
        />

        <Stack.Screen
          name="BookingDetails"
          component={BookingDetails}
        />

        <Stack.Screen
          name="StudentBottomTab"
          component={StudentBottomTab}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="MediaDetailPage"
          component={MediaDetailPage}
        />

        <Stack.Screen
          name="PrivacyPolicy"
          component={PrivacyPolicy}
        />

        <Stack.Screen
          name="TermsAndCondition"
          component={TermsAndCondition}
        />

        <Stack.Screen
          name="CancelChargesStudent"
          component={CancelChargesStudent}
        />

        <Stack.Screen
          name="ChatScreen"
          component={ChatScreen}
        />


        <Stack.Screen
          name="StudentMap"
          component={StudentMap}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default StudentRoutes;
