import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

//care home manager
import ChangePasswordScreen from '../AuthScreens/ChangePasswordScreen/ChangePasswordScreen';
import BottomTab from '../navigation/BottomTab';
import ProfileScreen from "../Screens/ProfileScreen/ProfileScreen";
import ActivityDetailScreen from '../Screens/ActivityDetailScreen/ActivityDetailScreen';
import ScheduleBookingScreen from '../Screens/ScheduleBookingScreen/ScheduleBookingScreen';
import EditProfileScreen from '../Screens/EditProfileScreen/EditProfileScreen';
import AccountDetailsScreen from '../StudentScreens/AccountDetailsScreen/AccountDetailsScreen';
import SettingsScreen from "../AuthScreens/SettingsScreen/SettingsScreen";
import ChangeLanguageScreen from "../AuthScreens/ChangeLanguageScreen/ChangeLanguageScreen";
import NotificationsScreen from "../AuthScreens/NotificationsScreen/NotificationsScreen";
import PrivacyPolicy from "../AuthScreens/PrivacyPolicy /PrivacyPolicy";
import TermsAndCondition from "../AuthScreens/TermsAndCondition/TermsAndCondition";
import RateExperienceScreen from "../Screens/RateExperience/RateExperienceScreen";
import StudentProfileScreen from "../SeniorCitizenScreens/StudentProfile/StudentProfileScreen";
import MediaDetailPage from "../Comman/MediaDetailPage";
import ChatScreen from "../AuthScreens/ChatScreen/ChatScreen";


const Stack = createStackNavigator();
function Routes(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={BottomTab}>
        <Stack.Screen
          name="BottomTab"
          component={BottomTab}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ProfileScreen"
          component={ProfileScreen}
        />

        <Stack.Screen
          name="ActivityDetailScreen"
          component={ActivityDetailScreen}
        />

        <Stack.Screen
          name="ScheduleBookingScreen"
          component={ScheduleBookingScreen}
        />

        <Stack.Screen
          name="ChangeLanguageScreen"
          component={ChangeLanguageScreen}
        />

        <Stack.Screen
          name="ChangePasswordScreen"
          component={ChangePasswordScreen}
        />

        <Stack.Screen
          name="EditProfileScreen"
          component={EditProfileScreen}
        />

        <Stack.Screen
          name="NotificationsScreen"
          component={NotificationsScreen}
        />

        <Stack.Screen
          name="SettingsScreen"
          component={SettingsScreen}
        />

        <Stack.Screen
          name="AccountDetailsScreen"
          component={AccountDetailsScreen}
        />

        <Stack.Screen
          name="PrivacyPolicy"
          component={PrivacyPolicy}
        />

        <Stack.Screen
          name="TermsAndCondition"
          component={TermsAndCondition}
        />

        <Stack.Screen
          name="RateExperienceScreen"
          component={RateExperienceScreen}
        />

        <Stack.Screen
          name="StudentProfileScreen"
          component={StudentProfileScreen}
        />

        <Stack.Screen
          name="MediaDetailPage"
          component={MediaDetailPage}
        />


        <Stack.Screen
          name="ChatScreen"
          component={ChatScreen}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default Routes;
