import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import AfterSplash from "../AuthScreens/AfterSplash/AfterSplash";
import SplashScreen from "../AuthScreens/SplashScreen/SplashScreen";
import ForgotPasswordScreen from "../AuthScreens/ForgotPasswordScreen/ForgotPasswordScreen";
import StudentSignUpScreen from "../StudentScreens/StudentSignUpScreen/StudentSignUpScreen";
import SeniorSignUpScreen from "../SeniorCitizenScreens/SeniorSignUpScreen/SeniorSignUpScreen";
import LoginScreen from "../AuthScreens/LoginScreen/LoginScreen";
const Stack = createStackNavigator();
function StartRoute() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName="SplashScreen">
                <Stack.Screen
                    name="SplashScreen"
                    component={SplashScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="ForgotPasswordScreen"
                    component={ForgotPasswordScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="StudentSignUpScreen"
                    component={StudentSignUpScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="SeniorSignUpScreen"
                    component={SeniorSignUpScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="LoginScreen"
                    component={LoginScreen}
                    options={{ headerShown: false }}
                />

            </Stack.Navigator>
        </NavigationContainer>

    )
}
export default StartRoute;