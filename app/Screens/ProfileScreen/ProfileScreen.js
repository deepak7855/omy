import React from 'react';
import { View, Text, DeviceEventEmitter } from 'react-native';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import styles from './ProfileScreenStyles';
import IconInput from '../../Comman/GInput';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import AppHeader from '../../Comman/AppHeader';
import Helper from '../../Lib/Helper';
import ImageLoadView from '../../Lib/ImageLoadView';
import LanguagesIndex from '../../Languages';
import fonts from '../../Assets/fonts';
import { handleNavigation } from '../../navigation/Navigation';
import * as RNLocalize from "react-native-localize";


export default class ProfileScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            managerDetails: {},
        }
        AppHeader({
            ...this.props.navigation, leftTitle:LanguagesIndex.translate('Profile'),
            bellIcon: true, settingsIcon: true, profileIcon: true,
            hideLeftBackIcon: true,
            settingIconClick: () => this.settingIconClick(),
            profileIconClick: () => this.profileIconClick(),
            bellIconClick: () => this.bellIconClick(),
        })
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    profileIconClick() {
        handleNavigation({ type: 'push', page: 'EditProfileScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    componentDidMount() {
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            AppHeader({
                ...this.props.navigation, leftTitle:LanguagesIndex.translate('Profile'),
                bellIcon: true, settingsIcon: true, profileIcon: true,
                hideLeftBackIcon: true,
                settingIconClick: () => this.settingIconClick(),
                profileIconClick: () => this.profileIconClick(),
                bellIconClick: () => this.bellIconClick(),
            })
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            Helper.updateUserData();
            this.methodFillForm();
        });
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
        this._unsubscribe();
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    methodFillForm() {
        if (Helper.user_data) {
            this.setState({ managerDetails: Helper.user_data });
        }
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.profile_view}>
                        <View style={styles.profile_img_view}>
                            <ImageLoadView
                                resizeMode={'cover'}
                                source={this.state.managerDetails.profile_picture ? { uri: this.state.managerDetails.profile_picture } : images.default}
                                // source={images.profile_care_home_img}
                                style={styles.profile_img} />
                        </View>
                    </View>

                    <View style={styles.input_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('NameInstitute')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.managerDetails.business_name}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('Name')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.managerDetails.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.managerDetails.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('email')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.managerDetails.email}
                        />


                        {/* <View
                            style={{
                                marginVertical: 10,
                                borderRadius: 15,
                                padding: 15,
                                backgroundColor: Colors.whiteThree,
                            }}>
                            <Text numberOfLines={1} style={{
                                fontSize: fonts.fontSize14,
                                color: Colors.warmGrey,
                                fontFamily: fonts.RoBoToMedium_1,
                            }}>{this.state.managerDetails.location ? this.state.managerDetails.location : LanguagesIndex.translate('Location')}</Text>
                        </View> */}


                        {/* <IconInput
                            placeholder={LanguagesIndex.translate('Location')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.managerDetails.location}
                        /> */}

                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            placeholderTextColor={Colors.greyishBrownTwo}
                            inputedit={false}
                            value={this.state.managerDetails.street_no}
                        />

                        <View style={styles.input_zip_city_view}>
                            <View style={styles.input_zip_view}>
                                <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.managerDetails.postcode}
                                />
                            </View>

                            <View style={styles.input_city_view}>

                                <View
                                    style={{
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.managerDetails.city ? this.state.managerDetails.city : LanguagesIndex.translate('City')}</Text>
                                </View>


                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('City')}
                                    placeholderTextColor={Colors.greyishBrownTwo}
                                    inputedit={false}
                                    value={this.state.managerDetails.city}
                                /> */}
                            </View>
                        </View>
                    </View>
                </KeyboardScroll>
            </View>
        )
    }

};





