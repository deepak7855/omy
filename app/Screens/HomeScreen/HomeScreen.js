import React from 'react';
import {
    Text, View, FlatList, SafeAreaView, TouchableOpacity, DeviceEventEmitter,
    ScrollView, TextInput, Image, ImageBackground, Keyboard
} from 'react-native';
import styles from './HomeScreenStyles';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import fonts from '../../Assets/fonts';
import AppHeader from '../../Comman/AppHeader';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import SplashScreen from 'react-native-splash-screen'
import NoData from '../../Comman/NoData';
import { notifyCountStatus, sendToken, updateDeviceToken } from '../../Api/NotifyCountApi';
import messaging from '@react-native-firebase/messaging';
// import {socketInit} from '../../Lib/SocketController';
import * as RNLocalize from "react-native-localize";

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            listItem: [],
            search_activity: '',
            ServiceActivities: [],
            arrRecentBookings: [],
            search_value: '',
            arrSearchingActivities: [],
        }
        this.createHeader();
    }

    componentDidMount() {
        Helper.navigationRef = this.props.navigation;
        this.getDate = DeviceEventEmitter.addListener('upDateLanguage', (data) => {
            RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
            this.getActivitiesServicesApiCall()
        })
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));

        setTimeout(() => {
            SplashScreen.hide();
        }, 500);
        this.listener = DeviceEventEmitter.addListener(Constants.USER_DETAILS, (value) => {
            this.createHeader()
        });
        this.getActivitiesServicesApiCall()
        this.getRecentBookingsApiCall()

        this._unsubscribeHomeFocus = this.props.navigation.addListener('focus', () => {
            notifyCountStatus()
            updateDeviceToken()
            // messaging().onTokenRefresh(async (fcmToken) => {
            //     sendToken(fcmToken)
            // });
        });
        this.messageListener()
    }

    messageListener = async () => {

        messaging().setBackgroundMessageHandler(async remoteMessage => {

        });

        //notification app forground
        messaging().onMessage(async notification => {
            let encryptedData = JSON.parse(notification.data.dictionary);
            if (Helper.currentPage != 'chat' || encryptedData.type != "chat") {
                DeviceEventEmitter.emit('showLocalNotification', notification);
            }
        });

        //When the application is running, but in the background
        messaging().onNotificationOpenedApp(async notification => {
            if (notification && notification.data) {
                this.handleNotificationNavigation(notification.data.type, notification.data.dictionary)
            }
        })

        // notification app kill
        messaging().getInitialNotification().then(async notification => {
            if (notification && notification.data) {
                this.handleNotificationNavigation(notification.data.type, notification.data.dictionary)
            }
        });
    }

    handleNotificationNavigation(type, dicData) {
        let encryptedData = JSON.parse(dicData);
        if (type != 'custom_notification' && type != 'chat') {
            handleNavigation({
                page: 'ActivityDetailScreen', type: 'push', navigation: this.props.navigation, passProps: {
                    booking_id: encryptedData.booking_id,
                    userType: "homeCare"
                }
            })
        }
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);
        this._unsubscribeHomeFocus();
        if (this.listener)
            this.listener.remove();
    }

    handleLocalizationChange = (lang) => {
        LanguagesIndex.setI18nConfig(lang);
        this.forceUpdate();
    };

    createHeader() {
        AppHeader({
            ...this.props.navigation, leftTitle: Helper.user_data.name,
            bellIcon: true, settingsIcon: true,
            leftIcon: Helper.user_data.profile_picture ? { uri: Helper.user_data.profile_picture } : images.default,
            leftIconStyle: { height: 31, width: 31, borderRadius: 31 / 2, resizeMode: 'cover' },
            leftClick: () => this.leftClick(),
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })
    }

    leftClick() {
        handleNavigation({ type: 'push', page: 'ProfileScreen', navigation: this.props.navigation })
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToSchedule(id) {
        Keyboard.dismiss();
        handleNavigation({
            type: 'push', page: 'ScheduleBookingScreen', navigation: this.props.navigation, passProps: {
                serviceId: id
            }
        })
    }

    goToActivityDetailScreen(booking_id) {
        handleNavigation({
            type: 'push', page: 'ActivityDetailScreen', navigation: this.props.navigation, passProps: {
                booking_id: booking_id,
                userTypeHomeScreen: "homeCareHomeScreen"
            }
        })
    }

    getActivitiesServicesApiCall = () => {
        Helper.globalLoader.showLoader();
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_activities, data, Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                this.setState({ ServiceActivities: response.data, })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }

    getRecentBookingsApiCall = () => {
        Helper.globalLoader.showLoader();
        let data = {}
        ApiCall.postMethodWithHeader(Constants.recent_bookings, data, Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();

            if (response.status == Constants.TRUE) {
                this.setState({ arrRecentBookings: response.data })
            }
            else {
                Helper.showToast(response.message)
            }
        }
        )
    }


    _renderRecentActivity = ({ item }) => {

        return (
            <TouchableOpacity
                onPress={() => this.goToActivityDetailScreen(item.id)}
                style={styles.recent_activity_view}>
                <View style={styles.dog_view}>
                    <View style={styles.flex_img_view}>
                        <Image
                            source={{ uri: item.booking_services[0].activity.icon }}
                            resizeMode={'contain'} style={styles.dog_img} />
                    </View>

                    <View style={styles.flex_text_view}>
                        <Text style={[styles.title_date_address_text, { textTransform: 'capitalize' }]}>{item.booking_services[0].activity.name}</Text>
                    </View>
                </View>

                <View style={styles.dog_view}>
                    <View style={styles.flex_img_view}>
                        <Image source={images.calendar_date_ic} resizeMode={'contain'} style={styles.calender_mg} />
                    </View>

                    <View style={styles.flex_text_view}>
                        {/* <Text style={styles.title_date_address_text}>{Helper.formatDate(item.booking_date, "DD/MM/YYYY")}{' '}|{' '}{item.booking_time}</Text> */}
                        <Text style={styles.title_date_address_text}>{Helper.formatDateUtcToLocal(item.booking_date,+`{"|"}`+item.booking_time )}</Text>

                        
                    </View>
                </View>


                <View style={styles.dog_view}>
                    <View style={styles.flex_img_view}>
                        <Image source={images.location_pin_ic} resizeMode={'contain'} style={styles.location_img} />
                    </View>

                    <View style={styles.flex_text_view}>
                        <Text style={styles.title_date_address_text}>{item.location}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }


    _renderActivity = ({ item }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.goToSchedule(item.id)}
                style={styles.activity_touch}>
                <View style={{
                    alignItems: 'center',
                    marginHorizontal: 5,
                }}>
                    <ImageBackground source={{ uri: item.image }} resizeMode={'contain'}
                        style={styles.activity_back_img}>
                        <Text style={[styles.activity_title_txt, { textTransform: 'capitalize' }]}>{item.name}</Text>
                    </ImageBackground>
                </View>
            </TouchableOpacity>
        )
    }

    _renderSearchActivity = ({ item }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.goToSchedule(item.id)}
                style={[{ padding: 10 }]}>
                <Text>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    searchActivity(val) {
        this.setState({ search_value: val }, () => {
            var results = [];
            if (val) {
                for (var i = 0; i < this.state.ServiceActivities.length; i++) {
                    if (this.state.ServiceActivities[i].name.toLowerCase().includes(val.toLowerCase())) {
                        results.push(this.state.ServiceActivities[i]);
                    }
                }
            }
            this.setState({ arrSearchingActivities: results })
        })
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={{ backgroundColor: Colors.white }}>
                        <View style={styles.search_box_blue}>
                            <View style={{ marginHorizontal: 16 }}>
                                <View style={styles.search_box_view}>
                                    <TouchableOpacity
                                        onPress={() => this.searchActivity()}
                                        style={styles.search_touch}>
                                        <Image source={images.search_ic} resizeMode={'contain'} style={styles.search_img} />
                                    </TouchableOpacity>

                                    <TextInput
                                        style={styles.input_text}
                                        placeholder={LanguagesIndex.translate('WhichActivityHave')}
                                        keyboardType={'default'}
                                        returnKeyType="done"
                                        placeholderTextColor={Colors.white}
                                        underlineColorAndroid='transparent'
                                        onChangeText={(val) => this.searchActivity(val)}
                                        value={this.state.search_value}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={{ paddingHorizontal: 20, backgroundColor: '#fff', borderRadius: 5, width: '100%' }}>
                            <FlatList
                                showsHorizontalScrollIndicator={false}
                                data={this.state.arrSearchingActivities}
                                keyboardShouldPersistTaps={'handled'}
                                renderItem={this._renderSearchActivity}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                        {this.state.arrRecentBookings.length > 0 ?
                            <View style={styles.recent_par_view}>
                                <View style={styles.recent_child_view}>
                                    <Text style={styles.recent_txt}>{LanguagesIndex.translate('RecentActivities')}</Text>
                                </View>
                            </View>
                            : null}
                    </View>

                    <FlatList
                        style={{ marginTop: -10 }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrRecentBookings}
                        renderItem={this._renderRecentActivity}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />

                    <View style={styles.activity_view}>
                        <View style={styles.view_activity}>
                            <Text style={styles.activity_txt}>{LanguagesIndex.translate('Activities')}</Text>
                        </View>
                    </View>
                    <FlatList
                        style={{ marginTop: -10, backgroundColor: Colors.white, }}
                        showsHorizontalScrollIndicator={false}
                        data={this.state.ServiceActivities}
                        horizontal
                        renderItem={this._renderActivity}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </ScrollView>
            </SafeAreaView>
        )
    }

};
