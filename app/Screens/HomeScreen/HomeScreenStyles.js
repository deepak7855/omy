import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default HomeScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteFive
    },
    search_box_blue: {
        backgroundColor: Colors.cerulean,
        height:65,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25
    },
    search_box_view: {
        backgroundColor: '#3a9de9',
        width: '100%',
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 25,
    },
    search_touch: {
        marginLeft: 20
    },
    search_img: {
        height: 18,
        width: 18
    },
    input_text: {
        height: 50,
        width: 280,
        paddingLeft: 10,
        color: Colors.white,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    recent_activity_view: {
        backgroundColor: Colors.white,
        marginTop: 5,
        paddingVertical: 10,
    },
    dog_view: {
        marginHorizontal: 16,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5
    },
    flex_img_view: {
        flex: .7
    },
    flex_text_view: {
        flex: 9.3
    },
    title_date_address_text: {
        color: Colors.warmGreyThree,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1,
        lineHeight: 14
    },
    dog_img:{
        width: 13, 
        height: 12.2
    },
    calender_mg:{
        width: 10.7, 
        height: 11.8
    },
    location_img:{
        width: 8.7, 
        height: 12.1
    },
    activity_touch:{
        // alignItems: 'center', 
        // marginHorizontal: 5, 
        paddingVertical: 10 
    },
    activity_back_img:{
        height: 311, 
        width: 141, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    activity_title_txt:{
        color: Colors.white, 
        fontSize: fonts.fontSize14, 
        fontFamily: fonts.RoBoToBold_1,
        fontWeight:'bold'
    },
    recent_par_view:{
        backgroundColor: Colors.white, 
        marginTop: 10
    },
    recent_child_view:{
        marginHorizontal: 16,
        paddingVertical: 5,
    },
    recent_txt:{
        fontWeight: 'bold', 
        color: Colors.blackTwo, 
        fontFamily: fonts.RoBoToBold_1, 
        fontSize: fonts.fontSize14,
    },
    activity_view:{
        backgroundColor: Colors.white, 
        marginTop: 10
    },
    view_activity:{
        marginHorizontal: 16, 
        marginVertical: 10
    },
    activity_txt:{
        fontWeight: 'bold', 
        color: Colors.blackTwo, 
        fontSize: fonts.fontSize14, 
        fontFamily: fonts.RoBoToBold_1
    },
    
});