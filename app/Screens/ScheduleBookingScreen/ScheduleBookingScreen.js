import React from 'react';
import {
    Text, View, FlatList, SafeAreaView, ScrollView, Keyboard,
    TouchableOpacity, Image, StyleSheet, TextInput, Dimensions
} from 'react-native';
import styles from './ScheduleBookingScreenStyles';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import fonts from '../../Assets/fonts';
import { GButton } from '../../Comman/GButton';
import AppHeader from '../../Comman/AppHeader';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import RNPickerSelect from 'react-native-picker-select';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants } from '../../Api';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { Modalize } from 'react-native-modalize';
import AddMoreRequest from './AddMoreRequest';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import * as RNLocalize from 'react-native-localize';

const CustomCalendarArrow = (direction) => {
    return (
        <Image source={direction == "left" ? images.previous_img : images.next_img} resizeMode={'contain'} style={{ height: 16.3, width: 8.2, }} />
    )
}

export default class ScheduleBookingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            change: false,
            isDatePickerVisible: false,
            activity_name: '',
            arrEditServices: [
                {
                    activity_id: '',
                    activity_duration: '',
                    student_count: '',
                    app_languagepreference: [],
                    short_note: ''
                }
            ],
            selectedBookingDate: '',
            booking_date: "",
            booking_time: "",
            location: Helper.user_data.street_no ? Helper.user_data.street_no : '',
            location_lat: Helper.user_data.lat ? Helper.user_data.lat : '',
            location_lng: Helper.user_data.lng ? Helper.user_data.lng : '',
            // selectSlot: '',

            arrActivity: [
                { label: LanguagesIndex.translate('SelectActivity'), value: null }
            ],


            arrDuration: [
                { label: LanguagesIndex.translate('SelectDuration'), value: null },
                { label: '1 hrs', value: '1' },
                { label: '2 hrs', value: '2' },
                { label: '3 hrs', value: '3' },
                { label: '4 hrs', value: '4' },
            ],

            arrStudentCount: [
                { label: LanguagesIndex.translate('SelectStudent'), value: null },
                { label: '1', value: '1' },
                { label: '2', value: '2' },
                { label: '3', value: '3' },
                { label: '4', value: '4' },
                { label: '5', value: '5' },
                { label: '6', value: '6' },
                { label: '7', value: '7' },
                { label: '8', value: '8' },
                { label: '9', value: '9' },
                { label: '10', value: '10' },
                { label: '11', value: '11' },
                { label: '12', value: '12' },
                { label: '13', value: '13' },
                { label: '14', value: '14' },
                { label: '15', value: '15' },
            ],
        }
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('ScheduleBooking'),
            borderBottomRadius: 25,
            bellIcon: true, settingsIcon: true,
            bellIconClick: () => this.bellIconClick(),
            settingIconClick: () => this.settingIconClick()
        })
    }

    settingIconClick() {
        handleNavigation({ type: 'push', page: 'SettingsScreen', navigation: this.props.navigation })
    }

    bellIconClick() {
        handleNavigation({ type: 'push', page: 'NotificationsScreen', navigation: this.props.navigation })
    }

    goToBookingService() {
        handleNavigation({ type: 'push', page: 'BookingServiceScreen', navigation: this.props.navigation })
    }

    componentDidMount() {
        this.getActivitiesApiCall();
        // this.getSlotsApiCall();
        if (this.props.route && this.props.route.params && this.props.route.params.serviceId) {
            this.setValues(0, "activity_id", this.props.route.params.serviceId)
        }
    }

    validateData() {

        let { arrEditServices, booking_date, booking_time, } = this.state;
        if (Object.keys(booking_date).length === 0) {
            Helper.showToast(LanguagesIndex.translate('Pleaseselectbookingdate'))
            return false
        } if (!booking_time) {
            Helper.showToast(LanguagesIndex.translate('Pleaseselectbookingtime'))
            return false
        }
        else {
            let reStatus = true;
            for (let index = 0; index < arrEditServices.length; index++) {
                if (!arrEditServices[index].activity_id || !arrEditServices[index].activity_duration || !arrEditServices[index].student_count || arrEditServices[index].app_languagepreference.length == 0 || !arrEditServices[index].short_note) {
                    Helper.showToast(LanguagesIndex.translate('Allfieldsarerequired'))
                    reStatus = false;
                    break;
                }
            }
            return reStatus;
        }
    }

    getBookingRequestApiCall = () => {
        Keyboard.dismiss()
        if (this.validateData()) {
            let data = new FormData();

            let fullUTCDate = moment(this.state.selectedBookingDate+moment(this.state.booking_time).format('HH:mm'),'YYYY-MM-DD HH:mm').utc().format('YYYY-MM-DD h:mm a');
            let finalUtc = fullUTCDate.split(' ');
            let timezone = RNLocalize.getTimeZone();
            data.append('location', this.state.location);
            data.append('booking_date', finalUtc[0]);
            data.append('booking_time', `${finalUtc[1]} ${finalUtc[2]}`);
            data.append('location_lat', this.state.location_lat);
            data.append('location_lng', this.state.location_lng);
            data.append('time_zone', timezone);
            // data.append('slot', this.state.selectSlot);
            data.append('services', JSON.stringify(this.state.arrEditServices));
            Helper.globalLoader.showLoader();
            // console.log("data------>>>>>",data)
            // return

            ApiCall.postMethodWithHeader(Constants.booking_request, data, Constants.APIImageUploadAndroid).then((response) => {
                Helper.globalLoader.hideLoader();

                Helper.showToast(response.message)
                if (response.status == Constants.TRUE) {
                    setTimeout(() => {
                        handleNavigation({ type: 'pop', navigation: this.props.navigation })
                    }, 100);
                }

            })
        }


    }





    getActivitiesApiCall = () => {
        Helper.globalLoader.showLoader();
        let data = {}
        ApiCall.postMethodWithHeader(Constants.get_activities, data, Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();

            if (response.status == Constants.TRUE) {
                const newColumns = response.data.map(item => {
                    const { id: value, name: label, ...rest } = item;
                    return { value, label, ...rest }
                }
                );
                this.state.arrActivity = [...this.state.arrActivity, ...newColumns]
            }
            else {
                Helper.showToast(response.message)
            }
            this.setState({})
        }
        )
    }


    selectDates(day) {
        let pressDate = moment(day.dateString).format("YYYY-MM-DD")
        let arrDates = { ...this.state.booking_date }
        arrDates = {};
        this.state.booking_time = "",
            this.setState({})
        if (this.state.booking_date[pressDate]) {
            delete arrDates[pressDate];
        } else {
            arrDates[pressDate] = { selected: true, selectedColor: Colors.darkSkyBlue, }
        }

        this.setState({ booking_date: arrDates, selectedBookingDate: pressDate })
    }

    renderLanguageOption = (language, isChosen) => {

        return (
            <View style={{
                marginHorizontal: 16,
                marginVertical: 15
            }}>
                <TouchableOpacity onPress={() => this.setValues()} style={{ flexDirection: 'row' }}>
                    <Image
                        source={isChosen ? images.radio_btn_selected : images.radio_btn_un_selected}
                        style={{
                            height: 14,
                            width: 14
                        }}
                    />
                    <Text style={{
                        marginLeft: 10,
                        fontSize: fonts.fontSize14,
                        fontFamily: fonts.RoBoToRegular_1,
                        lineHeight: 17,
                        color: Colors.warmGreyThree
                    }}>{language}</Text>
                </TouchableOpacity>
            </View>
        );
    };

    setValues(index, key, value) {
        let arrEditServices = [...this.state.arrEditServices]
        if (key == 'app_languagepreference') {
            let isExist = arrEditServices[index][key].indexOf(value);
            if (isExist > -1) {
                arrEditServices[index][key].splice(isExist, 1);
            } else {
                arrEditServices[index][key].push(value);
            }
            arrEditServices[index].language_preference = arrEditServices[index][key].toString();
        } else {
            arrEditServices[index][key] = value;
        }
        this.setState({ arrEditServices })
    }

    showDatePicker = () => {
        Keyboard.dismiss()
        this.setState({ isDatePickerVisible: true })
    };

    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false })
    };


    onAddMoreClose(addedService) {
        this.modalizeRef.close();
        this.state.arrEditServices.push(addedService);
        this.setState({ change: !this.state.change })
    }

    handleConfirm = (date) => {

        this.hideDatePicker();
        let currentDate = moment(new Date()).format('YYYY-MM-DD')
        let checkDate = moment(this.state.selectedBookingDate).format('YYYY-MM-DD')
        if (currentDate == checkDate) {
            let checkCurrentDate = moment(new Date(), 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')

            let tempSelectTime = moment(date).format('HH:mm')
            let compareD = this.state.selectedBookingDate + ' ' + tempSelectTime
            if (checkCurrentDate < compareD) {
                this.setState({ booking_time: date })
                return
            } else {
                Helper.showToast(LanguagesIndex.translate('Pleaseselectvalidtime'))
                this.setState({ booking_time: '' })
            }
        } else {
            this.setState({ booking_time: date })
        }
    };



    getDuration() {

        let format = 'HH:mm';
        let matchEndSlot = '';
        let createDuration = [{ label: LanguagesIndex.translate('SelectDuration'), value: null }];

        let availableTimes = [
            { startTime: moment('07:59', format), endTime: moment('12:00', format) },
            { startTime: moment('11:59', format), endTime: moment('16:00', format) },
            { startTime: moment('15:59', format), endTime: moment('20:00', format) }
        ];

        if (this.state.booking_time) {
            let bookingTime = moment(this.state.booking_time).format(format);

            availableTimes.forEach(element => {
                if (moment(bookingTime, format).isBetween(element.startTime, element.endTime)) {
                    matchEndSlot = element.endTime;
                }
            });

            if (matchEndSlot) {
                let nowHours = matchEndSlot.diff(moment(bookingTime, format), 'hours')
                for (let index = 1; index <= nowHours; index++) {
                    createDuration.push({ label: `${index} hrs`, value: index })
                }
            }
        }
        return createDuration;
    }

    renderServices = ({ item, index }) => {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('SelectActivity')}</Text>
                        <View style={{ flex: 1, }}>
                            <RNPickerSelect
                                placeholder={{}}
                                items={this.state.arrActivity}
                                value={item.activity_id}
                                Icon={false}
                                onValueChange={(value) => {
                                    this.setValues(index, "activity_id", value)
                                }}
                                useNativeAndroidPickerStyle={false}
                                style={pickerSelectStyles}
                                Icon={() => (<Image resizeMode='contain' source={images.drop_down}
                                    style={{
                                        marginHorizontal: 5,
                                        height: 6,
                                        width: 14,
                                        marginTop: 15,
                                        // tintColor:Colors.black,
                                        marginTop: 24,
                                        resizeMode: 'contain',
                                        // height: 6, 
                                        // // position: "absolute", 
                                        // right: 0, 
                                        // top: 24,
                                        //  width: 14
                                    }} />)}
                            />
                            {/* <Image source={images.drop_down} resizeMode={'contain'}
                                style={styles.drop_img} /> */}
                        </View>
                    </View>
                </View>

                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('ActivityDuration')}</Text>
                        <View style={{ flex: 1, }}>
                            <RNPickerSelect
                                placeholder={{}}
                                items={this.getDuration()}
                                // items={this.state.arrDuration}
                                value={item.activity_duration}
                                onValueChange={(value) => { this.setValues(index, "activity_duration", value) }}
                                useNativeAndroidPickerStyle={false}
                                style={pickerSelectStyles}
                                Icon={() => (<Image resizeMode='contain' source={images.drop_down}
                                    style={{
                                        marginHorizontal: 5,
                                        height: 6,
                                        width: 14,
                                        marginTop: 15,
                                        // tintColor:Colors.black,
                                        marginTop: 24,
                                        resizeMode: 'contain',
                                    }} />)}

                            />
                            {/* <Image source={images.drop_down} resizeMode={'contain'}
                                style={styles.drop_img} /> */}
                        </View>
                    </View>
                </View>

                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('NumberOfStudentsRequired')}</Text>
                        <View style={{ flex: 1, }}>
                            <RNPickerSelect
                                placeholder={{}}
                                items={this.state.arrStudentCount}
                                value={item.student_count}
                                onValueChange={(value) => { this.setValues(index, "student_count", value) }}
                                useNativeAndroidPickerStyle={false}
                                style={pickerSelectStyles}
                                Icon={() => (<Image resizeMode='contain' source={images.drop_down}
                                    style={{
                                        marginHorizontal: 5,
                                        height: 6,
                                        width: 14,
                                        marginTop: 15,
                                        // tintColor:Colors.black,
                                        marginTop: 24,
                                        resizeMode: 'contain',
                                    }} />)}
                            />
                            {/* <Image source={images.drop_down} resizeMode={'contain'}
                                style={styles.drop_img} /> */}
                        </View>
                    </View>
                </View>


                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('LanguagePreference')}</Text>
                    </View>

                    <View style={{
                        marginHorizontal: 16,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <TouchableOpacity onPress={() => this.setValues(index, 'app_languagepreference', 'de')} style={{ flexDirection: 'row', marginVertical: 10 }}>
                            <Image
                                source={item.app_languagepreference.includes('de') ? images.check : images.unchecked}
                                style={{
                                    height: 14,
                                    width: 14
                                }}
                            />
                            <Text style={{
                                marginLeft: 10,
                                fontSize: fonts.fontSize14,
                                fontFamily: fonts.RoBoToRegular_1,
                                lineHeight: 17,
                                color: Colors.warmGreyThree
                            }}>{LanguagesIndex.translate('German')}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setValues(index, 'app_languagepreference', 'fr')} style={{
                            flexDirection: 'row',
                            marginVertical: 10
                        }}>
                            <Image
                                source={item.app_languagepreference.includes('fr') ? images.check : images.unchecked}
                                style={{
                                    height: 14,
                                    width: 14
                                }}
                            />
                            <Text style={{
                                marginLeft: 10,
                                fontSize: fonts.fontSize14,
                                fontFamily: fonts.RoBoToRegular_1,
                                lineHeight: 17,
                                color: Colors.warmGreyThree
                            }}>{LanguagesIndex.translate('French')}</Text>
                        </TouchableOpacity>



                        <TouchableOpacity onPress={() => this.setValues(index, 'app_languagepreference', 'en')} style={{ flexDirection: 'row' }}>
                            <Image
                                source={item.app_languagepreference.includes('en') ? images.check : images.unchecked}
                                style={{
                                    height: 14,
                                    width: 14
                                }}
                            />
                            <Text style={{
                                marginLeft: 10,
                                fontSize: fonts.fontSize14,
                                fontFamily: fonts.RoBoToRegular_1,
                                lineHeight: 17,
                                color: Colors.warmGreyThree
                            }}>{LanguagesIndex.translate('English')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>




                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('ShortNote')}</Text>
                        <View style={styles.input_view}>
                            <TextInput
                                style={styles.input_text}
                                placeholder={LanguagesIndex.translate('WriteHere')}
                                underlineColorAndroid="transparent"
                                multiline={true}
                                keyboardType={'default'}
                                returnKeyType="next"
                                textAlignVertical="top"
                                value={item.short_note}
                                onChangeText={(short_note) => this.setValues(index, 'short_note', short_note)}
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.date_view}>
                        <Text style={styles.s_date_txt}>{LanguagesIndex.translate('SelectDate')}</Text>
                    </View>

                    <View style={styles.calender_view}>
                        <Calendar
                            style={{
                                borderWidth: .5,
                                borderColor: Colors.white,
                                borderRadius: 10,
                                elevation: 1.5,
                                backgroundColor: Colors.cerulean
                            }}
                            minDate={new Date()}
                            hideArrows={false}
                            onDayPress={(day) => this.selectDates(day)}
                            renderArrow={CustomCalendarArrow}
                            disableArrowLeft={false}
                            firstDay={1}
                            theme={{
                                "stylesheet.calendar.main": {
                                    container: {
                                        paddingLeft: 0,
                                        paddingRight: 0,
                                    },
                                },
                                'stylesheet.calendar.header': {
                                    week: {
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        backgroundColor: Colors.veryLightBlue,
                                        color: Colors.black,
                                    },
                                    headerContainer: {
                                        flexDirection: 'row',
                                    },
                                    arrow: {
                                        padding: 10,
                                        color: Colors.white,
                                    },
                                    arrowImage: {
                                        tintColor: Colors.white
                                    }
                                },
                                monthTextColor: Colors.white,
                                textDayFontWeight: 'bold',
                                textMonthFontWeight: 'bold',
                                textDayHeaderFontWeight: '300',
                                textDayFontSize: 16,
                                textMonthFontSize: 14,
                                textDayHeaderFontSize: 12,

                            }}

                            markingType={'custom'}
                            markedDates={this.state.booking_date}
                        />
                    </View>


                    <View style={[styles.time_parent_view, { marginTop: 5 }]}>
                        <View style={styles.time_view}>
                            <Text style={styles.time_txt}>{LanguagesIndex.translate('SelectTime')}</Text>
                            <TouchableOpacity onPress={() => this.showDatePicker()} style={styles.time_show_icon_view}>
                                <View style={styles.time_flex}>
                                    <Text style={styles.txt_time}>{this.state.booking_time ? moment(this.state.booking_time).format('h:mm a') : ""}</Text>
                                </View>

                                <View
                                    style={styles.clock_touch_flex}>
                                    <Image source={images.clock_ic} resizeMode={'contain'} style={styles.clock_img} />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <FlatList
                        data={this.state.arrEditServices}
                        renderItem={this.renderServices}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        showsVerticalScrollIndicator={false}
                    />

                    <View style={styles.btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('NEXT')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.getBookingRequestApiCall() }}
                        />
                    </View>
                </KeyboardScroll>

                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisible}
                    mode="time"
                    onConfirm={this.handleConfirm}
                    onCancel={this.hideDatePicker}
                    is24Hour={false}
                    headerTextIOS={'Pick a time'}
                    // minimumDate={new Date()}
                    date={new Date()}
                />


                <Modalize
                    modalHeight={Dimensions.get('window').height / 1.3}
                    ref={(getref) => this.modalizeRef = getref}
                >
                    <AddMoreRequest
                        onAdd={(addedService) => this.onAddMoreClose(addedService)}
                        arrStudentCount={this.state.arrStudentCount}
                        arrEditServices={this.state.arrEditServices}
                        arrDuration={this.getDuration()}
                        arrActivity={this.state.arrActivity} />
                </Modalize>


                <TouchableOpacity

                    onPress={() => this.modalizeRef.open()}
                    style={[styles.service_view]}>
                    <Image source={images.service_add_ic} resizeMode={'contain'} style={styles.plus_img} />
                    <Text style={styles.service_txt}>{LanguagesIndex.translate('Service')}</Text>
                </TouchableOpacity>

            </SafeAreaView>
        )
    }

};
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: fonts.fontSize12,
        height: 40,
        color: Colors.warmGrey,
        fontFamily: fonts.RoBoToMedium_1,
        width: '100%',
        textTransform: 'capitalize',
    },
    inputAndroid: {
        fontSize: fonts.fontSize12,
        height: 40,
        width: '100%',
        color: Colors.warmGrey,
        fontFamily: fonts.RoBoToMedium_1,
        textTransform: 'capitalize',
    },
});





