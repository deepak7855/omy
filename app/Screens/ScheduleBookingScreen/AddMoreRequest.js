import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, TextInput, Keyboard } from 'react-native';
import styles from './ScheduleBookingScreenStyles';

import fonts from '../../Assets/fonts';
import LanguagesIndex from '../../Languages';
import RNPickerSelect from 'react-native-picker-select';
import { images } from '../../Assets/imagesUrl';
import { GButton } from '../../Comman/GButton';
import Helper from '../../Lib/Helper';
import Colors from '../../Assets/Colors';

export default class AddMoreRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activity_id: '',
            activity_duration: '',
            student_count: '',
            app_languagepreference: [],
            language_preference: [],
            short_note: '',
        }
    }


    chooseLanguage(language) {

        let isExist = this.state.app_languagepreference.indexOf(language);
        if (isExist > -1) {
            this.state.app_languagepreference.splice(isExist, 1);
        } else {
            this.state.app_languagepreference.push(language);
        }
        this.state.language_preference = this.state.app_languagepreference.toString();
        this.setState({})
    }

    onAdd() {
        Keyboard.dismiss()
        if (!this.state.activity_id || !this.state.app_languagepreference || !this.state.activity_duration || !this.state.short_note) {
            Helper.showToast(LanguagesIndex.translate('Allfieldsarerequired'));
            return;
        }
        this.props.onAdd(this.state)
    }

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                marginTop: 15,
                marginBottom: 80
            }}>


                <View style={{ marginHorizontal: 16 }}>
                    <Text style={{ color: Colors.cerulean, fontSize: fonts.fontSize16, fontFamily: fonts.RoBoToMedium_1 }}>{LanguagesIndex.translate('AddNewActivity')}</Text>
                </View>

                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('SelectActivity')}</Text>
                        <View style={{ flex: 1, }}>
                            <RNPickerSelect
                                placeholder={{}}
                                items={this.props.arrActivity}
                                value={this.state.activity_id}
                                onValueChange={(value) => {
                                    let isExits = this.props.arrEditServices.some(el => el.activity_id === value);
                                    if (!isExits) {
                                        this.setState({ activity_id: value })
                                    } else {
                                        Helper.alert(LanguagesIndex.translate('ThisactivityalreadyselectedPleaseselectanotheractivity'))
                                    }
                                }}
                                useNativeAndroidPickerStyle={false}
                                style={pickerSelectStyles}
                                Icon={() => (<Image resizeMode='contain' source={images.drop_down}
                                    style={{
                                        marginHorizontal: 5,
                                        height: 6,
                                        width: 14,
                                        marginTop: 15,
                                        // tintColor:Colors.black,
                                        marginTop: 24,
                                        resizeMode: 'contain',
                                    }} />)}
                            />
                            {/* <Image source={images.drop_down} resizeMode={'contain'}
                                style={styles.drop_img} /> */}
                        </View>
                    </View>
                </View>




                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('ActivityDuration')}</Text>
                        <View style={{ flex: 1, }}>
                            <RNPickerSelect
                                placeholder={{}}
                                items={this.props.arrDuration}
                                value={this.state.activity_duration}
                                onValueChange={(value) => { this.setState({ activity_duration: value }) }}
                                useNativeAndroidPickerStyle={false}
                                style={pickerSelectStyles}
                                Icon={() => (<Image resizeMode='contain' source={images.drop_down}
                                    style={{
                                        marginHorizontal: 5,
                                        height: 6,
                                        width: 14,
                                        marginTop: 15,
                                        // tintColor:Colors.black,
                                        marginTop: 24,
                                        resizeMode: 'contain',
                                    }} />)}
                            />
                            {/* <Image source={images.drop_down} resizeMode={'contain'}
                                style={styles.drop_img} /> */}
                        </View>
                    </View>
                </View>



                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('NumberOfStudentsRequired')}</Text>
                        <View style={{ flex: 1, }}>
                            <RNPickerSelect
                                placeholder={{}}
                                items={this.props.arrStudentCount}
                                value={this.state.student_count}
                                onValueChange={(value) => { this.setState({ student_count: value }) }}
                                useNativeAndroidPickerStyle={false}
                                style={pickerSelectStyles}
                                Icon={() => (<Image resizeMode='contain' source={images.drop_down}
                                    style={{
                                        marginHorizontal: 5,
                                        height: 6,
                                        width: 14,
                                        marginTop: 15,
                                        // tintColor:Colors.black,
                                        marginTop: 24,
                                        resizeMode: 'contain',
                                    }} />)}
                            />
                            {/* <Image source={images.drop_down} resizeMode={'contain'}
                                style={styles.drop_img} /> */}
                        </View>
                    </View>
                </View>


                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('LanguagePreference')}</Text>
                    </View>

                    <View style={{
                        marginHorizontal: 16,
                        marginVertical: 15,
                        flexDirection: 'row'
                        , justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <TouchableOpacity onPress={() => this.chooseLanguage('de')} style={{ flexDirection: 'row' }}>
                            <Image
                                source={this.state.app_languagepreference.includes('de') ? images.check : images.unchecked}
                                style={{
                                    height: 14,
                                    width: 14
                                }}
                            />
                            <Text style={{
                                marginLeft: 10,
                                fontSize: fonts.fontSize14,
                                fontFamily: fonts.RoBoToRegular_1,
                                lineHeight: 17,
                                color: Colors.warmGreyThree
                            }}>{LanguagesIndex.translate('German')}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.chooseLanguage('fr')} style={{ flexDirection: 'row' }}>
                            <Image
                                source={this.state.app_languagepreference.includes('fr') ? images.check : images.unchecked}
                                style={{
                                    height: 14,
                                    width: 14
                                }}
                            />
                            <Text style={{
                                marginLeft: 10,
                                fontSize: fonts.fontSize14,
                                fontFamily: fonts.RoBoToRegular_1,
                                lineHeight: 17,
                                color: Colors.warmGreyThree
                            }}>{LanguagesIndex.translate('French')}</Text>
                        </TouchableOpacity>



                        <TouchableOpacity onPress={() => this.chooseLanguage('en')} style={{ flexDirection: 'row' }}>
                            <Image
                                source={this.state.app_languagepreference.includes('en') ? images.check : images.unchecked}
                                style={{
                                    height: 14,
                                    width: 14
                                }}
                            />
                            <Text style={{
                                marginLeft: 10,
                                fontSize: fonts.fontSize14,
                                fontFamily: fonts.RoBoToRegular_1,
                                lineHeight: 17,
                                color: Colors.warmGreyThree
                            }}>{LanguagesIndex.translate('English')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.location_activity_view}>
                    <View style={styles.location_activity_child_view}>
                        <Text style={styles.location_activity_txt}>{LanguagesIndex.translate('ShortNote')}</Text>
                        <View style={styles.input_view}>
                            <TextInput
                                style={styles.input_text}
                                placeholder={LanguagesIndex.translate('WriteHere')}
                                underlineColorAndroid="transparent"
                                multiline={true}
                                keyboardType={'default'}
                                returnKeyType="next"
                                textAlignVertical="top"
                                value={this.state.short_note}
                                onChangeText={(value) => this.setState({ short_note: value })}
                            />
                        </View>
                    </View>


                </View>

                <View onTouchStart={() => { this.onAdd() }} style={styles.btn_view}>
                    <GButton
                        Text={LanguagesIndex.translate('ADD_Button')}
                        width={'100%'}
                        height={50}
                        borderRadius={10}
                        onPress={() => { }}
                    />
                </View>
            </View>
        )
    }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: fonts.fontSize12,
        height: 40,
        color: Colors.warmGrey,
        fontFamily: fonts.RoBoToMedium_1,
        width: '100%',
        textTransform: 'capitalize',
    },
    inputAndroid: {
        fontSize: fonts.fontSize12,
        height: 40,
        width: '100%',
        color: Colors.warmGrey,
        fontFamily: fonts.RoBoToMedium_1,
        textTransform: 'capitalize',
    },
});
