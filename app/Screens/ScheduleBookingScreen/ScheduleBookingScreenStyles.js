import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';

export default ScheduleBookingScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteFive
    },
    date_view:{
      marginHorizontal: 16, 
      marginVertical: 10
    },
    s_date_txt:{
      color:Colors.blackTwo,
      fontSize:fonts.fontSize14,
      fontFamily:fonts.RoBoToMedium_1,
      fontWeight:'800'
    },
    calender_view:{
      marginHorizontal: 16, 
      marginVertical: 5
    },
    white_view:{
      height: 8, 
      backgroundColor: Colors.white, 
      width: '100%', 
      marginVertical: 5
    },
    time_parent_view:{
      backgroundColor: Colors.white
    },
    time_view:{
      marginHorizontal: 16, 
      marginVertical: 10
    },
    time_txt:{
      color: Colors.blackTwo, 
      fontSize: fonts.fontSize14, 
      fontFamily: fonts.RoBoToMedium_1, 
      lineHeight: 17
    },
    time_show_icon_view:{
      flexDirection: 'row', 
      justifyContent: 'space-between',
       marginVertical: 5
    },
    time_flex:{
      flex: 4
    },
    txt_time:{
      color: Colors.warmGreyThree, 
      fontSize: fonts.fontSize12, 
      fontFamily: fonts.RoBoToRegular_1, 
      lineHeight: 14
    },
    clock_touch_flex:{
      flex: 4, 
      alignItems: 'flex-end'
    },
    clock_img:{
      width: 19.3, 
      height: 19.3
    },
    location_activity_view:{
      backgroundColor: Colors.white, 
      marginTop: 5
    },
    location_activity_child_view:{
      marginHorizontal: 16, 
      marginVertical: 10
    },
    location_activity_txt:{
      color: Colors.blackTwo, 
      fontSize: fonts.fontSize14, 
      fontFamily: fonts.RoBoToMedium_1, 
      lineHeight: 17
    },
    view_loc:{
      flexDirection: 'row', 
      justifyContent: 'space-between', 
      marginVertical: 5
    },
    add_flex:{
      flex: 8
    },
    add_txt:{
      color: Colors.warmGreyThree, 
      fontSize: fonts.fontSize12, 
      fontFamily: fonts.RoBoToRegular_1, 
      lineHeight: 14
    },
    loc_touch_img:{
      flex: 2, 
      alignItems: 'flex-end'
    },
    loc_img:{
      width: 18, 
      height: 18
    },
    drop_img:{
      height: 6, 
      position: "absolute", 
      right: 0, 
      top: 24,
       width: 14
    },
    input_view:{
      width: '100%', 
      marginTop: 10
    },
    input_text:{
      fontSize: fonts.fontSize12,
      paddingLeft: 10,
      height: 70, 
      borderColor: Colors.whiteSix, 
      borderWidth: 1, 
      borderRadius: 10
    },
    service_view:{
      alignSelf: 'flex-end', 
      bottom: 25, 
      right: 16, 
      position: 'absolute', 
      backgroundColor: Colors.cerulean, 
      width: 50, 
      height: 50, 
      alignItems: 'center', 
      justifyContent: 'center', 
      borderRadius: 50 / 2
    },
    plus_img:{
      width: 14.1, 
      height: 14.1
    },
    service_txt:{
      fontSize: fonts.fontSize10, 
      fontFamily: fonts.RoBoToMedium_1, 
      color: Colors.white,
      textAlign:'center',
    },



  btn_view:{
    marginVertical: 10, 
    marginTop: 35, 
    marginHorizontal: 16
  },
});