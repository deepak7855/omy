import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default ActivityDetailScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1,
        backgroundColor: Colors.whiteFive
    },
    status_view: {
        backgroundColor: Colors.white,
        paddingVertical: 20
    },
    status_cha_view: {
        marginHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    status_txt: {
        fontSize: fonts.fontSize14,
        color: Colors.blackTwo,
        fontFamily: fonts.RoBoToMedium_1,
    },
    completed_txt: {
        fontSize: fonts.fontSize12,
        color: Colors.algaeGreen,
        fontFamily: fonts.RoBoToMedium_1,
        fontWeight: 'bold'
    },
    time_par_view: {
        marginTop: 5,
        backgroundColor: Colors.white,
        paddingVertical: 5
    },
    main_time_view: {
        marginHorizontal: 16,
        marginTop: 10
    },
    date_time_txt: {
        fontSize: fonts.fontSize14,
        color: Colors.blackTwo,
        fontFamily: fonts.RoBoToMedium_1,
    },
    date_time_formate_view: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    date: {
        fontSize: fonts.fontSize12,
        color: Colors.warmGreyThree,
        fontFamily: fonts.RoBoToRegular_1,
    },
    time: {
        fontSize: fonts.fontSize12,
        color: Colors.warmGreyThree,
        fontFamily: fonts.RoBoToRegular_1,
    },
    activity_duration: {
        marginTop: 5,
        backgroundColor: Colors.white,
        paddingVertical: 5
    },
    activity_view: {
        marginHorizontal: 16,
        marginTop: 10
    },
    activity_duration_title: {
        fontSize: fonts.fontSize14,
        color: Colors.blackTwo,
        fontFamily: fonts.RoBoToMedium_1,
    },
    text_activity_view: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    student_txt: {
        marginTop: 5,
        backgroundColor: Colors.white,
        paddingVertical: 5
    },
    student_name_view: {
        marginTop: 5,
        backgroundColor: Colors.white,
        paddingVertical: 5
    },
    name_view: {
        marginHorizontal: 16,
        marginTop: 10
    },
    student_name_txt: {
        fontSize: fonts.fontSize14,
        color: Colors.blackTwo,
        fontFamily: fonts.RoBoToMedium_1,
    },
    name_txt: {
        fontSize: fonts.fontSize12,
        color: Colors.warmGreyThree,
        fontFamily: fonts.RoBoToRegular_1,
    },
    complete_btn_view: {
        paddingVertical: 20,
        alignItems: 'center'
    },
    complete_btn_touch: {
        backgroundColor: Colors.cerulean,
        paddingVertical: 15,
        paddingHorizontal: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    complete_btn_txt: {
        color: Colors.white,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
    },
    acc_rej_btn_view: {
        backgroundColor: Colors.white,
        paddingVertical: 20
    },
    acc_rej_view: {
        marginHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    rej_btn_touch: {
        justifyContent: 'center',
        paddingHorizontal: 50,
        paddingVertical: 15,
        borderWidth: 1,
        borderColor: Colors.cerulean,
        alignItems: 'center',
        borderRadius: 10
    },
    rej_txt:{
        color: Colors.cerulean,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
    },
    accept_btn_touch:{
        paddingHorizontal: 50,
        paddingVertical: 15,
        backgroundColor: Colors.cerulean,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    rat_your_touch:{
        backgroundColor: Colors.cerulean, 
        paddingHorizontal: 10, 
        paddingVertical: 5,
        borderRadius: 5, 
        marginTop: 5
    },
    rat_your_txt:{
        fontSize: fonts.fontSize10,
         color: Colors.white,
        fontFamily: fonts.RoBoToRegular_1,
    }



});