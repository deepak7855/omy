import React from 'react';
import { Text, View, FlatList, Image, SafeAreaView, DeviceEventEmitter, } from 'react-native';
import styles from './ActivityDetailScreenStyles';
import Colors from '../../Assets/Colors';
import AppHeader from '../../Comman/AppHeader';
import { ApiCall, Constants } from '../../Api';
import Helper from '../../Lib/Helper';
import LanguagesIndex from '../../Languages';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import moment from 'moment';
import ListLoader from '../../Comman/ListLoader';
import { changeBookingStatus } from '../../Api/BookingApis';
import { handleNavigation } from '../../navigation/Navigation';
import fonts from '../../Assets/fonts';
import { images } from '../../Assets/imagesUrl';

export default class ActivityDetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            booking_id: this.props.route.params.booking_id,
            booking_service_id: this.props.route.params?.booking_service_id,
            userType: this.props.route.params?.userType,
            userTypeHomeScreen: this.props.route.params?.userTypeHomeScreen,
            bookingLatLong:'',
            arrBookingDetails: '',
            activityData: [],
            loading: true,
            allAccepted: true
        }

        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('ActivityDetail'),
            borderBottomRadius: 25,
        })

    }

    componentDidMount() {
        this._actDetailUnsubscribe = this.props.navigation.addListener('focus', () => {
            this.getBookingDetailApiCall();
        });

        // let startTime =  this.state.arrBookingDetails.booking_date

        // console.log("startTime,startTime",startTime);
    }

    componentWillUnmount() {
        this._actDetailUnsubscribe();
    }


    goToCancelChargesStudent() {
        handleNavigation({ type: 'push', page: 'CancelChargesStudent', navigation: this.props.navigation })
    }

    goToTrackStudent() {
        handleNavigation({
            type: 'push', page: 'MapTrackStudent', navigation: this.props.navigation, passProps: {
                booking_id:
                    this.state.booking_id,
                booking_service_id: this.state.booking_service_id,
                booking_time: this.state.arrBookingDetails.booking_time,
                bookingLatLong:this.state.bookingLatLong,
                student_lat: this.state.activityData[0]?.user_booking[0]?.user.lat,
                student_lng: this.state.activityData[0]?.user_booking[0]?.user.lng,
                student_id: this.state.activityData[0]?.user_booking[0]?.user_id,
            }
        })
    }

    cancelBookingApiCall = () => {
        Helper.backConfirm(LanguagesIndex.translate('Areyousureyouwanttocancelthisbooking'), (status) => {
            if (status) {
                Helper.globalLoader.showLoader();
                let data = {
                    booking_id: this.state.booking_id,
                    booking_service_id: this.state.booking_service_id
                }

                ApiCall.postMethodWithHeader(Constants.cancel_booking, JSON.stringify(data), Constants.APIPost).then((response) => {
                    Helper.globalLoader.hideLoader();
                    if ((response.status == Constants.TRUE) || (response.user_status == Constants.TRUE)) {
                        DeviceEventEmitter.emit('CancelBooking', "done")
                        this.props.navigation.goBack(null)
                        Helper.showToast(response.message)
                    }
                    else {
                        // Helper.showToast(response.message)
                    }
                });
            }
        });
    }


    //     fun(data){

    //   const local = moment.utc(data.booking_time, "hh:mm a").local().format("hh:mm a");

    // var suraction = moment(local, "hh:mm a").subtract(15, "minutes").format("hh:mm a")
    // console.log("jhvdashdvhsad----11--", local);
    // console.log("jhvdashdvhsad------", suraction);

    //     }

    getBookingDetailApiCall = () => {
        let data = new FormData();
        
        data.append('booking_id', this.state.booking_id);
        console.log("data ==>", data);
        ApiCall.postMethodWithHeader(Constants.booking_detail, data, Constants.APIImageUploadAndroid).then((response) => {

            if (response.status == Constants.TRUE && response.data && response.data.booking_time) {
                console.log("response.data ===>",response.data);
                this.setState({
                    arrBookingDetails: response.data,
                    bookingLatLong:{bookLat:response.data?.lat, bookLong:response.data?.lng},
                    loading: false,
                    activityData: response.data.booking_services,
                })
                //this.fun(response.data)
                for (let index = 0; index < response.data.booking_services[0].user_booking.length; index++) {
                    if (response.data.booking_services[0].user_booking[index].status != 'ACCEPT') {
                        this.setState({
                            allAccepted: false
                        })
                        break;
                    }
                }
            } else {
                Helper.showToast(response.message);
                this.props.navigation.goBack(null);
            }
        })
    }


    goToStudentProfile(item) {
        handleNavigation({
            type: 'push', page: 'StudentProfileScreen', navigation: this.props.navigation,
            passProps: {
                student_id: item.user_id,
            }
        })
    }

    goToChatScreen(item) {
        handleNavigation({
            type: 'push', page: 'ChatScreen', navigation: this.props.navigation,
            passProps: {
                other_user_id: item.user.id,
                name: item.user.name,
                picture: item.user.profile_picture,
                fromWhere: 'booking',
                extraData: {
                    bookingStatus: item.status,
                    cancelBy: item.cancelled_by_name,
                    givenRating: Number(item.rating_status),
                    serviceName: this.state.activityData[0].activity.name,
                }
            }
        })
    }


    goToRateYourExperience(item) {

        handleNavigation({
            type: 'push', page: 'RateExperienceScreen', navigation: this.props.navigation,
            passProps: {
                studentData: item, activityType: this.state.activityData
            }
        })
    }

    _renderStudentNameItem = ({ item }) => {
        //console.log("item start status", item)
        return (
            <View style={{ marginHorizontal: 16, flexDirection: 'row', justifyContent: 'space-between', }}>

                <View>
                    <TouchableOpacity
                        onPress={() => this.goToStudentProfile(item)}
                        style={{}}>
                        <Text style={[styles.name_txt, { color: Colors.cerulean }]}>{item.user.name}</Text>
                    </TouchableOpacity>
                    {Helper.user_data.user_type == 'BEOMY' && (item.status == 'COMPLETE' || item.status == 'ACCEPT') ?
                        <TouchableOpacity
                            onPress={() => this.goToChatScreen(item)}
                            style={{
                                backgroundColor: Colors.cerulean, paddingHorizontal: 5,
                                alignItems: 'center', marginTop: 5, paddingVertical: 5, borderRadius: 5
                            }}>
                            <Text style={[styles.name_txt, { color: Colors.white }]}>{LanguagesIndex.translate('Chat')}</Text>
                        </TouchableOpacity>
                        : null}
                </View>

                <View style={{ alignItems: 'flex-end', padding: 5 }}>
                <Text style={[styles.name_txt, { color: Helper.getBookingStatus(item.status).color }]}>{Helper.getBookingStatus(item.status).value} {item.status == 'CANCEL' && item.cancelled_by_name ? `${LanguagesIndex.translate('by')} ${item.cancelled_by_name}` : null}</Text>
                    {/* {item.status == "START" ?
                        <Text style={[styles.name_txt, { color: Helper.getBookingStatus(item.status).color }]}>{Helper.getBookingStatus(item.status == "START" ? LanguagesIndex.translate('INPROGRESS') : '').value} {item.status == 'CANCEL' && item.cancelled_by_name ? `${LanguagesIndex.translate('by')} ${item.cancelled_by_name}` : null}</Text>
                        :
                        <Text style={[styles.name_txt, { color: Helper.getBookingStatus(item.status).color }]}>{Helper.getBookingStatus(item.status).value} {item.status == 'CANCEL' && item.cancelled_by_name ? `${LanguagesIndex.translate('by')} ${item.cancelled_by_name}` : null}</Text>
                    } */}

                    {item.status == 'COMPLETE' && Number(item.rating_status) == 0 ?
                        <TouchableOpacity
                            onPress={() => this.goToRateYourExperience(item)}
                            style={styles.rat_your_touch}>
                            <Text style={styles.rat_your_txt}>{LanguagesIndex.translate('RateYourExperience')}</Text>
                        </TouchableOpacity> : null}
                </View>
            </View>
        )
    }

    showCompleteBtn() {
        
        let startTime = moment(this.state.arrBookingDetails.booking_time, "h:mm a").format("HHmm");

        // console.log("this.state.arrBookingDetails.booking_time", this.state.arrBookingDetails.booking_time)

        console.log('startTime', startTime)
        let currentTime = moment().format('HHmm');
        // console.log("this.state.arrBookingDetails.status======>", this.state.arrBookingDetails.status);
        //    when we make build it use 
        //return moment(this.state.arrBookingDetails.booking_date).isSameOrBefore(moment()) && startTime < currentTime && this.state.arrBookingDetails.status != 1 && this.state.activityData[0].user_booking && Number(this.state.activityData[0].user_booking.length) == Number(this.state.activityData[0].student_count)

        return moment(this.state.arrBookingDetails.booking_date).isSameOrBefore(moment()) && Helper.formatDateUtcToLocal.startTime < currentTime && this.state.arrBookingDetails.status != 1 && this.state.activityData[0].user_booking && Number(this.state.activityData[0].user_booking.length) == Number(this.state.activityData[0].student_count) 

    }

    ShowTrackButton() {
        let tackTime = moment.utc(this.state.arrBookingDetails.booking_time, "hh:mm a").local().format("hh:mm a");
        let subTractTime = moment(tackTime, "hh:mm a").subtract(15, "minutes").format("hh:mm a");
        let check = moment(new Date()).format("hh:mm a")
        var beginningTime = moment(check, 'h:mma');
        var endTime = moment(subTractTime, 'h:mma');

        console.log("subTractTime---------------", beginningTime);
        console.log("beginningTime---------------", endTime);

        console.log("isAfter----------", beginningTime.isAfter(endTime)); // true

        // console.log("this.state.arrBookingDetails.booking_date",this.state.arrBookingDetails.booking_date)

        let val = moment(this.state.arrBookingDetails.booking_date).isSameOrBefore(moment()) && beginningTime.isAfter(endTime);
        console.log('val----val---val', val)

        return val /// moment(this.state.arrBookingDetails.booking_date).isSameOrBefore(moment()) && Helper.formatDateUtcToLocal.tackTime < subTractTime && this.state.arrBookingDetails.status != 1 && this.state.activityData[0].user_booking && Number(this.state.activityData[0].user_booking.length) == Number(this.state.activityData[0].student_count)  

    }

    completeBooking() {
        Helper.backConfirm(LanguagesIndex.translate('Areyousureyouwanttocompletethisbooking'), (status) => {
            if (status) {
                let data = new FormData();
                data.append('booking_id', this.state.booking_id);
                data.append('status', "COMPLETE");
                changeBookingStatus(data, (res) => {
                    if (res.status == Constants.TRUE) {
                        DeviceEventEmitter.emit('changeBookingStatus', "done")
                        DeviceEventEmitter.emit('CancelBooking', "done")
                        this.getBookingDetailApiCall();
                    }
                })
            }
        })
    }

    _activityDetails = ({ item, index }) => {
        return (
            <View>
                <View style={styles.activity_duration}>
                    <View style={styles.activity_view}>
                        <Text style={styles.activity_duration_title}>{LanguagesIndex.translate('Activity&Duration')}</Text>
                        <View style={styles.text_activity_view}>
                            <Text style={[styles.date, { textTransform: 'capitalize' }]}>{item.activity ? item.activity.name : null}</Text>
                            <Text style={styles.date}>{item.activity_duration} {LanguagesIndex.translate('hr')}</Text>
                        </View>
                    </View>
                </View>

                {Helper.user_data.user_type == 'BEOMY' ?

                    <View style={styles.activity_duration}>
                        <View style={styles.activity_view}>
                            <Text style={styles.activity_duration_title}>{LanguagesIndex.translate('BookingAmount')}</Text>
                            <View style={styles.text_activity_view}>
                                <Text style={[styles.date, { textTransform: 'capitalize' }]}>{LanguagesIndex.translate('HourlyRate')}</Text>
                                <Text style={styles.date}>CHF {(Number(this.state.arrBookingDetails?.amount)) / Number(item.activity_duration)}/{LanguagesIndex.translate('hr')}</Text>
                            </View>
                            <View style={[styles.text_activity_view, { paddingVertical: 3 }]}>
                                <Text style={[styles.date, { textTransform: 'capitalize' }]}>{LanguagesIndex.translate('Activity')}</Text>
                                <Text style={styles.date}>CHF {Number(this.state.arrBookingDetails?.amount)}</Text>
                            </View>
                            <View style={[styles.text_activity_view, { paddingVertical: 3 }]}>
                                <Text style={[styles.date, { textTransform: 'capitalize' }]}>{LanguagesIndex.translate('Service Fee')}</Text>
                                <Text style={styles.date}>CHF {Number(this.state.arrBookingDetails.commission)}</Text>
                            </View>
                            <View style={[styles.text_activity_view, { paddingVertical: 3 }]}>
                                <Text style={[styles.date, { textTransform: 'capitalize' }]}>{LanguagesIndex.translate('TotalAmount')}</Text>
                                <Text style={styles.date}>CHF {Number(this.state.arrBookingDetails?.amount) + Number(this.state.arrBookingDetails.commission)}</Text>
                            </View>
                        </View>
                    </View>
                    : null
                }
                {this.state.userType == 'homeCare' ?
                    <View style={styles.activity_duration}>
                        <View style={styles.activity_view}>
                            <Text style={styles.activity_duration_title}>{LanguagesIndex.translate('NumberOfStudents')}</Text>
                            <View style={styles.text_activity_view}>
                                <Text style={styles.date}>{item.student_count}</Text>
                            </View>
                        </View>
                    </View> :
                    null
                }


                <View style={styles.student_name_view}>
                    <View style={styles.name_view}>
                        <Text style={styles.student_name_txt}>{LanguagesIndex.translate('StudentsName')}</Text>
                    </View>
                </View>

                {item.user_booking && item.user_booking.length > 0 ?
                    <FlatList
                        style={{ backgroundColor: Colors.white, paddingVertical: 5 }}
                        showsVerticalScrollIndicator={false}
                        data={item.user_booking}
                        renderItem={this._renderStudentNameItem}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    : null}


                <View style={{ marginHorizontal: 16, flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Text style={styles.name_txt}>
                        {/* {item.user.name} */}
                    </Text>
                    <Text style={[styles.name_txt, { color: Helper.getBookingStatus(item.status).color }]}>
                        {/* {Helper.getBookingStatus(item.status).value} */}
                    </Text>
                </View>


            </View>
        )
    }

    render() {

        // console.log("this.state.activityData---->",this.state.activityData[0]?.user_booking[0]?.user.lat)
        // console.log("this.state.activityData---->",this.state.activityData[0]?.user_booking[0]?.user.lng)
        console.log("this.state.activityData---->", this.state.activityData[0]?.user_booking[0]?.user_id)

        let { arrBookingDetails, loading } = this.state;
        let bookingStatus = this.state?.activityData[0]?.user_booking[0]?.status
        let bookingRejectStatus = this.props.route.params.BookingStatus
        console.log("bookingRejectStatus---bookingRejectStatus", bookingRejectStatus);

        let tackTime = moment.utc(this.state.arrBookingDetails.booking_time, "hh:mm a").local().format("hh:mm a");
        let subTractTime = moment(tackTime, "hh:mm a").subtract(1, "minutes").format("hh:mm a");
        let check = moment(new Date()).format("hh:mm a")
        var beginningTime = moment(check, 'h:mma');
        var endTime = moment(subTractTime, 'h:mma');

        console.log("subTractTime---------------", beginningTime);
        console.log("beginningTime---------------", endTime);

        console.log("isAfter----------", beginningTime.isAfter(endTime)); // true

         //console.log("this.state.arrBookingDetails.booking_date",this.state.arrBookingDetails)

        let val = moment(this.state.arrBookingDetails.booking_date).isSameOrBefore(moment()) && beginningTime.isAfter(endTime);
        console.log('val----val---val', val)
        
        //return val
        
        return (
            <SafeAreaView style={styles.safe_area_view}>
                {loading ? <ListLoader /> :
                    arrBookingDetails && arrBookingDetails.booking_time ?
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={styles.time_par_view}>
                                <View style={styles.main_time_view}>
                                    <Text style={styles.date_time_txt}>{LanguagesIndex.translate('Date&Time')}</Text>
                                    <View style={styles.date_time_formate_view}>
                                        <Text style={styles.date}>{arrBookingDetails ?
                                            // Helper.formatDate(arrBookingDetails.booking_date, "DD/MM/YYYY") 
                                            Helper.formatDateUtcToLocal(arrBookingDetails.booking_date, arrBookingDetails.booking_time, 'date')

                                            : ''}</Text>
                                        <Text style={styles.time}>{arrBookingDetails ?
                                            // arrBookingDetails.booking_time 
                                            Helper.formatDateUtcToLocal(arrBookingDetails.booking_date, arrBookingDetails.booking_time, 'time')

                                            : ''}</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.time_par_view}>
                                <View style={styles.main_time_view}>
                                    <Text style={styles.date_time_txt}>{LanguagesIndex.translate('Address')}</Text>
                                    <View style={{ paddingVertical: 10, }}>
                                        <Text style={styles.date}>{arrBookingDetails ? arrBookingDetails.location : ''}</Text>
                                    </View>
                                </View>
                            </View>

                            <FlatList
                                data={this.state.activityData}
                                renderItem={this._activityDetails}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator={false}
                            />

                            {bookingRejectStatus == "REJECT" ? null :
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 16 }}>
                                    {this.state.userType == 'homeCare' || this.state.userTypeHomeScreen == 'homeCareHomeScreen' ? null : bookingStatus == "ACCEPT" ? <TouchableOpacity
                                        onPress={() => this.cancelBookingApiCall()}
                                        style={{
                                            width: 150,
                                            height: 45,
                                            backgroundColor: Colors.cerulean,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            borderRadius: 10
                                        }}>
                                        <Text style={{
                                            color: Colors.white,
                                            fontSize: fonts.fontSize14,
                                            fontFamily: fonts.RoBoToMedium_1,
                                        }}>{LanguagesIndex.translate('Cancel')}</Text>
                                    </TouchableOpacity> : null
                                    }

                                    {bookingStatus == "REJECT" || bookingStatus == "CANCEL" || bookingStatus == "COMPLETE"? null :
                                        // add after client request after 25 july feedback
                                        <>


                                        {Helper.user_data.user_type == "BEOMY" || Helper.user_data.user_type == "ENTERPRISE" || bookingStatus == "IN PROGRESS" || (this.showCompleteBtn() || val) ? 
                                                <TouchableOpacity
                                                    onPress={() => this.completeBooking()}
                                                    style={{
                                                        width: 150,

                                                        height: 45,
                                                        backgroundColor: Colors.cerulean,
                                                        alignItems: 'center',
                                                        justifyContent: 'center',
                                                        borderRadius: 10
                                                    }}>
                                                    <Text style={{
                                                        color: Colors.white,
                                                        fontSize: fonts.fontSize14,
                                                        fontFamily: fonts.RoBoToMedium_1,
                                                    }}>{LanguagesIndex.translate('Complete')}</Text>
                                                </TouchableOpacity>:
                                                null}
                                                
                                        </>
                                    }




                                    {/* {bookingStatus == "REJECT" || bookingStatus == "CANCEL" || bookingStatus == "COMPLETE"? null :
                                        // add after client feedback
                                        <>
                                        {console.log("this.showCompleteBtn()-------",this.showCompleteBtn()) }
                                        {console.log("this.showCompleteBtn()-----bookingStatus--",bookingStatus) }
                                        {console.log("this.showCompleteBtn()----val---",val) }

                                        {Helper.user_data.user_type == "BEOMY"  ? 
                                            (this.showCompleteBtn() || val) ?
                                                <TouchableOpacity
                                                    onPress={() => this.completeBooking()}
                                                    style={{
                                                        width: 150,

                                                        height: 45,
                                                        backgroundColor: Colors.cerulean,
                                                        alignItems: 'center',
                                                        justifyContent: 'center',
                                                        borderRadius: 10
                                                    }}>
                                                    <Text style={{
                                                        color: Colors.white,
                                                        fontSize: fonts.fontSize14,
                                                        fontFamily: fonts.RoBoToMedium_1,
                                                    }}>{LanguagesIndex.translate('Complete')}</Text>
                                                </TouchableOpacity>
                                                :  null 
                                                
                                                :

                                                
                                                (bookingStatus == "ACCEPT" || bookingStatus == "IN PROGRESS" && Helper.user_data.user_type == "ENTERPRISE") ?
                                                <TouchableOpacity
                                                onPress={() => this.completeBooking()}
                                                style={{
                                                    width: 150,

                                                    height: 45,
                                                    backgroundColor: Colors.cerulean,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    borderRadius: 10
                                                }}>
                                                    {console.log("bookingStatus-------",bookingStatus)}
                                                    {console.log("bookingStatus----ssss---",Helper.user_data.user_type)}
                                                <Text style={{
                                                    color: Colors.white,
                                                    fontSize: fonts.fontSize14,
                                                    fontFamily: fonts.RoBoToMedium_1,
                                                }}>{LanguagesIndex.translate('Complete')}</Text>
                                            </TouchableOpacity>:
                                            null
                                            }
                                        </>
                                    } */}

                                </View>
                            }

                            {/* tracking button */}

                            {this.state.userType == 'homeCare' || this.state.userTypeHomeScreen == 'homeCareHomeScreen' || bookingRejectStatus == "REJECT" ?
                                null :
                                <>
                                    {bookingRejectStatus == "CANCEL" || bookingRejectStatus == "COMPLETE" ? null :
                                        <>
                                            {this.ShowTrackButton() && this.state.activityData[0]?.user_booking[0]?.user?.status == "1" ?
                                                <View style={{ marginTop: 25, marginHorizontal: 16 }}>
                                                    <TouchableOpacity
                                                        onPress={() => this.goToTrackStudent()}
                                                        style={{
                                                            width: 170,
                                                            height: 44,
                                                            backgroundColor: Colors.cerulean,
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                            borderRadius: 10
                                                        }}>
                                                        <Text style={{
                                                            color: Colors.white,
                                                            fontSize: fonts.fontSize14,
                                                            fontFamily: fonts.RoBoToMedium_1,
                                                        }}>{LanguagesIndex.translate('TRACKSTUDENT')}</Text>
                                                    </TouchableOpacity>
                                                </View> :
                                                null
                                            }
                                        </>
                                    }
                                </>
                            }



                            {/* {this.state.userType == 'homeCare' || this.state.userTypeHomeScreen == 'homeCareHomeScreen' ?
                                null :
                                <>
                                    {this.ShowTrackButton() && this.state.activityData[0]?.user_booking[0]?.user?.status == "2" ?
                                        <View style={{ marginTop: 25, marginHorizontal: 16 }}>
                                            <TouchableOpacity
                                                onPress={() => this.goToTrackStudent()}
                                                style={{
                                                    width: 170,
                                                    height: 44,
                                                    backgroundColor: Colors.cerulean,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    borderRadius: 10
                                                }}>
                                                <Text style={{
                                                    color: Colors.white,
                                                    fontSize: fonts.fontSize14,
                                                    fontFamily: fonts.RoBoToMedium_1,
                                                }}>{LanguagesIndex.translate('TRACKSTUDENT')}</Text>
                                            </TouchableOpacity>
                                        </View> :
                                        null
                                    }</>
                            } */}

                            {this.state.userType == 'homeCare' || this.state.userTypeHomeScreen == 'homeCareHomeScreen' ? null : bookingStatus == "ACCEPT" ? <View style={{ paddingHorizontal: 16, paddingVertical: 15, alignItems: 'flex-start', marginTop: 10 }}>
                                <TouchableOpacity
                                    onPress={() => this.goToCancelChargesStudent()} >
                                    <Text style={{ color: Colors.black, fontSize: fonts.fontSize14 }}>{LanguagesIndex.translate('Forcancellationpolicyandcharges')}{" "}<Text style={{ color: Colors.cerulean, lineHeight: 15, textDecorationLine: 'underline' }}>{LanguagesIndex.translate('clickhere')}</Text></Text>
                                </TouchableOpacity>
                            </View> : null}
                        </ScrollView>
                        : null
                }
            </SafeAreaView>
        )
    }
};
