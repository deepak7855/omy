import React from 'react';
import {
    Text, View, ScrollView, Keyboard, Image, Dimensions,
    TouchableOpacity, SafeAreaView,Modal,FlatList
} from 'react-native';
import styles from './SignUpScreenStyles';
import { GButton } from '../../Comman/GButton';
import { SocialButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import IconInput from '../../Comman/GInput';
import { handleNavigation } from '../../navigation/Navigation';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import LanguagesIndex from '../../Languages';
import { FacebookLogin, } from '../../Comman/SocialLogin';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import CameraController from '../../Lib/CameraController';



const DeviceH = Dimensions.get('window').height;
const DeviceW = Dimensions.get('window').width;
export default class SignUpScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatarSource: '', 
            CheckedBox: false,

            userSignUpEnterForm: {
                profile_picture: '',
                business_name: '',
                name: '',
                mobile_number: '',
                email: '',
                // location: '',
                street_no: '',
                postcode: '',
                city: '',
                password: '',
                confirm_password: '',
                user_type: 'ENTERPRISE',
                device_type: '',
                device_id: '',
                lat: '',
                lng: '',

                // lat: '26.4873983',
                // lng: '74.630054',

                validators: {
                    business_name: { required: true, minLength: 2, maxLength: 45, },
                    name: { required: true, minLength: 2, maxLength: 45, },
                    mobile_number: { required: true, minLengthDigit: 9, maxLengthDigit: 9 },
                    email: { required: true, email: true },
                    // location: { required: true },
                    street_no: { required: true, minLength: 1, maxLength: 100 },
                    postcode: { required: true, minLengthDigit: 4, maxLengthDigit: 10 },
                    city: { required: true, minLength: 1, maxLength: 45 },
                    password: { required: true, minLength: 8, maxLength: 15 },
                    confirm_password: { required: true, matchWith: "password" },
                },
                modalVisible:false,
                postalData:[],
                modalVisibleCity:false
            }

        }
    }

    setValues(key, value) {
        let userSignUpEnterForm = { ...this.state.userSignUpEnterForm }
        // if (key == 'city') {
        //     value = value.replace(/[^a-zA-Z ]/g, '');
        // } else
         if (key == 'mobile_number') {
            value = value.replace(/\D/g, ''); 
        } 
        userSignUpEnterForm[key] = value
        this.setState({ userSignUpEnterForm })
    }

    onChangeCheckedBox = () => {
        this.setState({ CheckedBox: !this.state.CheckedBox })
    }

    goToLogin() {
        this.props.navigation.navigate('LoginScreen')
    }


    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    getPostalsCode = () => {
        let data = {
            postcode:this.state.search_p
        };
        console.log('datadata',data)
        this.setState({postalData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    postalData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderList = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.setValues('postcode',item.code)
                            //this.setValues('city','')
                            
                            this.setModalVisible(false)
                            setTimeout(() => {
                                this.setValues('city','')
                                this.setState({postalData:[]})
                                this.getCity()
                            },1000)
                            
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.code}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    postalModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisible(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By Postal')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            keyboardType={'numeric'}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_p:search}, () => {
                                this.getPostalsCode()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.postalData}
                        renderItem={this._renderList}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisible(false) }}
                        />
                    </View>
                </Modal>


        )
    }


    setModalVisibleCity = (visible) => {
        this.setState({ modalVisibleCity: visible });
    }

    getCity = () => {
        let data = {
            postcode:this.state.userSignUpEnterForm.postcode,
            city:this.state.search_c
        };
        console.log('datadata',data)
        this.setState({cityData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    cityData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderListC = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            
                            //this.setValues('postcode',item.code)
                            this.setValues('city',item.city)
                            this.setModalVisibleCity(false)
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.city}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    cityModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisibleCity}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisibleCity(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By City')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_c:search}, () => {
                                this.getCity()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.cityData}
                        renderItem={this._renderListC}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisibleCity(false) }}
                        />
                    </View>
                </Modal>


        )
    }


    async goToEnterpriseSingUp() { 
        let isValid = Helper.validate(this.state.userSignUpEnterForm);
        if (isValid) {
            if (!this.state.CheckedBox) {
                Helper.showToast(LanguagesIndex.translate('Pleaseacceptourtermsandcondition'))
                return
            }
            
            Keyboard.dismiss()
            Helper.globalLoader.showLoader();
            await Helper.GetAddressFromLatLong(`${this.state.userSignUpEnterForm.street_no} ${this.state.userSignUpEnterForm.city} ${this.state.userSignUpEnterForm.postcode}`, (resp) => {

                if(!resp){
                    Helper.globalLoader.hideLoader();
                    return;
                }
            
                this.state.userSignUpEnterForm.lat = resp.lat;
                this.state.userSignUpEnterForm.lng = resp.lng;
                this.setState({})

                let data = new FormData(); 
                data.append('device_type', Helper.device_type)
                data.append('device_id', Helper.device_id)
                data.append('business_name', this.state.userSignUpEnterForm.business_name)
                data.append('name', this.state.userSignUpEnterForm.name)
                data.append('mobile_number', this.state.userSignUpEnterForm.mobile_number)
                data.append('email', this.state.userSignUpEnterForm.email)
                data.append('street_no', this.state.userSignUpEnterForm.street_no)
                data.append('postcode', this.state.userSignUpEnterForm.postcode)
                data.append('city', this.state.userSignUpEnterForm.city)
                data.append('password', this.state.userSignUpEnterForm.password)
                data.append('user_type', this.state.userSignUpEnterForm.user_type)
                data.append('lat', this.state.userSignUpEnterForm.lat)
                data.append('lng', this.state.userSignUpEnterForm.lng)
                if (this.state.userSignUpEnterForm.profile_picture) {
                    data.append('profile_picture', {
                        uri: this.state.userSignUpEnterForm.profile_picture,
                        name: 'test.jpeg',
                        type: 'image/jpeg'
                    });
                }

                ApiCall.postMethodWithHeader(Constants.SIGNUP, data, Constants.APIImageUploadAndroid).then((response) => {
                    Helper.globalLoader.hideLoader();
                    if (response.status == Constants.TRUE) {
                        Helper.showToast(response.message);
                        setTimeout(() => {
                            handleNavigation({ type: 'pop', navigation: this.props.navigation });
                        }, 200);
                    }
                    else {
                        Helper.showToast(response.message)
                    }
                }
                ).catch(err => {
                })
            })
        }

    }


    chooseImage = () => {
        CameraController.open((response) => {
            if (response) {
               // this.UploadMediaMethod(response.uri);
                this.UploadMediaMethod(response.path);
            }
        });
    }
  
    UploadMediaMethod(uri) {
        this.setValues('profile_picture', uri)
        this.setState({ avatarSource: uri })
    }

    gotoFacebookSocialSide = () => {
        Helper.globalLoader.showLoader();
        FacebookLogin((result) => {
            if (result) {
                result.user_type = this.state.userSignUpEnterForm.user_type;
                this.callSocialLogin(result);
            } else {
                Helper.globalLoader.hideLoader();
            }
        })
    }


    appleSignIn = (result) => {
        if (!result.user) return
        try {
            let formdata = {
                device_id: Helper.device_id,
                device_type: Helper.device_type,
                social_id: result.user,
                social_type: 'APPLE',
                name: '',
                email: '',
                profile_picture: '',
                user_type: this.state.userSignUpEnterForm.user_type
            }

            if (result.email) {
                formdata.email = result.email;
            }
            if (result.fullName && result.fullName.givenName) {
                formdata.name = result.fullName.givenName;
            }

            Helper.globalLoader.showLoader();
            this.callSocialLogin(formdata);

        } catch (error) {
        }
    }

    callSocialLogin = (result) => {
        result.lan = LanguagesIndex.MyLanguage;
        ApiCall.postMethod(Constants.user_social, JSON.stringify(result), Constants.APIPost,).then(
            (response) => {
                Keyboard.dismiss()
                Helper.globalLoader.hideLoader();
                if (response.status == Constants.TRUE) {
                    Helper.token = response.token
                    AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                    AsyncStorageHelper.setData("token", response.token)
                    Helper.user_data = response.data
                    LanguagesIndex.MyLanguage = response.data.default_lang
                    AsyncStorageHelper.setData("lan", LanguagesIndex.MyLanguage)

                    if (response.data.user_type == 'HAVEOMY') {
                        Helper.navRef.switchNavigation('2');
                    } else if (response.data.user_type == 'BEOMY') {
                        Helper.navRef.switchNavigation('3');
                    } else if (response.data.user_type == 'ENTERPRISE') {
                        Helper.navRef.switchNavigation('1');
                    }

                } else {
                    Helper.globalLoader.hideLoader();
                    Helper.showToast(response.message)
                }
            }).catch(err => {
                Helper.globalLoader.hideLoader();
            })
    }

    termsCondition(value) {
        if (value == 1) {
            handleNavigation({ type: 'push', page: 'TermsAndCondition', navigation: this.props.navigation })
        } else {
            handleNavigation({ type: 'push', page: 'PrivacyPolicy', navigation: this.props.navigation })
        }
    }

    render() {
        return (
            <View style={styles.safe_area_view}>
                {this.state.modalVisible ? this.postalModal() : null}
                {this.state.modalVisibleCity ? this.cityModal() : null}
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>

                    <View>
                        <Image source={images.Sign_up_178} resizeMode={'cover'}
                            style={{ height: DeviceH / 3.5, width: DeviceW }} />
                        <View style={styles.welcome_text_view}>
                            <Text style={styles.welcome_text}>{LanguagesIndex.translate('Welcome!')}</Text>
                        </View>

                        <View style={styles.user_pro_img_view}>
                            <Image
                                resizeMode={'cover'}
                                source={this.state.userSignUpEnterForm.profile_picture ? { uri: this.state.userSignUpEnterForm.profile_picture } : images.user_box}
                                style={styles.user_img} />

                            <TouchableOpacity
                                onPress={() => { this.chooseImage() }}
                                style={styles.camera_img_touch}>
                                <Image resizeMode={'cover'} source={images.user_camera} style={styles.camera_img} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.input_parent_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('NameOfCompany')}
                            placeholderTextColor={Colors.warmGrey}
                            setFocus={() => { this.name.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(business_name) => this.setValues('business_name', business_name)}
                            value={this.state.userSignUpEnterForm.business_name}
                        />

                        <IconInput

                            placeholder={LanguagesIndex.translate('ManagerName')}
                            placeholderTextColor={Colors.warmGrey}
                            getFocus={(input) => { this.name = input }}
                            setFocus={(input) => { this.mobile_number.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(name) => this.setValues('name', name)}
                            value={this.state.userSignUpEnterForm.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            placeholderTextColor={Colors.warmGrey}
                            getFocus={(input) => { this.mobile_number = input }}
                            setFocus={(input) => { this.email.focus(); }}
                            returnKeyType="next"
                            keyboardType={'number-pad'}
                            onChangeText={(mobile_number) => this.setValues('mobile_number', mobile_number)}
                            value={this.state.userSignUpEnterForm.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('email')}
                            placeholderTextColor={Colors.warmGrey}
                            getFocus={(input) => { this.email = input }}
                            setFocus={(input) => { this.street_no.focus(); }}
                            returnKeyType="next"
                            keyboardType={'email-address'}
                            onChangeText={(email) => this.setValues('email', email)}
                            value={this.state.userSignUpEnterForm.email}
                        />
 
                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            placeholderTextColor={Colors.warmGrey}
                            getFocus={(input) => { this.street_no = input }}
                            setFocus={(input) => { this.postcode.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(street_no) => this.setValues('street_no', street_no)}
                            value={this.state.userSignUpEnterForm.street_no}
                        />

                        <View style={styles.post_city_parent_view}>
                            <View style={styles.post_view}>
                            <TouchableOpacity 
                                onPress={() => this.setModalVisible(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userSignUpEnterForm.postcode ? this.state.userSignUpEnterForm.postcode : LanguagesIndex.translate('Post')}</Text>

                                </TouchableOpacity>
                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    placeholderTextColor={Colors.warmGrey}
                                    getFocus={(input) => { this.postcode = input }}
                                    setFocus={(input) => { this.city.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'number-pad'}
                                    onChangeText={(postcode) => this.setValues('postcode', postcode)}
                                    value={this.state.userSignUpEnterForm.postcode}
                                /> */}
                            </View>

                            <View style={styles.city_view}>

                                <View style={styles.post_view}>
                                    {/* <IconInput
                                        placeholder={LanguagesIndex.translate('City')}
                                        placeholderTextColor={Colors.warmGrey}
                                        getFocus={(input) => { this.city = input }}
                                        setFocus={(input) => { this.password.focus(); }}
                                        returnKeyType="next"
                                        keyboardType={'default'}
                                        onChangeText={(city) => this.setValues('city', city)}
                                        value={this.state.userSignUpEnterForm.city}
                                    /> */}
                                    <TouchableOpacity 
                                onPress={() => this.setModalVisibleCity(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userSignUpEnterForm.city ? this.state.userSignUpEnterForm.city : LanguagesIndex.translate('City')}</Text>

                                </TouchableOpacity>
                                </View>



                                {/* <View
                                    style={{
                                        marginVertical: 10,
                                        borderRadius: 15,
                                        padding: 15,
                                        backgroundColor: Colors.whiteThree,
                                    }}>
                                    <Text numberOfLines={1} style={{
                                        fontSize: fonts.fontSize14,
                                        color: Colors.warmGrey,
                                        fontFamily: fonts.RoBoToMedium_1,
                                    }}>{this.state.userSignUpEnterForm.city ? this.state.userSignUpEnterForm.city : LanguagesIndex.translate('City')}</Text>
                                </View> */}
                            </View>
                        </View>

                        <IconInput
                            placeholder={LanguagesIndex.translate('Password')}
                            placeholderTextColor={Colors.warmGrey}
                            secureTextEntry={true}
                            getFocus={(input) => { this.password = input }}
                            setFocus={(input) => { this.confirm_password.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(password) => this.setValues('password', password)}
                            value={this.state.userSignUpEnterForm.password}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('ConfirmPassword')}
                            placeholderTextColor={Colors.warmGrey}
                            secureTextEntry={true}
                            getFocus={(input) => { this.confirm_password = input }}
                            setFocus={() => { }}
                            returnKeyType="done"
                            keyboardType={'default'}
                            onChangeText={(confirm_password) => this.setValues('confirm_password', confirm_password)}
                            value={this.state.userSignUpEnterForm.confirm_password}
                        />
                    </View>

                    <View
                        style={styles.check_box_text_view}>
                        <TouchableOpacity
                            onPress={() => { this.onChangeCheckedBox(); }}
                            hitSlop={{ top: 20, left: 20, right: 20, bottom: 20 }}
                            style={styles.check_box_touch} >
                            <Image resizeMode={'cover'} source={this.state.CheckedBox ? images.check : images.unchecked} style={styles.box_check_img} />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row',flexWrap:'wrap', alignItems: 'flex-start', marginLeft: 10, }}>
                            <Text style={styles.terms_text}>{LanguagesIndex.translate('IAgreeTermsPrivacyPolicy')} </Text>
                            <Text onPress={() => this.termsCondition(1)} style={[styles.terms_text, { color: Colors.azul }]}>{LanguagesIndex.translate('Terms&Conditions')} </Text>
                            <Text style={styles.terms_text}>{LanguagesIndex.translate('and')} </Text>
                            <Text onPress={() => this.termsCondition(2)} style={[styles.terms_text, { color: Colors.azul }]}>{LanguagesIndex.translate('PrivacyPolicy')}</Text>
                        </View>
                    </View>

                    <View style={styles.sign_up_btn}>
                        <GButton
                            Text={LanguagesIndex.translate('SIGN_UP_PAGE')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.goToEnterpriseSingUp() }}
                        />
                    </View>

                    <View style={styles.or_view}>
                        <View style={styles.or_line_view}></View>
                        <Text style={styles.or_text}>{LanguagesIndex.translate('or')}</Text>
                        <View style={styles.or_line_view}></View>
                    </View>

                    <View style={styles.social_btn_view}>
                        <SocialButton
                            facebookSignIn={this.gotoFacebookSocialSide}
                            appleSignIn={this.appleSignIn}
                        />
                    </View>
 
                    <TouchableOpacity
                        onPress={() => { this.goToLogin() }} style={styles.already_account_touch}>
                        <Text style={styles.already_text}>{LanguagesIndex.translate('alreadyAccount')}{' '}<Text style={styles.login_text}>{LanguagesIndex.translate('log_in')}</Text></Text>
                    </TouchableOpacity>
                </KeyboardScroll> 
 

            </View>
        )
    }

};





