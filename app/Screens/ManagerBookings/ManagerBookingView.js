import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native';
import styles from './BookingUpComingScreenStyles';
import { images } from '../../Assets/imagesUrl';
import Helper from '../../Lib/Helper';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';
import { handleNavigation } from '../../navigation/Navigation';

export default class MangerBookingView extends Component {
    constructor(props) {
        super(props);
        this.state = {
 
        }
    } 
    
    gotToActivityDetail() {
        handleNavigation({ type: 'push', page: 'ActivityDetailScreen', navigation: this.props.navigation,passProps:{
            booking_id:this.props.order.booking_services[0].booking_id,userType:"homeCare"
        } })
    }

    render() {
        let { order, type } = this.props; 
        return (
            <View style={styles.history_view}>
                <TouchableOpacity onPress={() => this.gotToActivityDetail()}>
                    <View style={styles.img_calender_location_list_view}>
                        <View style={styles.flex_img_view}>
                            <Image source={{uri: order.booking_services[0].activity.icon}} resizeMode={'contain'} style={styles.history_dog_img} />
                        </View>

                        <View style={styles.flex_text_view}>
                            <Text style={styles.dog_title}>
                                {order.booking_services[0].activity.name}
                                </Text>
                        </View>
                    </View>

                    <View style={styles.img_calender_location_list_view}>
                        <View style={styles.flex_img_view}>
                            <Image source={images.calendar_date_ic} resizeMode={'contain'} style={styles.history_calender_img} />
                        </View>

                        <View style={styles.flex_text_view}>
                            <Text style={styles.history_date_title}>{
                             Helper.formatDateUtcToLocal(order.booking_date,order.booking_time,'')

                            // Helper.formatDate(order.booking_date, "DD/MM/YYYY")} | {order.booking_time
                        }</Text>
                        </View>
                    </View>

                    <View style={styles.img_calender_location_list_view}>
                        <View style={styles.flex_img_view}>
                            <Image source={images.location_pin_ic} resizeMode={'contain'} style={styles.history_location_img} />
                        </View>

                        <View style={styles.flex_text_view}>
                            <Text numberOfLines={2} style={styles.history_address}>{order.location}</Text>
                        </View>
                        {/* <View style={{ flex: 3.4, alignItems: 'flex-end' }}>
                        <Text style={{ color: Helper.getBookingStatus(order.booking_services[0].user_booking[0].status).color, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToRegular_1, textTransform: 'capitalize' }}>{Helper.getBookingStatus(order.booking_services[0].user_booking[0].status).value} {order.booking_services[0].user_booking[0].status == 'CANCEL' && order.booking_services[0].user_booking[0].cancelled_by_name ? `${LanguagesIndex.translate('by')} ${order.booking_services[0].user_booking[0].cancelled_by_name}` : null}</Text>
                        </View>

                        <View>
                            <Text style={[styles.completed_text, { color: Helper.getBookingStatus(order.status).color }]}>{Helper.getBookingStatus(order.status).value} {order.status == 'CANCEL' && order.cancelled_by_name ? `${LanguagesIndex.translate('by')} ${arrBookingDetails.cancelled_by_name}` : null}</Text>
                        </View> */}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
