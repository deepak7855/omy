import { StyleSheet } from 'react-native';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/fonts';



export default RateExperienceStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1, backgroundColor: Colors.white
    },
    mainViewRatingCss: { padding: 15 },
    ratingNameCss: { color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToRegular_1 },
    ratingMainView: { flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between' },
    bgCss: { backgroundColor: Colors.cerulean, height: 165, borderBottomLeftRadius: 25, borderBottomRightRadius: 25 },
    profileViewImgCss: { alignSelf: 'center', borderWidth: 1, borderRadius: 20, borderColor: Colors.white, },
    profileImgCss: { height: 82, width: 82,borderRadius:20 },
    userTextCss: { textAlign: 'center', marginTop: 10, color: Colors.white, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1 },
    lineBottomCss: { backgroundColor: Colors.white, height: 1, alignSelf: 'center', width: 50, marginVertical: 10 },
    docIconCss: { height: 12, width: 12 },
    dogTextWhite: { marginLeft: 10, color: Colors.white, fontSize: fonts.fontSize10, fontFamily: fonts.RoBoToRegular_1 },
    lineBorderCss: { borderBottomWidth: 8, borderBottomColor: '#f8f8f8', padding: 15 },
    textCss: { color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1 },
    selectRadioCss: { height: 17, width: 17 },
    pragrapCss: { color: Colors.warmGrey, fontSize: fonts.fontSize12, fontFamily: fonts.RoBoToRegular_1, marginLeft: 5 },
    _textInputCss: { 
      borderRadius: 8, borderWidth: 1, 
      // borderColor: '#dbdbdb',
       marginTop: 10
       },
    samTextCss: { color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.RoBoToMedium_1 },
    login_btn_view: { marginVertical: 10, marginTop: 30, marginHorizontal: 15 },

    modal_color_view:{
      flex: 1,
      alignItems: 'center',
      backgroundColor: Colors.cerulean,
      padding: 100, 
      borderBottomRightRadius: 30, 
      borderBottomLeftRadius: 30,
    },
    thank_you_img_view:{
      alignItems: 'center',
    },
    thank_img:{
      width: 114, 
      height: 114
    },
    thank_you_text_view:{
      alignItems: 'center', 
      marginTop: 45
    },
    thank_you_text:{
      fontFamily: fonts.RoBoToBold_1, 
      fontSize: fonts.fontSize24, 
      color: Colors.white
    },
    line_view_parent:{
      alignSelf: 'center',
       marginTop: 30
    },
    line_view:{
      backgroundColor: Colors.babyBlue, 
      height: 1, 
      width: 70
    },
    text_view:{
      alignSelf: 'center', 
      justifyContent: 'center', 
      marginTop: 30
    },
    soon_text:{
      textAlign: 'center', 
      color: Colors.white, 
      fontSize: fonts.fontSize13, 
      fontFamily: fonts.RoBoToRegular_1, 
      lineHeight: 24
    },
    btn_view: {
      marginVertical: 10,
      marginTop: 35,
      marginHorizontal: 16
    },
    
});