import React, { Component } from 'react';
import { View, Text, Image, TextInput, Modal } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { AirbnbRating } from 'react-native-ratings';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import { GButton } from '../../Comman/GButton';
import styles from './RateExperienceStyles';
import { handleNavigation } from '../../navigation/Navigation';
import LanguagesIndex from '../../Languages';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants } from '../../Api';
import ImageLoadView from '../../Lib/ImageLoadView';
import KeyboardScroll from '../../Comman/KeyboardScroll';

export default class RateExperienceScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item_data: this.props.route.params.studentData,
            activityType: this.props.route.params.activityType ? this.props.route.params.activityType : "",
            studentRatingForm: {
                note: '',
            },
            overall_exp: '',
            ratingCountOverall: 0,

            punctual: '',
            ratingCountpunctual: 0,

            friendly: '',
            ratingCountfriendly: 0,

            performance: '',
            ratingCountperformance: 0,

            value: '',
            ratingCountvalue: 0,

            recommend: '',
            isYes: false,
            isNo: false,
            modalVisible: false,
        };
        AppHeader({
            ...this.props.navigation, leftTitle: LanguagesIndex.translate('RateYourExperience'),
        })
    }

    goToThankYou(visible) {
        this.setState({ modalVisible: visible });
    }

    componentDidMount() {
    }

    goToHomeScreen() {
        handleNavigation({ type: 'push', page: 'BottomTab', navigation: this.props.navigation })
    }

    changeClick = (v) => {
        this.setState({ recommend: v })
    }

    setValues(key, value) {
        let studentRatingForm = { ...this.state.studentRatingForm }
        studentRatingForm[key] = value
        this.setState({ studentRatingForm })
    }

    goBack() {
        handleNavigation({ type: 'pop', navigation: this.props.navigation })
    }

    getRatingsApiCall = () => {
        if (this.state.ratingCountOverall == "") {
            Helper.showToast(LanguagesIndex.translate('Pleaserateoverallexperience'))
            return
        } else if (this.state.ratingCountpunctual == "") {
            Helper.showToast(LanguagesIndex.translate('Pleaseratepunctual'))
            return
        } else if (this.state.ratingCountfriendly == "") {
            Helper.showToast(LanguagesIndex.translate('Pleaseratefriendly'))
            return
        } else if (this.state.ratingCountperformance == "") {
            Helper.showToast(LanguagesIndex.translate('Pleaserateperformance'))
            return
        } else if (this.state.ratingCountvalue == "") {
            Helper.showToast(LanguagesIndex.translate('Pleaseratevalue'))
            return
        } else if (this.state.recommend == "") {
            Helper.showToast(LanguagesIndex.translate('Wouldyourecommendyesorno'))
            return
        } else if (this.state.studentRatingForm.note == "") {
            Helper.showToast(LanguagesIndex.translate('Entershortnote'))
            return
        }

        let data = {
            student_id: this.state.item_data.user_id,
            booking_service_id: this.state.item_data.booking_service_id,
            booking_id: this.state.item_data.booking_id,
            overall_exp: this.state.ratingCountOverall,
            punctual: this.state.ratingCountpunctual,
            friendly: this.state.ratingCountfriendly,
            performance: this.state.ratingCountperformance,
            value: this.state.ratingCountvalue,
            recommend: this.state.recommend,
            note: this.state.studentRatingForm.note,
        }
        Helper.globalLoader.showLoader();
        ApiCall.postMethodWithHeader(Constants.ratings, JSON.stringify(data), Constants.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == Constants.TRUE) {
                this.goBack()
                Helper.showToast(response.message)
            }
            else {
                Helper.showToast(response.message)
            }
        })
    }

    renderRecommendOption = (recommend_value, isChosen,) => {
        let chooseRecommend = () => {
            if (recommend_value === "Yes") {
                this.setState({
                    recommend: '1',
                    isYes: true,
                    isNo: false,
                });
            } else {
                this.setState({
                    recommend: '0',
                    isYes: false,
                    isNo: true,
                });
            }
        };


        return (
            <View style={{ marginRight: 40 }} >
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={chooseRecommend} >
                    <Image
                        source={isChosen ? images.radio_btn_selected : images.radio_btn_un_selected}
                        style={styles.selectRadioCss}
                    />
                    <Text style={styles.pragrapCss}>{recommend_value}</Text>
                </TouchableOpacity>
            </View>
        );
    };


    render() {
        let name = ''
        {
            this.state.activityType.map((item, index) => (
                name = name ? name + ', ' + item.activity.name : item.activity.name
            ))
        }
        let icon = ''
        {
            this.state.activityType.map((item, index) => (
                icon = icon ? icon + ', ' + item.activity.icon : item.activity.icon
            ))
            
        }

        return (
            <View style={styles.safe_area_view}>
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.bgCss}>
                        <View style={styles.profileViewImgCss}>
                            <ImageLoadView
                                resizeMode={'cover'}
                                source={this.state.item_data.user.profile_picture ? { uri: this.state.item_data.user.profile_picture } : images.default}
                                style={styles.profileImgCss} />
                        </View>
                        <Text style={styles.userTextCss}>{this.state.item_data.user.name}</Text>
                        <View style={styles.lineBottomCss} />
                        <View style={{ alignSelf: 'center', flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={{ uri: icon }}
                                style={[styles.docIconCss, { tintColor: Colors.white }]} />
                            <Text style={[styles.dogTextWhite, { textTransform: 'capitalize' }]}>{name}</Text>

                        </View>
                    </View>
                    <View style={styles.lineBorderCss}>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('OverallExperience')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    reviews={false}
                                    fontSize={10}
                                    size={18.8}
                                    reviewSize={0}
                                    defaultRating={this.state.overall_exp}
                                    onFinishRating={(overall_exp) => this.setState({ ratingCountOverall: overall_exp })}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Punctual')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    reviews={false}
                                    fontSize={10}
                                    size={18.8}
                                    reviewSize={0}
                                    defaultRating={this.state.punctual}
                                    onFinishRating={(punctual) => this.setState({ ratingCountpunctual: punctual })}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Friendly')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    reviews={false}
                                    fontSize={10}
                                    size={18.8}
                                    reviewSize={0}
                                    defaultRating={this.state.friendly}
                                    onFinishRating={(friendly) => this.setState({ ratingCountfriendly: friendly })}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Performance')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    reviews={false}
                                    fontSize={10}
                                    size={18.8}
                                    reviewSize={0}
                                    defaultRating={this.state.performance}
                                    onFinishRating={(performance) => this.setState({ ratingCountperformance: performance })}
                                />
                            </View>
                        </View>
                        <View style={styles.ratingMainView}>
                            <Text style={styles.ratingNameCss}>{LanguagesIndex.translate('Value')}</Text>
                            <View>
                                <AirbnbRating
                                    count={5}
                                    reviews={false}
                                    defaultRating={1}
                                    fontSize={10}
                                    size={18.8}
                                    reviewSize={0}
                                    defaultRating={this.state.value}
                                    onFinishRating={(value) => this.setState({ ratingCountvalue: value })}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={styles.lineBorderCss}>
                        <Text style={styles.textCss}>{LanguagesIndex.translate('WouldYouRecommend')}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>

                            {this.renderRecommendOption(LanguagesIndex.translate('Yes'), this.state.isYes,)}
                            {this.renderRecommendOption(LanguagesIndex.translate('No'), this.state.isNo,)}

                        </View>
                    </View>
                    <View style={{ padding: 15 }}>
                        <Text style={styles.samTextCss}>{LanguagesIndex.translate('Note')}</Text>
                        <View style={styles._textInputCss}>
                            <TextInput
                                placeholder={LanguagesIndex.translate('WriteHere')}
                                multiline={true}
                                returnKeyType={'done'}
                                placeholderTextColor={Colors.warmGrey}
                                onChangeText={(note) => this.setValues('note', note)}
                                value={this.state.studentRatingForm.note}
                                style={{ height: 75, textAlignVertical: 'top', paddingHorizontal: 10, }}
                            />
                        </View>
                    </View>
                    <View style={styles.login_btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('SUBMIT')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            // onPress={() => { this.goToThankYou(true) }}
                            onPress={() => { this.getRatingsApiCall() }}
                        />
                    </View>
                </KeyboardScroll>

                <Modal animationType={"slide"} transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>

                        <View style={styles.thank_you_img_view}>
                            <Image source={images.thankyou_check_ic} resizeMode={'contain'} style={styles.thank_img} />
                        </View>

                        <View style={styles.thank_you_text_view}>
                            <Text style={styles.thank_you_text}>Thank You</Text>
                        </View>

                        <View style={styles.line_view_parent}>
                            <View style={styles.line_view}></View>
                        </View>

                        <View style={styles.text_view}>
                            <Text style={styles.soon_text}>Your request has been sent, We will notify you soon.</Text>
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Go To Home'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.goToHomeScreen(!this.state.modalVisible) }}
                        />
                    </View>
                </Modal>


            </View>
        );
    }
}
