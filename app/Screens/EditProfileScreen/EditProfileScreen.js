import React from 'react';
import {
    View, Image, StyleSheet, Text, TouchableOpacity, Keyboard,
    SafeAreaView, DeviceEventEmitter,Modal,FlatList
} from 'react-native';
import styles from './EditProfileScreenStyles';
import { GButton } from '../../Comman/GButton';
import Colors from '../../Assets/Colors';
import { images } from '../../Assets/imagesUrl';
import IconInput from '../../Comman/GInput';
import fonts from '../../Assets/fonts';
import AppHeader from '../../Comman/AppHeader';
import KeyboardScroll from '../../Comman/KeyboardScroll';
import Helper from '../../Lib/Helper';
import { ApiCall, Constants, AsyncStorageHelper } from '../../../app/Api';
import LanguagesIndex from '../../Languages';
import { handleNavigation } from '../../navigation/Navigation';
import ImageLoadView from '../../Lib/ImageLoadView';
import CameraController from '../../Lib/CameraController';

export default class EditProfileScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatarSource: '',
            userManagerEditForm: {
                profile_picture: Helper.user_data.profile_picture ? Helper.user_data.profile_picture : '',
                business_name: Helper.user_data.business_name ? Helper.user_data.business_name : '',
                name: Helper.user_data.name ? Helper.user_data.name : '',
                mobile_number: Helper.user_data.mobile_number ? Helper.user_data.mobile_number : '',
                email: Helper.user_data.email ? Helper.user_data.email : '',
                // location: Helper.user_data.location ? Helper.user_data.location : '',
                lat: Helper.user_data.lat ? Helper.user_data.lat : '',
                lng: Helper.user_data.lng ? Helper.user_data.lng : '',
                street_no: Helper.user_data.street_no ? Helper.user_data.street_no : '',
                postcode: Helper.user_data.postcode ? Helper.user_data.postcode : '',
                city: Helper.user_data.city ? Helper.user_data.city : '',
                validators: {
                    business_name: { required: true, minLength: 2, maxLength: 100 },
                    name: { required: true, minLength: 2, maxLength: 45 },
                    mobile_number: { required: true, minLengthDigit: 9, maxLengthDigit: 9 },
                    email: { required: true, email: true },
                    // location: { required: true, },
                    street_no: { required: true, minLength: 1, maxLength: 100 },
                    postcode: { required: true, minLengthDigit: 4, maxLengthDigit: 10 },
                    city: { required: true, minLength: 1, maxLength: 45 },
                }
                ,
                modalVisible:false,
                postalData:[],
                modalVisibleCity:false
            }

        }
        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('EditProfile') })
    }


    setValues(key, value) {
        let userManagerEditForm = { ...this.state.userManagerEditForm }
        // if (key == 'city') { value = value.replace(/[^a-zA-Z ]/g, ''); }
        // else { value = value; }
        if (key == 'mobile_number') {
            value = value.replace(/\D/g, '');
        }
        userManagerEditForm[key] = value
        this.setState({ userManagerEditForm })
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    getPostalsCode = () => {
        let data = {
            postcode:this.state.search_p
        };
        console.log('datadata',data)
        this.setState({postalData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    postalData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderList = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.setValues('postcode',item.code)
                            //this.setValues('city','')
                            
                            this.setModalVisible(false)
                            setTimeout(() => {
                                this.setValues('city','')
                                this.setState({postalData:[]})
                                this.getCity()
                            },1000)
                            
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.code}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    postalModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisible(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By Postal')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            keyboardType={'numeric'}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_p:search}, () => {
                                this.getPostalsCode()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.postalData}
                        renderItem={this._renderList}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisible(false) }}
                        />
                    </View>
                </Modal>


        )
    }


    setModalVisibleCity = (visible) => {
        this.setState({ modalVisibleCity: visible });
    }

    getCity = () => {
        let data = {
            postcode:this.state.userManagerEditForm.postcode,
            city:this.state.search_c
        };
        console.log('datadata',data)
        this.setState({cityData:[]})
        ApiCall.postMethodWithHeader(Constants.get_postcode, JSON.stringify(data), Constants.APIPost).then((response) => {
            console.log('responseresponseresponse',response)
            if (response.status == Constants.TRUE && response.data) {
                this.setState({
                    cityData:response.data
                })


            } else {
                Helper.showToast(response.message);
               
            }
        })
    }

    _renderListC = ({ item }) => {
        return (
            <View style={{width:'100%'}}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            
                            //this.setValues('postcode',item.code)
                            this.setValues('city',item.city)
                            this.setModalVisibleCity(false)
                        }}
                        style={{padding:15,borderBottomColor:Colors.warmGrey,borderBottomWidth:1}}>
                        <Text >{item.city}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    cityModal = () => {
        return (
            <Modal animationType={"slide"} transparent={true}
                    visible={this.state.modalVisibleCity}
                    onRequestClose={() => { }}>
                    <View style={styles.modal_color_view}>
                        <TouchableOpacity
                            onPress={() => this.setModalVisibleCity(false)} style={{position:'absolute',width:30,height:30,right:15,top:35}}>
                            <Image source={images.cross} resizeMode={'contain'} style={{width:35,height:35,tintColor:Colors.cerulean}} />
                        </TouchableOpacity>
                        <View style={{width:'100%'}}>
                            <IconInput
                            placeholder={LanguagesIndex.translate('Search By City')}
                            imagePath={images.search_ic}
                            height={12}
                            width={15}
                            placeholderTextColor={Colors.warmGrey}
                            returnKeyType="next"
                            onChangeText={(search) => this.setState({search_c:search}, () => {
                                this.getCity()
                            })}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                        <FlatList
                        style={{  }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.cityData}
                        renderItem={this._renderListC}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        />
                        </View>
                    </View>

                    <View style={styles.btn_view}>
                        <GButton
                            Text='Cancel'
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.setModalVisibleCity(false) }}
                        />
                    </View>
                </Modal>


        )
    }


    chooseImage = () => {
        CameraController.open((response) => {
            if (response) {
                this.UploadMediaMethod(response.path);
            }
        });
    }

    UploadMediaMethod(uri) {
        this.setValues('profile_picture', uri)
        this.setState({ avatarSource: uri })
    }

    async EditMangerUpdateProfile() {
        Keyboard.dismiss();
        let isValid = Helper.validate(this.state.userManagerEditForm);
        if (isValid) {

            Helper.globalLoader.showLoader();
            await Helper.GetAddressFromLatLong(`${this.state.userManagerEditForm.street_no} ${this.state.userManagerEditForm.city} ${this.state.userManagerEditForm.postcode}`, (resp) => {

                if (!resp) {
                    Helper.globalLoader.hideLoader();
                    return;
                }

                this.state.userManagerEditForm.lat = resp.lat;
                this.state.userManagerEditForm.lng = resp.lng;
                this.setState({})
                let data = new FormData();
                data.append('business_name', this.state.userManagerEditForm.business_name);
                data.append('name', this.state.userManagerEditForm.name);
                data.append('mobile_number', this.state.userManagerEditForm.mobile_number);
                data.append('email', this.state.userManagerEditForm.email);
                data.append('lat', this.state.userManagerEditForm.lat);
                data.append('lng', this.state.userManagerEditForm.lng);
                data.append('street_no', this.state.userManagerEditForm.street_no);
                data.append('postcode', this.state.userManagerEditForm.postcode);
                data.append('city', this.state.userManagerEditForm.city);
                if (this.state.avatarSource) {
                    data.append('profile_picture', {
                        uri: this.state.avatarSource,
                        name: 'test.jpg',
                        type: 'image/jpeg'
                    });
                }
                ApiCall.postMethodWithHeader(Constants.update_profile, data, Constants.APIImageUploadAndroid).then((response) => {
                    Helper.globalLoader.hideLoader();
                    if (response.status == Constants.TRUE) {

                        Helper.showToast(response.message)
                        AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                        Helper.user_data = response.data
                        DeviceEventEmitter.emit(Constants.USER_DETAILS, response.data);
                        setTimeout(() => {
                            handleNavigation({ type: 'pop', navigation: this.props.navigation });
                        }, 200);
                    }
                })
            })
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>
                {this.state.modalVisible ? this.postalModal() : null}
                {this.state.modalVisibleCity ? this.cityModal() : null}
                <KeyboardScroll contentContainerStyle={{ paddingBottom: 22 }}>
                    <View style={styles.pare_profile_view}>
                        <View style={styles.blue_view}>

                        </View>
                        <View style={styles.profile_parent_view}>
                            <View style={styles.profile_img_view}>
                                <ImageLoadView
                                    resizeMode={'cover'}
                                    source={this.state.avatarSource ? { uri: this.state.avatarSource } : Helper.user_data.profile_picture ? { uri: Helper.user_data.profile_picture } : images.default}
                                    style={styles.profile_img}
                                />
                            </View>

                            <TouchableOpacity
                                onPress={() => { this.chooseImage() }}
                                style={styles.camera_img_touch}>
                                <Image resizeMode={'cover'}
                                    source={images.user_camera}
                                    style={styles.camera_img} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.input_view}>
                        <IconInput
                            placeholder={LanguagesIndex.translate('NameInstitute')}
                            setFocus={() => { this.name.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(business_name) => this.setValues('business_name', business_name)}
                            value={this.state.userManagerEditForm.business_name}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('Name')}
                            getFocus={(input) => { this.name = input }}
                            setFocus={(input) => { this.mobile_number.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(name) => this.setValues('name', name)}
                            value={this.state.userManagerEditForm.name}
                        />

                        <IconInput
                            phoneCode
                            placeholder={LanguagesIndex.translate('ContactNumber')}
                            getFocus={(input) => { this.mobile_number = input }}
                            setFocus={(input) => { this.email.focus(); }}
                            returnKeyType="next"
                            keyboardType={'number-pad'}
                            onChangeText={(mobile_number) => this.setValues('mobile_number', mobile_number)}
                            value={this.state.userManagerEditForm.mobile_number}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('email')}
                            getFocus={(input) => { this.email = input }}
                            // setFocus={(input) => { this.location.focus(); }}
                            returnKeyType="next"
                            inputedit={Helper.user_data.email ? false : true}
                            keyboardType={'email-address'}
                            onChangeText={(email) => this.setValues('email', email)}
                            value={this.state.userManagerEditForm.email}
                        />

                        <IconInput
                            placeholder={LanguagesIndex.translate('Street/No')}
                            getFocus={(input) => { this.street_no = input }}
                            setFocus={(input) => { this.postcode.focus(); }}
                            returnKeyType="next"
                            keyboardType={'default'}
                            onChangeText={(street_no) => this.setValues('street_no', street_no)}
                            value={this.state.userManagerEditForm.street_no}
                        />

                        <View style={styles.post_city_view}>
                            <View style={styles.post_view}>
                            <TouchableOpacity 
                                onPress={() => this.setModalVisible(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userManagerEditForm.postcode ? this.state.userManagerEditForm.postcode : LanguagesIndex.translate('Post')}</Text>

                                </TouchableOpacity>
                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('Post')}
                                    getFocus={(input) => { this.postcode = input }}
                                    setFocus={(input) => { this.city.focus(); }}
                                    returnKeyType="next"
                                    keyboardType={'number-pad'}
                                    onChangeText={(postcode) => this.setValues('postcode', postcode)}
                                    value={this.state.userManagerEditForm.postcode}
                                /> */}
                            </View>

                            <View style={styles.city_view}>

                                {/* <IconInput
                                    placeholder={LanguagesIndex.translate('City')}
                                    getFocus={(input) => { this.city = input }}
                                    returnKeyType="done"
                                    keyboardType={'default'}
                                    onChangeText={(city) => this.setValues('city', city)}
                                    value={this.state.userManagerEditForm.city}
                                /> */}
                                <TouchableOpacity 
                                onPress={() => this.setModalVisibleCity(true)}
                                style={{backgroundColor: Colors.whiteThree,height:50,borderRadius:15,justifyContent:'center',paddingHorizontal:15}}>
                                <Text style={{color: Colors.warmGrey}}>{this.state.userManagerEditForm.city ? this.state.userManagerEditForm.city : LanguagesIndex.translate('City')}</Text>

                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={styles.update_btn_view}>
                        <GButton
                            Text={LanguagesIndex.translate('UPDATE')}
                            width={'100%'}
                            height={50}
                            borderRadius={10}
                            onPress={() => { this.EditMangerUpdateProfile() }}
                        />
                    </View>

                </KeyboardScroll>
            </SafeAreaView>
        )
    }

};
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: fonts.fontSize14,
        height: 50,
        color: Colors.warmGrey,
        fontFamily: fonts.RoBoToMedium_1,
        width: '100%',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 10,

        marginLeft: 10
    },
    inputAndroid: {
        fontSize: fonts.fontSize14,
        height: 50,
        width: '100%',
        color: Colors.warmGrey,
        marginRight: 20,
        marginLeft: 8,
        marginBottom: 10,
        fontFamily: fonts.RoBoToMedium_1,

    },
});





