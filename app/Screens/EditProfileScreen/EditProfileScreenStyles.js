import { StyleSheet } from 'react-native';
import fonts from '../../Assets/fonts';
import Colors from '../../Assets/Colors';


export default EditProfileScreenStyles = StyleSheet.create({
    safe_area_view: {
        flex: 1, 
        backgroundColor: Colors.white
    },
    pare_profile_view:{
        height: 120, 
        width: '100%'
    },
    blue_view:{
        backgroundColor: Colors.cerulean, 
        height: 50, 
        width: '100%', 
        borderBottomLeftRadius: 25, 
        borderBottomRightRadius: 25
    },
    profile_parent_view: {
        alignSelf: 'center',
        marginTop: -40,
        height: 95,
        width: 90
    },
    profile_img_view: {
        height: 82,
        width: 82,
        borderRadius: 20,
    },
    profile_img: {
        height: '100%',
        width: '100%',
        borderRadius: 20,
    },
    camera_img_touch: {
        height: 26,
        width: 26,
        position: 'absolute',
        bottom: 0,
        left: 30
    },
    camera_img: {
        height: '100%',
        width: '100%',
    },
    input_view: {
        marginVertical: 10,
        marginTop: 10,
        marginHorizontal: 16
    },
    post_city_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    post_view: {
        flex: 4,
        marginRight: 5
    },
    city_view: {
        flex: 4,
        marginLeft: 5
    },
    update_btn_view: {
        marginVertical: 10,
        marginTop: 33,
        marginHorizontal: 16
    },
    
   modal_color_view:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.white,
    paddingVertical: 100, 
    paddingHorizontal:40,
    //borderBottomRightRadius: 30, 
    //borderBottomLeftRadius: 30,
  },

});