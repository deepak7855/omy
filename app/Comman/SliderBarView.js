import React, { PureComponent } from 'react'
import { Text, View, StyleSheet,Image, Dimensions } from 'react-native'
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Colors from '../Assets/Colors';
import fonts from '../Assets/fonts';
import LanguagesIndex from '../Languages';
import { images } from '../Assets/imagesUrl';

const ScreenWidth = Dimensions.get('screen').width;

const CustomSliderMarkerLeft = (props) => {
    return (
        <View style={{ width: 20, height: 20, borderRadius: 20 / 2, borderColor: Colors.cerulean, borderWidth: 2, backgroundColor: Colors.white }} />
    );
}
const CustomSliderMarkerRight = (props) => {
    return (
        <View style={{ justifyContent: "center", alignItems: "center", width: 20, height: 20, borderRadius: 20 / 2, borderColor: Colors.cerulean, borderWidth: 2 }}>
            <View style={{ width: 10, height: 10, borderRadius: 10 / 2, backgroundColor: Colors.cerulean }} />
        </View>
    );
}


export default class SliderBarView extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            rangeValue: this.props.value
        }
    }

    valuesChange = (values) => {
        this.setState({ rangeValue: values });
    }

    render() {
        let { value, label, min, max, type } = this.props;

        return (
            <View>
                <View style={{ marginHorizontal: 18 }}>
                    <Text style={[styles.text_of_css, { marginBottom: 0 }]}>{label}</Text>
                    {type == 'distance' ?
                        <Text style={[styles.lang_select_text, { marginLeft: 0, marginTop: 10 }]}>{this.state.rangeValue[0]}{LanguagesIndex.translate('Km')} - {this.state.rangeValue[1]}{LanguagesIndex.translate('Km')}</Text>
                        : type == 'price' ?
                            <Text style={[styles.lang_select_text, { marginLeft: 0, marginTop: 10 }]}>CHF {this.state.rangeValue[0]} - CHF {this.state.rangeValue[1]}</Text>
                            : null
                    }
                </View>
                <View style={{ marginLeft: 23 }}>
                    <MultiSlider
                        selectedStyle={{ backgroundColor: Colors.cerulean }}
                        unselectedStyle={{ backgroundColor: Colors.warmGrey }}
                        markerStyle={{ backgroundColor: '#E2003F', height: 20, width: 20 }}
                        trackStyle={{ borderRadius: 7, height: 3 }}
                        values={[value[0], value[1]]}
                        sliderLength={ScreenWidth / 1.12}
                        touchDimensions={{ height: 100, width: 100, borderRadius: 15 }}
                        min={min}
                        max={max}
                        onValuesChange={(values) => { this.valuesChange(values); }}
                        onValuesChangeFinish={(values) => this.props.onChangeFinish(values)}
                        isMarkersSeparated={true}
                        customMarkerLeft={(e) => {
                            return (
                                <CustomSliderMarkerLeft currentValue={e.currentValue} />
                            )
                        }}
                        customMarkerRight={(e) => {
                            return (
                                <CustomSliderMarkerRight />
                            )
                        }}
                        step={1}
                    />
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({

    text_of_css: {
        color: Colors.black,
        fontSize: fonts.fontSize14,
        fontFamily: fonts.RoBoToMedium_1,
        marginBottom: 10
    },
    lang_select_text: {
        marginLeft: 10,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1,
        color: Colors.warmGreyThree
    },
});