import { LoginManager, AccessToken } from 'react-native-fbsdk';
import Helper from '../Lib/Helper';
import { Platform } from 'react-native';
// import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';

export function configureGoogleLogin() {
    try {
        GoogleSignin.configure({
            webClientId: Helper.googleClientId,
            offlineAccess: true,
            hostedDomain: '',
            loginHint: '',
            forceConsentPrompt: true,
            accountName: '',
        });
    } catch ({ message }) {
    }
}

export function googleLogin(cb) {
    try {
        GoogleSignin.hasPlayServices().then(() => {
            GoogleSignin.signOut().then(() => {
                GoogleSignin.signIn().then((result) => {
                    let form = { 
                        device_type: Helper.device_type,
                        device_id: Helper.device_id,
                        social_type: 'GOOGLE',
                        social_id: result.user.id,
                        name: '',
                        email: '',
                        profile_image: '',
                    }

                    if (result.user.email) {
                        form.email = result.user.email;
                    }
                    if (result.user.name) {
                        form.name = result.user.name;
                    }
                    if (result.user.photo) {
                        form.profile_image = result.user.photo;
                    }
                    cb(form)
                }).catch((error) => {
                    cb(false)

                })
            })
        })

    } catch (error) {
        cb(false);
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
        } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
        } else {
            // some other error happened
        }
    }
}

export function FacebookLogin(cb) {
    LoginManager.logOut();
    try {
        LoginManager.setLoginBehavior(Platform.OS == 'android' ? 'web_only' : 'browser');
        LoginManager.logInWithPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    cb(false);
                } else {
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            let token = data.accessToken.toString();
                            fetch('https://graph.facebook.com/me?fields=id,email,name,picture.width(720).height(720).as(picture),friends&access_token=' + token)
                                .then((response) => response.json())
                                .then((json) => {

                                    let form = { 
                                        device_type: Helper.device_type,
                                        device_id: Helper.device_id,
                                        social_id: json.id,
                                        social_type: 'FACEBOOK',
                                        name: json.name,
                                        email: '',
                                        profile_picture: '',
                                    }

                                    if (json.email) {
                                        form.email = json.email;
                                    }
                                    if (json.name) {
                                        form.name = json.name;
                                    }
                                    if (json.picture.data.url) {
                                        form.profile_picture = json.picture.data.url;
                                    }
                                    cb(form);

                                })
                                .catch((err) => {
                                    cb(false);
                                })
                        },
                        function (error) {
                            cb(false);
                        }
                    )
                }
            },
            function (error) {
                cb(false);
            }
        );
    } catch ({ message }) {
    }
}


