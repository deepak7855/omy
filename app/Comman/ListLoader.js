import React from 'react';
import {
    View,
    ActivityIndicator,
} from 'react-native';
import Colors from '../Assets/Colors';

export default class ListLoader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{marginTop:10}}>
                <ActivityIndicator animating={true} size={this.props.size || 'large'} color={this.props.color || Colors.cerulean} />
            </View>
        );
    }
}