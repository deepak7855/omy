import React, { Component } from 'react';
import { StyleSheet, View, Text, } from 'react-native';
import fonts from '../Assets/fonts';
import Colors from '../Assets/Colors';
import LanguagesIndex from '../Languages';
//import BackgroundTimer from 'react-native-background-timer';

export default class TimeShow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            singleUserTimeOutValue: this.props.maxCountDownTime ? this.props.maxCountDownTime : 0, //600,
            singleUserTimeOut: null
        }
    }

    componentWillUnmount() {

        this.state.singleUserTimeOutValue = 0
        if (this.state.singleUserTimeOut) {
           // console.log('componentWillUnmount BackgroundTimer clear')
            // BackgroundTimer.clearInterval(this.state.singleUserTimeOut);
            clearInterval(this.state.singleUserTimeOut);
        }
    }
    componentDidMount() {

        if (this.state.singleUserTimeOut) {
            this.state.singleUserTimeOutValue = this.props.maxCountDownTime ? this.props.maxCountDownTime : 0
           // console.log('componentWillUnmount BackgroundTimer clear')
            clearInterval(this.state.singleUserTimeOut);
            // BackgroundTimer.clearInterval(this.state.singleUserTimeOut);
        }
        // this.state.singleUserTimeOut =  BackgroundTimer.setInterval(() => {
            this.state.singleUserTimeOut =  setInterval(() => {

            if (this.state.singleUserTimeOutValue <= 0) {
               // console.log('componentWillUnmount BackgroundTimer clear')

                // BackgroundTimer.clearInterval(this.state.singleUserTimeOut);
               clearInterval(this.state.singleUserTimeOut);

                this.props.timeOutComplete()
            }
            else {
                this.setState({ singleUserTimeOutValue: this.state.singleUserTimeOutValue - 1000 })
            }
        }, 1000);
    }
    
    secondsToHms(duration) {
        let seconds = Math.floor((duration / 1000) % 60);
        let minutes = Math.floor((duration / (1000 * 60)) % 60);
        let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds;
    }

    render() {
       // console.log("---------------",this.secondsToHms(this.state.singleUserTimeOutValue))
        return this.state.singleUserTimeOutValue ? (
            <View style={{ width: '100%', }}>
                {this.props.type == 'list' ?
                    <Text style={{ color: Colors.blackTwo, fontSize: fonts.fontSize16, }}>{this.secondsToHms(this.state.singleUserTimeOutValue)}</Text>
                    :
                    <Text style={{ color: Colors.blackTwo, fontSize: fonts.fontSize16, }} >{LanguagesIndex.translate('Youhave')}{" "}{this.secondsToHms(this.state.singleUserTimeOutValue)}{" "}{LanguagesIndex.translate('lefttoanswerthisrequest')}</Text>
                }
            </View>
        ) : (<View />);
    }
}