import React from 'react';
import { Text, View,TextInput, Image, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import Colors from '../Assets/Colors';
import { images } from '../Assets/imagesUrl';
import fonts from '../Assets/fonts';
import { debounce } from 'lodash';
import LanguagesIndex from '../Languages';

export default class ActivitiesRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }



    render() {
        let { item, selected } = this.props;
        return (
            <View style={styles.checkboxViewcss}>
                <TouchableOpacity style={styles._samViewTextIc}
                    onPress={() => { this.props.onSelectActivities(item.id) }}>
                    <Image source={selected ? images.check : images.unchecked}
                        style={styles.samImgCss} />
                    <Text style={styles.samGreyTextCss}>{item.name}</Text>
                </TouchableOpacity>
                <View style={styles._samViewTextIc}>

                    {/* <View style={{ height: 40, width: 125, backgroundColor: Colors.whiteThree, borderRadius: 5, }}> */}
                        <TextInput
                            style={{ borderRadius: 5,backgroundColor: Colors.whiteThree,width:125,height:40,alignItems:'center',
                            paddingLeft:5,textAlign:'left', color: Colors.warmGrey, fontSize: fonts.fontSize12,
                            fontFamily:fonts.RoBoToRegular_1 }}
                            placeholder={LanguagesIndex.translate('price')}
                            keyboardType={'number-pad'}
                            maxLength={15}
                            placeholderTextColor={Colors.warmGrey}
                            defaultValue={item.price?item.price.toString():''}
                            // onChangeText={(price) => this.setState({ price })}
                            value={item.price}
                            // onChangeText={debounce((price) => {
                            //     this.props.onAdd(item.id, price)
                            // }, 500)}
                            editable={selected}
                            onChangeText={(price)=>{
                                if (!isNaN(price) || price === ''){
                                    this.props.onAdd(item.id, price.trim())
                                }
                            }}

                        />
                    {/* </View>  */}
                </View>
            </View>
        )
    }

};

const styles = StyleSheet.create({
    checkboxViewcss: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 12
    },
    _samViewTextIc: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    samImgCss: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
        marginRight: 10
    },
    samGreyTextCss: {
        color: Colors.warmGrey,
        fontSize: fonts.fontSize12,
        fontFamily: fonts.RoBoToRegular_1
    },
    editIconCss: {
        height: 17,
        width: 17,
        resizeMode: 'contain',
        marginLeft: 10
    },


})






