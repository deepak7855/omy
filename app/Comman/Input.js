import React, { Component } from 'react';
import { View,Text, StyleSheet, Image, TextInput, TouchableOpacity, } from 'react-native';
import Colors from '../Assets/Colors';
import fonts from '../Assets/fonts';


class Input extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View style={styles.input_container}>
                <View style={styles.image_view}>
                    {this.props.showCurrency ?
                        <Text>CHF</Text>
                        :
                        <Image
                            source={this.props.imagePath}
                            resizeMode='contain'
                            style={{ height: this.props.height, width: this.props.width, }}
                        />
                    }
                </View>
                <View style={{ backgroundColor: Colors.whiteTwo, height: 40, width: 1 }}></View>
                <View style={styles.input_view}>
                    <TextInput
                        secureTextEntry={this.props.secureTextEntry}
                        placeholder={this.props.placeholder}
                        underlineColorAndroid="transparent"
                        autoCorrect={false}
                        value={this.props.value}
                        onChangeText={this.props.onChangeText}
                        keyboardType={this.props.keyboardType}
                        maxLength={this.props.maxLength}
                        returnKeyType={this.props.returnKeyType}
                        blurOnSubmit={this.props.blurOnSubmit || false}
                        onSubmitEditing={this.props.setFocus}
                        ref={this.props.getFocus}
                        editable={this.props.inputdit}
                        placeholder={this.props.placeholder}
                        style={styles.input_style}
                        placeholderTextColor={this.props.placeholderTextColor}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => { this.props.imagePathRight && this.props.show() }}
                    style={styles.image_right_view}>
                    <Image
                        source={this.props.imagePathRight}
                        resizeMode='contain'
                        style={{ height: this.props.rightHeight, width: this.props.rightWidth, }}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

export default Input;


const styles = StyleSheet.create({
    input_style: {
        fontSize: fonts.fontSize14,
        color: Colors.warmGrey,
        alignItems: 'center',
        height: 50,

    },
    input_container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
        borderRadius: 15,
        backgroundColor: Colors.whiteThree,
        height: 50,
    },
    input_view: {
        flex: 0.9,
        paddingLeft: 5,
        justifyContent: 'center'
    },
    image_view: {
        flex: 0.1,
        justifyContent: 'center',
        marginLeft: 18,

    },
    image_right_view: {
        flex: 0.1,
        justifyContent: 'center',

    }

});
