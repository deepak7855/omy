import React from 'react';
import { View, Text, Image, TouchableOpacity, DeviceEventEmitter } from 'react-native';
import Colors from '../Assets/Colors';
import { images } from '../Assets/imagesUrl';
import fonts from '../Assets/fonts';


export class LeftHeader extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let { hideLeftBackIcon, leftTitle, leftIcon, leftIconStyle, leftTitleStyle, leftClick, isAlsoBack } = this.props;

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                {hideLeftBackIcon ? null :
                    <TouchableOpacity
                        hitSlop={{ top: 80, left: 80, right: 80 }}
                        onPress={() => leftClick()} style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                        {isAlsoBack ?
                            <Image
                                resizeMode={'contain'}
                                source={images.back_btn}
                                style={[{ height: 15, width: 15,marginRight:10, resizeMode: 'contain' }]} />
                            : null}
                        <Image
                            resizeMode={'contain'}
                            source={leftIcon ? leftIcon : images.back_btn}
                            style={[{ height: 12.3, width: 7, resizeMode: 'contain' }, leftIconStyle]} />
                    </TouchableOpacity>
                }
                <Text onPress={() => leftClick()}  style={[{ color: Colors.white, marginLeft: 10, fontSize: fonts.fontSize16, fontFamily: fonts.RoBoToBold_1, textTransform: 'capitalize' }, leftTitleStyle]}>{leftTitle}</Text>
            </View>
        );
    }
}

export class RightHeader extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            Count: 0
        };
    }
    componentDidMount() {
        this.Notification_Count = DeviceEventEmitter.addListener('Notification_Count', (data) => {
            this.setState({ Count: data })
        })
    }

    componentWillUnmount() {
        this.Notification_Count.remove()
    }
    render() {
        let { rightIcon, rightIconStyle, profileIcon, profileIconClick, bellIcon, bellIconClick, settingsIcon, settingIconClick,
            moreOptionsIcon, moreOptionsIconClick } = this.props;

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}>
                {profileIcon ?
                    <TouchableOpacity
                        onPress={() => profileIconClick()}
                        style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                        <Image
                            resizeMode={'contain'}
                            source={rightIcon ? rightIcon : images.user_edit_ic}
                            style={[{ height: 18.5, width: 18.5, resizeMode: 'contain', marginRight: 5 }, rightIconStyle]} />
                    </TouchableOpacity>
                    : null
                }

                {bellIcon ?
                    <View>
                        <TouchableOpacity
                            onPress={() => bellIconClick()}
                            style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                            <Image
                                resizeMode={'contain'}
                                source={rightIcon ? rightIcon : images.bell_ic}
                                style={[{ height: 18.5, width: 18.5, resizeMode: 'contain', marginRight: 5 }, rightIconStyle]} />
                        </TouchableOpacity>
                        {Number(this.state.Count > 0) ?
                            <View style={{ position: 'absolute', right: 1, marginRight: -2, marginTop: -8, backgroundColor: 'red', width: 18, height: 18, borderRadius: 18 / 2, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 10, color: '#fff', fontWeight: 'bold' }}>{this.state.Count <= 9 ? this.state.Count : '9+'}</Text>
                            </View>
                            : null}
                    </View>

                    : null
                }

                {settingsIcon ?
                    <TouchableOpacity
                        onPress={() => settingIconClick()}
                        style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                        <Image
                            resizeMode={'contain'}
                            source={rightIcon ? rightIcon : images.settings_ic}
                            style={[{ height: 18.5, width: 18.5, resizeMode: 'contain', marginRight: 5 }, rightIconStyle]} />
                    </TouchableOpacity>
                    : null
                }

                {moreOptionsIcon ?
                    <TouchableOpacity
                        onPress={() => moreOptionsIconClick()}
                        style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                        <Image
                            resizeMode={'contain'}
                            source={rightIcon ? rightIcon : images.more_options}
                            style={[{ height: 18.5, width: 18.5, resizeMode: 'contain', marginRight: 5 }, rightIconStyle]} />
                    </TouchableOpacity>
                    : null
                }
            </View>
        );
    }
}

export default AppHeader = (props) => {

    return (
        props.setOptions({
            headerStyle: {
                backgroundColor: props.headerBg ? props.headerBg : Colors.cerulean,
                borderBottomLeftRadius: props.borderBottomRadius ? props.borderBottomRadius : 0,
                borderBottomRightRadius: props.borderBottomRadius ? props.borderBottomRadius : 0,
                elevation: 0,
                shadowOpacity: 0
            },

            headerLeft: () =>
                <LeftHeader
                    leftClick={() => {
                        if (props.leftClick) {
                            props.leftClick()
                        } else {
                            props.goBack()
                        }
                    }}
                    leftIcon={props.leftIcon}
                    isAlsoBack={props.isAlsoBack}
                    leftTitleStyle={props.leftTitleStyle}
                    leftIconStyle={props.leftIconStyle}
                    leftTitle={props.leftTitle}
                    hideLeftBackIcon={props.hideLeftBackIcon}
                />,
            headerRight: () =>
                <RightHeader
                    {...props}
                />,
            headerTitle: () => <></>,
        })

    )
} 