import React from 'react';
import { Text, View,Platform, TouchableOpacity, Image } from 'react-native';
import Colors from '../Assets/Colors';
import fonts from '../Assets/fonts';
import { images } from '../Assets/imagesUrl';
import LanguagesIndex from '../Languages';
import { SignInWithAppleButton } from 'react-native-apple-authentication';
import DeviceInfo from "react-native-device-info";
let systemVersion = DeviceInfo.getSystemVersion();



export const GButton = (params) => (
    <TouchableOpacity activeOpacity={0.5} onPress={params.onPress}
        style={{
            alignItems: 'center', borderRadius: params.borderRadius, backgroundColor: Colors.darkSkyBlue,
            width: params.width, alignSelf: 'center', height: params.height, justifyContent: 'center', marginVertical: params.marginVertical
        }}>
        <Text style={{
            fontSize: fonts.fontSize14, color: Colors.white, lineHeight: 17,
            fontFamily: fonts.RoBoToMedium_1
        }}>
            {params.Text}</Text>
    </TouchableOpacity>
);


export const SocialButton = (params) => (
    <>
        <TouchableOpacity activeOpacity={0.5} onPress={params.facebookSignIn}
            style={{
                alignItems: 'center', borderRadius: 10, backgroundColor: Colors.frenchBlue, width: '100%', alignSelf: 'center', height: 50, justifyContent: 'center',
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Image
                    resizeMode='contain'
                    source={images.facebook_ic}
                    style={{ width: 8, height: 15, marginHorizontal: 5 }}
                />

                <Text style={{
                    fontSize: fonts.fontSize14, color: Colors.white, lineHeight: 17, marginHorizontal: 5, fontFamily: fonts.RoBoToMedium_1
                }}>
                    {LanguagesIndex.translate('FACEBOOK')}</Text>
            </View>
        </TouchableOpacity>
        {Platform.OS == 'ios' && parseFloat(systemVersion) >= 13.0 ?
            SignInWithAppleButton({ buttonText: '', callBack: params.appleSignIn, buttonStyle: {height: 50,marginTop:20,width:'100%'} })
            : null}
    </>
); 