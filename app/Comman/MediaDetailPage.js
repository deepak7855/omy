import React, { Component } from 'react';
import { View, TouchableOpacity, SafeAreaView, ActivityIndicator, Image } from 'react-native';
import Colors from '../Assets/Colors';
import { images } from '../Assets/imagesUrl';
import AppHeader from '../Comman/AppHeader';
import ImageLoadView from '../Lib/ImageLoadView';
import Video from 'react-native-video';
import LanguagesIndex from '../Languages';

export default class MediaDetailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            paused: true,
        };
        AppHeader({ ...this.props.navigation, leftTitle: LanguagesIndex.translate('Media') })
    }

    goBack = () => {
        this.props.navigation.goBack(null);
    }

    render() {
        let { item } = this.props.route.params;
        //console.log('---',this.props.route.params)
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: item.media_type == 'video' ? Colors.black : Colors.white }}>
                {item.media_type == 'video' ?
                    <>
                        <Video
                            source={{ uri: item.media_url }}
                            paused={this.state.paused}
                            resizeMode='contain'
                            // fullscreen
                            onLoad={() => this.setState({ loading: false })}
                            onLoadStart={() => { console.log("onLoadStart"), this.setState({ loading: true }) }}
                            // onLoadEnd={() => {console.log("onLoadEnd"), this.setState({ loading: false }) }}
                            poster={item.media_thumb}
                            controls
                            style={{ height: '100%', width: '100%' }}

                        />
                        {this.state.loading == true && this.state.paused == false ?
                            <View style={{ height: '100%', width: '100%', justifyContent: 'center', position: 'absolute', zIndex: 1, }}>
                                <ActivityIndicator size="large" color={Colors.cerulean} />
                            </View> : null
                        }

                        {this.state.paused ?
                            <TouchableOpacity onPress={() => this.setState({ paused: false })} style={{ position: 'absolute', zIndex: 1, top: 0, right: 0, bottom: 0, left: 0, justifyContent: "center", alignItems: "center", }}>
                                <Image
                                    style={{ height: 25, width: 25 }}
                                    // resizeMode="cover"
                                    resizeMode={'contain'}
                                    source={images.play} />
                            </TouchableOpacity>
                            : null}


                    </>
                    :
                    <>
                        <ImageLoadView
                            style={{ height: '100%', width: '100%' }}
                            loaderLeft={'50%'}
                            loaderBottom={'50%'}
                            source={{ uri: item.media_url }}
                            resizeMode={'contain'}
                        />
                    </>
                }
            </SafeAreaView>
        );
    }
}
