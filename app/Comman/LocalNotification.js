import React, { Component } from 'react';
import { View, TouchableOpacity, SafeAreaView, Platform, Easing, Text, DeviceEventEmitter, Animated } from 'react-native';
import Colors from '../Assets/Colors';
import fonts from '../Assets/fonts';
import Helper from '../Lib/Helper';
import { handleNavigation } from '../navigation/Navigation';
import DeviceInfo from "react-native-device-info";

export default class LocalNotification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataLocalNotification: '',
            animatedValue: new Animated.Value(1),
        };
    }

    animateView() {
        Animated.timing(this.state.animatedValue,
            {
                toValue: 1,
                // toValue: 1,
                duration: 200,
                useNativeDriver: true,
                easing: Easing.linear,
            }
        ).start();
        setTimeout(() => {
            this.hideToast();
        }, 5000);
    }

    hideToast() {
        this.localNotificationShowTimeout = setTimeout(() => {
            if (this.state.dataLocalNotification) {
                this.setState({ dataLocalNotification: null })
            }
            if (this.localNotificationShowTimeout) {
                clearTimeout(this.localNotificationShowTimeout)
            }
        }, 6000);
        Animated.timing(this.state.animatedValue,
            {
                toValue: 1,
               // toValue: 0,
                duration: 200,
                useNativeDriver: true,
                easing: Easing.linear,
            }
        ).start();
    }

    componentDidMount() {
        this.showLocalNotification = DeviceEventEmitter.addListener("showLocalNotification", (response) => {
            if (response) {
                if (this.localNotificationShowTimeout) {
                    clearTimeout(this.localNotificationShowTimeout)
                }
                this.setState({ dataLocalNotification: response })
                this.animateView()
            }
        })
    }

    componentWillUnmount() {
        this.showLocalNotification.remove()
    }

    handleNotificationNavigation(type, notiData) {
        let encryptedData = JSON.parse(notiData);
        if (type == "chat") {
            DeviceEventEmitter.emit('backChat', 1);
        }
         
        if (type != 'custom_notification') {
            setTimeout(() => {
                if (Helper.navigationRef.canGoBack()) {
                    Helper.navigationRef.goBack();
                }
                if (type == "chat") {
                    setTimeout(() => {
                        handleNavigation({
                            type: 'push', page: 'ChatScreen', navigation: Helper.navigationRef,
                            passProps: {
                                other_user_id: encryptedData.sender_id,
                                name: encryptedData.name,
                                picture: encryptedData.profile_picture,
                                fromWhere: 'booking',
                                extraData: {
                                    bookingStatus: "ACCEPT",
                                }
                            }
                        })
                    }, 1000);
                } else {
                    if (Helper.user_data.user_type === 'HAVEOMY') {
                        handleNavigation({
                            page: 'BookingDetails', type: 'push', navigation: Helper.navigationRef, passProps: {
                                booking_id: encryptedData.booking_id
                            }
                        })
                    } else {
                        handleNavigation({
                            page: 'ActivityDetailScreen', type: 'push', navigation: Helper.navigationRef, passProps: {
                                booking_id: encryptedData.booking_id,
                                booking_service_id: encryptedData.booking_service_id,
                                BookingStatus: encryptedData.status
                            }
                        })
                    }
                }
            }, 2000);
        }
    }

    render() {
        let notiData = '';
        let notiDataType = '';
        if (this.state.dataLocalNotification) {
            notiData = this.state.dataLocalNotification.data.dictionary;
            notiDataType = this.state.dataLocalNotification.data.type;
        }

        let animation = this.state.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-100, 0]
        })
        return (
            <SafeAreaView style={{ width: '100%', position: 'absolute', zIndex: 1, top: Platform.OS == 'ios' && DeviceInfo.hasNotch() ? 40 : 0 }}>
                <Animated.View style={{ backgroundColor: Colors.white, transform: [{ translateY: animation }] }}>
                    {this.state.dataLocalNotification ?
                        <TouchableOpacity onPress={() => this.setState({ dataLocalNotification: null }, () => {
                            this.handleNotificationNavigation(notiDataType, notiData);
                        })} style={{ paddingVertical: 15, width: '100%', paddingHorizontal: 15, }}>
                            <Text numberOfLines={1} style={{ fontFamily: fonts.RoBoToRegular_1, fontSize: 16, color: Colors.darkSkyBlue, width: '90%' }}>{this.state.dataLocalNotification.notification.title}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', width: '90%' }}>
                                <Text numberOfLines={1} style={{ fontFamily: fonts.RobotoRegular, fontSize: 12, color: Colors.darkSkyBlue }}>{this.state.dataLocalNotification.notification.body}</Text>
                            </View>
                        </TouchableOpacity>
                        : null}
                </Animated.View>
            </SafeAreaView>
        );
    }
}
