import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Text } from 'react-native';
import Colors from '../Assets/Colors';
import fonts from '../Assets/fonts';


class GInput extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View style={[styles.input_container,this.props.style]}>

                <View style={styles.input_view}>

                    {this.props.phoneCode ?
                        <View style={{ paddingLeft: 8 }}>
                            <Text style={{
                                fontSize: fonts.fontSize14,
                                color: Colors.warmGrey,
                            }}>+41</Text>
                        </View>
                        :
                        null
                    }
                    {this.props.fixedValue ?
                        <View style={{ paddingLeft: 8 }}>
                            <Text style={{
                                fontSize: fonts.fontSize14,
                                color: Colors.warmGrey,
                            }}>{this.props.fixedValue}</Text>
                        </View>
                        :
                        null
                    }

                    <TextInput
                        secureTextEntry={this.props.secureTextEntry}
                        placeholder={this.props.placeholder}
                        placeholderTextColor={this.props.placeholderTextColor}
                        underlineColorAndroid="transparent"
                        autoCorrect={false}
                        value={this.props.value}
                        onChangeText={this.props.onChangeText}
                        keyboardType={this.props.keyboardType}
                        maxLength={this.props.maxLength}
                        returnKeyType={this.props.returnKeyType}
                        blurOnSubmit={this.props.blurOnSubmit}
                        onSubmitEditing={this.props.setFocus}
                        ref={this.props.getFocus}
                        editable={this.props.inputedit}
                        placeholder={this.props.placeholder}
                        style={[styles.input_style,this.props.style]}
                        multiline={this.props.multiline}
                        placeholderTextColor={this.props.placeholderTextColor}
                    />
                </View>

            </View>
        );
    }
}

export default GInput;


const styles = StyleSheet.create({
    input_style: {
        fontSize: fonts.fontSize14,
        color: Colors.warmGrey,
        alignItems: 'center',
        width: '100%',
        height: 50,
        marginLeft: 5,
        fontFamily: fonts.RoBoToMedium_1,

    },
    input_container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
        borderRadius: 15,
        backgroundColor: Colors.whiteThree,
        height: 50,
    },
    input_view: {
        flex: 0.9,
        paddingLeft: 5,
        //  justifyContent: 'center',
        flexDirection: 'row', alignItems: 'center'
    },
    image_view: {
        flex: 0.1, justifyContent: 'center', marginLeft: 18,

    },
    image_right_view: {
        flex: 0.1, justifyContent: 'center',

    }

});
