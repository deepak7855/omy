import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import fonts from '../Assets/fonts';
import Colors from '../Assets/Colors';
import LanguagesIndex from '../Languages';


export default class NoData extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{ alignSelf: 'center', marginTop: 25 }}>
                <Text style={{ fontFamily: fonts.RoBoToRegular_1, color: Colors.cerulean, fontSize: 16 ,textAlign:'center'}}>
                    {this.props.message || LanguagesIndex.translate('Nodatafound')}
                </Text>
            </View>
        );
    }
}