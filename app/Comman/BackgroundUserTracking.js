//import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import BackgroundGeolocation from '@darron1217/react-native-background-geolocation';

import { Alert, DeviceEventEmitter } from 'react-native';
import { ApiCall, AsyncStorageHelper, Constants } from '../Api';
import Helper from '../Lib/Helper';

export function configureBackgroundTracking() {
    BackgroundGeolocation.configure({
        desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
        stationaryRadius: 50,
        distanceFilter: 50,
        notificationsEnabled:false,
        notificationTitle: 'Background tracking',
        notificationText: 'enabled',
        debug: false,
        startOnBoot: true,
        stopOnTerminate: false,
        startForeground: true,
        locationProvider: BackgroundGeolocation.DISTANCE_FILTER_PROVIDER,
        interval: 10000,
        fastestInterval: 5000,
        activitiesInterval: 10000,
        stopOnStillActivity: false,
        // url: `${Config.url}park-later-checkin`,
        httpHeaders: {
            'X-FOO': 'bar',
            'Authorization': "Bearer " + Helper.token,
        },
        // customize post properties
        postTemplate: {
            lat: '@latitude',
            lng: '@longitude',
            foo: 'bar' // you can also add your own properties
        }
    });

    BackgroundGeolocation.on('location', (location) => {
         
        console.log("user tracking location  sir==>>",location)
//return;
        AsyncStorageHelper.getData('runningBooking').then((runningBooking) => {

            // alert("user tracking location  frst ==>>",location)
             console.log("runningBooking==>>",runningBooking)

            if(runningBooking.booking_id){
                //alert(location.latitude+'  '+ location.longitude)
             console.log("lat long ===>",location.latitude+'  '+ location.longitude)
             console.log("runningBooking==>>",runningBooking)
                let data = new FormData();
                data.append('booking_id', runningBooking.booking_id)
                data.append('booking_service_id', runningBooking.booking_service_id)
                data.append('lat', location.latitude)
                data.append('lng', location.longitude)
                data.append('start_time', runningBooking?.start_time)
                data.append('tracking_type', "0")
                // alert("user tracking---->>>==========++++",data)
                console.log("user tracking---->>>====++++",data)

                ApiCall.postMethodWithHeader(Constants.user_tracking, data, Constants.APIImageUploadAndroid).then((response) => {
                     
                }
                ).catch(err => {
                    Helper.globalLoader.hideLoader();
                })
            }

        })
        // handle your locations here
        // to perform long running operation on iOS
        // you need to create background task
        BackgroundGeolocation.startTask(taskKey => {
            // execute long running task
            // eg. ajax post location
            // IMPORTANT: task has to be ended by endTask
            BackgroundGeolocation.endTask(taskKey);
        });
    });

    BackgroundGeolocation.on('stationary', (stationaryLocation) => {
        // handle stationary locations here
        // Actions.sendLocation(stationaryLocation);
    });

    BackgroundGeolocation.on('error', (error) => {
    });

    BackgroundGeolocation.on('start', () => {
    });

    BackgroundGeolocation.on('stop', () => {
    });

    BackgroundGeolocation.on('authorization', (status) => {
        if (status !== BackgroundGeolocation.AUTHORIZED) {
            // we need to set delay or otherwise alert may not be shown
            setTimeout(() =>
                Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
                    { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
                    { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
                ]), 1000);
        }
    });

    BackgroundGeolocation.on('background', () => {
    });

    BackgroundGeolocation.on('foreground', () => {
    });

    BackgroundGeolocation.on('abort_requested', () => {

        // Here we can decide whether we want stop the updates or not.
        // If you've configured the server to return 285, then it means the server does not require further update.
        // So the normal thing to do here would be to `BackgroundGeolocation.stop()`.
        // But you might be counting on it to receive location updates in the UI, so you could just reconfigure and set `url` to null.
    });

    BackgroundGeolocation.checkStatus(status => {
        // you don't need to check status before start (this is just the example)
        if (!status.isRunning) {
            BackgroundGeolocation.start(); //triggers start on start event
        }
    });

    BackgroundGeolocation.on('http_authorization', () => {
    });
}

export function startUserTracking() {
    BackgroundGeolocation.start();
}

export function stopUserTracking() {
    BackgroundGeolocation.checkStatus(status => {
        if (status.isRunning) {
            BackgroundGeolocation.stop();
        }
    });
}