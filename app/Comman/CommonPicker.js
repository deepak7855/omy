import React from 'react';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../Assets/Colors';
import fonts from '../Assets/fonts';
import { images } from '../Assets/imagesUrl';
import RNPickerSelect from 'react-native-picker-select';


export default class TextInputCommon extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            secureTextEntry: this.props.placeHolder == 'Password' ? true : false
        }
    }

    render() {
        return (
            <View style={{ backgroundColor: Colors.whiteThree, borderRadius: 15, marginHorizontal: this.props.marginHorizontal, marginTop: this.props.marginTop }}>
                <View style={{ flex: 1, paddingLeft: 10, }}>
                    <RNPickerSelect
                        placeholder={this.props.placeHolder}
                        items={this.props.items}
                        value={this.props.selectValue}
                        onValueChange={(value) => this.props.onValueChange(value)}
                        useNativeAndroidPickerStyle={false}
                        style={this.props.style}
                    />

                    <Image source={images.drop_down} style={{ height: 6, resizeMode: "contain", position: "absolute", right: 15, top: 24, width: 14 }} />
                </View>
            </View>

        );
    }

    hideShowpass() {
        this.setState({ secureTextEntry: !this.state.secureTextEntry })
    }

};


const styles = StyleSheet.create({


})
