

// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { Platform } from "react-native";
import Helper from './Helper';
import { check, request, requestMultiple, PERMISSIONS, openSettings } from 'react-native-permissions';
import RNThumbnail from 'react-native-thumbnail';
import { createThumbnail } from "react-native-create-thumbnail";
import LanguagesIndex from '../Languages';

export default class CameraController {
    static async open(cb) {
        Helper.cameraAlert(LanguagesIndex.translate('Selectimagefrom'), "Camera", "Gallery", "Cancel", (status) => {
            if (status == 'Camera') {
                CameraController.checkPremission(PERMISSIONS.ANDROID.CAMERA, PERMISSIONS.IOS.CAMERA, cb, "Camera");
            } else if (status == 'Gallery') {
                CameraController.checkPremission(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE, PERMISSIONS.IOS.PHOTO_LIBRARY, cb, "Gallery");
            }
        });
    }

    static async chooseMediaType(cb) {
        Helper.selectMediaAlert(LanguagesIndex.translate('Selectmediatype'), "Image", "Video", "Cancel", (status) => {
            if (status == 'Image') {
                this.open(cb)
            } else if (status == 'Video') {
                this.openVideo(cb)
            }
        });
    }

    static checkPremission = async (androidType, iosType, cb, launchType) => {

        await check(Platform.select({
            android: androidType,
            ios: iosType
        })).then(result => {
            if (result === "granted") {
                this.selecteImage(cb, launchType);
                return;
            }
            if (result === "blocked" || result === "unavailable") {
                Helper.permissionConfirm(LanguagesIndex.translate('AccesstothecamerahasbeenprohibitedpleaseenableitintheSettingsapptocontinue'), ((status) => {
                    if (status) {
                        openSettings().catch(() => {
                            console.warn('cannot open settings')
                        });
                    }
                }));
                return;
            }
            request(
                Platform.select({
                    android: androidType,
                    ios: iosType
                })
            ).then((status) => {
                if (status === "granted") {
                    this.selecteImage(cb, launchType);
                } else {
                }
            });
        });
    }

    static selecteImage(cb, launchType) {
        if (launchType === "Camera") {
            // ImagePicker.launchCamera({ mediaType: "photo" },(response)=>{
            //     response.media_type = 'image';
            //     cb(response);
            // });
            ImagePicker.openCamera({
                mediaType: "photo",
              }).then((response) => {
                response.media_type = 'image';
                cb(response);
              });
        } else {
            // ImagePicker.launchImageLibrary({ mediaType: "photo" },(response)=>{
            //     response.media_type = 'image';
            //     cb(response);
            // });
            ImagePicker.openPicker({
                mediaType: "photo",
              }).then((response) => {
                response.media_type = 'image';
                cb(response);
              });
        }
    }


    static async openVideo(cb) {
        Helper.cameraAlert(LanguagesIndex.translate('Selectvideofrom'), "Camera", "Gallery", "Cancel", (status) => {
            if (status == 'Camera') {
                CameraController.checkVideoPremission([PERMISSIONS.ANDROID.CAMERA, PERMISSIONS.ANDROID.RECORD_AUDIO], [PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.MICROPHONE], cb, "Camera");
            } else if (status == 'Gallery') {
                CameraController.checkVideoPremission([PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE], [PERMISSIONS.IOS.PHOTO_LIBRARY], cb, "Gallery");
            }
        });
    }

    static checkVideoPremission = async (androidType, iosType, cb, launchType) => {

        await check(Platform.select({
            android: androidType[0],
            ios: iosType[0]
        })).then(result => {
            if (result === "granted") {
                this.selectVideo(cb, launchType);
                return;
            }
            if (result === "blocked" || result === "unavailable") {
                Helper.permissionConfirm(LanguagesIndex.translate('Accesstothe')+`${launchType}`+LanguagesIndex.translate('hasbeenprohibitedpleaseenableitintheSettingsapptocontinue'), ((status) => {
                    if (status) {
                        openSettings().catch(() => {
                            console.warn('cannot open settings')
                        });
                    }
                }));
                return;
            }
            requestMultiple(
                Platform.select({
                    android: androidType,
                    ios: iosType
                })
            ).then((status) => {
                if (status === "granted") {
                    this.selectVideo(cb, launchType);
                } else {
                }
            });
        });
    }

    static selectVideo(cb, launchType) {
        if (launchType === "Camera") {
            // ImagePicker.launchCamera({ mediaType: "video" },(response)=>{
            //     console.log("video from camera response ==> ", response);
            //     response.media_type = 'video';
            //     cb(response);
            // });
            ImagePicker.openCamera({
                mediaType: "video",
              }).then((response) => {
                response.media_type = 'video';
                cb(response);
              });
        } else {
            // ImagePicker.launchImageLibrary({ mediaType: "video" },(response)=>{
            //     console.log("video from gallery response 123 ==> ", response);
            //     response.media_type = 'video';
            //     cb(response);
            // });
            ImagePicker.openPicker({
                mediaType: "video",
              }).then((response) => {
                response.media_type = 'video';
                cb(response);
              });
        }
    }

    static createThumbnail(filepath, cb) {
        console.log("filepath thunmnail -------",filepath);
        createThumbnail({
            url: filepath,
            timeStamp: 10000,
            format: "jpeg",
        })
            .then((response) => {
                console.log("thumbnail response =======>>>>", response);
                cb(response.path)
            })
            .catch((err) => { console.log({ err }); cb(false) });
        // RNThumbnail.get(filepath).then((result) => {
        //     console.log("RNThumbnail res ======> ", result );
        //     cb(result.path)
        // }).catch((error) => {
        //     console.log("RNThumbnail error ======> ", error );
        //     cb(false)

        // });
    }
} 
 