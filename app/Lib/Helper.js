/*
Install below npm before use it 

npm install --save react-native-loading-spinner-overlay@latest

add this line to App.js in constructor
 Helper.registerLoader(this);
 in render function
 <View style={{ flex: 1 }}>
        <Spinner visible={this.state.loader} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
 </View>
*/

import * as React from 'react';
import Toast from 'react-native-root-toast';
import { Alert, Linking, Platform } from 'react-native';
// import moment from 'moment/min/moment-with-locales';
import moment from 'moment';
import Colors from '../Assets/Colors';
import { Constants, ApiCall, AsyncStorageHelper } from '../Api';
import LanguagesIndex from '../Languages';



export default class Helper extends React.Component {

    static device_type = Platform.OS === "ios" ? 'ios' : 'android';
    url = "";
    static mainApp;
    static bookingState = false;
    static toast;
    static currentPage = '';
    static user_id = "";
    static globalLoader;
    static device_id = "123654";
    static navigationRef;
    static activeChatId = '';
    static isNetConnected = true;
    static navRef = null;
    static isUploadNewPost = false
    static arrAdsData = []
    static currentIndexAds = 0
    static token = ""

    static onChatHistory = false;
    static customDisconnet = false;
    static onChatScreen = false;

    static user_data = ''
    static PaymentValue = false;
    static isSocketConnected = false;


    static registerLoader(mainApp) {
        Helper.globalLoader = mainApp;
    }
    static getFormData(obj) {
        let formData = new FormData();
        for (let i in obj) {
            formData.append(i, obj[i]);
        }
        return formData;
    }
    static registerToast(toast) {
        Helper.toast = toast;
    }

    static showToast(msg, position = 'TOP') {
        // Keyboard.dismiss();
        if (msg) {
            Toast.show(msg, {
                // duration: 2000,
                // duration: Toast.durations.SHORT,
                duration: Toast.durations.LONG,
                position: Toast.positions[position],
                backgroundColor: 'black',
                opacity: 1.0,
                textColor: 'white',
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                containerStyle: { marginTop: 25 }
            });
        }
    }

    static alert(alertMessage, cb) {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            alertMessage,
            [
                { text: LanguagesIndex.translate('OK'), onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static titleAlert(title, alertMessage, cb) {
        Alert.alert(
            title,
            alertMessage,
            [
                { text: LanguagesIndex.translate('OK'), onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static titleConfirm(title, alertMessage, cb) {
        setTimeout(() => {
            Alert.alert(
                title,
                alertMessage,
                [
                    { text: LanguagesIndex.translate('OK'), onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                    { text: LanguagesIndex.translate('Cancel'), onPress: () => { if (cb) cb(false); }, style: 'cancel' },
                ],
                { cancelable: false }
            )
        }, 10);
    }

    static capitalizeFirstLetter(name) {
        let splitStr = name.split(' ');
        for (let i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }

    static capitalizeFirstLetterComma(name) {
        let splitStr = name.split(',');
        for (let i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(',');
    }


    static confirm(alertMessage, cb) {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            alertMessage,
            [
                { text: LanguagesIndex.translate('OK'), onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: LanguagesIndex.translate('Cancel'), onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static backConfirm(alertMessage, cb) {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            alertMessage,
            [
                { text: LanguagesIndex.translate('CONFIRM'), onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: LanguagesIndex.translate('BACK'), onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static confirmMove(alertMessage, okText, cancelText, cb) {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            alertMessage,
            [
                { text: okText, onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: cancelText, onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static cameraAlert(alertMessage, Camera, Gallery, Cancel, cb) {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            alertMessage,
            [
                { text: LanguagesIndex.translate('Camera'), onPress: () => { if (cb) cb(Camera); console.log('OK Pressed') } },
                { text: LanguagesIndex.translate('Gallery'), onPress: () => { if (cb) cb(Gallery); console.log('OK Pressed') } },
                { text: LanguagesIndex.translate('Cancel'), onPress: () => { if (cb) cb(Cancel); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static selectMediaAlert(alertMessage, Image, Video, Cancel, cb) {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            alertMessage,
            [
                { text: LanguagesIndex.translate('Image'), onPress: () => { if (cb) cb(Image); console.log('OK Pressed') } },
                { text: LanguagesIndex.translate('Video'), onPress: () => { if (cb) cb(Video); console.log('OK Pressed') } },
                { text: LanguagesIndex.translate('Cancel'), onPress: () => { if (cb) cb(Cancel); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static logoutConfirm(title, alertMessage, cb) {
        Alert.alert(
            alertMessage,
            [
                { text: LanguagesIndex.translate('OK'), onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: LanguagesIndex.translate('Cancel'), onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static permissionConfirm(alertMessage, cb) {
        Alert.alert(
            LanguagesIndex.translate('kAppName'),
            alertMessage,
            [
                { text: LanguagesIndex.translate('NOTNOW'), onPress: () => { if (cb) cb(false); }, style: 'cancel' },
                { text: LanguagesIndex.translate('SETTINGS'), onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static async setData(key, val) {
        try {
            let tempVal = JSON.stringify(val);
            await AsyncStorage.setItem(key, tempVal);
        } catch (error) {
            console.error(error, "AsyncStorage")
            // Error setting data 
        }
    }

    static async getData(key) {
        try {
            let value = await AsyncStorage.getItem(key);
            if (value) {
                let newValue = JSON.parse(value);
                return newValue;
            } else {
                return value;
            }
        } catch (error) {
            console.error(error, "AsyncStorage")
            // Error retrieving data
        }
    }

    static formatDate(date, format) {
        return moment(date).locale(LanguagesIndex.MyLanguage).format(format);
    }

    static formatLanguageDate(date, format) {
        if (LanguagesIndex.MyLanguage == 'en') {
            return moment(date).locale('en').format(format);
        }
        let convertDate = moment(date).locale('en').format(format);
        let month = moment(date).locale('en').format('MMM');
        let languageMonth = ''
        if (LanguagesIndex.MyLanguage == 'de') {
            languageMonth = Helper.monthGerman(month)
        }
        else if (LanguagesIndex.MyLanguage == 'fr') {
            languageMonth = Helper.monthFrench(month)
        }
        else {
            languageMonth = month
        }
        return convertDate.replace(month, languageMonth)
    }
    static monthGerman(month){
        switch (month) {
            case "Jan":
                return 'Jan'
            case "Feb":
                return 'Feb'
            case "Mar":
                return 'März'
            case "Apr":
                return 'Apr'
            case "May":
                return 'Mai'
            case "June":
                return 'Juni'
            case "July":
                return 'Juli'
            case "Aug":
                return 'Aug'
            case "Sept":
                return 'Sept'
            case "Oct":
                return 'Okt'
            case 'Nov':
                return 'Nov'
            case 'Dec':
                return 'Dez'
            default:
                return 'Mai'
        }
    }
    static monthFrench(month){
        switch (month) {
            case "Jan":
                return 'janv'
            case "Feb":
                return 'févr'
            case "Mar":
                return 'mars'
            case "Apr":
                return 'avril'
            case "May":
                return 'mai'
            case "June":
                return 'juin'
            case "July":
                return 'juil'
            case "Aug":
                return 'août'
            case "Sept":
                return 'sept'
            case "Oct":
                return 'oct'
            case "Nov":
                return 'nov'
            case "Dec":
                return 'déc'
            default:
                return 'déc'
        }
    }

    static formatDateUtcToLocal(date, time, returnType) {
        let conTime = moment(time, 'h:mm a').format('HH:mm:ss');
        console.log(date, "date,time", conTime)

        if (returnType == 'date') {
            return moment.utc(`${date} ${conTime}`, 'YYYY-MM-DD HH:mm:ss').local().format('DD/MM/YYYY');
        }
        else if (returnType == 'time') {
            return moment.utc(`${date} ${conTime}`, 'YYYY-MM-DD HH:mm:ss').local().format('h:mm a');
        }
        else {
            return moment.utc(`${date} ${conTime}`, 'YYYY-MM-DD HH:mm:ss').local().format('DD/MM/YYYY | h:mm a');

        }


    }

    static getBookingStatus(status) {

        let returnVal = {
            value: '',
            color: ''
        }
        switch (status) {
            case 'REJECT':
                returnVal = {
                    value: LanguagesIndex.translate('Rejected'),
                    color: Colors.brick
                }
                break;
            case 'COMPLETE':
                returnVal = {
                    value: LanguagesIndex.translate('Completed'),
                    color: Colors.algaeGreen
                }
                break;
            case 'ACCEPT':
                returnVal = {
                    value: LanguagesIndex.translate('Accepted'),
                    color: Colors.algaeGreen
                }
                break;
            case 'PENDING':
                returnVal = {
                    value: LanguagesIndex.translate('Pending'),
                    color: Colors.algaeGreen
                }
                break;
            case 'CANCEL':
                returnVal = {
                    value: LanguagesIndex.translate('Cancelled'),
                    color: Colors.brick
                }
                break;
            default:
                returnVal = {
                    value: status,
                    color: Colors.brick
                }
                break;
        }

        return returnVal;
    }

    static validate(form, validations, val) {
        let isValidForm = true;
        let errors = {};
        let message = '';
        if (!validations) {
            validations = form.validators;
        }

        let customValidator = {
            name: LanguagesIndex.translate('yourname'),
            email: LanguagesIndex.translate('youremailaddress'),
            password: LanguagesIndex.translate('yourpassword'),
            confirm_password: LanguagesIndex.translate('confirmpassword'),
            mobile_number: LanguagesIndex.translate('MobileNumber'),
            university: LanguagesIndex.translate('Institutionname'),
            departement: LanguagesIndex.translate('Department'),
            street_no: LanguagesIndex.translate('Streetno'),
            postcode: LanguagesIndex.translate('PostCode'),
            city: LanguagesIndex.translate('city'),
            location: LanguagesIndex.translate('location'),

            behalf_name: LanguagesIndex.translate('NameofBehalf'),
            behalf_mobile_number: LanguagesIndex.translate('BehalfMobileNumber'),

            username: LanguagesIndex.translate('username'),
            email_optional: LanguagesIndex.translate('validemailaddress'),
            country_code: LanguagesIndex.translate('Countrycode'),
            behalf_email: LanguagesIndex.translate('EmailofBehalf'),
            behalf_location: LanguagesIndex.translate('behalflocation'),
            behalf_street_no: LanguagesIndex.translate('behalfstreetno'),
            behalf_postcode: LanguagesIndex.translate('behalfpostcode'),
            behalf_city: LanguagesIndex.translate('behalfcity'),
            behalf_relation: LanguagesIndex.translate('ChooseRelationshipwithBehalf'),
            price: LanguagesIndex.translate('price'),

            enter_email: LanguagesIndex.translate('emailaddress'),

            oldpass: LanguagesIndex.translate('oldpassword'),
            newpassword: LanguagesIndex.translate('newpassword'),
            verify_password: LanguagesIndex.translate('verifypassword'),
            current_password: LanguagesIndex.translate('currentpassword'),
            new_password: LanguagesIndex.translate('newpassword'),

            email_university: LanguagesIndex.translate('EmailUniversity'),
            business_name: LanguagesIndex.translate('businessname'),

            note: LanguagesIndex.translate('note'),

        };


        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;; //email
        var numberRegex = /^\d+$/; // number
        var regularCharacter = new RegExp("^(?=.{8})");//password
        var regular = new RegExp("^(?=.*[A-Z])");//password
        var regularSpecial = new RegExp("^(?=.*[!@#\$%\^&\*])");//password
        var regularNumber = new RegExp("^(?=.*[0-9])");//password
        for (let val in validations) {
            if (!isValidForm) break;
            if (form[val]) {
                for (let i in validations[val]) {
                    var valData = validations[val][i];

                    if (i == "required" && !form[val]) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = LanguagesIndex.translate('Enter') + value;
                    }

                    else if ((i == "minLength" || i == "minLengthDigit") && form[val].length < valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = '';

                        if (i == "minLengthDigit") {
                            if (Number(valData) > 1) {
                                cStr = ' digits';
                            } else {
                                cStr = ' digits';
                            }
                        } else {
                            if (Number(valData) > 1) {
                                cStr = ' characters';
                            } else {
                                cStr = ' character';
                            }
                        }

                        if (val == 'IBAN_number') {
                            message = `${value} ${LanguagesIndex.translate('shouldbe')} ${valData} ${cStr}`;
                        } else {
                            message = `${value} ${LanguagesIndex.translate('mustBeAtLeast')} ${valData} ${cStr}`;
                        }
                    }

                    else if ((i == "maxLength" || i == "maxLengthDigit") && form[val].length > valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "maxLengthDigit" ? " digit" : " characters";
                        message = `${value} ${LanguagesIndex.translate('shouldBeSmallerThan')} ${Number(valData) + 1} ${cStr}`;
                    }

                    else if (i == "matchWith" && form[val] != form[valData]) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        let value2 = (valData.charAt(0).toUpperCase() + valData.slice(1)).split("_").join(" ");
                        message = `${value} ${LanguagesIndex.translate('and')} ${value2} ${LanguagesIndex.translate('doesNotMatch')}`;
                    }
                    else if (i == "email" && reg.test(form[val]) == false) {
                        isValidForm = false;
                        message = LanguagesIndex.translate('PleaseEnterValidEmailAddress');
                    }
                    else if (i == "password" && regularCharacter.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = `${value} ${LanguagesIndex.translate('AtLeast8character')}`;
                    }
                    else if (i == "password" && regular.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = `${value} ${LanguagesIndex.translate('AtLeastOneUppercase')}`;
                    }
                    else if (i == "password" && regularSpecial.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = `${value} ${LanguagesIndex.translate('AtOneSpecialCharacter')}`;
                    }
                    else if (i == "password" && regularNumber.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = `${value} ${LanguagesIndex.translate('AtLeastOneNumber')}`;
                    }

                    else if (i == "numeric" && numberRegex.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = `${value} ${LanguagesIndex.translate('shouldBeNumberOnly')}`;
                    }

                    if (message) {
                        this.showToast(message);
                        break;
                    }

                }
            }
            else {
                let temMsg;
                if (customValidator[val]) {
                    temMsg = customValidator[val];
                } else {
                    temMsg = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                }
                this.showToast(temMsg == LanguagesIndex.translate('ChooseRelationshipwithBehalf') ? temMsg : LanguagesIndex.translate('Enter') + " " + temMsg);
                // this.showToast(temMsg + " is required");
                isValidForm = false;
                break;
            }
        }
        return isValidForm;
    }

    static ratingPopup() {
        Helper.confirm(Platform.OS == 'ios' ? "Please Rate us on app store" : "Please Rate us on play store", (status) => {
            if (status) {
                Helper.setData('isRate', 1);
                if (Platform.OS == 'ios') {
                    Linking.openURL(Constants.APP_STORE_LINK).catch(err => console.error('An error occurred', err));
                }
                else {
                    Linking.openURL(Constants.PLAY_STORE_LINK).catch(err => console.error('An error occurred', err));
                }
            }
        })
    }


    static openLinkingUrl = async (url) => {

        let getTUrl = url
        if (!url.includes('http://') && !url.includes('https://')) {
            getTUrl = 'http://' + url
        }
        const supported = await Linking.canOpenURL(getTUrl);
        if (supported) {
            await Linking.openURL(getTUrl).catch(err => console.error('An error occurred', err));
        }
        else {
            getUrl = "http://www.google.com/search?q=" + url
            const newTLink = await Linking.canOpenURL(getUrl);
            if (newTLink) {
                await Linking.openURL(getUrl).catch(err => console.error('An error occurred', err));
            }
        }
    }

    static GetAddressFromLatLong(address, cb) {
        // add this parmas for country &components=country:CH
        console.log(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&region=ch&key=${Constants.geoCodeKey}`)
        fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&region=ch&key=${Constants.geoCodeKey}`)
            .then((response) => response.json())
            .then((responseJson) => {
                return cb(responseJson.results[0].geometry.location)
            }).catch((err) => {
                cb(false)
                console.log(err, "err")
                Helper.alert(LanguagesIndex.translate('Unabletogetyourlocation'))
            })
    }

    static updateUserData(cb) {
        ApiCall.postMethodWithHeader(Constants.get_profile, JSON.stringify({}), Constants.APIPost).then((response) => {
            if (response.data) {
                AsyncStorageHelper.setData(Constants.USER_DETAILS, response.data)
                Helper.user_data = response.data
                if (cb) {
                    cb(true)
                }
            } else {
                if (cb) {
                    cb(false)
                }
            }

        })
    }
}