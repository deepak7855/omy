import * as React from 'react';
import Helper from '../Lib/Helper';
import Constants from '../Api/Constants';
import { DeviceEventEmitter } from 'react-native';
import SocketIOClient from 'socket.io-client';
import NetInfo from "@react-native-community/netinfo";
import LanguagesIndex from '../Languages';
import { AsyncStorageHelper } from '../Api';

export const events = {
    send_message: 'send_message',
    get_message_history: 'get_message_history',
    get_user_thread: 'get_user_thread',
    read_message_update: 'read_message_update',
    user_block_unblock: 'user_block_unblock',
    user_block_unblock_response: 'user_block_unblock_response',
    multi_user: 'multi_user',
    receive_message: 'receive_message',
    check_user_status: 'check_user_status',
    get_message_history_response: 'get_message_history_response',
    get_user_thread_response: 'get_user_thread_response',
    read_message_update_response: 'read_message_update_response',
    check_user_status_response: 'check_user_status_response',
    get_unread_count: 'get_unread_count',
    get_unread_count_response: 'get_unread_count_response'
}

export default class SocketController extends React.Component {
    constructor(props) {
        super(props)
    }

    static checkConnection(cb) {
        AsyncStorageHelper.getData(Constants.USER_DETAILS).then((userdata) => {
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    if (Helper.isSocketConnected) {
                        cb(true);
                        return;
                    } else {
                        SocketController.socketInit();
                        cb(false);
                    }
                } else {
                    Helper.alert(LanguagesIndex.translate('YoureofflinePleasecheckinternetconnection'));
                }

            }).catch((error) => {

            });
        });
    }

    static socketInit() {
        this.socket = SocketIOClient(Constants.chatUrl, {
            forceNew: true,
            reconnection: true,
            reconnectionAttempts: Infinity,
            reconnectionDelay: 1000,
            reconnectionDelayMax: 3000,
            query: { 'user_id': Helper.user_data.id, 'device_id': Helper.device_id },
        });

        this.socket.on('connect', (data) => {
            console.log('socket connected')
            Helper.isSocketConnected = true;
        });

        this.socket.on('error', (error) => {
            console.log('socket error', error)
        });

        this.socket.on('disconnect', (data) => {
            console.log('socket disconnected')
            Helper.isSocketConnected = false;
            // this.reConnect()
        });


        this.socket.on(events.multi_user, (data) => {
            DeviceEventEmitter.emit(events.multi_user, data);
        });

        this.socket.on(events.receive_message, (data) => {
            DeviceEventEmitter.emit(events.receive_message, data);
            SocketController.getUnreadCount();
        });

        this.socket.on(events.get_user_thread_response, (response) => {
            DeviceEventEmitter.emit(events.get_user_thread_response, response)
        });

        this.socket.on(events.user_block_unblock_response, (data) => {
            DeviceEventEmitter.emit(events.user_block_unblock_response, data);
        });

        this.socket.on(events.check_user_status_response, (data) => {
            DeviceEventEmitter.emit(events.check_user_status_response, data);
        });

        this.socket.on(events.get_message_history_response, (response) => {
            DeviceEventEmitter.emit(events.get_message_history_response, response)
        });

        this.socket.on(events.get_unread_count_response, (response) => {
            DeviceEventEmitter.emit(events.get_unread_count_response, response)
        });
    }

    static reConnect() {
        if (Helper.user_data) {
            this.socket.connect();
        }
    }

    static disconnetSocket() {
        this.socket.disconnect();
    }

    static fireEvent(eventname, formdata) {
        this.socket.emit(eventname, formdata);
    }

    static emitSocketEvent = (eventname, formdata) => {
        if (!Helper.isSocketConnected) {
            this.reConnect();
            setTimeout(() => {
                this.fireEvent(eventname, formdata);
            }, 1000);
        }
        else {
            this.fireEvent(eventname, formdata);
        }
    }

    static getUnreadCount() {
        let formdata = {
            user_id: Helper.user_data.id,
        }
        SocketController.emitSocketEvent(events.get_unread_count, formdata);
    }
}