import React from 'react';
import NavigationProvider from './app/navigation/NavigationContext';
import { Text, TextInput ,LogBox} from 'react-native';
import firebase from '@react-native-firebase/app'
import Helper from './app/Lib/Helper';
import LanguagesIndex from './app/Languages';
import fonts from './app/Assets/fonts';
import { AsyncStorageHelper } from './app/Api';
import * as RNLocalize from "react-native-localize";
import { sendToken, updateDeviceToken } from './app/Api/NotifyCountApi';
import messaging from '@react-native-firebase/messaging';

if (Text.defaultProps == null) Text.defaultProps = {};
Text.defaultProps.allowFontScaling = false;
if (TextInput.defaultProps == null) TextInput.defaultProps = {};
TextInput.defaultProps.allowFontScaling = false;
//<--------Set allowFontScaling false for Screen

if (Text.defaultProps == null) Text.defaultProps = {};
// Text.defaultProps.allowFontScaling = false;
Text.defaultProps.style = { fontFamily: fonts.RoBoToBold_1 };

TextInput.defaultProps.style = { fontFamily: fonts.RoBoToMedium_1 };

// TextInput.defaultProps.style = {fontFamily:fonts.RoBoToRegular_1};


export default class App extends React.Component {

  componentDidMount() {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`','componentWillReceiveProps']); 
    updateDeviceToken();
    AsyncStorageHelper.getData('lan').then((data) => {
      if (data) {
        LanguagesIndex.MyLanguage = data
        RNLocalize.addEventListener("change", this.handleLocalizationChange(LanguagesIndex.MyLanguage));
      }
    })
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener("change", this.handleLocalizationChange);
  }

  handleLocalizationChange = (lang) => {
    LanguagesIndex.setI18nConfig(lang);
    this.forceUpdate();
  };

  

  render() {
    return (
      <NavigationProvider />
    )
  }
};